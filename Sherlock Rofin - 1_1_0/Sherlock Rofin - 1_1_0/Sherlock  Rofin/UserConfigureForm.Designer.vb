﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserConfigureForm
    Inherits VB2005_Sherlock7.FormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.GradientPanel2 = New VB2005_Sherlock7.GradientPanel
        Me.txtUser2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPassword2 = New System.Windows.Forms.TextBox
        Me.GradientPanel3 = New VB2005_Sherlock7.GradientPanel
        Me.txtUser1 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPassword1 = New System.Windows.Forms.TextBox
        Me.GradientPanel4 = New VB2005_Sherlock7.GradientPanel
        Me.txtUser3 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtPassword3 = New System.Windows.Forms.TextBox
        Me.GradientPanel1 = New VB2005_Sherlock7.GradientPanel
        Me.txtUser4 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPassword4 = New System.Windows.Forms.TextBox
        Me.GradientPanel5 = New VB2005_Sherlock7.GradientPanel
        Me.txtUser5 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtPassword5 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.pnlKeyboard.SuspendLayout()
        Me.GradientPanel2.SuspendLayout()
        Me.GradientPanel3.SuspendLayout()
        Me.GradientPanel4.SuspendLayout()
        Me.GradientPanel1.SuspendLayout()
        Me.GradientPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(811, 291)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(212, 161)
        Me.btnExit.TabIndex = 0
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.Transparent
        Me.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Save
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnSave.Location = New System.Drawing.Point(511, 291)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(304, 160)
        Me.btnSave.TabIndex = 5
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'GradientPanel2
        '
        Me.GradientPanel2.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel2.Controls.Add(Me.txtUser2)
        Me.GradientPanel2.Controls.Add(Me.Label2)
        Me.GradientPanel2.Controls.Add(Me.Label1)
        Me.GradientPanel2.Controls.Add(Me.txtPassword2)
        Me.GradientPanel2.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel2.Location = New System.Drawing.Point(8, 96)
        Me.GradientPanel2.Name = "GradientPanel2"
        Me.GradientPanel2.Size = New System.Drawing.Size(502, 84)
        Me.GradientPanel2.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel2.TabIndex = 1
        '
        'txtUser2
        '
        Me.txtUser2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser2.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser2.Location = New System.Drawing.Point(145, 5)
        Me.txtUser2.Margin = New System.Windows.Forms.Padding(0)
        Me.txtUser2.Name = "txtUser2"
        Me.txtUser2.Size = New System.Drawing.Size(279, 35)
        Me.txtUser2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(37, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 22)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(46, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Role 2"
        '
        'txtPassword2
        '
        Me.txtPassword2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword2.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword2.Location = New System.Drawing.Point(145, 43)
        Me.txtPassword2.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPassword2.Name = "txtPassword2"
        Me.txtPassword2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword2.Size = New System.Drawing.Size(279, 35)
        Me.txtPassword2.TabIndex = 3
        '
        'GradientPanel3
        '
        Me.GradientPanel3.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel3.Controls.Add(Me.txtUser1)
        Me.GradientPanel3.Controls.Add(Me.Label5)
        Me.GradientPanel3.Controls.Add(Me.Label6)
        Me.GradientPanel3.Controls.Add(Me.txtPassword1)
        Me.GradientPanel3.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel3.Location = New System.Drawing.Point(8, 7)
        Me.GradientPanel3.Name = "GradientPanel3"
        Me.GradientPanel3.Size = New System.Drawing.Size(502, 84)
        Me.GradientPanel3.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel3.TabIndex = 0
        '
        'txtUser1
        '
        Me.txtUser1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser1.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser1.Location = New System.Drawing.Point(145, 5)
        Me.txtUser1.Margin = New System.Windows.Forms.Padding(0)
        Me.txtUser1.Name = "txtUser1"
        Me.txtUser1.Size = New System.Drawing.Size(279, 35)
        Me.txtUser1.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(37, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 22)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Password"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(46, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 30)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Role 1"
        '
        'txtPassword1
        '
        Me.txtPassword1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword1.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword1.Location = New System.Drawing.Point(145, 43)
        Me.txtPassword1.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword1.Size = New System.Drawing.Size(279, 35)
        Me.txtPassword1.TabIndex = 3
        '
        'GradientPanel4
        '
        Me.GradientPanel4.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel4.Controls.Add(Me.txtUser3)
        Me.GradientPanel4.Controls.Add(Me.Label7)
        Me.GradientPanel4.Controls.Add(Me.Label8)
        Me.GradientPanel4.Controls.Add(Me.txtPassword3)
        Me.GradientPanel4.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel4.Location = New System.Drawing.Point(8, 185)
        Me.GradientPanel4.Name = "GradientPanel4"
        Me.GradientPanel4.Size = New System.Drawing.Size(502, 84)
        Me.GradientPanel4.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel4.TabIndex = 2
        '
        'txtUser3
        '
        Me.txtUser3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser3.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser3.Location = New System.Drawing.Point(145, 5)
        Me.txtUser3.Margin = New System.Windows.Forms.Padding(0)
        Me.txtUser3.Name = "txtUser3"
        Me.txtUser3.Size = New System.Drawing.Size(279, 35)
        Me.txtUser3.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(37, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 22)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Password"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(46, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(86, 30)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Role 3"
        '
        'txtPassword3
        '
        Me.txtPassword3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword3.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword3.Location = New System.Drawing.Point(145, 43)
        Me.txtPassword3.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPassword3.Name = "txtPassword3"
        Me.txtPassword3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword3.Size = New System.Drawing.Size(279, 35)
        Me.txtPassword3.TabIndex = 3
        '
        'GradientPanel1
        '
        Me.GradientPanel1.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel1.Controls.Add(Me.txtUser4)
        Me.GradientPanel1.Controls.Add(Me.Label3)
        Me.GradientPanel1.Controls.Add(Me.Label4)
        Me.GradientPanel1.Controls.Add(Me.txtPassword4)
        Me.GradientPanel1.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel1.Location = New System.Drawing.Point(8, 274)
        Me.GradientPanel1.Name = "GradientPanel1"
        Me.GradientPanel1.Size = New System.Drawing.Size(502, 84)
        Me.GradientPanel1.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel1.TabIndex = 11
        '
        'txtUser4
        '
        Me.txtUser4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser4.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser4.Location = New System.Drawing.Point(145, 5)
        Me.txtUser4.Margin = New System.Windows.Forms.Padding(0)
        Me.txtUser4.Name = "txtUser4"
        Me.txtUser4.Size = New System.Drawing.Size(279, 35)
        Me.txtUser4.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(37, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 22)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Password"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(46, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 30)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Role 4"
        '
        'txtPassword4
        '
        Me.txtPassword4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword4.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword4.Location = New System.Drawing.Point(145, 43)
        Me.txtPassword4.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPassword4.Name = "txtPassword4"
        Me.txtPassword4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword4.Size = New System.Drawing.Size(279, 35)
        Me.txtPassword4.TabIndex = 3
        '
        'GradientPanel5
        '
        Me.GradientPanel5.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel5.Controls.Add(Me.txtUser5)
        Me.GradientPanel5.Controls.Add(Me.Label9)
        Me.GradientPanel5.Controls.Add(Me.Label10)
        Me.GradientPanel5.Controls.Add(Me.txtPassword5)
        Me.GradientPanel5.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel5.Location = New System.Drawing.Point(8, 363)
        Me.GradientPanel5.Name = "GradientPanel5"
        Me.GradientPanel5.Size = New System.Drawing.Size(502, 84)
        Me.GradientPanel5.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel5.TabIndex = 4
        '
        'txtUser5
        '
        Me.txtUser5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser5.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser5.Location = New System.Drawing.Point(145, 5)
        Me.txtUser5.Margin = New System.Windows.Forms.Padding(0)
        Me.txtUser5.Name = "txtUser5"
        Me.txtUser5.Size = New System.Drawing.Size(279, 35)
        Me.txtUser5.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(37, 51)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(95, 22)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Password"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(46, 6)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 30)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Role 5"
        '
        'txtPassword5
        '
        Me.txtPassword5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword5.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword5.Location = New System.Drawing.Point(145, 43)
        Me.txtPassword5.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPassword5.Name = "txtPassword5"
        Me.txtPassword5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword5.Size = New System.Drawing.Size(279, 35)
        Me.txtPassword5.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 32.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(516, 7)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(499, 284)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "User Access Levels"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'UserConfigureForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GradientPanel5)
        Me.Controls.Add(Me.GradientPanel1)
        Me.Controls.Add(Me.GradientPanel4)
        Me.Controls.Add(Me.GradientPanel3)
        Me.Controls.Add(Me.GradientPanel2)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "UserConfigureForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configure User Levels"
        Me.Controls.SetChildIndex(Me.pnlKeyboard, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnSave, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel2, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel3, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel4, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel1, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel5, 0)
        Me.Controls.SetChildIndex(Me.Label11, 0)
        Me.pnlKeyboard.ResumeLayout(False)
        Me.pnlKeyboard.PerformLayout()
        Me.GradientPanel2.ResumeLayout(False)
        Me.GradientPanel2.PerformLayout()
        Me.GradientPanel3.ResumeLayout(False)
        Me.GradientPanel3.PerformLayout()
        Me.GradientPanel4.ResumeLayout(False)
        Me.GradientPanel4.PerformLayout()
        Me.GradientPanel1.ResumeLayout(False)
        Me.GradientPanel1.PerformLayout()
        Me.GradientPanel5.ResumeLayout(False)
        Me.GradientPanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GradientPanel2 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPassword2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUser2 As System.Windows.Forms.TextBox
    Friend WithEvents GradientPanel3 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents txtUser1 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPassword1 As System.Windows.Forms.TextBox
    Friend WithEvents GradientPanel4 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents txtUser3 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPassword3 As System.Windows.Forms.TextBox
    Friend WithEvents GradientPanel1 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents txtUser4 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPassword4 As System.Windows.Forms.TextBox
    Friend WithEvents GradientPanel5 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents txtUser5 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPassword5 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label

End Class
