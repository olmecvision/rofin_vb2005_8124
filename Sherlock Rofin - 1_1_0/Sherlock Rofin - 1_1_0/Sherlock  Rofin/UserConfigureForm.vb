﻿Public Class UserConfigureForm

    Private Sub UserConfigureForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim users As List(Of UserInfo) = ConfigSettings.ReadConfigSettings()
        If (users IsNot Nothing) Then

            For Each user As UserInfo In users

                Select Case user.UserNumber
                    Case 1
                        txtUser1.Text = user.UserName
                        txtPassword1.Text = user.Password
                        Exit Select
                    Case 2
                        txtUser2.Text = user.UserName
                        txtPassword2.Text = user.Password
                        Exit Select
                    Case 3
                        txtUser3.Text = user.UserName
                        txtPassword3.Text = user.Password
                        Exit Select
                    Case 4
                        txtUser4.Text = user.UserName
                        txtPassword4.Text = user.Password
                        Exit Select
                    Case 5
                        txtUser5.Text = user.UserName
                        txtPassword5.Text = user.Password
                        Exit Select
                End Select

            Next

        End If

        btnExit.Focus()

    End Sub

#Region "Button Events"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        ' build a collection of user objects
        Dim users As New List(Of UserInfo)

        ' user 1
        If (Not String.IsNullOrEmpty(txtUser1.Text) And Not String.IsNullOrEmpty(txtPassword1.Text)) Then
            Dim user1 As UserInfo = New UserInfo()
            user1.UserNumber = 1
            user1.UserName = txtUser1.Text
            user1.Password = SecurityLogic.Encrypt(txtPassword1.Text)
            users.Add(user1)
        End If

        ' user 2
        If (Not String.IsNullOrEmpty(txtUser2.Text) And Not String.IsNullOrEmpty(txtPassword2.Text)) Then
            Dim user2 As UserInfo = New UserInfo()
            user2.UserNumber = 2
            user2.UserName = txtUser2.Text
            user2.Password = SecurityLogic.Encrypt(txtPassword2.Text)
            users.Add(user2)
        End If

        ' user 3
        If (Not String.IsNullOrEmpty(txtUser3.Text) And Not String.IsNullOrEmpty(txtPassword3.Text)) Then
            Dim user3 As UserInfo = New UserInfo()
            user3.UserNumber = 3
            user3.UserName = txtUser3.Text
            user3.Password = SecurityLogic.Encrypt(txtPassword3.Text)
            users.Add(user3)
        End If

        ' user 4
        If (Not String.IsNullOrEmpty(txtUser4.Text) And Not String.IsNullOrEmpty(txtPassword4.Text)) Then
            Dim user4 As UserInfo = New UserInfo()
            user4.UserNumber = 4
            user4.UserName = txtUser4.Text
            user4.Password = SecurityLogic.Encrypt(txtPassword4.Text)
            users.Add(user4)
        End If

        ' user 5
        If (Not String.IsNullOrEmpty(txtUser5.Text) And Not String.IsNullOrEmpty(txtPassword5.Text)) Then
            Dim user5 As UserInfo = New UserInfo()
            user5.UserNumber = 5
            user5.UserName = txtUser5.Text
            user5.Password = SecurityLogic.Encrypt(txtPassword5.Text)
            users.Add(user5)
        End If

        ' save the user information to the setting file
        ConfigSettings.WriteConfigSettings(users)

        ' close the form
        Me.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

#End Region

End Class
