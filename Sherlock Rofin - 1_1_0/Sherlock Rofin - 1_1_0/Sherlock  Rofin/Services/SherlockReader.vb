﻿Imports System.IO

Public Class SherlockReader

    Public Shared Function ReadProperty(ByRef sherlock As IpeEngCtrlLib.Engine, ByVal name As String, ByVal dataType As IpeEngCtrlLib.I_VAR_TYPE) As Object

        If (sherlock Is Nothing) Then
            Return Nothing
        End If

        If (String.IsNullOrEmpty(name)) Then
            Return Nothing
        End If

        Try

            Select Case dataType
                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL
                    Dim b As Boolean
                    sherlock.VarGetBool(name, b)
                    Return b

                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT
                    Dim i As Integer
                    sherlock.VarGetDouble(name, i)
                    Return i

                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_DOUBLE
                    Dim d As Double
                    sherlock.VarGetDouble(name, d)
                    Return d

                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_STRING
                    Dim s As String = String.Empty
                    sherlock.VarGetString(name, s)
                    Return s

                Case Else

            End Select

        Catch ex As Exception

        End Try

        Return Nothing

    End Function

End Class
