﻿Imports System.IO

Public Class CurrentFormatService

    Public Shared Function ReadCurrentFormat(ByVal filePath As String) As String

        Dim currentFormat As String = String.Empty

        Try

            Using reader As New StreamReader(filePath)

                currentFormat = reader.ReadLine()

            End Using

        Catch ex As Exception
            'MessageBox.Show(String.Format("Failed reading current format configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Return currentFormat.ToUpper()

    End Function

    Public Shared Sub SaveCurrentFormat(ByRef currentFormat As FormatInfo)

        If (currentFormat Is Nothing) Then
            Return
        End If

        Dim filePath As String = My.Settings.CurrentFormatFile
        If (String.IsNullOrEmpty(filePath)) Then
            Return
        End If

        Try

            Using writer As New StreamWriter(filePath)

                If (My.Settings.ScanFolderForInvestigations) Then
                    writer.WriteLine(currentFormat.FormatFile.ToUpper())
                Else
                    writer.WriteLine(String.Format("{0} - {1}", currentFormat.FormatCode1, currentFormat.FormatCode2))
                End If
                writer.Flush()

            End Using

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed saving current format.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

    End Sub

End Class
