﻿Imports System.IO

Public Class HardwareConfigurationReader

    Public Shared Function ReadConfiguration(ByVal filePath As String) As List(Of LaserInfo)

        Dim collection As New Dictionary(Of Integer, LaserInfo)()

        Try
            Using reader As New StreamReader(filePath)

                Dim line As String
                Dim row() As String

                line = reader.ReadLine()

                While (Not String.IsNullOrEmpty(line))

                    ' get the row elements
                    row = line.Split(",")

                    If (row Is Nothing) Then
                        Continue While
                    End If

                    If (row.Length = 0) Then
                        Continue While
                    End If

                    ' set up a new object
                    Dim info As New HardwareConfigurationInfo()
                    Int32.TryParse(row(0), info.LaserId)
                    info.LaserName = row(1)
                    Int32.TryParse(row(2), info.HeadId)
                    info.HeadName = row(3)
                    info.InstanceType = row(4).ToLowerInvariant()
                    info.IpAddress = row(5)
                    Int32.TryParse(row(6), info.Port)
                    info.DriverFileName = row(7)

                    ' process the row
                    ProcessRow(collection, info)

                    ' read the next line
                    line = reader.ReadLine()

                End While

            End Using

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed reading cameras configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Dim list As New List(Of LaserInfo)

        If (Not collection Is Nothing) Then
            For Each item As LaserInfo In collection.Values
                list.Add(item)
            Next

        End If
        Return list

    End Function

    Public Shared Function IsInstanceTypeLocal(ByVal instanceType As String) As Boolean

        If (String.IsNullOrEmpty(instanceType)) Then
            Return False
        End If

        If Not (instanceType = "local") Then
            Return False
        End If

        Return True

    End Function

    Public Shared Function IsInstanceTypeRemote(ByVal instanceType As String) As Boolean

        If (String.IsNullOrEmpty(instanceType)) Then
            Return False
        End If

        If Not (instanceType = "remote") Then
            Return False
        End If

        Return True

    End Function

    Public Shared Sub ProcessRow(ByRef collection As Dictionary(Of Integer, LaserInfo), ByVal info As HardwareConfigurationInfo)

        If (Not IsRowValid(info)) Then
            Return
        End If

        Dim found As LaserInfo = AddOrLookupLaser(collection, info)
        If Not (found Is Nothing) Then
            AddHead(found, info)
        End If

    End Sub

    Public Shared Function AddOrLookupLaser(ByRef collection As Dictionary(Of Integer, LaserInfo), ByVal info As HardwareConfigurationInfo) As LaserInfo

        ' does the laser already exist?
        If (collection.ContainsKey(info.LaserId)) Then
            Return collection(info.LaserId)
        End If

        ' create the new laser
        Dim newLaser As New LaserInfo()
        newLaser.LaserId = info.LaserId
        newLaser.LaserName = info.LaserName
        newLaser.Heads = New List(Of HeadInfo)()

        ' add to the collection
        collection.Add(info.LaserId, newLaser)

        Return newLaser

    End Function

    Public Shared Sub AddHead(ByRef laser As LaserInfo, ByVal info As HardwareConfigurationInfo)

        ' does the laser already exist?
        For Each head As HeadInfo In laser.Heads
            If (head.HeadId = info.HeadId) Then
                Return
            End If
        Next

        ' add the head
        Dim newHead As New HeadInfo()
        newHead.HeadId = info.HeadId
        newHead.HeadName = info.HeadName
        newHead.LaserId = laser.LaserId
        newHead.InstanceType = info.InstanceType
        newHead.IpAddress = info.IpAddress
        newHead.Port = info.Port
        newHead.DriverFileName = info.DriverFileName

        ' add the sherlock engine and attach to the head
        Dim ipeEngCtrlType As Type
        Dim engineInstance As IpeEngCtrlLib.Engine ' = New IpeEngCtrlLib.Engine()
        Dim err As IpeEngCtrlLib.I_ENG_ERROR

        ipeEngCtrlType = Type.GetTypeFromProgID("IpeEngCtrl.EngineNg")
        engineInstance = Activator.CreateInstance(ipeEngCtrlType)

        ' set the driver
        If (Not String.IsNullOrEmpty(info.DriverFileName)) Then
            err = engineInstance.EngSetAcqIni(info.DriverFileName)
            If (err <> IpeEngCtrlLib.I_ENG_ERROR.I_OK) Then
                MsgBox(String.Format("Error loading driver {0}.", info.DriverFileName), MsgBoxStyle.Critical, "Error")
            End If
        End If

        ' initialise the instance
        err = engineInstance.EngInitialize()
        If (err <> IpeEngCtrlLib.I_ENG_ERROR.I_OK) Then
            MsgBox("Error loading engine.", MsgBoxStyle.Critical, "Error")
        End If

        ' assign the engine to the head
        newHead.Engine = engineInstance

        ' add to the collection
        laser.Heads.Add(newHead)

    End Sub

    Public Shared Function IsRowValid(ByVal info As HardwareConfigurationInfo) As Boolean

        If (info Is Nothing) Then
            Return False
        End If

        If (info.LaserId < 1 Or info.LaserId > 4) Then
            Return False
        End If

        If (info.HeadId < 1 Or info.HeadId > 4) Then
            Return False
        End If

        If Not (IsInstanceTypeLocal(info.InstanceType) Or IsInstanceTypeRemote(info.InstanceType)) Then
            Return False
        End If

        If (IsInstanceTypeRemote(info.InstanceType) And (String.IsNullOrEmpty(info.IpAddress) Or info.Port = 0)) Then
            Return False
        End If

        Return True

    End Function

End Class
