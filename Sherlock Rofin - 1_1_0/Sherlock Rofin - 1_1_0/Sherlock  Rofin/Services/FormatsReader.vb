﻿Imports System.IO

Public Class FormatsReader

    Public Shared Function ReadFormats(ByVal filePath As String) As Dictionary(Of String, FormatInfo)

        Dim collection As New Dictionary(Of String, FormatInfo)()

        Try
            If (My.Settings.ScanFolderForInvestigations) Then

                Dim directory As New IO.DirectoryInfo(My.Settings.InvestigationsFolder)
                Dim files As IO.FileInfo() = directory.GetFiles("*.ivs")

                For Each file As IO.FileInfo In files

                    ' set up a new object
                    Dim info As New FormatInfo()
                    info.FormatCode1 = file.Name.ToUpper()
                    info.FormatCode2 = String.Empty
                    info.FormatDescription = file.Name
                    info.FormatFile = file.Name

                    ' add to the collection
                    collection.Add(info.FormatCode1, info)
                Next

            Else

                Using reader As New StreamReader(filePath)

                    Dim line As String
                    Dim row() As String

                    line = reader.ReadLine()

                    While (Not String.IsNullOrEmpty(line))

                        ' get the row elements
                        row = line.Split(",")

                        If (row Is Nothing) Then
                            Continue While
                        End If

                        If (row.Length = 0) Then
                            Continue While
                        End If

                        ' set up a new object
                        Dim info As New FormatInfo()
                        info.FormatCode1 = row(0)
                        info.FormatCode2 = row(1)
                        info.FormatDescription = row(2)
                        info.FormatFile = row(3)

                        ' add to the collection
                        collection.Add(String.Format("{0} - {1}", info.FormatCode1, info.FormatCode2), info)

                        ' read the next line
                        line = reader.ReadLine()

                    End While

                End Using

            End If

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed reading formats configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Return collection

    End Function

    Public Shared Function GetFormat(ByRef collection As Dictionary(Of String, FormatInfo), ByVal formatKey As String) As FormatInfo

        If (collection Is Nothing) Then
            Return Nothing
        End If

        If (Not collection.ContainsKey(formatKey)) Then
            Return Nothing
        End If

        Return collection(formatKey)

    End Function

    Public Shared Function GetFirst(ByRef collection As Dictionary(Of String, FormatInfo)) As FormatInfo

        If (collection Is Nothing) Then
            Return Nothing
        End If

        Dim enumerator As IEnumerator = collection.GetEnumerator()
        enumerator.MoveNext()

        Return CType(enumerator.Current, KeyValuePair(Of String, FormatInfo)).Value

    End Function

End Class
