﻿Imports System.IO
Imports System.Threading

Public Class EncoderService

    Private WithEvents m_EncoderReaderTimer As System.Timers.Timer
    Private WithEvents m_EncoderAutoTriggerTimer As System.Timers.Timer
    Private m_Encoder1PositionDelay As Integer
    Private m_Encoder2PositionDelay As Integer
    Private m_EncoderCardId As Short
    Private m_AutoTriggerEnabled As Boolean
    Private m_ThreadList As List(Of Thread) = New List(Of Thread)

#Region "Public Methods"

    Public Sub StopEncoder()
        terminated = True
        For Each thread As Thread In m_ThreadList
            If thread.IsAlive Then
                Try
                    thread.Abort()
                Catch ex As Exception

                End Try
            End If
        Next
    End Sub
    Public Sub Start()
        If (My.Settings.Encoder1Enable Or My.Settings.Encoder2Enable) Then
            Dim ret As Short
            Dim cardIdInBit As Short
            Dim manualId As Short
            Dim defaultSw As Short

            Try
                ' set the base encoder properties
                ret = B_8124_initialSw(cardIdInBit, manualId, defaultSw)
                If (ret <> 0) Then
                    MsgBox("Encoder Error: " & CStr(ret))
                    End
                End If

                ' configure encoder 1
                If (My.Settings.Encoder1Enable) Then
                    ConfigureEncoder(0, My.Settings.Encoder1InputMode, My.Settings.Encoder1PulseWidth, My.Settings.Encoder1FilterEnabled, My.Settings.Encoder1LatchRisingEdge, My.Settings.Encoder1SleepDuration)

                    ' setup the threads
                    Dim thread As Thread = New Thread(AddressOf Encoder0Process)
                    m_ThreadList.Add(thread)

                    ' start the threads
                    thread.IsBackground = True
                    thread.Start()

                    'increase thread priority of encoder processess
                    thread.Priority = ThreadPriority.Highest

                End If

                ' configure encoder 2
                If (My.Settings.Encoder2Enable) Then
                    ConfigureEncoder(1, My.Settings.Encoder2InputMode, My.Settings.Encoder2PulseWidth, My.Settings.Encoder2FilterEnabled, My.Settings.Encoder2LatchRisingEdge, My.Settings.Encoder2SleepDuration)

                    ' setup the threads
                    Dim thread As Thread = New Thread(AddressOf Encoder1Process)
                    m_ThreadList.Add(thread)
                    ' start the threads
                    thread.IsBackground = True
                    thread.Start()

                    'increase thread priority of encoder processess
                    thread.Priority = ThreadPriority.Highest

                End If

                ' start the encoder values reader thread
                m_EncoderReaderTimer = New System.Timers.Timer(100)
                m_EncoderReaderTimer.Start()

                ' auto trigger
                m_EncoderAutoTriggerTimer = New System.Timers.Timer(100)

            Catch ex As Exception
                MessageBox.Show(String.Format("Failed to start thread.{0}{1}", vbCrLf + vbCrLf, ex.Message))
            End Try
        End If
    End Sub

    Public Sub StartAutoTrigger()

        If (Not m_AutoTriggerEnabled) Then
            If Not (m_EncoderAutoTriggerTimer Is Nothing) Then
                m_EncoderAutoTriggerTimer.Start()

                m_AutoTriggerEnabled = True
            End If
        End If

    End Sub

    Public Sub StopAutoTrigger()

        If (m_AutoTriggerEnabled) Then
            m_EncoderAutoTriggerTimer.Stop()

            m_AutoTriggerEnabled = False

            ' reset the encoder value
            B_8124_set_encoder_value(m_EncoderCardId, 0, 0)
            B_8124_set_encoder_value(m_EncoderCardId, 1, 0)

            ' clear the fifo array
            B_8124_reset_fifo(m_EncoderCardId, 0)
            B_8124_reset_fifo(m_EncoderCardId, 1)

            ' clear the comparator
            B_8124_set_comparator_data(m_EncoderCardId, 0, 0)
            B_8124_set_comparator_data(m_EncoderCardId, 1, 0)

        End If

    End Sub

    Public Sub ClearEncoder()
        ' reset the encoder value
        B_8124_set_encoder_value(m_EncoderCardId, 0, 0)
        B_8124_set_encoder_value(m_EncoderCardId, 1, 0)

        ' clear the fifo array
        B_8124_reset_fifo(m_EncoderCardId, 0)
        B_8124_reset_fifo(m_EncoderCardId, 1)

        ' clear the comparator
        B_8124_set_comparator_data(m_EncoderCardId, 0, 0)
        B_8124_set_comparator_data(m_EncoderCardId, 1, 0)
    End Sub

#End Region

#Region "Private Methods"

    Private Sub ConfigureEncoder(channel As Integer, inputMode As Short, pulseWidth As Short, filterEnabled As Boolean, latchRisingEdge As Boolean, sleepDuration As Integer)

        'set Encoder input mode
        B_8124_set_encoder_input_mode(m_EncoderCardId, channel, inputMode)

        'Config PWM source
        B_8124_set_pwm_source(m_EncoderCardId, channel, 1, 0, 0) 'Src = comparator

        ' configure the pulse width
        B_8124_set_pwm_pulse_width(m_EncoderCardId, channel, pulseWidth)

        'Config FIFO
        Dim fifoLevel As Short = 20 ' Low level = 256 (1/4 FIFO size)
        B_8124_set_fifo_level(m_EncoderCardId, channel, fifoLevel)

        ' reset the encoder value
        B_8124_set_encoder_value(m_EncoderCardId, channel, 0)

        ' enable/disable the input filter 
        B_8124_set_input_filter_enable(m_EncoderCardId, channel, filterEnabled)

        ' set the latch
        B_8124_set_latch(m_EncoderCardId, channel, 1, 1)

        ' clear the fifo array
        B_8124_reset_fifo(m_EncoderCardId, channel)

        ' clear the comparator
        B_8124_set_comparator_data(m_EncoderCardId, channel, 0)

    End Sub

    Private Sub Encoder0Process()
        Try
            While Not terminated
                EncoderProcess(0, My.Settings.Encoder1SleepDuration)
            End While
        Catch ex As ThreadAbortException

        End Try

    End Sub

    Dim terminated As Boolean = False

    Private Sub Encoder1Process()
        Try
            While Not terminated
                EncoderProcess(1, My.Settings.Encoder2SleepDuration)
            End While
        Catch ex As ThreadAbortException
            
        End Try

    End Sub

    Private Sub EncoderProcess(channel As Integer, sleepDuration As Integer)

        Dim latchValue As Integer
        Dim fifoValue As Integer
        Dim tempFifoValue As Long
        Dim cmpData As Integer
        Dim shift_required As Boolean
        Dim positionDelay As Integer
        Dim encoderValueCurrent As Integer
        Const maxUnsigned32 As Long = 4294967291
        Const maxSigned32 As Integer = 2147483647
        Dim fifoStatus As Short
        Dim latchValue64 As Long
        Dim positionDelay64 As Long


        'Dim stopWatch As Stopwatch = stopWatch.StartNew()

        'set interupt on latch 0 (16 being pin 5 on interupt port)
        B_8124_set_int_factor(m_EncoderCardId, channel, 16)

        ' get the position delay
        positionDelay = IIf(channel = 0, Encoder1PositionDelay, Encoder2PositionDelay)

        'waits for interupt with no timeout
        B_8124_wait_single_int(m_EncoderCardId, channel, 4, -1)

        'gets the latch accurate value after interupt
        B_8124_get_latch_value(m_EncoderCardId, channel, latchValue)

        'convert to int64 variables for arithmatic exception debug
        latchValue64 = Convert.ToInt64(latchValue)
        positionDelay64 = Convert.ToInt64(positionDelay)

        'takes encoder value when triggered and adds position delay
        'tempFifoValue = latchValue + positionDelay
        tempFifoValue = latchValue64 + positionDelay64

        If tempFifoValue > maxSigned32 Then
            tempFifoValue = tempFifoValue - maxUnsigned32
        End If

        fifoValue = Convert.ToInt32(tempFifoValue)

        'get the comparator value
        B_8124_get_comparator_data(m_EncoderCardId, channel, cmpData)

        'get the current encoder value
        B_8124_get_encoder_value(m_EncoderCardId, channel, encoderValueCurrent)

        'where to write?
        shift_required = (encoderValueCurrent > cmpData Or fifoValue < cmpData)

        'System.Diagnostics.Trace.WriteLine(String.Format("comparitor value is {0}, fifo status is {1}, current encoder {2}", cmpData, fifo_sts_value, encoderValueCurrent))

        ' shift fifo if we need to
        If (shift_required) Then
            'time the routine
            'stopWatch.Start()

            'get the fifo status to validate fifo values
            B_8124_get_fifo_sts(m_EncoderCardId, channel, fifoStatus)

            'if the fifo is full or the fifo has stored triggers but the system is writing direct to comparitor (not shifting) reset fifo
            If ((fifoStatus = 3) Or ((fifoStatus <> 0) And (shift_required))) Then
                B_8124_reset_fifo(m_EncoderCardId, channel)
            End If

            B_8124_set_comparator_data(m_EncoderCardId, channel, fifoValue)
            'output the routine duration
            'stopWatch.Stop()
            ' Dim d As Decimal = (stopWatch.ElapsedMilliseconds)
            'System.Diagnostics.Trace.WriteLine(String.Format("Encoder {0}: Routine Milliseconds: {1}", channel, d))
        Else
            'writes position delay into fifo in order to trigger camera at correct time
            B_8124_set_fifo_data(m_EncoderCardId, channel, fifoValue)
        End If

        'retrigger delay set from config file
        Thread.Sleep(sleepDuration)

        ' clear interrupt
        B_8124_reset_int(m_EncoderCardId, channel, 16)

    End Sub
#End Region

#Region "Callback Methods"

    Private Sub EncoderValueTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles m_EncoderReaderTimer.Elapsed

        Dim encoderPosition As Integer
        B_8124_get_encoder_value(m_EncoderCardId, 0, encoderPosition)

        Dim cmpData As Integer
        B_8124_get_comparator_data(m_EncoderCardId, 0, cmpData)

        Dim fifoStatus As Short
        B_8124_get_fifo_sts(m_EncoderCardId, 0, fifoStatus)

        Dim args As EncoderPositionEventArgs = New EncoderPositionEventArgs(encoderPosition, cmpData, fifoStatus)
        OnEncoderPositionChanged(args)

    End Sub

    Private Sub EncoderAutoTriggerTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles m_EncoderAutoTriggerTimer.Elapsed

        B_8124_manual_trigger(m_EncoderCardId, 0)
        B_8124_manual_trigger(m_EncoderCardId, 1)

    End Sub

#End Region

#Region "Properties"

    Public Property Encoder1PositionDelay() As Integer
        Get
            Return m_Encoder1PositionDelay
        End Get
        Set(ByVal value As Integer)
            m_Encoder1PositionDelay = value
        End Set
    End Property

    Public Property Encoder2PositionDelay() As Integer
        Get
            Return m_Encoder2PositionDelay
        End Get
        Set(ByVal value As Integer)
            m_Encoder2PositionDelay = value
        End Set
    End Property

    Public Property AutoTriggerEnabled() As Boolean
        Get
            Return m_AutoTriggerEnabled
        End Get
        Set(ByVal value As Boolean)
            m_AutoTriggerEnabled = value
        End Set
    End Property

#End Region

#Region "Event Args"

    Public Class EncoderPositionEventArgs
        Inherits EventArgs
        Private ReadOnly _encoderPosition As Integer
        Private ReadOnly _cmpData As Integer
        Private ReadOnly _fifoStatus As Short

        Public Sub New(ByVal encoderPosition As Integer, ByVal cmpData As Integer, ByVal fifoStatus As Short)
            Me._encoderPosition = encoderPosition
            Me._cmpData = cmpData
            Me._fifoStatus = fifoStatus
        End Sub

        Public ReadOnly Property EncoderPosition() As Integer
            Get
                Return _encoderPosition
            End Get
        End Property

        Public ReadOnly Property CmpData() As Integer
            Get
                Return _cmpData
            End Get
        End Property

        Public ReadOnly Property FifoStatus() As Short
            Get
                Return _fifoStatus
            End Get
        End Property
    End Class

#End Region

#Region "Events"

    Public Delegate Sub EncoderPositionDelegate(ByVal sender As Object, ByVal e As EncoderPositionEventArgs)
    Public Event EncoderPositionChanged As EncoderPositionDelegate

    Protected Overridable Sub OnEncoderPositionChanged(ByVal e As EncoderPositionEventArgs)
        RaiseEvent EncoderPositionChanged(Me, e)
    End Sub

#End Region


End Class
