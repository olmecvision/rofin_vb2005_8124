﻿Imports System.IO

Public Class CamerasReader

    Public Shared Function ReadCameras(ByVal filePath As String) As Dictionary(Of String, CameraInfo)

        Dim collection As New Dictionary(Of String, CameraInfo)()

        Try
            Using reader As New StreamReader(filePath)

                Dim line As String
                Dim row() As String

                line = reader.ReadLine()

                While (Not String.IsNullOrEmpty(line))

                    ' get the row elements
                    row = line.Split(",")

                    If (row Is Nothing) Then
                        Continue While
                    End If

                    If (row.Length = 0) Then
                        Continue While
                    End If

                    ' set up a new object
                    Dim info As New CameraInfo()
                    info.CameraId = row(0)
                    info.CameraName = row(1)
                    info.SubCamera1 = CType(row(2), Boolean)
                    info.SubCamera2 = CType(row(3), Boolean)
                    info.SubCamera3 = CType(row(4), Boolean)

                    ' add to the collection
                    collection.Add(info.CameraId, info)

                    ' read the next line
                    line = reader.ReadLine()

                End While

            End Using

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed reading cameras configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Return collection

    End Function

    Public Shared Function GetCamera(ByRef collection As Dictionary(Of String, CameraInfo), ByVal cameraId As String) As CameraInfo

        If (collection Is Nothing) Then
            Return Nothing
        End If

        If (Not collection.ContainsKey(cameraId)) Then
            Return Nothing
        End If

        Return collection(cameraId)

    End Function

End Class
