﻿Imports System.IO
Imports MigraDoc.DocumentObjectModel
Imports MigraDoc.DocumentObjectModel.Tables
Imports MigraDoc.Rendering

Public Class ReportWriter

    Public Shared Function WriteShiftReport(ByRef m_MainScreen As MainForm, ByVal filePath As String) As Boolean

        If (String.IsNullOrEmpty(filePath)) Then
            Throw New ArgumentNullException("filePath")
        End If

        'Dim sherlock As IpeEngCtrlLib.Engine = m_MainScreen.Sherlock
        Dim doc As Document = New Document()
        Dim section As Section = doc.AddSection()

        ' image
        section.AddImage(String.Format("{0}\\report-logo.gif", My.Settings.InvestigationsFolder))
        section.AddParagraph()

        Dim p1 As Paragraph = section.AddParagraph(String.Format("Shift Report: {0} -> {1}",
            m_MainScreen.ShiftStart.ToString("yyyy-MM-dd HH:mm"),
            DateTime.Now.ToString("yyyy-MM-dd HH:mm")))

        p1.Format.Font.Bold = True
        p1.Format.Font.Color = Colors.Black
        p1.Format.Font.Underline = Underline.None
        section.AddParagraph()

        Dim statsTable As Table = section.AddTable()
        statsTable.Borders.Visible = True
        statsTable.AddColumn(Unit.FromCentimeter(8))
        statsTable.AddColumn(Unit.FromCentimeter(4))

        Dim header As Row = statsTable.AddRow()
        header.Shading.Color = Colors.LightGray

        header.Cells(0).Format.Alignment = ParagraphAlignment.Center
        header.Cells(0).AddParagraph("Information")

        header.Cells(1).Format.Alignment = ParagraphAlignment.Center
        header.Cells(1).AddParagraph("Value")

        Dim statsCollection As Dictionary(Of String, StatisticInfo) = m_MainScreen.Statistics()
        If (statsCollection IsNot Nothing) Then
            For Each stat As KeyValuePair(Of String, StatisticInfo) In statsCollection

                If (Not stat.Value.DisplayOnShiftReport) Then
                    Continue For
                End If

                If (Not String.IsNullOrEmpty(stat.Value.Fn)) Then
                    If (Not stat.Value.IsFirst) Then
                        Continue For
                    End If
                End If

                Dim row As Row = statsTable.AddRow()

                ' stat name
                Dim statName As String = stat.Value.StatisticName
                If (String.IsNullOrEmpty(stat.Value.Fn) And stat.Value.AllHeads) Then
                    statName = statName + String.Format(" - L{0} - H{1} ", stat.Value.LaserId, stat.Value.HeadId)
                End If

                ' stat value
                Dim statisticValue As String = String.Empty
                Dim stats = m_MainScreen.Statistics

                Select Case stat.Value.Fn

                    Case "TOTAL"
                        statisticValue = StatisticsReader.GetStatisticTotalValue(stats, stat.Value.StatisticId)

                    Case Else
                        statisticValue = StatisticsReader.GetStatisticValue(stat.Value)

                End Select

                row.Cells(0).Format.Alignment = ParagraphAlignment.Left
                row.Cells(0).AddParagraph(statName)

                row.Cells(1).Format.Alignment = ParagraphAlignment.Right
                row.Cells(1).AddParagraph(statisticValue)

            Next

        End If

        Dim renderer As PdfDocumentRenderer = New PdfDocumentRenderer(True, PdfSharp.Pdf.PdfFontEmbedding.Always)
        renderer.Document = doc
        renderer.RenderDocument()

        renderer.PdfDocument.Save(filePath)

    End Function

    Public Shared Function WriteBatchReport(ByRef m_MainScreen As MainForm, ByVal filePath As String) As Boolean

        If (String.IsNullOrEmpty(filePath)) Then
            Throw New ArgumentNullException("filePath")
        End If

        'Dim sherlock As IpeEngCtrlLib.Engine = m_MainScreen.Sherlock
        Dim doc As Document = New Document()
        Dim section As Section = doc.AddSection()

        ' image
        Dim logoFile = My.Settings("LogoPath") + "\\report-logo.png"
        'section.AddImage(String.Format("{0}\\report-logo.png", My.Settings.InvestigationsFolder))
        section.AddImage(logoFile)
        section.AddParagraph()

        Dim p1 As Paragraph = section.AddParagraph(String.Format("Batch Report: {0} -> {1}",
            m_MainScreen.BatchStart.ToString("yyyy-MM-dd HH:mm"),
            DateTime.Now.ToString("yyyy-MM-dd HH:mm")))

        p1.Format.Font.Bold = True
        p1.Format.Font.Color = Colors.Black
        p1.Format.Font.Underline = Underline.None
        section.AddParagraph()

        Dim statsTable As Table = section.AddTable()
        statsTable.Borders.Visible = True
        statsTable.AddColumn(Unit.FromCentimeter(8))
        statsTable.AddColumn(Unit.FromCentimeter(4))

        Dim header As Row = statsTable.AddRow()
        header.Shading.Color = Colors.LightGray

        header.Cells(0).Format.Alignment = ParagraphAlignment.Center
        header.Cells(0).AddParagraph("Information")

        header.Cells(1).Format.Alignment = ParagraphAlignment.Center
        header.Cells(1).AddParagraph("Value")

        '' batch info
        'Dim worksOrderRow As Row = statsTable.AddRow()
        'worksOrderRow.Cells(0).Format.Alignment = ParagraphAlignment.Left
        'worksOrderRow.Cells(0).AddParagraph("Works Order")

        Dim statsCollection As Dictionary(Of String, StatisticInfo) = m_MainScreen.Statistics()
        If (statsCollection IsNot Nothing) Then
            For Each stat As KeyValuePair(Of String, StatisticInfo) In statsCollection

                If (Not stat.Value.DisplayOnBatchReport) Then
                    Continue For
                End If

                If (Not String.IsNullOrEmpty(stat.Value.Fn)) Then
                    If (Not stat.Value.IsFirst) Then
                        Continue For
                    End If
                End If

                Dim row As Row = statsTable.AddRow()

                ' stat name
                Dim statName As String = stat.Value.StatisticName
                If (String.IsNullOrEmpty(stat.Value.Fn) And stat.Value.AllHeads) Then
                    statName = statName + String.Format(" - L{0} - H{1} ", stat.Value.LaserId, stat.Value.HeadId)
                End If

                ' stat value
                Dim statisticValue As String = String.Empty
                Dim stats = m_MainScreen.Statistics

                Select Case stat.Value.Fn

                    Case "TOTAL"
                        statisticValue = StatisticsReader.GetStatisticTotalValue(stats, stat.Value.StatisticId)

                    Case Else
                        statisticValue = StatisticsReader.GetStatisticValue(stat.Value)

                End Select

                row.Cells(0).Format.Alignment = ParagraphAlignment.Left
                row.Cells(0).AddParagraph(statName)

                row.Cells(1).Format.Alignment = ParagraphAlignment.Right
                row.Cells(1).AddParagraph(statisticValue)

            Next

        End If
        If False Then
            section.AddParagraph()

            ' images
            Dim imagesTable As Table = section.AddTable()
            imagesTable.Borders.Visible = False
            imagesTable.AddColumn(Unit.FromCentimeter(8))
            imagesTable.AddColumn(Unit.FromCentimeter(8))

            ' row 1
            Dim imageRow1 As Row = imagesTable.AddRow()

            imageRow1.Cells(0).Format.Alignment = ParagraphAlignment.Center
            Dim img1 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow1.Cells(0).AddImage(String.Format("{0}\\1.bmp", My.Settings.InvestigationsFolder))
            'img1.LockAspectRatio = True
            img1.Width = Unit.FromCentimeter(8)
            img1.Height = Unit.FromCentimeter(8)

            imageRow1.Cells(1).Format.Alignment = ParagraphAlignment.Center
            Dim img2 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow1.Cells(1).AddImage(String.Format("{0}\\2.bmp", My.Settings.InvestigationsFolder))
            'img2.LockAspectRatio = True
            img2.Width = Unit.FromCentimeter(8)
            img2.Height = Unit.FromCentimeter(8)

            ' row 2
            Dim imageRow2 As Row = imagesTable.AddRow()

            imageRow2.Cells(0).Format.Alignment = ParagraphAlignment.Center
            Dim img3 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow2.Cells(0).AddImage(String.Format("{0}\\3.bmp", My.Settings.InvestigationsFolder))
            'img3.LockAspectRatio = True
            img3.Width = Unit.FromCentimeter(8)
            img3.Height = Unit.FromCentimeter(8)
            'img3.ScaleHeight = 1

            imageRow2.Cells(1).Format.Alignment = ParagraphAlignment.Center
            Dim img4 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow2.Cells(1).AddImage(String.Format("{0}\\4.bmp", My.Settings.InvestigationsFolder))
            'img4.LockAspectRatio = True
            img4.Width = Unit.FromCentimeter(8)
            img4.Height = Unit.FromCentimeter(8)

            ' row 3
            Dim imageRow3 As Row = imagesTable.AddRow()

            imageRow3.Cells(0).Format.Alignment = ParagraphAlignment.Center
            Dim img5 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow3.Cells(0).AddImage(String.Format("{0}\\5.bmp", My.Settings.InvestigationsFolder))
            'img5.LockAspectRatio = True
            img5.Width = Unit.FromCentimeter(8)
            img5.Height = Unit.FromCentimeter(8)

            imageRow3.Cells(1).Format.Alignment = ParagraphAlignment.Center
            Dim img6 As MigraDoc.DocumentObjectModel.Shapes.Image = imageRow3.Cells(1).AddImage(String.Format("{0}\\6.bmp", My.Settings.InvestigationsFolder))
            ' img6.LockAspectRatio = True
            img6.Width = Unit.FromCentimeter(8)
            img6.Height = Unit.FromCentimeter(8)

            ' end
            'section.AddParagraph()
        End If
        Dim renderer As PdfDocumentRenderer = New PdfDocumentRenderer(True, PdfSharp.Pdf.PdfFontEmbedding.Always)
        renderer.Document = doc
        renderer.RenderDocument()

        renderer.PdfDocument.Save(filePath)

    End Function

End Class
