﻿Imports System.IO

Public Class VariablesReader

    Public Shared Function ReadVariables(ByVal filePath As String) As Dictionary(Of String, VariableInfo)

        Dim collection As New Dictionary(Of String, VariableInfo)()

        Try
            Using reader As New StreamReader(filePath)

                Dim line As String
                Dim row() As String

                line = reader.ReadLine()

                While (Not String.IsNullOrEmpty(line))

                    ' get the row elements
                    row = line.Split(",")

                    If (row Is Nothing) Then
                        Continue While
                    End If

                    If (row.Length = 0) Then
                        Continue While
                    End If

                    ' set up a new object
                    Dim info As New VariableInfo()
                    info.VariableId = row(0)
                    info.VariableName = row(1)

                    'data type
                    Dim strDataType = String.Format("I_VAR_{0}", row(2).ToUpperInvariant())
                    info.DataType = DirectCast([Enum].Parse(GetType(IpeEngCtrlLib.I_VAR_TYPE), strDataType), IpeEngCtrlLib.I_VAR_TYPE)

                    If (info.DataType = IpeEngCtrlLib.I_VAR_TYPE.I_VAR_DOUBLE Or info.DataType = IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT) Then
                        info.MinimumValue = row(3)
                        info.MaximumValue = row(4)
                    End If

                    ' add to the collection
                    collection.Add(info.VariableId, info)

                    ' read the next line
                    line = reader.ReadLine()

                End While

            End Using

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed reading variables configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Return collection

    End Function

    Public Shared Function GetVariable(ByRef collection As Dictionary(Of String, VariableInfo), ByVal variableId As String) As VariableInfo

        If (collection Is Nothing) Then
            Return Nothing
        End If

        If (Not collection.ContainsKey(variableId)) Then
            Return Nothing
        End If

        Return collection(variableId)

    End Function

    Public Shared Sub UpdateValues(ByRef collection As Dictionary(Of String, VariableInfo), ByRef sherlock As IpeEngCtrlLib.Engine)

        If (collection Is Nothing) Then
            Return
        End If

        If (sherlock Is Nothing) Then
            Return
        End If

        ' update all items
        For Each item As VariableInfo In collection.Values()

            ' check if the item exists in sherlock
            item.Value = SherlockReader.ReadProperty(sherlock, item.VariableId, item.DataType)

        Next

    End Sub

End Class
