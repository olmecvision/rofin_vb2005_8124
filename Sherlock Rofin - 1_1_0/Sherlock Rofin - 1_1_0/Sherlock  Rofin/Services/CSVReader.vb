﻿Imports System.IO

Public Class CSVReader

    Public Shared Function ReadFile(ByVal filePath As String) As List(Of FormatInfo)

        Dim formatCollection As New List(Of FormatInfo)()

        Try
            Using reader As New StreamReader(filePath)

                Dim line As String
                Dim row() As String

                line = reader.ReadLine()

                While (Not String.IsNullOrEmpty(line))

                    ' get the row elements
                    row = line.Split(",")

                    If (row Is Nothing) Then
                        Continue While
                    End If

                    If (row.Length = 0) Then
                        Continue While
                    End If

                    ' set up a new format object
                    Dim info As New FormatInfo()
                    info.FormatName = row(0)

                    ' load all of the properties
                    For i As Integer = 1 To (row.Length - 1) Step 2
                        info.Properties.Add(row(i), row(i + 1))
                    Next

                    ' add to the collection
                    formatCollection.Add(info)

                    ' read the next line
                    line = reader.ReadLine()

                End While

            End Using

        Catch ex As Exception

        End Try

        Return formatCollection

    End Function

End Class
