﻿Imports System.Security.Cryptography
Imports System.IO

Public NotInheritable Class SecurityLogic
    Private Sub New()
    End Sub

    Private Shared ReadOnly ENCRYPTIONKEY As String = "&%#@?,:*"

    Public Shared Function Encrypt(ByVal strText As String) As String
        Try
            'throw new Exception("The method or operation is not implemented.");
            Dim byKey As Byte() = New Byte(19) {}
            Dim dv As Byte() = {&H12, &H34, &H56, &H78, &H90, &HAB, _
             &HCD, &HEF}

            byKey = System.Text.Encoding.UTF8.GetBytes(ENCRYPTIONKEY.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputArray As Byte() = System.Text.Encoding.UTF8.GetBytes(strText)

            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(byKey, dv), CryptoStreamMode.Write)
            cs.Write(inputArray, 0, inputArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Shared Function Decrypt(ByVal strText As String) As String
        Try
            Dim bKey As Byte() = New Byte(19) {}
            Dim IV As Byte() = {&H12, &H34, &H56, &H78, &H90, &HAB, _
             &HCD, &HEF}

            bKey = System.Text.Encoding.UTF8.GetBytes(ENCRYPTIONKEY.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray As [Byte]() = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream()

            Dim cs As New CryptoStream(ms, des.CreateDecryptor(bKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class
