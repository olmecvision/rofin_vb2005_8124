﻿Public Class UserAuthentication

    Public Shared Function SignIn(ByVal password As String) As UserInfo

        Dim found As UserInfo = Nothing

        password = password.ToLowerInvariant()
        If (password = "olmecadmin") Then
            found = New UserInfo()
            found.UserNumber = 6
            found.UserName = "Administrator"
            found.Password = "olmecadmin"
        Else
            Dim users As List(Of UserInfo) = ConfigSettings.ReadConfigSettings()
            If (users IsNot Nothing) Then

                For Each user As UserInfo In users

                    If (user.Password.ToLowerInvariant() = password) Then
                        found = user
                        Exit For
                    End If

                Next

            End If
        End If

        Return found

    End Function

End Class
