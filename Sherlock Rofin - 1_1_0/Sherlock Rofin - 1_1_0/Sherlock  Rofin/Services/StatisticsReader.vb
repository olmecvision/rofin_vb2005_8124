﻿Imports System.IO

Public Class StatisticsReader

    Public Shared Function ReadStatistics(ByVal filePath As String, allHeads As List(Of HeadInfo)) As Dictionary(Of String, StatisticInfo)

        Dim collection As New Dictionary(Of String, StatisticInfo)()

        Try
            Using reader As New StreamReader(filePath)

                Dim line As String
                Dim row() As String

                line = reader.ReadLine()

                While (Not String.IsNullOrEmpty(line))

                    ' get the row elements
                    row = line.Split(",")

                    If (row Is Nothing) Then
                        Continue While
                    End If

                    If (row.Length = 0) Then
                        Continue While
                    End If

                    Dim laserNumber As Integer = CType(row(6), Integer)
                    Dim headNumber As Integer = CType(row(7), Integer)
                    Dim index As Integer = 0

                    For Each head As HeadInfo In allHeads

                        If ((laserNumber = 0 And headNumber = 0) Or (laserNumber = head.LaserId And headNumber = head.HeadId)) Then

                            ' set up a new object
                            Dim info As New StatisticInfo()
                            info.StatisticId = String.Format("{0}_{1}_{2}", head.LaserId, head.HeadId, row(0))

                            ' format the label
                            'If (cameraNumber > 0) Then
                            info.StatisticName = row(1)
                            'Else
                            'info.StatisticName = String.Format("{0} - Cam {1}", row(1), i)
                            'End If

                            'data type
                            Dim strDataType = String.Format("I_VAR_{0}", row(2).ToUpperInvariant())
                            info.DataType = DirectCast([Enum].Parse(GetType(IpeEngCtrlLib.I_VAR_TYPE), strDataType), IpeEngCtrlLib.I_VAR_TYPE)

                            'display
                            info.Display = CType(row(3), Boolean)
                            info.DisplayOnShiftReport = CType(row(4), Boolean)
                            info.DisplayOnBatchReport = CType(row(5), Boolean)
                            info.LaserId = head.LaserId
                            info.HeadId = head.HeadId
                            info.AllHeads = (laserNumber = 0 And headNumber = 0)
                            info.IsFirst = (index = 0)


                            info.Fn = IIf((row(8) = "NONE"), String.Empty, row(7))

                            ' add to the collection
                            collection.Add(info.StatisticId, info)

                            index = index + 1
                        End If

                    Next

                    ' read the next line
                    line = reader.ReadLine()

                End While

            End Using

        Catch ex As Exception
            MessageBox.Show(String.Format("Failed reading statistics configuration file.{0}{1}", vbCrLf + vbCrLf, ex.Message))
        End Try

        Return collection

    End Function

    Public Shared Function GetStatistic(ByRef collection As Dictionary(Of String, StatisticInfo), ByVal statisticId As String) As StatisticInfo

        If (collection Is Nothing) Then
            Return Nothing
        End If

        If (Not collection.ContainsKey(statisticId)) Then
            Return Nothing
        End If

        Return collection(statisticId)

    End Function

    Public Shared Function GetStatisticValue(ByRef stat As StatisticInfo) As String

        If (stat Is Nothing) Then
            Return String.Empty
        End If

        If (stat.Value Is Nothing) Then
            Return String.Empty
        End If

        If ((TypeOf stat.Value Is Double) Or (TypeOf stat.Value Is Decimal)) Then
            Return CType(stat.Value, Decimal).ToString("F2")
        Else
            Return stat.Value.ToString()
        End If

    End Function

    'todo: this
    Public Shared Function GetStatisticTotalValue(ByRef statsCollection As Dictionary(Of String, StatisticInfo), ByVal stat As String) As String

        If (statsCollection Is Nothing) Then
            Return String.Empty
        End If

        If (String.IsNullOrEmpty(stat)) Then
            Return String.Empty
        End If

        Dim statPart = stat.Remove(0, 4)
        Dim totalValue As Decimal = 0

        ' get all the stats
        For Each item As KeyValuePair(Of String, StatisticInfo) In statsCollection

            If (item.Key.EndsWith(statPart)) Then

                If ((TypeOf item.Value.Value Is Double) Or (TypeOf item.Value.Value Is Decimal) Or (TypeOf item.Value.Value Is Integer)) Then
                    totalValue += CType(item.Value.Value, Decimal)
                End If
            End If

        Next

        Return totalValue.ToString()

    End Function


    Public Shared Sub UpdateValues(ByRef collection As Dictionary(Of String, StatisticInfo), ByRef mainForm As MainForm)

        If (collection Is Nothing) Then
            Return
        End If

        If (mainForm Is Nothing) Then
            Return
        End If

        ' update all items
        For Each item As StatisticInfo In collection.Values()

            ' get the correct engine
            Dim head As HeadInfo = mainForm.GetHead(item.LaserId, item.HeadId)
            If (head IsNot Nothing) Then

                Dim sherlockEngine As IpeEngCtrlLib.Engine = head.Engine

                ' check if the item exists in sherlock
                Dim variableName As String = item.StatisticId.Remove(0, 4)
                item.Value = SherlockReader.ReadProperty(sherlockEngine, variableName, item.DataType)

            End If

        Next

    End Sub

End Class
