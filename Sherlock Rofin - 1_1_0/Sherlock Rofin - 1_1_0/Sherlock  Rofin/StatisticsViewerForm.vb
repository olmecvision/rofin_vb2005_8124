﻿
Imports System.IO
Public Class StatisticsViewerForm

    Dim m_MainScreen As MainForm

    Private Property lbl22 As Label

    Private Property lblValue22 As Label

    Private Property lbl23 As Label

    Private Property lblValue23 As Label

    Private Property lbl24 As Label

    Private Property lblValue24 As Label

    Private Property lbl25 As Label

    Private Property lblValue25 As Label

    Private Sub StatisticsViewerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        UpdateScreen()

    End Sub

    Public Sub UpdateScreen()

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        If (m_MainScreen.Statistics Is Nothing) Then
            Return
        End If

        ' build a collection of the label controls
        Dim labelCtrls As List(Of Label) = GetLabelControlsCollection()
        Dim valueCtrls As List(Of Label) = GetValueControlsCollection()

        Dim iIndex As Integer

        ' first hide all of the controls
        For iIndex = 0 To labelCtrls.Count - 1
            labelCtrls.Item(iIndex).Visible = False
            valueCtrls.Item(iIndex).Visible = False
        Next

        ' populate the controls
        Dim enumerator As IEnumerator = m_MainScreen.Statistics.GetEnumerator()

        iIndex = 0
        While (enumerator.MoveNext())

            ' get the item
            Dim item As StatisticInfo = CType(enumerator.Current, KeyValuePair(Of String, StatisticInfo)).Value

            If (Not item.Display) Then
                Continue While
            End If

            If (iIndex >= labelCtrls.Count) Then
                Continue While
            End If

            If (Not String.IsNullOrEmpty(item.Fn)) Then
                If (Not item.IsFirst) Then
                    Continue While
                End If
            End If

            'set the statistic label
            labelCtrls.Item(iIndex).Text = item.StatisticName
            If (String.IsNullOrEmpty(item.Fn) And item.AllHeads) Then
                labelCtrls.Item(iIndex).Text = labelCtrls.Item(iIndex).Text + String.Format(" - L{0} - H{1} ", item.LaserId, item.HeadId)
            End If

            labelCtrls.Item(iIndex).Visible = True

            ' set the statistic value
            Dim statisticValue As String = String.Empty
            Dim stats = m_MainScreen.Statistics

            Select Case item.Fn

                Case "TOTAL"
                    statisticValue = StatisticsReader.GetStatisticTotalValue(stats, item.StatisticId)

                Case Else
                    statisticValue = StatisticsReader.GetStatisticValue(item)

            End Select


            'If (item.Value IsNot Nothing) Then

            '    If (item.CameraNumber = 1) Then
            '        Dim stats = m_MainScreen.Statistics
            '        statisticValue = StatisticsReader.GetStatisticAggregateValue(stats, item.StatisticId)
            '    Else
            '        statisticValue = StatisticsReader.GetStatisticValue(item)
            '    End If
            'End If

            valueCtrls.Item(iIndex).Text = statisticValue
            valueCtrls.Item(iIndex).Visible = True

            ' increment the index
            iIndex = iIndex + 1

        End While

    End Sub

    Private Function GetLabelControlsCollection() As List(Of Label)

        ' build a collection of the label controls
        Dim ctrls As List(Of Label) = New List(Of Label)()
        ctrls.Add(lbl1)
        ctrls.Add(lbl2)
        ctrls.Add(lbl3)
        ctrls.Add(lbl4)
        ctrls.Add(lbl5)
        ctrls.Add(lbl6)
        ctrls.Add(lbl7)
        ctrls.Add(lbl8)
        ctrls.Add(lbl9)
        ctrls.Add(lbl10)
        ctrls.Add(lbl11)
        ctrls.Add(lbl12)
        ctrls.Add(lbl13)
        ctrls.Add(lbl14)
        ctrls.Add(lbl15)
        ctrls.Add(lbl16)
        ctrls.Add(lbl17)
        ctrls.Add(lbl18)


        Return ctrls

    End Function

    Private Function GetValueControlsCollection() As List(Of Label)

        ' build a collection of the label controls
        Dim ctrls As List(Of Label) = New List(Of Label)()
        ctrls.Add(lblValue1)
        ctrls.Add(lblValue2)
        ctrls.Add(lblValue3)
        ctrls.Add(lblValue4)
        ctrls.Add(lblValue5)
        ctrls.Add(lblValue6)
        ctrls.Add(lblValue7)
        ctrls.Add(lblValue8)
        ctrls.Add(lblValue9)
        ctrls.Add(lblValue10)
        ctrls.Add(lblValue11)
        ctrls.Add(lblValue12)
        ctrls.Add(lblValue13)
        ctrls.Add(lblValue14)
        ctrls.Add(lblValue15)
        ctrls.Add(lblValue16)
        ctrls.Add(lblValue17)
        ctrls.Add(lblValue18)


        Return ctrls

    End Function

    Private Function GenerateRandomDigitCode(length As String) As String

        Dim random As Random = New Random()
        Dim str As String = String.Empty

        For i As Integer = 0 To length - 1
            str = String.Concat(str, random.Next(10).ToString())
        Next
        Return str

    End Function

#Region "Button Events"

    Private Sub btnShiftReport_Click(sender As System.Object, e As System.EventArgs) Handles btnShiftReport.Click

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim batchId = "Unknown"
        Dim statsCollection As Dictionary(Of String, StatisticInfo) = m_MainScreen.Statistics()
        Dim stat As StatisticInfo = StatisticsReader.GetStatistic(statsCollection, "1_EXT_100")
        If (stat IsNot Nothing) Then
            batchId = StatisticsReader.GetStatisticValue(stat)
        End If

        Dim fileName As String = String.Format("shift_report_{0}_{1}.pdf", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), batchId)
        Dim filePath As String = String.Format("C:\olmec\shift_report\{0}", fileName)

        ReportWriter.WriteShiftReport(m_MainScreen, filePath)

        'reset the stats
        m_MainScreen.VarSetBool(MainForm.STA_RESET_STATISTICS, True)

        ' show the PDF
        Dim frmPDFViewer As PDFViewerForm

        ' load the configure form
        frmPDFViewer = New PDFViewerForm()
        frmPDFViewer.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmPDFViewer.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        frmPDFViewer.Url = String.Format("file:///{0}", filePath)
        'frmPDFViewer.Url = String.Format("file:///C:\olmec\shift_report\{0}", fileName)

        Cursor = Cursors.Arrow

        frmPDFViewer.ShowDialog()

    End Sub

    Private Sub btnResetStats_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetStats.Click

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        m_MainScreen.VarSetBool(MainForm.STA_RESET_STATISTICS, True)

        Dim result As DialogResult = MessageBox.Show(String.Format("Would you like to remove all of the saved images?{0}", vbCrLf + vbCrLf), "Vision System", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If (result = Windows.Forms.DialogResult.Yes) Then

            Dim directory As DirectoryInfo = New DirectoryInfo(My.Settings("ImageDirectory"))
            If (directory IsNot Nothing) Then
                If (directory.Exists()) Then

                    For Each file As FileSystemInfo In directory.GetFileSystemInfos(My.Settings("ImageWildcard"))
                        file.Delete()
                    Next
                End If
            End If
        End If

        UpdateScreen()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

#End Region

#Region "Properties"

    Public Property MainScreen() As MainForm
        Get
            Return m_MainScreen
        End Get
        Set(ByVal value As MainForm)
            m_MainScreen = value
        End Set
    End Property

#End Region

End Class
