﻿Imports System.Xml
Imports System.Configuration
Imports System.Reflection

Public Class PDFHelper
    Public Shared Function ReadSetting(ByVal key As String) As String
        Return ConfigurationManager.AppSettings(key)
    End Function

    Public Shared Sub WriteConfigSettings(ByVal users As List(Of UserInfo))

        Dim Doc As New XmlDocument

        'Create a 'vanilla' XML Declaration (processing instruction)
        Dim myXmlDeclaration As XmlDeclaration
        myXmlDeclaration = Doc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
        Doc.AppendChild(myXmlDeclaration)

        '<-Root Created
        Dim Root As XmlElement
        Root = Doc.CreateElement("Users")

        For Each user As UserInfo In users

            If (user IsNot Nothing) Then

                'Create a user child node
                Dim userNode As XmlElement
                userNode = Doc.CreateElement("User")

                'Create a UserNumber attribute
                Dim userNumberAttribute As XmlAttribute = Doc.CreateAttribute("UserNumber")
                userNumberAttribute.Value = user.UserNumber
                userNode.Attributes.Append(userNumberAttribute)

                'Create a UserName attribute
                Dim userNameAttribute As XmlAttribute = Doc.CreateAttribute("UserName")
                userNameAttribute.Value = user.UserName
                userNode.Attributes.Append(userNameAttribute)

                'Create a Password attribute
                Dim passwordAttribute As XmlAttribute = Doc.CreateAttribute("Password")
                passwordAttribute.Value = user.Password
                userNode.Attributes.Append(passwordAttribute)

                'userNode.Attributes.Append(New XmlAttribute())
                Root.AppendChild(userNode)

            End If

        Next

        'add root to doc
        Doc.AppendChild(Root)

        'Write XML to a file
        Dim settingsFilePath As String = My.Settings("ApplicationSettingsFile")
        Dim Output As New XmlTextWriter(settingsFilePath, System.Text.Encoding.UTF8)
        Doc.WriteTo(Output)

        Output.Close()

    End Sub

    Public Shared Function ReadConfigSettings() As List(Of UserInfo)

        ' build a collection of user objects
        Dim users As New List(Of UserInfo)
        Dim Doc As New XmlDocument

        'Create a 'vanilla' XML Declaration (processing instruction)
        Dim settingsFilePath As String = My.Settings("ApplicationSettingsFile")
        Doc.Load(settingsFilePath)

        ' get the users
        Dim usersNodes As XmlNodeList = Doc.SelectNodes("/Users/User")
        If (usersNodes IsNot Nothing) Then

            For Each user As XmlNode In usersNodes
                Dim newUser As UserInfo = New UserInfo()

                ' User Number
                Dim userNumber As Integer = 0
                If (Integer.TryParse(user.Attributes("UserNumber").Value, userNumber)) Then
                    newUser.UserNumber = userNumber
                End If

                ' User Name
                newUser.UserName = user.Attributes("UserName").Value

                ' Password
                newUser.Password = SecurityLogic.Decrypt(user.Attributes("Password").Value)

                ' add to the collection
                users.Add(newUser)
            Next

        End If

        Return users

    End Function
End Class
