﻿Public Class UserInfo

    Public Property UserNumber() As Integer
        Get
            Return m_UserNumber
        End Get
        Set(ByVal value As Integer)
            m_UserNumber = value
        End Set
    End Property
    Private m_UserNumber As Integer

    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(ByVal value As String)
            m_UserName = value
        End Set
    End Property
    Private m_UserName As String
    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(ByVal value As String)
            m_Password = value
        End Set
    End Property
    Private m_Password As String

End Class
