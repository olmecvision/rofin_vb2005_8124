﻿Public Class CameraInfo

    Public Property CameraId() As String
        Get
            Return m_CameraId
        End Get
        Set(ByVal value As String)
            m_CameraId = value
        End Set
    End Property
    Private m_CameraId As String

    Public Property CameraName() As String
        Get
            Return m_CameraName
        End Get
        Set(ByVal value As String)
            m_CameraName = value
        End Set
    End Property
    Private m_CameraName As String

End Class
