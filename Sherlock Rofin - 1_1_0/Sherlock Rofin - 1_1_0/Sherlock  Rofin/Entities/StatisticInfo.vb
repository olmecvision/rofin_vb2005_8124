﻿Public Class StatisticInfo

    Public Property StatisticId() As String
        Get
            Return m_StatisticId
        End Get
        Set(ByVal value As String)
            m_StatisticId = value
        End Set
    End Property
    Private m_StatisticId As String

    Public Property StatisticName() As String
        Get
            Return m_StatisticName
        End Get
        Set(ByVal value As String)
            m_StatisticName = value
        End Set
    End Property
    Private m_StatisticName As String

    Public Property DataType() As IpeEngCtrlLib.I_VAR_TYPE
        Get
            Return m_DataType
        End Get
        Set(ByVal value As IpeEngCtrlLib.I_VAR_TYPE)
            m_DataType = value
        End Set
    End Property
    Private m_DataType As IpeEngCtrlLib.I_VAR_TYPE

    Public Property Value() As Object
        Get
            Return m_Value
        End Get
        Set(ByVal value As Object)
            m_Value = value
        End Set
    End Property
    Private m_Value As Object

    Public Property Display() As Boolean
        Get
            Return m_Display
        End Get
        Set(ByVal value As Boolean)
            m_Display = value
        End Set
    End Property
    Private m_Display As Boolean

    Public Property DisplayOnShiftReport() As Boolean
        Get
            Return m_DisplayOnShiftReport
        End Get
        Set(ByVal value As Boolean)
            m_DisplayOnShiftReport = value
        End Set
    End Property
    Private m_DisplayOnShiftReport As Boolean

    Public Property DisplayOnBatchReport() As Boolean
        Get
            Return m_DisplayOnBatchReport
        End Get
        Set(ByVal value As Boolean)
            m_DisplayOnBatchReport = value
        End Set
    End Property
    Private m_DisplayOnBatchReport As Boolean

    Public Property LaserId() As String
        Get
            Return m_LaserId
        End Get
        Set(ByVal value As String)
            m_LaserId = value
        End Set
    End Property
    Private m_LaserId As String

    Public Property HeadId() As String
        Get
            Return m_HeadId
        End Get
        Set(ByVal value As String)
            m_HeadId = value
        End Set
    End Property
    Private m_HeadId As String

    Public Property AllHeads() As String
        Get
            Return m_AllHeads
        End Get
        Set(ByVal value As String)
            m_AllHeads = value
        End Set
    End Property
    Private m_AllHeads As String

    Public Property Fn() As String
        Get
            Return m_Fn
        End Get
        Set(ByVal value As String)
            m_Fn = value
        End Set
    End Property
    Private m_Fn As String

    Public Property IsFirst() As Boolean
        Get
            Return m_IsFirst
        End Get
        Set(ByVal value As Boolean)
            m_IsFirst = value
        End Set
    End Property
    Private m_IsFirst As Boolean

End Class
