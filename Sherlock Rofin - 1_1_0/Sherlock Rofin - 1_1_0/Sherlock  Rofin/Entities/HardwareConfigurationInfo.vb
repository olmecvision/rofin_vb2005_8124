﻿Public Class HardwareConfigurationInfo

    Public Property LaserId() As Integer
        Get
            Return m_LaserId
        End Get
        Set(ByVal value As Integer)
            m_LaserId = value
        End Set
    End Property
    Private m_LaserId As Integer

    Public Property LaserName() As String
        Get
            Return m_LaserName
        End Get
        Set(ByVal value As String)
            m_LaserName = value
        End Set
    End Property
    Private m_LaserName As String

    Public Property HeadId() As Integer
        Get
            Return m_HeadId
        End Get
        Set(ByVal value As Integer)
            m_HeadId = value
        End Set
    End Property
    Private m_HeadId As Integer

    Public Property HeadName() As String
        Get
            Return m_HeadName
        End Get
        Set(ByVal value As String)
            m_HeadName = value
        End Set
    End Property
    Private m_HeadName As String

    Public Property InstanceType() As String
        Get
            Return m_InstanceType
        End Get
        Set(ByVal value As String)
            m_InstanceType = value
        End Set
    End Property
    Private m_InstanceType As String

    Public Property IpAddress() As String
        Get
            Return m_IpAddress
        End Get
        Set(ByVal value As String)
            m_IpAddress = value
        End Set
    End Property
    Private m_IpAddress As String

    Public Property Port() As Integer
        Get
            Return m_Port
        End Get
        Set(ByVal value As Integer)
            m_Port = value
        End Set
    End Property
    Private m_Port As Integer

    Public Property DriverFileName() As String
        Get
            Return m_DriverFileName
        End Get
        Set(ByVal value As String)
            m_DriverFileName = value
        End Set
    End Property
    Private m_DriverFileName As String

End Class
