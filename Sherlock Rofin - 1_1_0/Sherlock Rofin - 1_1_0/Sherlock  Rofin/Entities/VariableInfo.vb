﻿Public Class VariableInfo

    Public Property VariableId() As String
        Get
            Return m_VariableId
        End Get
        Set(ByVal value As String)
            m_VariableId = value
        End Set
    End Property
    Private m_VariableId As String

    Public Property VariableName() As String
        Get
            Return m_VariableName
        End Get
        Set(ByVal value As String)
            m_VariableName = value
        End Set
    End Property
    Private m_VariableName As String

    Public Property MinimumValue() As Integer
        Get
            Return m_MinimumValue
        End Get
        Set(ByVal value As Integer)
            m_MinimumValue = value
        End Set
    End Property
    Private m_MinimumValue As Integer

    Public Property MaximumValue() As Integer
        Get
            Return m_MaximumValue
        End Get
        Set(ByVal value As Integer)
            m_MaximumValue = value
        End Set
    End Property
    Private m_MaximumValue As Integer

    Public Property DataType() As IpeEngCtrlLib.I_VAR_TYPE
        Get
            Return m_DataType
        End Get
        Set(ByVal value As IpeEngCtrlLib.I_VAR_TYPE)
            m_DataType = value
        End Set
    End Property
    Private m_DataType As IpeEngCtrlLib.I_VAR_TYPE

    Public Property Value() As Object
        Get
            Return m_Value
        End Get
        Set(ByVal value As Object)
            m_Value = value
        End Set
    End Property
    Private m_Value As Object

End Class
