﻿Public Class LaserInfo

    Public Property LaserId() As String
        Get
            Return m_LaserId
        End Get
        Set(ByVal value As String)
            m_LaserId = value
        End Set
    End Property
    Private m_LaserId As String

    Public Property LaserName() As String
        Get
            Return m_LaserName
        End Get
        Set(ByVal value As String)
            m_LaserName = value
        End Set
    End Property
    Private m_LaserName As String

    Public Property Heads() As List(Of HeadInfo)
        Get
            Return m_Heads
        End Get
        Set(ByVal value As List(Of HeadInfo))
            m_Heads = value
        End Set
    End Property
    Private m_Heads As List(Of HeadInfo)

End Class