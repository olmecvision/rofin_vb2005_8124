﻿Public Class FormatInfo

    Public Property FormatCode1() As String
        Get
            Return m_FormatCode1
        End Get
        Set(ByVal value As String)
            m_FormatCode1 = value
        End Set
    End Property
    Private m_FormatCode1 As String

    Public Property FormatCode2() As String
        Get
            Return m_FormatCode2
        End Get
        Set(ByVal value As String)
            m_FormatCode2 = value
        End Set
    End Property
    Private m_FormatCode2 As String

    Public Property FormatDescription() As String
        Get
            Return m_FormatDescription
        End Get
        Set(ByVal value As String)
            m_FormatDescription = value
        End Set
    End Property
    Private m_FormatDescription As String

    Public Property FormatFile() As String
        Get
            Return m_FormatFile
        End Get
        Set(ByVal value As String)
            m_FormatFile = value
        End Set
    End Property
    Private m_FormatFile As String

End Class
