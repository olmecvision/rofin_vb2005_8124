Visual Basic 2005/Sherlock 7 demonstration project

This project shows how to 
- Create a Sherlock 7 object in a Visual Basic 2005 project
- Load an investigation
- Connect a Sherlock 7 image window to an instance of the Ipe Display Control
- Run the investigation 
- Read and write Sherlock variables from the Visual Basic front end

You should first run the Sherlock investigation (Simple1.ivs) by itself to make sure that it can acquire from your camera.  If you do not have a camera, change the image source for imgA to either File to load a static image, or Sequence to select a sequence of monochrome images.  (The images in <Sherlock 7>\Images\Sequence are color, and will not work with this investigation.)

You can either run the Visual Basic project VB2005 Sherlock7.vbproj from within the Visual Basic 2005 IDE, or run the compiled program VB2005 Sherlock7.exe from the \bin\release directory.  The executable loads Simple1.ivs from the same directory as VB2005 Sherlock7.exe.

Execute the investigation from the Visual Basic form by clicking either the Run Once or Run Continuously button.  The investigation performs thresholding and connectivity ("blob analysis") on the pixels inside RectA.  At the end of each pass through the investigation, the number of blobs found and the areas of the blobs are displayed on the Visual Basic form.

You can control how the Connectivity algorithm operates by changing the level for the threshold preprocessor, the maximum number of blobs to measure, and the minimum and maximum areas of the blobs.  You can also select whether black or white blobs are measured, and whether the blobs are labeled (numbered).  

When you terminate Visual Basic application execution, the current state of the control settings are saved to Simple1.ivs if the "Save settings" checkbox is checked.
