﻿Public Class FormBase
    Private _textBoxControls As List(Of Control) = New List(Of Control)()
    Private _txtCurrentControl As Control


    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If (Not DesignMode) Then
            Dim logoFile = My.Settings("LogoPath") + "\\screen-logo.png"
            PictureBox2.Load(logoFile)

            'find instances of the custom text boxes
            GetAllTextBoxControls(Me)

            'hook a popup keyboard to the controls
            WirePopupKeyboard()
        End If

    End Sub

    Private Sub GetAllTextBoxControls(ByVal container As Control)
        For Each c As Control In container.Controls
            GetAllTextBoxControls(c)

            If ((TypeOf c Is TextBox) Or (TypeOf c Is NumericUpDown)) Then
                _textBoxControls.Add(c)
            End If

        Next
    End Sub

    Private Sub WirePopupKeyboard()
        If (_textBoxControls Is Nothing Or _textBoxControls.Count = 0) Then
            Return
        End If

        For Each item As Control In _textBoxControls
            AddHandler item.GotFocus, AddressOf txt_GotFocus
            AddHandler item.LostFocus, AddressOf txt_LostFocus
        Next
    End Sub

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As EventArgs)

        _txtCurrentControl = TryCast(sender, Control)
        If _txtCurrentControl IsNot Nothing Then

            pnlKeyboard.Show()
            pnlKeyboard.BringToFront()

        End If

    End Sub

    Private Sub txt_LostFocus(ByVal sender As Object, ByVal e As EventArgs)

        'pnlKeyboard.Hide()

    End Sub

    Private Sub KeyboardControl1_UserKeyPressed(ByVal sender As System.Object, ByVal e As VB2005_Sherlock7.KeyboardControl.KeyboardEventArgs) Handles KeyboardControl1.UserKeyPressed

        If _txtCurrentControl IsNot Nothing Then

            _txtCurrentControl.Focus()
            SendKeys.Send(e.KeyboardKeyPressed)

        End If

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        pnlKeyboard.Hide()
    End Sub
End Class
