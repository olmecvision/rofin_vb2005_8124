﻿Imports System.Drawing.Drawing2D

Public Class GradientPanel

    Inherits Windows.Forms.Panel

    Public Sub New()

        MyBase.New()


        'Set the appropriate styles on the panel
        SetStyle(ControlStyles.DoubleBuffer, True)
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)

    End Sub

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)

        'We are going to draw the background with a linear gradient brush with a dark blue color.
        'Dim br As New LinearGradientBrush(Me.ClientRectangle, SystemColors.InactiveCaption, _
        '                                    Color.DarkBlue, LinearGradientMode.ForwardDiagonal)

        Dim br As New LinearGradientBrush(Me.ClientRectangle, m_StartColour, _
                                    m_EndColour, m_GradientMode)

        e.Graphics.FillRectangle(br, Me.ClientRectangle)
        'br.Dispose()

        ' Call the base paint event to draw the rest of the panel contents.
        MyBase.OnPaint(e)

    End Sub

    Public Property StartColour() As Color
        Get
            Return m_StartColour
        End Get
        Set(ByVal value As Color)
            m_StartColour = value
        End Set
    End Property
    Private m_StartColour As Color

    Public Property EndColour() As Color
        Get
            Return m_EndColour
        End Get
        Set(ByVal value As Color)
            m_EndColour = value
        End Set
    End Property
    Private m_EndColour As Color

    Public Property GradientMode() As LinearGradientMode
        Get
            Return m_GradientMode
        End Get
        Set(ByVal value As LinearGradientMode)
            m_GradientMode = value
        End Set
    End Property
    Private m_GradientMode As LinearGradientMode

End Class
