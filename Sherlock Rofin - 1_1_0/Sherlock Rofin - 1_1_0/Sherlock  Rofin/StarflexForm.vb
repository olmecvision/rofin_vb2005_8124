﻿Public Class StarflexForm
    Private _simulation As Boolean

    Public Event SimulationChanged()

    Public Property Simulation() As Boolean
        Get
            Return _simulation
        End Get
        Set(value As Boolean)
            If value <> _simulation Then
                _simulation = value
                chkSimulationMode.Checked = _simulation
                RaiseEvent SimulationChanged()
            End If
        End Set
    End Property

    Private Sub IndexChanged(sender As System.Object, e As System.EventArgs) Handles cbState.SelectedIndexChanged, cbMessage.SelectedIndexChanged
        btnSend.Enabled = cbMessage.SelectedIndex > -1 And cbState.SelectedIndex > -1
    End Sub

    Private Const MaximumMessagesInList As Integer = 500

    Public Sub AddData(ByVal txData As Boolean, ByVal message As String)
        Dim item As ListViewItem = New ListViewItem(DateTime.Now.ToString())
        item.SubItems.Add(message)
        If txData Then
            item.BackColor = Color.Red
        Else
            item.BackColor = Color.Green
        End If
        If lvMessages.Items.Count = 0 Then
            lvMessages.Items.Add(item)
        Else
            lvMessages.Items.Insert(0, item)
        End If
        If lvMessages.Items.Count > MaximumMessagesInList Then
            lvMessages.Items.RemoveAt(lvMessages.Items.Count - 1)
        End If
    End Sub

    Private Sub btnSend_Click(sender As System.Object, e As System.EventArgs) Handles btnSend.Click
        If cbMessage.Text = "Camera Status" Then
            Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraStatus(Convert.ToUInt32(numLaserUnitId.Value), Convert.ToUInt32(numHead.Value), cbState.Text.Equals("OK"))
        ElseIf cbMessage.Text = "Holes" Then
            Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleStatus(Convert.ToUInt32(numLaserUnitId.Value), Convert.ToUInt32(numHead.Value), cbState.Text.Equals("OK"))
        ElseIf cbMessage.Text = "Holes Diameter" Then
            Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleDiameterStatus(Convert.ToUInt32(numLaserUnitId.Value), Convert.ToUInt32(numHead.Value), cbState.Text.Equals("OK"))
        ElseIf cbMessage.Text = "Ready Status" Then
            Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraReady(Convert.ToUInt32(numLaserUnitId.Value), Convert.ToUInt32(numHead.Value), cbState.Text.Equals("OK"))
        End If
    End Sub

End Class