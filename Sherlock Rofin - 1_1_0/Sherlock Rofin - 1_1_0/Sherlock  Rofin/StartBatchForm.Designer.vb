﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StartBatchForm
    Inherits VB2005_Sherlock7.FormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GradientPanel3 = New VB2005_Sherlock7.GradientPanel()
        Me.lblValue6 = New System.Windows.Forms.Label()
        Me.hCamera6 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera6 = New System.Windows.Forms.Label()
        Me.lblValue5 = New System.Windows.Forms.Label()
        Me.hCamera5 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera5 = New System.Windows.Forms.Label()
        Me.lblValue4 = New System.Windows.Forms.Label()
        Me.lblValue3 = New System.Windows.Forms.Label()
        Me.lblValue2 = New System.Windows.Forms.Label()
        Me.lblValue1 = New System.Windows.Forms.Label()
        Me.txtHoleDiameter = New System.Windows.Forms.TextBox()
        Me.txtHoleDiameter2 = New System.Windows.Forms.TextBox()
        Me.txtReelId = New System.Windows.Forms.TextBox()
        Me.txtPrintReference = New System.Windows.Forms.TextBox()
        Me.txtWorksOrder = New System.Windows.Forms.TextBox()
        Me.hCamera4 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera4 = New System.Windows.Forms.Label()
        Me.hCamera3 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera3 = New System.Windows.Forms.Label()
        Me.hCamera2 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera2 = New System.Windows.Forms.Label()
        Me.hCamera1 = New System.Windows.Forms.HScrollBar()
        Me.lblCamera1 = New System.Windows.Forms.Label()
        Me.lbl5 = New System.Windows.Forms.Label()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.pnlKeyboard.SuspendLayout()
        Me.GradientPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GradientPanel3
        '
        Me.GradientPanel3.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel3.Controls.Add(Me.lblValue6)
        Me.GradientPanel3.Controls.Add(Me.hCamera6)
        Me.GradientPanel3.Controls.Add(Me.lblCamera6)
        Me.GradientPanel3.Controls.Add(Me.lblValue5)
        Me.GradientPanel3.Controls.Add(Me.hCamera5)
        Me.GradientPanel3.Controls.Add(Me.lblCamera5)
        Me.GradientPanel3.Controls.Add(Me.lblValue4)
        Me.GradientPanel3.Controls.Add(Me.lblValue3)
        Me.GradientPanel3.Controls.Add(Me.lblValue2)
        Me.GradientPanel3.Controls.Add(Me.lblValue1)
        Me.GradientPanel3.Controls.Add(Me.txtHoleDiameter)
        Me.GradientPanel3.Controls.Add(Me.txtHoleDiameter2)
        Me.GradientPanel3.Controls.Add(Me.txtReelId)
        Me.GradientPanel3.Controls.Add(Me.txtPrintReference)
        Me.GradientPanel3.Controls.Add(Me.txtWorksOrder)
        Me.GradientPanel3.Controls.Add(Me.hCamera4)
        Me.GradientPanel3.Controls.Add(Me.lblCamera4)
        Me.GradientPanel3.Controls.Add(Me.hCamera3)
        Me.GradientPanel3.Controls.Add(Me.lblCamera3)
        Me.GradientPanel3.Controls.Add(Me.hCamera2)
        Me.GradientPanel3.Controls.Add(Me.lblCamera2)
        Me.GradientPanel3.Controls.Add(Me.hCamera1)
        Me.GradientPanel3.Controls.Add(Me.lblCamera1)
        Me.GradientPanel3.Controls.Add(Me.lbl5)
        Me.GradientPanel3.Controls.Add(Me.lbl4)
        Me.GradientPanel3.Controls.Add(Me.lbl3)
        Me.GradientPanel3.Controls.Add(Me.lbl2)
        Me.GradientPanel3.Controls.Add(Me.lbl1)
        Me.GradientPanel3.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel3.Location = New System.Drawing.Point(0, 8)
        Me.GradientPanel3.Name = "GradientPanel3"
        Me.GradientPanel3.Size = New System.Drawing.Size(712, 442)
        Me.GradientPanel3.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel3.TabIndex = 14
        '
        'lblValue6
        '
        If False Then
            Me.lblValue6.BackColor = System.Drawing.Color.Transparent
            Me.lblValue6.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue6.ForeColor = System.Drawing.Color.White
            Me.lblValue6.Location = New System.Drawing.Point(486, 346)
            Me.lblValue6.Name = "lblValue6"
            Me.lblValue6.Size = New System.Drawing.Size(71, 36)
            Me.lblValue6.TabIndex = 82
            Me.lblValue6.Text = "Yes"
            Me.lblValue6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.lblValue6.Visible = False
            '
            'hCamera6
            '
            Me.hCamera6.LargeChange = 1
            Me.hCamera6.Location = New System.Drawing.Point(305, 346)
            Me.hCamera6.Maximum = 1
            Me.hCamera6.Name = "hCamera6"
            Me.hCamera6.Size = New System.Drawing.Size(165, 36)
            Me.hCamera6.TabIndex = 81
            Me.hCamera6.Value = 1
            '
            'lblCamera6
            '
            Me.lblCamera6.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera6.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera6.ForeColor = System.Drawing.Color.White
            Me.lblCamera6.Location = New System.Drawing.Point(19, 346)
            Me.lblCamera6.Name = "lblCamera6"
            Me.lblCamera6.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera6.TabIndex = 80
            Me.lblCamera6.Text = "Camera 6 Enable"
            Me.lblCamera6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.lblCamera6.Visible = False
            '
            'lblValue5
            '
            Me.lblValue5.BackColor = System.Drawing.Color.Transparent
            Me.lblValue5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue5.ForeColor = System.Drawing.Color.White
            Me.lblValue5.Location = New System.Drawing.Point(486, 309)
            Me.lblValue5.Name = "lblValue5"
            Me.lblValue5.Size = New System.Drawing.Size(71, 36)
            Me.lblValue5.TabIndex = 79
            Me.lblValue5.Text = "Yes"
            Me.lblValue5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.lblValue5.Visible = False
            '
            'hCamera5
            '
            Me.hCamera5.LargeChange = 1
            Me.hCamera5.Location = New System.Drawing.Point(305, 309)
            Me.hCamera5.Maximum = 1
            Me.hCamera5.Name = "hCamera5"
            Me.hCamera5.Size = New System.Drawing.Size(165, 36)
            Me.hCamera5.TabIndex = 78
            Me.hCamera5.Value = 1
            '
            'lblCamera5
            '
            Me.lblCamera5.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera5.ForeColor = System.Drawing.Color.White
            Me.lblCamera5.Location = New System.Drawing.Point(19, 309)
            Me.lblCamera5.Name = "lblCamera5"
            Me.lblCamera5.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera5.TabIndex = 77
            Me.lblCamera5.Text = "Camera 5 Enable"
            Me.lblCamera5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.lblCamera5.Visible = False
            '
            'lblValue4
            '
            Me.lblValue4.BackColor = System.Drawing.Color.Transparent
            Me.lblValue4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue4.ForeColor = System.Drawing.Color.White
            Me.lblValue4.Location = New System.Drawing.Point(486, 272)
            Me.lblValue4.Name = "lblValue4"
            Me.lblValue4.Size = New System.Drawing.Size(71, 36)
            Me.lblValue4.TabIndex = 76
            Me.lblValue4.Text = "Yes"
            Me.lblValue4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.lblValue4.Visible = False
            '
            'lblValue3
            '
            Me.lblValue3.BackColor = System.Drawing.Color.Transparent
            Me.lblValue3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue3.ForeColor = System.Drawing.Color.White
            Me.lblValue3.Location = New System.Drawing.Point(486, 235)
            Me.lblValue3.Name = "lblValue3"
            Me.lblValue3.Size = New System.Drawing.Size(71, 36)
            Me.lblValue3.TabIndex = 75
            Me.lblValue3.Text = "Yes"
            Me.lblValue3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.lblValue3.Visible = False
            '
            'lblValue2
            '
            Me.lblValue2.BackColor = System.Drawing.Color.Transparent
            Me.lblValue2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue2.ForeColor = System.Drawing.Color.White
            Me.lblValue2.Location = New System.Drawing.Point(486, 198)
            Me.lblValue2.Name = "lblValue2"
            Me.lblValue2.Size = New System.Drawing.Size(71, 36)
            Me.lblValue2.TabIndex = 74
            Me.lblValue2.Text = "Yes"
            Me.lblValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'lblValue1
            '
            Me.lblValue1.BackColor = System.Drawing.Color.Transparent
            Me.lblValue1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblValue1.ForeColor = System.Drawing.Color.White
            Me.lblValue1.Location = New System.Drawing.Point(486, 161)
            Me.lblValue1.Name = "lblValue1"
            Me.lblValue1.Size = New System.Drawing.Size(71, 36)
            Me.lblValue1.TabIndex = 73
            Me.lblValue1.Text = "Yes"
            Me.lblValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        End If

        '
        'txtHoleDiameter 1
        '
        Me.txtHoleDiameter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHoleDiameter.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHoleDiameter.Location = New System.Drawing.Point(305, 124)
        Me.txtHoleDiameter.Margin = New System.Windows.Forms.Padding(0)
        Me.txtHoleDiameter.MaxLength = 4
        Me.txtHoleDiameter.Name = "txtHoleDiameter"
        Me.txtHoleDiameter.Size = New System.Drawing.Size(100, 35)
        Me.txtHoleDiameter.TabIndex = 72

        'txtHoleDiameter 2
        '
        Me.txtHoleDiameter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHoleDiameter2.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHoleDiameter2.Location = New System.Drawing.Point(305, 160)
        Me.txtHoleDiameter2.Margin = New System.Windows.Forms.Padding(0)
        Me.txtHoleDiameter2.MaxLength = 4
        Me.txtHoleDiameter2.Name = "txtHoleDiameter2"
        Me.txtHoleDiameter2.Size = New System.Drawing.Size(100, 35)
        Me.txtHoleDiameter2.TabIndex = 73

        '
        'txtReelId
        '
        Me.txtReelId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtReelId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReelId.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReelId.Location = New System.Drawing.Point(305, 88)
        Me.txtReelId.Margin = New System.Windows.Forms.Padding(0)
        Me.txtReelId.MaxLength = 3
        Me.txtReelId.Name = "txtReelId"
        Me.txtReelId.Size = New System.Drawing.Size(100, 35)
        Me.txtReelId.TabIndex = 71
        '
        'txtPrintReference
        '
        Me.txtPrintReference.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrintReference.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrintReference.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrintReference.Location = New System.Drawing.Point(305, 52)
        Me.txtPrintReference.Margin = New System.Windows.Forms.Padding(0)
        Me.txtPrintReference.MaxLength = 10
        Me.txtPrintReference.Name = "txtPrintReference"
        Me.txtPrintReference.Size = New System.Drawing.Size(272, 35)
        Me.txtPrintReference.TabIndex = 70
        '
        'txtWorksOrder
        '
        Me.txtWorksOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtWorksOrder.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtWorksOrder.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorksOrder.Location = New System.Drawing.Point(305, 16)
        Me.txtWorksOrder.Margin = New System.Windows.Forms.Padding(0)
        Me.txtWorksOrder.MaxLength = 5
        Me.txtWorksOrder.Name = "txtWorksOrder"
        Me.txtWorksOrder.Size = New System.Drawing.Size(156, 35)
        Me.txtWorksOrder.TabIndex = 69
        '
        'hCamera4
        '
        If False Then
            Me.hCamera4.LargeChange = 1
            Me.hCamera4.Location = New System.Drawing.Point(305, 272)
            Me.hCamera4.Maximum = 1
            Me.hCamera4.Name = "hCamera4"
            Me.hCamera4.Size = New System.Drawing.Size(165, 36)
            Me.hCamera4.TabIndex = 29
            Me.hCamera4.Value = 1
            '
            'lblCamera4
            '
            Me.lblCamera4.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera4.ForeColor = System.Drawing.Color.White
            Me.lblCamera4.Location = New System.Drawing.Point(19, 272)
            Me.lblCamera4.Name = "lblCamera4"
            Me.lblCamera4.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera4.TabIndex = 28
            Me.lblCamera4.Text = "Camera 4 Enable"
            Me.lblCamera4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.lblCamera4.Visible = False
            '
            'hCamera3
            '
            Me.hCamera3.LargeChange = 1
            Me.hCamera3.Location = New System.Drawing.Point(305, 235)
            Me.hCamera3.Maximum = 1
            Me.hCamera3.Name = "hCamera3"
            Me.hCamera3.Size = New System.Drawing.Size(165, 36)
            Me.hCamera3.TabIndex = 26
            Me.hCamera3.Value = 1
            '
            'lblCamera3
            '
            Me.lblCamera3.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera3.ForeColor = System.Drawing.Color.White
            Me.lblCamera3.Location = New System.Drawing.Point(19, 235)
            Me.lblCamera3.Name = "lblCamera3"
            Me.lblCamera3.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera3.TabIndex = 25
            Me.lblCamera3.Text = "Camera 3 Enable"
            Me.lblCamera3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.lblCamera3.Visible = False
            '
            'hCamera2
            '
            Me.hCamera2.LargeChange = 1
            Me.hCamera2.Location = New System.Drawing.Point(305, 198)
            Me.hCamera2.Maximum = 1
            Me.hCamera2.Name = "hCamera2"
            Me.hCamera2.Size = New System.Drawing.Size(165, 36)
            Me.hCamera2.TabIndex = 23
            Me.hCamera2.Value = 1
            '
            'lblCamera2
            '
            Me.lblCamera2.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera2.ForeColor = System.Drawing.Color.White
            Me.lblCamera2.Location = New System.Drawing.Point(19, 198)
            Me.lblCamera2.Name = "lblCamera2"
            Me.lblCamera2.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera2.TabIndex = 22
            Me.lblCamera2.Text = "Camera 2 Enable"
            Me.lblCamera2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'hCamera1
            '
            Me.hCamera1.LargeChange = 1
            Me.hCamera1.Location = New System.Drawing.Point(305, 161)
            Me.hCamera1.Maximum = 1
            Me.hCamera1.Name = "hCamera1"
            Me.hCamera1.Size = New System.Drawing.Size(165, 36)
            Me.hCamera1.TabIndex = 20
            Me.hCamera1.Value = 1
            '
            'lblCamera1
            '
            Me.lblCamera1.BackColor = System.Drawing.Color.Transparent
            Me.lblCamera1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
            Me.lblCamera1.ForeColor = System.Drawing.Color.White
            Me.lblCamera1.Location = New System.Drawing.Point(19, 161)
            Me.lblCamera1.Name = "lblCamera1"
            Me.lblCamera1.Size = New System.Drawing.Size(271, 36)
            Me.lblCamera1.TabIndex = 19
            Me.lblCamera1.Text = "Camera 1 Enable"
            Me.lblCamera1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        End If
        '
        'lbl4
        '
        Me.lbl4.BackColor = System.Drawing.Color.Transparent
        Me.lbl4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl4.ForeColor = System.Drawing.Color.White
        Me.lbl4.Location = New System.Drawing.Point(19, 125)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(271, 36)
        Me.lbl4.TabIndex = 16
        Me.lbl4.Text = "Laser 1 Hole Diameter (µm)"
        Me.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl5
        '
        Me.lbl5.BackColor = System.Drawing.Color.Transparent
        Me.lbl5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl5.ForeColor = System.Drawing.Color.White
        Me.lbl5.Location = New System.Drawing.Point(19, 161)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(271, 36)
        Me.lbl5.TabIndex = 16
        Me.lbl5.Text = "Laser 2 Hole Diameter (µm)"
        Me.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl3
        '
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl3.ForeColor = System.Drawing.Color.White
        Me.lbl3.Location = New System.Drawing.Point(19, 89)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(271, 36)
        Me.lbl3.TabIndex = 13
        Me.lbl3.Text = "Reel ID"
        Me.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl2
        '
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl2.ForeColor = System.Drawing.Color.White
        Me.lbl2.Location = New System.Drawing.Point(19, 53)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(271, 36)
        Me.lbl2.TabIndex = 10
        Me.lbl2.Text = "Print Reference"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl1.ForeColor = System.Drawing.Color.White
        Me.lbl1.Location = New System.Drawing.Point(19, 17)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(271, 36)
        Me.lbl1.TabIndex = 0
        Me.lbl1.Text = "Works Order"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(726, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(298, 209)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Start a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "New Batch"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.Transparent
        Me.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Image = Global.VB2005_Sherlock7.My.Resources.Resources.OK
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnSave.Location = New System.Drawing.Point(724, 327)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(150, 122)
        Me.btnSave.TabIndex = 21
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(874, 326)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(150, 122)
        Me.btnExit.TabIndex = 20
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'StartBatchForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GradientPanel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "StartBatchForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Start New Batch"
        Me.Controls.SetChildIndex(Me.pnlKeyboard, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel3, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnSave, 0)
        Me.pnlKeyboard.ResumeLayout(False)
        Me.pnlKeyboard.PerformLayout()
        Me.GradientPanel3.ResumeLayout(False)
        Me.GradientPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GradientPanel3 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents hCamera4 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera4 As System.Windows.Forms.Label
    Friend WithEvents hCamera3 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera3 As System.Windows.Forms.Label
    Friend WithEvents hCamera2 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera2 As System.Windows.Forms.Label
    Friend WithEvents hCamera1 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera1 As System.Windows.Forms.Label
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtReelId As System.Windows.Forms.TextBox
    Friend WithEvents txtPrintReference As System.Windows.Forms.TextBox
    Friend WithEvents txtWorksOrder As System.Windows.Forms.TextBox
    Friend WithEvents txtHoleDiameter As System.Windows.Forms.TextBox
    Friend WithEvents txtHoleDiameter2 As System.Windows.Forms.TextBox
    Friend WithEvents lblValue4 As System.Windows.Forms.Label
    Friend WithEvents lblValue3 As System.Windows.Forms.Label
    Friend WithEvents lblValue2 As System.Windows.Forms.Label
    Friend WithEvents lblValue1 As System.Windows.Forms.Label
    Friend WithEvents lblValue6 As System.Windows.Forms.Label
    Friend WithEvents hCamera6 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera6 As System.Windows.Forms.Label
    Friend WithEvents lblValue5 As System.Windows.Forms.Label
    Friend WithEvents hCamera5 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCamera5 As System.Windows.Forms.Label
End Class
