﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StatisticsViewerForm
    Inherits VB2005_Sherlock7.FormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GradientPanel3 = New VB2005_Sherlock7.GradientPanel()
        Me.lblValue18 = New System.Windows.Forms.Label()
        Me.lblValue17 = New System.Windows.Forms.Label()
        Me.lbl17 = New System.Windows.Forms.Label()
        Me.lblValue16 = New System.Windows.Forms.Label()
        Me.lbl16 = New System.Windows.Forms.Label()
        Me.lblValue15 = New System.Windows.Forms.Label()
        Me.lbl15 = New System.Windows.Forms.Label()
        Me.lblValue14 = New System.Windows.Forms.Label()
        Me.lblValue13 = New System.Windows.Forms.Label()
        Me.lblValue12 = New System.Windows.Forms.Label()
        Me.lbl14 = New System.Windows.Forms.Label()
        Me.lbl13 = New System.Windows.Forms.Label()
        Me.lbl12 = New System.Windows.Forms.Label()
        Me.lblValue11 = New System.Windows.Forms.Label()
        Me.lbl11 = New System.Windows.Forms.Label()
        Me.lblValue10 = New System.Windows.Forms.Label()
        Me.lbl10 = New System.Windows.Forms.Label()
        Me.lblValue9 = New System.Windows.Forms.Label()
        Me.lblValue8 = New System.Windows.Forms.Label()
        Me.lblValue7 = New System.Windows.Forms.Label()
        Me.lbl9 = New System.Windows.Forms.Label()
        Me.lbl8 = New System.Windows.Forms.Label()
        Me.lbl7 = New System.Windows.Forms.Label()
        Me.lblValue6 = New System.Windows.Forms.Label()
        Me.lblValue5 = New System.Windows.Forms.Label()
        Me.lblValue4 = New System.Windows.Forms.Label()
        Me.lbl6 = New System.Windows.Forms.Label()
        Me.lbl5 = New System.Windows.Forms.Label()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.lblValue3 = New System.Windows.Forms.Label()
        Me.lblValue2 = New System.Windows.Forms.Label()
        Me.lblValue1 = New System.Windows.Forms.Label()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lbl18 = New System.Windows.Forms.Label()
        Me.btnResetStats = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnShiftReport = New System.Windows.Forms.Button()
        Me.btnViewReports = New System.Windows.Forms.Button()
        Me.GradientPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlKeyboard
        '
        Me.pnlKeyboard.Dock = System.Windows.Forms.DockStyle.None
        Me.pnlKeyboard.Location = New System.Drawing.Point(1125, 1219)
        Me.pnlKeyboard.Margin = New System.Windows.Forms.Padding(2)
        '
        'GradientPanel3
        '
        Me.GradientPanel3.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel3.Controls.Add(Me.lblValue18)
        Me.GradientPanel3.Controls.Add(Me.lblValue17)
        Me.GradientPanel3.Controls.Add(Me.lbl17)
        Me.GradientPanel3.Controls.Add(Me.lblValue16)
        Me.GradientPanel3.Controls.Add(Me.lbl16)
        Me.GradientPanel3.Controls.Add(Me.lblValue15)
        Me.GradientPanel3.Controls.Add(Me.lbl15)
        Me.GradientPanel3.Controls.Add(Me.lblValue14)
        Me.GradientPanel3.Controls.Add(Me.lblValue13)
        Me.GradientPanel3.Controls.Add(Me.lblValue12)
        Me.GradientPanel3.Controls.Add(Me.lbl14)
        Me.GradientPanel3.Controls.Add(Me.lbl13)
        Me.GradientPanel3.Controls.Add(Me.lbl12)
        Me.GradientPanel3.Controls.Add(Me.lblValue11)
        Me.GradientPanel3.Controls.Add(Me.lbl11)
        Me.GradientPanel3.Controls.Add(Me.lblValue10)
        Me.GradientPanel3.Controls.Add(Me.lbl10)
        Me.GradientPanel3.Controls.Add(Me.lblValue9)
        Me.GradientPanel3.Controls.Add(Me.lblValue8)
        Me.GradientPanel3.Controls.Add(Me.lblValue7)
        Me.GradientPanel3.Controls.Add(Me.lbl9)
        Me.GradientPanel3.Controls.Add(Me.lbl8)
        Me.GradientPanel3.Controls.Add(Me.lbl7)
        Me.GradientPanel3.Controls.Add(Me.lblValue6)
        Me.GradientPanel3.Controls.Add(Me.lblValue5)
        Me.GradientPanel3.Controls.Add(Me.lblValue4)
        Me.GradientPanel3.Controls.Add(Me.lbl6)
        Me.GradientPanel3.Controls.Add(Me.lbl5)
        Me.GradientPanel3.Controls.Add(Me.lbl4)
        Me.GradientPanel3.Controls.Add(Me.lblValue3)
        Me.GradientPanel3.Controls.Add(Me.lblValue2)
        Me.GradientPanel3.Controls.Add(Me.lblValue1)
        Me.GradientPanel3.Controls.Add(Me.lbl3)
        Me.GradientPanel3.Controls.Add(Me.lbl2)
        Me.GradientPanel3.Controls.Add(Me.lbl1)
        Me.GradientPanel3.Controls.Add(Me.lbl18)
        Me.GradientPanel3.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel3.Location = New System.Drawing.Point(8, 7)
        Me.GradientPanel3.Name = "GradientPanel3"
        Me.GradientPanel3.Size = New System.Drawing.Size(712, 719)
        Me.GradientPanel3.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel3.TabIndex = 16
        '
        'lblValue18
        '
        Me.lblValue18.BackColor = System.Drawing.Color.Transparent
        Me.lblValue18.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue18.ForeColor = System.Drawing.Color.White
        Me.lblValue18.Location = New System.Drawing.Point(433, 567)
        Me.lblValue18.Name = "lblValue18"
        Me.lblValue18.Size = New System.Drawing.Size(170, 27)
        Me.lblValue18.TabIndex = 39
        Me.lblValue18.Text = "Label 18 Value"
        '
        'lblValue17
        '
        Me.lblValue17.BackColor = System.Drawing.Color.Transparent
        Me.lblValue17.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue17.ForeColor = System.Drawing.Color.White
        Me.lblValue17.Location = New System.Drawing.Point(433, 535)
        Me.lblValue17.Name = "lblValue17"
        Me.lblValue17.Size = New System.Drawing.Size(170, 27)
        Me.lblValue17.TabIndex = 37
        Me.lblValue17.Text = "Label 17 Value"
        '
        'lbl17
        '
        Me.lbl17.BackColor = System.Drawing.Color.Transparent
        Me.lbl17.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl17.ForeColor = System.Drawing.Color.White
        Me.lbl17.Location = New System.Drawing.Point(16, 533)
        Me.lbl17.Name = "lbl17"
        Me.lbl17.Size = New System.Drawing.Size(350, 27)
        Me.lbl17.TabIndex = 36
        Me.lbl17.Text = "Label 17"
        '
        'lblValue16
        '
        Me.lblValue16.BackColor = System.Drawing.Color.Transparent
        Me.lblValue16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue16.ForeColor = System.Drawing.Color.White
        Me.lblValue16.Location = New System.Drawing.Point(433, 502)
        Me.lblValue16.Name = "lblValue16"
        Me.lblValue16.Size = New System.Drawing.Size(170, 27)
        Me.lblValue16.TabIndex = 35
        Me.lblValue16.Text = "Label 16 Value"
        '
        'lbl16
        '
        Me.lbl16.BackColor = System.Drawing.Color.Transparent
        Me.lbl16.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl16.ForeColor = System.Drawing.Color.White
        Me.lbl16.Location = New System.Drawing.Point(16, 500)
        Me.lbl16.Name = "lbl16"
        Me.lbl16.Size = New System.Drawing.Size(350, 27)
        Me.lbl16.TabIndex = 34
        Me.lbl16.Text = "Label 16"
        '
        'lblValue15
        '
        Me.lblValue15.BackColor = System.Drawing.Color.Transparent
        Me.lblValue15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue15.ForeColor = System.Drawing.Color.White
        Me.lblValue15.Location = New System.Drawing.Point(433, 470)
        Me.lblValue15.Name = "lblValue15"
        Me.lblValue15.Size = New System.Drawing.Size(170, 27)
        Me.lblValue15.TabIndex = 33
        Me.lblValue15.Text = "Label 15 Value"
        '
        'lbl15
        '
        Me.lbl15.BackColor = System.Drawing.Color.Transparent
        Me.lbl15.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl15.ForeColor = System.Drawing.Color.White
        Me.lbl15.Location = New System.Drawing.Point(16, 468)
        Me.lbl15.Name = "lbl15"
        Me.lbl15.Size = New System.Drawing.Size(350, 27)
        Me.lbl15.TabIndex = 32
        Me.lbl15.Text = "Label 15"
        '
        'lblValue14
        '
        Me.lblValue14.BackColor = System.Drawing.Color.Transparent
        Me.lblValue14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue14.ForeColor = System.Drawing.Color.White
        Me.lblValue14.Location = New System.Drawing.Point(433, 437)
        Me.lblValue14.Name = "lblValue14"
        Me.lblValue14.Size = New System.Drawing.Size(170, 27)
        Me.lblValue14.TabIndex = 31
        Me.lblValue14.Text = "Label 14 Value"
        '
        'lblValue13
        '
        Me.lblValue13.BackColor = System.Drawing.Color.Transparent
        Me.lblValue13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue13.ForeColor = System.Drawing.Color.White
        Me.lblValue13.Location = New System.Drawing.Point(433, 405)
        Me.lblValue13.Name = "lblValue13"
        Me.lblValue13.Size = New System.Drawing.Size(170, 27)
        Me.lblValue13.TabIndex = 30
        Me.lblValue13.Text = "Label 13 Value"
        '
        'lblValue12
        '
        Me.lblValue12.BackColor = System.Drawing.Color.Transparent
        Me.lblValue12.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue12.ForeColor = System.Drawing.Color.White
        Me.lblValue12.Location = New System.Drawing.Point(433, 372)
        Me.lblValue12.Name = "lblValue12"
        Me.lblValue12.Size = New System.Drawing.Size(170, 27)
        Me.lblValue12.TabIndex = 29
        Me.lblValue12.Text = "Label 12 Value"
        '
        'lbl14
        '
        Me.lbl14.BackColor = System.Drawing.Color.Transparent
        Me.lbl14.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl14.ForeColor = System.Drawing.Color.White
        Me.lbl14.Location = New System.Drawing.Point(16, 436)
        Me.lbl14.Name = "lbl14"
        Me.lbl14.Size = New System.Drawing.Size(350, 27)
        Me.lbl14.TabIndex = 28
        Me.lbl14.Text = "Label 14"
        '
        'lbl13
        '
        Me.lbl13.BackColor = System.Drawing.Color.Transparent
        Me.lbl13.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl13.ForeColor = System.Drawing.Color.White
        Me.lbl13.Location = New System.Drawing.Point(16, 403)
        Me.lbl13.Name = "lbl13"
        Me.lbl13.Size = New System.Drawing.Size(350, 27)
        Me.lbl13.TabIndex = 27
        Me.lbl13.Text = "Label 13"
        '
        'lbl12
        '
        Me.lbl12.BackColor = System.Drawing.Color.Transparent
        Me.lbl12.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl12.ForeColor = System.Drawing.Color.White
        Me.lbl12.Location = New System.Drawing.Point(16, 370)
        Me.lbl12.Name = "lbl12"
        Me.lbl12.Size = New System.Drawing.Size(350, 27)
        Me.lbl12.TabIndex = 26
        Me.lbl12.Text = "Label 12"
        '
        'lblValue11
        '
        Me.lblValue11.BackColor = System.Drawing.Color.Transparent
        Me.lblValue11.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue11.ForeColor = System.Drawing.Color.White
        Me.lblValue11.Location = New System.Drawing.Point(433, 340)
        Me.lblValue11.Name = "lblValue11"
        Me.lblValue11.Size = New System.Drawing.Size(170, 27)
        Me.lblValue11.TabIndex = 25
        Me.lblValue11.Text = "Label 11 Value"
        '
        'lbl11
        '
        Me.lbl11.BackColor = System.Drawing.Color.Transparent
        Me.lbl11.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl11.ForeColor = System.Drawing.Color.White
        Me.lbl11.Location = New System.Drawing.Point(16, 338)
        Me.lbl11.Name = "lbl11"
        Me.lbl11.Size = New System.Drawing.Size(350, 27)
        Me.lbl11.TabIndex = 24
        Me.lbl11.Text = "Label 11"
        '
        'lblValue10
        '
        Me.lblValue10.BackColor = System.Drawing.Color.Transparent
        Me.lblValue10.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue10.ForeColor = System.Drawing.Color.White
        Me.lblValue10.Location = New System.Drawing.Point(433, 307)
        Me.lblValue10.Name = "lblValue10"
        Me.lblValue10.Size = New System.Drawing.Size(170, 27)
        Me.lblValue10.TabIndex = 23
        Me.lblValue10.Text = "Label 10 Value"
        '
        'lbl10
        '
        Me.lbl10.BackColor = System.Drawing.Color.Transparent
        Me.lbl10.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl10.ForeColor = System.Drawing.Color.White
        Me.lbl10.Location = New System.Drawing.Point(16, 306)
        Me.lbl10.Name = "lbl10"
        Me.lbl10.Size = New System.Drawing.Size(350, 27)
        Me.lbl10.TabIndex = 22
        Me.lbl10.Text = "Label 10"
        '
        'lblValue9
        '
        Me.lblValue9.BackColor = System.Drawing.Color.Transparent
        Me.lblValue9.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue9.ForeColor = System.Drawing.Color.White
        Me.lblValue9.Location = New System.Drawing.Point(433, 275)
        Me.lblValue9.Name = "lblValue9"
        Me.lblValue9.Size = New System.Drawing.Size(170, 27)
        Me.lblValue9.TabIndex = 21
        Me.lblValue9.Text = "Label 9 Value"
        '
        'lblValue8
        '
        Me.lblValue8.BackColor = System.Drawing.Color.Transparent
        Me.lblValue8.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue8.ForeColor = System.Drawing.Color.White
        Me.lblValue8.Location = New System.Drawing.Point(433, 242)
        Me.lblValue8.Name = "lblValue8"
        Me.lblValue8.Size = New System.Drawing.Size(170, 27)
        Me.lblValue8.TabIndex = 20
        Me.lblValue8.Text = "Label 8 Value"
        '
        'lblValue7
        '
        Me.lblValue7.BackColor = System.Drawing.Color.Transparent
        Me.lblValue7.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue7.ForeColor = System.Drawing.Color.White
        Me.lblValue7.Location = New System.Drawing.Point(433, 210)
        Me.lblValue7.Name = "lblValue7"
        Me.lblValue7.Size = New System.Drawing.Size(170, 27)
        Me.lblValue7.TabIndex = 19
        Me.lblValue7.Text = "Label 7 Value"
        '
        'lbl9
        '
        Me.lbl9.BackColor = System.Drawing.Color.Transparent
        Me.lbl9.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl9.ForeColor = System.Drawing.Color.White
        Me.lbl9.Location = New System.Drawing.Point(16, 273)
        Me.lbl9.Name = "lbl9"
        Me.lbl9.Size = New System.Drawing.Size(350, 27)
        Me.lbl9.TabIndex = 18
        Me.lbl9.Text = "Label 9"
        '
        'lbl8
        '
        Me.lbl8.BackColor = System.Drawing.Color.Transparent
        Me.lbl8.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl8.ForeColor = System.Drawing.Color.White
        Me.lbl8.Location = New System.Drawing.Point(16, 240)
        Me.lbl8.Name = "lbl8"
        Me.lbl8.Size = New System.Drawing.Size(350, 27)
        Me.lbl8.TabIndex = 17
        Me.lbl8.Text = "Label 8"
        '
        'lbl7
        '
        Me.lbl7.BackColor = System.Drawing.Color.Transparent
        Me.lbl7.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl7.ForeColor = System.Drawing.Color.White
        Me.lbl7.Location = New System.Drawing.Point(16, 208)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(350, 27)
        Me.lbl7.TabIndex = 16
        Me.lbl7.Text = "Label 7"
        '
        'lblValue6
        '
        Me.lblValue6.BackColor = System.Drawing.Color.Transparent
        Me.lblValue6.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue6.ForeColor = System.Drawing.Color.White
        Me.lblValue6.Location = New System.Drawing.Point(433, 177)
        Me.lblValue6.Name = "lblValue6"
        Me.lblValue6.Size = New System.Drawing.Size(170, 27)
        Me.lblValue6.TabIndex = 15
        Me.lblValue6.Text = "Label 6 Value"
        '
        'lblValue5
        '
        Me.lblValue5.BackColor = System.Drawing.Color.Transparent
        Me.lblValue5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue5.ForeColor = System.Drawing.Color.White
        Me.lblValue5.Location = New System.Drawing.Point(433, 143)
        Me.lblValue5.Name = "lblValue5"
        Me.lblValue5.Size = New System.Drawing.Size(170, 27)
        Me.lblValue5.TabIndex = 14
        Me.lblValue5.Text = "Label 5 Value"
        '
        'lblValue4
        '
        Me.lblValue4.BackColor = System.Drawing.Color.Transparent
        Me.lblValue4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue4.ForeColor = System.Drawing.Color.White
        Me.lblValue4.Location = New System.Drawing.Point(433, 111)
        Me.lblValue4.Name = "lblValue4"
        Me.lblValue4.Size = New System.Drawing.Size(170, 27)
        Me.lblValue4.TabIndex = 13
        Me.lblValue4.Text = "Label 4 Value"
        '
        'lbl6
        '
        Me.lbl6.BackColor = System.Drawing.Color.Transparent
        Me.lbl6.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl6.ForeColor = System.Drawing.Color.White
        Me.lbl6.Location = New System.Drawing.Point(16, 176)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(350, 27)
        Me.lbl6.TabIndex = 12
        Me.lbl6.Text = "Label 6"
        '
        'lbl5
        '
        Me.lbl5.BackColor = System.Drawing.Color.Transparent
        Me.lbl5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl5.ForeColor = System.Drawing.Color.White
        Me.lbl5.Location = New System.Drawing.Point(16, 143)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(350, 27)
        Me.lbl5.TabIndex = 11
        Me.lbl5.Text = "Label 5"
        '
        'lbl4
        '
        Me.lbl4.BackColor = System.Drawing.Color.Transparent
        Me.lbl4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl4.ForeColor = System.Drawing.Color.White
        Me.lbl4.Location = New System.Drawing.Point(16, 110)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(350, 27)
        Me.lbl4.TabIndex = 10
        Me.lbl4.Text = "Label 4"
        '
        'lblValue3
        '
        Me.lblValue3.BackColor = System.Drawing.Color.Transparent
        Me.lblValue3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue3.ForeColor = System.Drawing.Color.White
        Me.lblValue3.Location = New System.Drawing.Point(433, 76)
        Me.lblValue3.Name = "lblValue3"
        Me.lblValue3.Size = New System.Drawing.Size(170, 19)
        Me.lblValue3.TabIndex = 9
        Me.lblValue3.Text = "Label 3 Value"
        '
        'lblValue2
        '
        Me.lblValue2.BackColor = System.Drawing.Color.Transparent
        Me.lblValue2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue2.ForeColor = System.Drawing.Color.White
        Me.lblValue2.Location = New System.Drawing.Point(433, 46)
        Me.lblValue2.Name = "lblValue2"
        Me.lblValue2.Size = New System.Drawing.Size(170, 27)
        Me.lblValue2.TabIndex = 8
        Me.lblValue2.Text = "Label 2 Value"
        '
        'lblValue1
        '
        Me.lblValue1.BackColor = System.Drawing.Color.Transparent
        Me.lblValue1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblValue1.ForeColor = System.Drawing.Color.White
        Me.lblValue1.Location = New System.Drawing.Point(433, 14)
        Me.lblValue1.Name = "lblValue1"
        Me.lblValue1.Size = New System.Drawing.Size(170, 27)
        Me.lblValue1.TabIndex = 7
        Me.lblValue1.Text = "Label 1 Value"
        '
        'lbl3
        '
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl3.ForeColor = System.Drawing.Color.White
        Me.lbl3.Location = New System.Drawing.Point(16, 78)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(350, 27)
        Me.lbl3.TabIndex = 6
        Me.lbl3.Text = "Label 3"
        '
        'lbl2
        '
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl2.ForeColor = System.Drawing.Color.White
        Me.lbl2.Location = New System.Drawing.Point(16, 46)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(350, 27)
        Me.lbl2.TabIndex = 4
        Me.lbl2.Text = "Label 2"
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl1.ForeColor = System.Drawing.Color.White
        Me.lbl1.Location = New System.Drawing.Point(16, 13)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(350, 27)
        Me.lbl1.TabIndex = 0
        Me.lbl1.Text = "Label 1"
        '
        'lbl18
        '
        Me.lbl18.BackColor = System.Drawing.Color.Transparent
        Me.lbl18.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl18.ForeColor = System.Drawing.Color.White
        Me.lbl18.Location = New System.Drawing.Point(16, 566)
        Me.lbl18.Name = "lbl18"
        Me.lbl18.Size = New System.Drawing.Size(350, 27)
        Me.lbl18.TabIndex = 46
        Me.lbl18.Text = "Label 18"
        '
        'btnResetStats
        '
        Me.btnResetStats.BackColor = System.Drawing.Color.Transparent
        Me.btnResetStats.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnResetStats.FlatAppearance.BorderSize = 0
        Me.btnResetStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnResetStats.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Reset
        Me.btnResetStats.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnResetStats.Location = New System.Drawing.Point(724, 452)
        Me.btnResetStats.Margin = New System.Windows.Forms.Padding(0)
        Me.btnResetStats.Name = "btnResetStats"
        Me.btnResetStats.Size = New System.Drawing.Size(150, 122)
        Me.btnResetStats.TabIndex = 15
        Me.btnResetStats.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(874, 452)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(150, 122)
        Me.btnExit.TabIndex = 14
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(724, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 187)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Inspection Statistics"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnShiftReport
        '
        Me.btnShiftReport.BackColor = System.Drawing.Color.Transparent
        Me.btnShiftReport.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnShiftReport.FlatAppearance.BorderSize = 0
        Me.btnShiftReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShiftReport.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Shift_Report_01
        Me.btnShiftReport.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnShiftReport.Location = New System.Drawing.Point(725, 210)
        Me.btnShiftReport.Margin = New System.Windows.Forms.Padding(0)
        Me.btnShiftReport.Name = "btnShiftReport"
        Me.btnShiftReport.Size = New System.Drawing.Size(299, 122)
        Me.btnShiftReport.TabIndex = 18
        Me.btnShiftReport.UseVisualStyleBackColor = False
        '
        'btnViewReports
        '
        Me.btnViewReports.BackColor = System.Drawing.Color.Transparent
        Me.btnViewReports.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnViewReports.FlatAppearance.BorderSize = 0
        Me.btnViewReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewReports.Image = Global.VB2005_Sherlock7.My.Resources.Resources.View_Reports_01
        Me.btnViewReports.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnViewReports.Location = New System.Drawing.Point(725, 331)
        Me.btnViewReports.Margin = New System.Windows.Forms.Padding(0)
        Me.btnViewReports.Name = "btnViewReports"
        Me.btnViewReports.Size = New System.Drawing.Size(299, 122)
        Me.btnViewReports.TabIndex = 19
        Me.btnViewReports.UseVisualStyleBackColor = False
        Me.btnViewReports.Visible = False
        '
        'StatisticsViewerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnViewReports)
        Me.Controls.Add(Me.btnShiftReport)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GradientPanel3)
        Me.Controls.Add(Me.btnResetStats)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "StatisticsViewerForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Statistics"
        Me.Controls.SetChildIndex(Me.pnlKeyboard, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnResetStats, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel3, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.btnShiftReport, 0)
        Me.Controls.SetChildIndex(Me.btnViewReports, 0)
        Me.GradientPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GradientPanel3 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents btnResetStats As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblValue3 As System.Windows.Forms.Label
    Friend WithEvents lblValue2 As System.Windows.Forms.Label
    Friend WithEvents lblValue1 As System.Windows.Forms.Label
    Friend WithEvents lblValue11 As System.Windows.Forms.Label
    Friend WithEvents lbl11 As System.Windows.Forms.Label
    Friend WithEvents lblValue10 As System.Windows.Forms.Label
    Friend WithEvents lbl10 As System.Windows.Forms.Label
    Friend WithEvents lblValue9 As System.Windows.Forms.Label
    Friend WithEvents lblValue8 As System.Windows.Forms.Label
    Friend WithEvents lblValue7 As System.Windows.Forms.Label
    Friend WithEvents lbl9 As System.Windows.Forms.Label
    Friend WithEvents lbl8 As System.Windows.Forms.Label
    Friend WithEvents lbl7 As System.Windows.Forms.Label
    Friend WithEvents lblValue6 As System.Windows.Forms.Label
    Friend WithEvents lblValue5 As System.Windows.Forms.Label
    Friend WithEvents lblValue4 As System.Windows.Forms.Label
    Friend WithEvents lbl6 As System.Windows.Forms.Label
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblValue18 As System.Windows.Forms.Label
    Friend WithEvents lblValue17 As System.Windows.Forms.Label
    Friend WithEvents lbl17 As System.Windows.Forms.Label
    Friend WithEvents lblValue16 As System.Windows.Forms.Label
    Friend WithEvents lbl16 As System.Windows.Forms.Label
    Friend WithEvents lblValue15 As System.Windows.Forms.Label
    Friend WithEvents lbl15 As System.Windows.Forms.Label
    Friend WithEvents lblValue14 As System.Windows.Forms.Label
    Friend WithEvents lblValue13 As System.Windows.Forms.Label
    Friend WithEvents lblValue12 As System.Windows.Forms.Label
    Friend WithEvents lbl14 As System.Windows.Forms.Label
    Friend WithEvents lbl13 As System.Windows.Forms.Label
    Friend WithEvents lbl12 As System.Windows.Forms.Label
    Friend WithEvents lbl18 As System.Windows.Forms.Label
    Friend WithEvents btnShiftReport As System.Windows.Forms.Button
    Friend WithEvents btnViewReports As System.Windows.Forms.Button

End Class
