﻿Public Class StartBatchForm

    Dim m_MainScreen As MainForm

    Private Sub StartBatchForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        UpdateScreen()

    End Sub

#Region "Private Functions"

    'todo: this
    Private Sub UpdateScreen()

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        'Dim fileName = My.Settings("CamerasDefinitionFile")
        'Dim m_Cameras As Dictionary(Of String, CameraInfo) = CamerasReader.ReadCameras(fileName)
        'If (m_Cameras Is Nothing) Then
        '    Return
        'End If

        'hCamera1.Visible = False
        'hCamera2.Visible = False
        'hCamera3.Visible = False
        'hCamera4.Visible = False
        'hCamera5.Visible = False
        'hCamera6.Visible = False

        'Dim iIndex As Integer = 1

        'For Each camera As KeyValuePair(Of String, CameraInfo) In m_Cameras

        '    Select Case iIndex
        '        Case 1
        '            hCamera1.Visible = camera.Value.SubCamera1
        '            lblCamera1.Visible = camera.Value.SubCamera1
        '            lblValue1.Visible = camera.Value.SubCamera1

        '            hCamera2.Visible = camera.Value.SubCamera2
        '            lblCamera2.Visible = camera.Value.SubCamera2
        '            lblValue2.Visible = camera.Value.SubCamera2

        '            hCamera3.Visible = camera.Value.SubCamera3
        '            lblCamera3.Visible = camera.Value.SubCamera3
        '            lblValue3.Visible = camera.Value.SubCamera3

        '        Case 2
        '            hCamera4.Visible = camera.Value.SubCamera1
        '            lblCamera4.Visible = camera.Value.SubCamera1
        '            lblValue4.Visible = camera.Value.SubCamera1

        '            hCamera5.Visible = camera.Value.SubCamera2
        '            lblCamera5.Visible = camera.Value.SubCamera2
        '            lblValue5.Visible = camera.Value.SubCamera2

        '            hCamera6.Visible = camera.Value.SubCamera3
        '            lblCamera6.Visible = camera.Value.SubCamera3
        '            lblValue6.Visible = camera.Value.SubCamera3

        '    End Select

        '    iIndex = iIndex + 1

        'Next

    End Sub

#End Region

#Region "Button Events"

    'todo: this
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If (m_MainScreen Is Nothing) Then
            Return
        End If



        ' close the form
        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub hCamera1_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera1.ValueChanged
        lblValue1.Text = IIf(hCamera1.Value = 1, "Yes", "No")
    End Sub

    Private Sub hCamera2_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera2.ValueChanged
        lblValue2.Text = IIf(hCamera2.Value = 1, "Yes", "No")
    End Sub

    Private Sub hCamera3_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera3.ValueChanged
        lblValue3.Text = IIf(hCamera3.Value = 1, "Yes", "No")
    End Sub

    Private Sub hCamera4_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera4.ValueChanged
        lblValue4.Text = IIf(hCamera4.Value = 1, "Yes", "No")
    End Sub

    Private Sub hCamera5_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera5.ValueChanged
        lblValue5.Text = IIf(hCamera5.Value = 1, "Yes", "No")
    End Sub

    Private Sub hCamera6_ValueChanged(sender As System.Object, e As System.EventArgs) Handles hCamera6.ValueChanged
        lblValue6.Text = IIf(hCamera6.Value = 1, "Yes", "No")
    End Sub

#End Region

#Region "Properties"

    Public Property MainScreen() As MainForm
        Get
            Return m_MainScreen
        End Get
        Set(ByVal value As MainForm)
            m_MainScreen = value
        End Set
    End Property

#End Region

    Private Sub NumericTextBox_Enter(sender As System.Object, e As System.EventArgs) Handles txtHoleDiameter.Enter
        KeyboardControl1.SetMode(True)
    End Sub

    Private Sub TextTextBox_Enter(sender As System.Object, e As System.EventArgs) Handles txtWorksOrder.Enter, txtReelId.Enter, txtPrintReference.Enter
        KeyboardControl1.SetMode(False)
    End Sub
End Class