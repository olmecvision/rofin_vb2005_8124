﻿Option Strict Off
Option Explicit On
Module Module1
    '  Copyright (C) 1995-2007 Adlink Technology INC.
    '  All rights reserved.

    '  System: Section 5.2
    Declare Function B_8124_initialSw Lib "8124x64.dll" Alias "_8124_initialSw" (ByRef CardIdInBit As Short, ByVal ManualId As Short, ByVal DefaultSw As Short) As Short
    Declare Function B_8124_close Lib "8124x64.dll" Alias "_8124_close" () As Short
    Declare Function B_8124_get_version Lib "8124x64.dll" Alias "_8124_get_version" (ByVal CardId As Short, ByRef Firmware_ver As Integer, ByRef Driver_ver As Integer, ByRef DLL_ver As Integer) As Short
    '
    '  Encoder & Counter: Section 5.3
    Declare Function B_8124_set_encoder_input_mode Lib "8124x64.dll" Alias "_8124_set_encoder_input_mode" (ByVal CardId As Short, ByVal Channel As Short, ByVal IptMode As Short) As Short
    Declare Function B_8124_set_encoder_value Lib "8124x64.dll" Alias "_8124_set_encoder_value" (ByVal CardId As Short, ByVal Channel As Short, ByVal EncValue As Integer) As Short
    Declare Function B_8124_get_encoder_value Lib "8124x64.dll" Alias "_8124_get_encoder_value" (ByVal CardId As Short, ByVal Channel As Short, ByRef EncValue As Integer) As Short
    Declare Function B_8124_set_encoder_up_down_count Lib "8124x64.dll" Alias "_8124_set_encoder_up_down_count" (ByVal CardId As Short, ByVal Channel As Short, ByVal Inverse As Short) As Short
    Declare Function B_8124_set_ez_clear Lib "8124x64.dll" Alias "_8124_set_ez_clear" (ByVal CardId As Short, ByVal Channel As Short, ByVal Enable As Short, ByVal ClrLogic As Short) As Short
    Declare Function B_8124_set_counter_source Lib "8124x64.dll" Alias "_8124_set_counter_source" (ByVal CardId As Short, ByVal Channel As Short, ByVal CtnSrc As Short) As Short
    '
    ' PWM Configuration: Section 5.4
    Declare Function B_8124_set_pwm_source Lib "8124x64.dll" Alias "_8124_set_pwm_source" (ByVal CardId As Short, ByVal Channel As Short, ByVal CmpEn As Short, ByVal LinearEn As Short, ByVal TimerEn As Short) As Short
    Declare Function B_8124_set_pwm_mode Lib "8124x64.dll" Alias "_8124_set_pwm_mode" (ByVal CardId As Short, ByVal Channel As Short, ByVal PulseOrToggle As Short) As Short
    Declare Function B_8124_set_pwm_pulse_width Lib "8124x64.dll" Alias "_8124_set_pwm_pulse_width" (ByVal CardId As Short, ByVal Channel As Short, ByVal WidthPara As Short) As Short
    Declare Function B_8124_set_pwm_toggle_dir Lib "8124x64.dll" Alias "_8124_set_pwm_toggle_dir" (ByVal CardId As Short, ByVal Channel As Short, ByVal ToggleDir As Short) As Short
    '
    ' TRG-OUT & TTL-OUT Configuration: Section 5.5
    Declare Function B_8124_set_trigger_source Lib "8124x64.dll" Alias "_8124_set_trigger_source" (ByVal CardId As Short, ByVal Channel As Short, ByVal GroupSel As Short, ByVal PwmInBit As Short, ByVal TtlInInBit As Short, ByVal EzInBit As Short) As Short
    Declare Function B_8124_set_trgOut_logic Lib "8124x64.dll" Alias "_8124_set_trgOut_logic" (ByVal CardId As Short, ByVal Channel As Short, ByVal Logic As Short) As Short
    Declare Function B_8124_set_ttlOut_source Lib "8124x64.dll" Alias "_8124_set_ttlOut_source" (ByVal CardId As Short, ByVal Channel As Short, ByVal TtlOutSrc As Short) As Short
    Declare Function B_8124_set_ttlOut Lib "8124x64.dll" Alias "_8124_set_ttlOut" (ByVal CardId As Short, ByVal Channel As Short, ByVal TtlOutValue As Short) As Short
    '
    '  Comparator: Section 5.6
    Declare Function B_8124_set_comparator_data Lib "8124x64.dll" Alias "_8124_set_comparator_data" (ByVal CardId As Short, ByVal Channel As Short, ByVal CmpData As Integer) As Short
    Declare Function B_8124_get_comparator_data Lib "8124x64.dll" Alias "_8124_get_comparator_data" (ByVal CardId As Short, ByVal Channel As Short, ByRef CmpData As Integer) As Short
    '
    ' FIFO Comparing: Section 5.7
    Declare Function B_8124_reset_fifo Lib "8124x64.dll" Alias "_8124_reset_fifo" (ByVal CardId As Short, ByVal Channel As Short) As Short
    Declare Function B_8124_get_fifo_sts Lib "8124x64.dll" Alias "_8124_get_fifo_sts" (ByVal CardId As Short, ByVal Channel As Short, ByRef FifoSts As Short) As Short
    Declare Function B_8124_set_fifo_data Lib "8124x64.dll" Alias "_8124_set_fifo_data" (ByVal CardId As Short, ByVal Channel As Short, ByVal FifoData As Integer) As Short
    Declare Function B_8124_set_fifo_array Lib "8124x64.dll" Alias "_8124_set_fifo_array" (ByVal CardId As Short, ByVal Channel As Short, ByVal DataArr() As Integer, ByVal ArraySize As Short) As Short
    Declare Function B_8124_set_fifo_shift Lib "8124x64.dll" Alias "_8124_set_fifo_shift" (ByVal CardId As Short, ByVal Channel As Short) As Short
    Declare Function B_8124_set_fifo_level Lib "8124x64.dll" Alias "_8124_set_fifo_level" (ByVal CardId As Short, ByVal Channel As Short, ByVal Level As Short) As Short
    Declare Function B_8124_get_fifo_level Lib "8124x64.dll" Alias "_8124_get_fifo_level" (ByVal CardId As Short, ByVal Channel As Short, ByRef Level As Short) As Short
    '
    '  Linear Comparing: Section 5.8
    Declare Function B_8124_set_linear_source Lib "8124x64.dll" Alias "_8124_set_linear_source" (ByVal CardId As Short, ByVal Channel As Short, ByVal SetNumInBit As Short) As Short
    Declare Function B_8124_set_linear_compare Lib "8124x64.dll" Alias "_8124_set_linear_compare" (ByVal CardId As Short, ByVal SetNum As Short, ByVal CntNum As Short, ByVal StartPoint As Integer, ByVal RepeatTimes As Double, ByVal Interval As Short) As Short
    Declare Function B_8124_enable_linear_set Lib "8124x64.dll" Alias "_8124_enable_linear_set" (ByVal CardId As Short, ByVal SetNum As Short, ByVal Enable As Short) As Short
    '
    '  Manual Trigger: Section 5.9
    Declare Function B_8124_manual_trigger Lib "8124x64.dll" Alias "_8124_manual_trigger" (ByVal CardId As Short, ByVal Channel As Short) As Short
    '
    '  Digital Input: Section 5.10
    Declare Function B_8124_get_di_sts Lib "8124x64.dll" Alias "_8124_get_di_sts" (ByVal CardId As Short, ByRef TtlStsInBit As Short, ByRef LtcStsInBit As Short, ByRef EzStsInBit As Short) As Short
    '
    '  Latch: Section 5.11
    Declare Function B_8124_set_latch Lib "8124x64.dll" Alias "_8124_set_latch" (ByVal CardId As Short, ByVal Channel As Short, ByVal Enable As Short, ByVal EdgeSel As Short) As Short
    Declare Function B_8124_get_latch_event_sts Lib "8124x64.dll" Alias "_8124_get_latch_event_sts" (ByVal CardId As Short, ByRef LatchEventInBit As Short) As Short
    Declare Function B_8124_get_latch_value Lib "8124x64.dll" Alias "_8124_get_latch_value" (ByVal CardId As Short, ByVal Channel As Short, ByRef LatchValue As Integer) As Short
    '
    '  Timer: Section 5.12
    Declare Function B_8124_set_Timer Lib "8124x64.dll" Alias "_8124_set_Timer" (ByVal CardId As Short, ByVal Channel As Short, ByVal WaitTtlIn As Short, ByVal TtlTrigLogic As Short, ByVal Interval As Short) As Short
    Declare Function B_8124_start_timer Lib "8124x64.dll" Alias "_8124_start_timer" (ByVal CardId As Short, ByVal Channel As Short, ByVal Start As Short) As Short
    '
    '  Interrupt: Section 5.13
    Declare Function B_8124_set_int_factor Lib "8124x64.dll" Alias "_8124_set_int_factor" (ByVal CardId As Short, ByVal Channel As Short, ByVal IntFactorsInBit As Short) As Short
    Declare Function B_8124_get_int_factor Lib "8124x64.dll" Alias "_8124_get_int_factor" (ByVal CardId As Short, ByVal Channel As Short, ByRef IntFactorsInBit As Short) As Short
    Declare Function B_8124_wait_single_int Lib "8124x64.dll" Alias "_8124_wait_single_int" (ByVal CardId As Short, ByVal Channel As Short, ByVal FactorBitNum As Short, ByVal TimeOutMs As Integer) As Short
    Declare Function B_8124_reset_int Lib "8124x64.dll" Alias "_8124_reset_int" (ByVal CardId As Short, ByVal Channel As Short, ByVal FactorBitNum As Short) As Short

    'filter
    Declare Function B_8124_set_input_filter_enable Lib "8124x64.dll" Alias "_8124_set_input_filter_enable" (ByVal CardId As Short, ByVal Channel As Short, ByVal Enable As Short) As Short

End Module