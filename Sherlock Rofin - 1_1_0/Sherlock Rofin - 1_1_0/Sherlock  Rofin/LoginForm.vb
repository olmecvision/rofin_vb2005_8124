﻿Public Class LoginForm

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'txtUserName.Focus()
        'pnlKeyboard.Show()
        'pnlKeyboard.BringToFront()

    End Sub

    Public Property User() As UserInfo
        Get
            Return m_User
        End Get
        Set(ByVal value As UserInfo)
            m_User = value
        End Set
    End Property

    Private m_User As UserInfo

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        ' get the authentication information
        Dim password As String

        password = txtPassword.Text

        ' validate the entry
        If (String.IsNullOrEmpty(password)) Then
            Return
        End If

        ' authenticate the user
        Me.User = UserAuthentication.SignIn(password)
        If (Me.User Is Nothing) Then
            MessageBox.Show("Incorrect password", "Sign In", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        ' set the dialog result
        Me.DialogResult = Windows.Forms.DialogResult.OK

        ' close the form
        Me.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click

    End Sub
    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged

    End Sub
End Class
