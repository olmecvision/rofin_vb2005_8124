﻿Public Class SystemConfigureForm

    Dim m_MainScreen As MainForm = MainForm

    Private Sub SystemConfigureForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        If (Not DesignMode) Then
            Dim variables As Dictionary(Of String, VariableInfo) = m_MainScreen.Variables
            Dim statistics As Dictionary(Of String, StatisticInfo) = m_MainScreen.Statistics

            Dim firstHead As HeadInfo = m_MainScreen.GetFirstHead()
            If (firstHead IsNot Nothing) Then
                VariablesReader.UpdateValues(variables, firstHead.Engine)
            End If

            StatisticsReader.UpdateValues(statistics, MainScreen)

            UpdateScreen()
        End If

    End Sub

#Region "Private Functions"

    Private Sub UpdateScreen()

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        If (m_MainScreen.Variables Is Nothing) Then
            Return
        End If

        ' build a collection of the label controls
        Dim labelCtrls As List(Of Label) = GetLabelControlsCollection()
        'Dim scrollCtrls As List(Of HScrollBar) = GetScrollControlsCollection()
        Dim valueCtrls As List(Of Label) = GetValueControlsCollection()
        Dim numCtrls As List(Of NumericUpDown) = GetnumericControlsCollection()

        Dim iIndex As Integer

        ' first hide all of the controls
        For iIndex = 0 To labelCtrls.Count - 1
            labelCtrls.Item(iIndex).Visible = False
            'scrollCtrls.Item(iIndex).Visible = False
            valueCtrls.Item(iIndex).Visible = False
            numCtrls.Item(iIndex).Visible = False
        Next

        ' populate the controls
        Dim enumerator As IEnumerator = m_MainScreen.Variables.GetEnumerator()

        ' make sure that we have more than 5 items in the list
        If (m_MainScreen.Variables.Count < 6) Then
            Return
        End If

        ' skip over the first 5
        Skip(enumerator, 5)

        iIndex = 0
        While (enumerator.MoveNext())

            ' get the item
            Dim item As VariableInfo = CType(enumerator.Current, KeyValuePair(Of String, VariableInfo)).Value

            'set the statistic label
            labelCtrls.Item(iIndex).Text = item.VariableName
            labelCtrls.Item(iIndex).Visible = True

            ' set the statistic value
            Dim variableValue As String = "Unknown" 'As Integer = 0
            Dim strVariableValue As String = "Unknown"

            'If (item.Value IsNot Nothing) Then
            '    variableValue = If((item.Value), 1, 0)
            '    strVariableValue = item.Value.ToString()
            'End If

            ' handle the min/max range
            Select Case item.DataType
                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL

                    numCtrls.Item(iIndex).Minimum = 0
                    numCtrls.Item(iIndex).Maximum = 1
                    numCtrls.Item(iIndex).Increment = 1
                    numCtrls.Item(iIndex).DecimalPlaces = 0

                    If (item.Value IsNot Nothing) Then
                        variableValue = If((item.Value), 1, 0)
                    End If

                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_DOUBLE, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT
                    numCtrls.Item(iIndex).Minimum = item.MinimumValue
                    numCtrls.Item(iIndex).Increment = 1000
                    numCtrls.Item(iIndex).DecimalPlaces = 0
                    If (item.MaximumValue - item.MinimumValue) < 100000 Then
                        numCtrls.Item(iIndex).Increment = 100
                        numCtrls.Item(iIndex).DecimalPlaces = 0
                    End If

                    If (item.MaximumValue - item.MinimumValue) < 1000 Then
                        numCtrls.Item(iIndex).Increment = 2
                        numCtrls.Item(iIndex).DecimalPlaces = 0
                    End If

                    If (item.MaximumValue - item.MinimumValue) < 500 Then
                        numCtrls.Item(iIndex).Increment = 1
                        numCtrls.Item(iIndex).DecimalPlaces = 0
                    End If


                    If (item.MaximumValue - item.MinimumValue) < 100 Then
                        numCtrls.Item(iIndex).Increment = 0.1
                        numCtrls.Item(iIndex).DecimalPlaces = 1
                    End If


                    If (item.MaximumValue - item.MinimumValue) < 10 Then
                        numCtrls.Item(iIndex).Increment = 0.01
                        numCtrls.Item(iIndex).DecimalPlaces = 2
                    End If

                    If (item.MaximumValue - item.MinimumValue) < 6 Then
                        numCtrls.Item(iIndex).Increment = 0.001
                        numCtrls.Item(iIndex).DecimalPlaces = 3
                    End If

                    numCtrls.Item(iIndex).Maximum = item.MaximumValue

                    If (item.Value IsNot Nothing) Then
                        variableValue = item.Value
                    End If

            End Select

            ' scrollbar
            'scrollCtrls.Item(iIndex).Value = variableValue
            'scrollCtrls.Item(iIndex).Visible = True

            ' value control
            valueCtrls.Item(iIndex).Text = variableValue
            valueCtrls.Item(iIndex).Visible = True


            Try
                numCtrls.Item(iIndex).Value = variableValue
                numCtrls.Item(iIndex).Visible = True
            Catch
                numCtrls.Item(iIndex).Visible = False
            End Try

            ' increment the index
            iIndex = iIndex + 1

        End While

    End Sub

    Private Function GetLabelControlsCollection() As List(Of Label)

        ' build a collection of the label controls
        Dim ctrls As List(Of Label) = New List(Of Label)()
        ctrls.Add(lbl1)
        ctrls.Add(lbl2)
        ctrls.Add(lbl3)
        ctrls.Add(lbl4)
        ctrls.Add(lbl5)
        ctrls.Add(lbl6)
        ctrls.Add(lbl7)
        ctrls.Add(lbl8)
        ctrls.Add(lbl9)
        ctrls.Add(lbl10)
        ctrls.Add(lbl11)
        ctrls.Add(lbl12)
        ctrls.Add(lbl13)
        ctrls.Add(lbl14)
        ctrls.Add(lbl15)
        ctrls.Add(lbl16)
        ctrls.Add(lbl17)
        ctrls.Add(lbl18)



        Return ctrls

    End Function

    'Private Function GetScrollControlsCollection() As List(Of HScrollBar)

    '    ' build a collection of the label controls
    '    Dim ctrls As List(Of HScrollBar) = New List(Of HScrollBar)()
    '    ctrls.Add(hScroll1)
    '    ctrls.Add(hScroll2)
    '    ctrls.Add(hScroll3)
    '    ctrls.Add(hScroll4)
    '    ctrls.Add(hScroll5)
    '    ctrls.Add(hScroll6)
    '    ctrls.Add(hScroll7)
    '    ctrls.Add(hScroll8)
    '    ctrls.Add(hScroll9)
    '    ctrls.Add(hScroll10)
    '    ctrls.Add(hScroll11)
    '    ctrls.Add(hScroll12)
    '    ctrls.Add(hScroll13)
    '    ctrls.Add(hScroll14)
    '    ctrls.Add(hScroll15)
    '    ctrls.Add(hScroll16)
    '    ctrls.Add(hScroll17)
    '    ctrls.Add(hScroll18)

    '    Return ctrls

    'End Function

    Private Function GetValueControlsCollection() As List(Of Label)

        ' build a collection of the label controls
        Dim ctrls As List(Of Label) = New List(Of Label)()
        ctrls.Add(lblValue1)
        ctrls.Add(lblValue2)
        ctrls.Add(lblValue3)
        ctrls.Add(lblValue4)
        ctrls.Add(lblValue5)
        ctrls.Add(lblValue6)
        ctrls.Add(lblValue7)
        ctrls.Add(lblValue8)
        ctrls.Add(lblValue9)
        ctrls.Add(lblValue10)
        ctrls.Add(lblValue11)
        ctrls.Add(lblValue12)
        ctrls.Add(lblValue13)
        ctrls.Add(lblValue14)
        ctrls.Add(lblValue15)
        ctrls.Add(lblValue16)
        ctrls.Add(lblValue17)
        ctrls.Add(lblValue18)

        Return ctrls

    End Function

    Private Function GetnumericControlsCollection() As List(Of NumericUpDown)

        ' build a collection of the label controls
        Dim ctrls As List(Of NumericUpDown) = New List(Of NumericUpDown)()
        ctrls.Add(NumericUpDown1)
        ctrls.Add(NumericUpDown2)
        ctrls.Add(NumericUpDown3)
        ctrls.Add(NumericUpDown4)
        ctrls.Add(NumericUpDown5)
        ctrls.Add(NumericUpDown6)
        ctrls.Add(NumericUpDown7)
        ctrls.Add(NumericUpDown8)
        ctrls.Add(NumericUpDown9)
        ctrls.Add(NumericUpDown10)
        ctrls.Add(NumericUpDown11)
        ctrls.Add(NumericUpDown12)
        ctrls.Add(NumericUpDown13)
        ctrls.Add(NumericUpDown14)
        ctrls.Add(NumericUpDown15)
        ctrls.Add(NumericUpDown16)
        ctrls.Add(NumericUpDown17)
        ctrls.Add(NumericUpDown18)

        Return ctrls

    End Function

    Private Sub Skip(ByRef enumerator As IEnumerator, ByVal numberOfItems As Integer)

        If (enumerator Is Nothing) Then
            Return
        End If

        For iIndex As Integer = 1 To numberOfItems
            enumerator.MoveNext()
        Next

    End Sub

#End Region

#Region "Button Events"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        If (m_MainScreen.Variables Is Nothing) Then
            Return
        End If

        '
        ' write the variables
        '

        ' build a collection of the label controls
        Dim labelCtrls As List(Of Label) = GetLabelControlsCollection()
        'Dim scrollCtrls As List(Of HScrollBar) = GetScrollControlsCollection()
        Dim valueCtrls As List(Of Label) = GetValueControlsCollection()
        Dim numCtrls As List(Of NumericUpDown) = GetnumericControlsCollection()


        Dim iIndex As Integer

        ' first hide all of the controls
        For iIndex = 0 To labelCtrls.Count - 1
            labelCtrls.Item(iIndex).Visible = False
            'scrollCtrls.Item(iIndex).Visible = False
            valueCtrls.Item(iIndex).Visible = False
            numCtrls.Item(iIndex).Visible = False
        Next

        ' populate the controls
        Dim enumerator As IEnumerator = m_MainScreen.Variables.GetEnumerator()

        ' make sure that we have more than 5 items in the list
        If (m_MainScreen.Variables.Count < 6) Then
            Return
        End If

        ' skip over the first 5
        Skip(enumerator, 5)

        iIndex = 0
        While (enumerator.MoveNext())

            ' get the item
            Dim item As VariableInfo = CType(enumerator.Current, KeyValuePair(Of String, VariableInfo)).Value

            ' write the variable back to sherlock
            Select Case item.DataType
                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL
                    'm_MainScreen.Sherlock.VarSetBool(item.VariableId, scrollCtrls.Item(iIndex).Value)
                    m_MainScreen.VarSetBool(item.VariableId, numCtrls.Item(iIndex).Value)


                Case IpeEngCtrlLib.I_VAR_TYPE.I_VAR_DOUBLE, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT
                    ' m_MainScreen.Sherlock.VarSetDouble(item.VariableId, scrollCtrls.Item(iIndex).Value)
                    m_MainScreen.VarSetDouble(item.VariableId, numCtrls.Item(iIndex).Value)

            End Select

            ' increment the index
            iIndex = iIndex + 1

        End While

        '' save the settings back to the file
        m_MainScreen.SaveInspection()

        ' update the vars
        m_MainScreen.UpdateAllItems()

        ' close the form
        Me.Close()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    ''Private Sub hScroll1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll1.ValueChanged
    ''    lblValue1.Text = hScroll1.Value.ToString()
    ''End Sub

    'Private Sub hScroll2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged
    '    lblValue2.Text = hScroll2.Value.ToString()
    'End Sub

    'Private Sub hScroll3_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll3.ValueChanged
    '    lblValue3.Text = hScroll3.Value.ToString()
    'End Sub

    'Private Sub hScroll4_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll4.ValueChanged
    '    lblValue4.Text = hScroll4.Value.ToString()
    'End Sub

    'Private Sub hScroll5_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll5.ValueChanged
    '    lblValue5.Text = hScroll5.Value.ToString()
    'End Sub

    'Private Sub hScroll6_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll6.ValueChanged
    '    lblValue6.Text = hScroll6.Value.ToString()
    'End Sub

    'Private Sub hScroll7_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll7.ValueChanged
    '    lblValue7.Text = hScroll7.Value.ToString()
    'End Sub

    'Private Sub hScroll8_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll8.ValueChanged
    '    lblValue8.Text = hScroll8.Value.ToString()
    'End Sub

    'Private Sub hScroll9_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll2.ValueChanged, hScroll9.ValueChanged
    '    lblValue9.Text = hScroll9.Value.ToString()
    'End Sub

    'Private Sub hScroll10_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll10.ValueChanged
    '    lblValue10.Text = hScroll10.Value.ToString()
    'End Sub

    'Private Sub hScroll11_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll11.ValueChanged
    '    lblValue11.Text = hScroll11.Value.ToString()
    'End Sub

    'Private Sub hScroll12_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll12.ValueChanged
    '    lblValue12.Text = hScroll12.Value.ToString()
    'End Sub

    'Private Sub hScroll13_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll13.ValueChanged
    '    lblValue13.Text = hScroll13.Value.ToString()
    'End Sub

    'Private Sub hScroll14_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll14.ValueChanged
    '    lblValue14.Text = hScroll14.Value.ToString()
    'End Sub

    'Private Sub hScroll15_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll15.ValueChanged
    '    lblValue15.Text = hScroll15.Value.ToString()
    'End Sub

    'Private Sub hScroll16_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll16.ValueChanged
    '    lblValue16.Text = hScroll16.Value.ToString()
    'End Sub

    'Private Sub hScroll17_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll17.ValueChanged
    '    lblValue17.Text = hScroll17.Value.ToString()
    'End Sub


    'Private Sub hScroll18_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScroll18.ValueChanged
    '    lblValue18.Text = hScroll18.Value.ToString()
    'End Sub

#End Region

#Region "Properties"

    Public Property MainScreen() As MainForm
        Get
            Return m_MainScreen
        End Get
        Set(ByVal value As MainForm)
            m_MainScreen = value
        End Set
    End Property

#End Region


End Class


