﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If disposing Then
            'If Not (hSherlock Is Nothing) Then 'if Sherlock object exists, destroy it
            '    hSherlock.EngTerminate()
            '    hSherlock = Nothing
            'End If
m_EncoderService.StopEncoder()

            'If Not (m_ipeEng1 Is Nothing) Then
            '    m_ipeEng1.EngTerminate()
            '    m_ipeEng1 = Nothing
            'End If
            'If Not (m_ipeEng2 Is Nothing) Then
            '    m_ipeEng2.EngTerminate()
            '    m_ipeEng2 = Nothing
            'End If
            'If Not (m_ipeEng3 Is Nothing) Then
            '    m_ipeEng3.EngTerminate()
            '    m_ipeEng3 = Nothing
            'End If
            'If Not (m_ipeEng4 Is Nothing) Then
            '    m_ipeEng4.EngTerminate()
            '    m_ipeEng4 = Nothing
            'End If
            'If Not (m_ipeEng5 Is Nothing) Then
            '    m_ipeEng5.EngTerminate()
            '    m_ipeEng5 = Nothing
            'End If
            'If Not (m_ipeEng6 Is Nothing) Then
            '    m_ipeEng6.EngTerminate()
            '    m_ipeEng6 = Nothing
            'End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblResultMs = New System.Windows.Forms.Label()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.picResult = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnRunContinuously = New System.Windows.Forms.Button()
        Me.btnRunOnce = New System.Windows.Forms.Button()
        Me.btnHalt = New System.Windows.Forms.Button()
        Me.frmSettings = New System.Windows.Forms.GroupBox()
        Me.chkLabels = New System.Windows.Forms.CheckBox()
        Me.chkBlackBlobs = New System.Windows.Forms.CheckBox()
        Me.lblMaxArea = New System.Windows.Forms.Label()
        Me.hscrMaxArea = New System.Windows.Forms.HScrollBar()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblMinArea = New System.Windows.Forms.Label()
        Me.hscrMinArea = New System.Windows.Forms.HScrollBar()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblThreshold = New System.Windows.Forms.Label()
        Me.hscrThreshold = New System.Windows.Forms.HScrollBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblMaxReturned = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.hscrMaxReturned = New System.Windows.Forms.HScrollBar()
        Me.frmReadings = New System.Windows.Forms.GroupBox()
        Me.lstAreas = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblCount = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkSaveSettings = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblZoomFactor = New System.Windows.Forms.Label()
        Me.hscrDisplayZoom = New System.Windows.Forms.HScrollBar()
        Me.lblRoleLevel = New System.Windows.Forms.Label()
        Me.lblFormat = New System.Windows.Forms.Label()
        Me.lblHealthy = New System.Windows.Forms.Label()
        Me.lblSwitch = New System.Windows.Forms.Label()
        Me.actionTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblPercentageYield2 = New System.Windows.Forms.Label()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblLowerLimit3 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.lblUpperLimit3 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblPercentageYield3 = New System.Windows.Forms.Label()
        Me.txtEncoderPosition = New System.Windows.Forms.TextBox()
        Me.pnlEncoder = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFifoStatus = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtComparator = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnSystemConfiguration = New System.Windows.Forms.Button()
        Me.btnImages = New System.Windows.Forms.Button()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.btnManual = New System.Windows.Forms.Button()
        Me.btnStatistics = New System.Windows.Forms.Button()
        Me.btnSetup = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnLogout = New System.Windows.Forms.Button()
        Me.btnStartBatch = New System.Windows.Forms.Button()
        Me.btnEndBatch = New System.Windows.Forms.Button()
        Me.btnFormat = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.pnlMainLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.pnlButtons = New System.Windows.Forms.TableLayoutPanel()
        Me.pnlSystemStats = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnlLowerLimit = New System.Windows.Forms.Panel()
        Me.lblLowerLimitLabel = New System.Windows.Forms.Label()
        Me.lblLowerLimit = New System.Windows.Forms.Label()
        Me.pnlYield = New System.Windows.Forms.Panel()
        Me.lblYieldLabel = New System.Windows.Forms.Label()
        Me.lblYield = New System.Windows.Forms.Label()
        Me.pnlUpperLimit = New System.Windows.Forms.Panel()
        Me.lblUpperLimitLabel = New System.Windows.Forms.Label()
        Me.lblUpperLimit = New System.Windows.Forms.Label()
        Me.pnlFrameRate = New System.Windows.Forms.Panel()
        Me.lblFrameRateLabel = New System.Windows.Forms.Label()
        Me.lblFrameRate = New System.Windows.Forms.Label()
        Me.pnlOtherStats = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblWorkOrder = New System.Windows.Forms.Label()
        Me.lblPrintRef = New System.Windows.Forms.Label()
        Me.lblReelId = New System.Windows.Forms.Label()
        Me.lbllaserDiameter = New System.Windows.Forms.Label()
        Me.btnDecreaseFineAdjust = New System.Windows.Forms.Button()
        Me.btnIncreaseFineAdjust = New System.Windows.Forms.Button()
        Me.lblPositionNormal = New System.Windows.Forms.Label()
        Me.lblPositionCorrection = New System.Windows.Forms.Label()
        Me.Panel3.SuspendLayout()
        CType(Me.picResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.frmSettings.SuspendLayout()
        Me.frmReadings.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.pnlEncoder.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSystemStats.SuspendLayout()
        Me.pnlLowerLimit.SuspendLayout()
        Me.pnlYield.SuspendLayout()
        Me.pnlUpperLimit.SuspendLayout()
        Me.pnlFrameRate.SuspendLayout()
        Me.pnlOtherStats.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(307, 98)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(104, 40)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(8, 60)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 25)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Result"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Black
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.lblResultMs)
        Me.Panel3.Controls.Add(Me.lblResult)
        Me.Panel3.Controls.Add(Me.picResult)
        Me.Panel3.Location = New System.Drawing.Point(1716, 139)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 154)
        Me.Panel3.TabIndex = 29
        '
        'lblResultMs
        '
        Me.lblResultMs.AutoSize = True
        Me.lblResultMs.BackColor = System.Drawing.Color.Transparent
        Me.lblResultMs.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!)
        Me.lblResultMs.ForeColor = System.Drawing.Color.White
        Me.lblResultMs.Location = New System.Drawing.Point(89, 31)
        Me.lblResultMs.Name = "lblResultMs"
        Me.lblResultMs.Size = New System.Drawing.Size(0, 25)
        Me.lblResultMs.TabIndex = 32
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.BackColor = System.Drawing.Color.Transparent
        Me.lblResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.ForeColor = System.Drawing.Color.White
        Me.lblResult.Location = New System.Drawing.Point(0, 76)
        Me.lblResult.Margin = New System.Windows.Forms.Padding(0)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(139, 73)
        Me.lblResult.TabIndex = 31
        Me.lblResult.Text = "N/A"
        '
        'picResult
        '
        Me.picResult.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Star_white
        Me.picResult.Location = New System.Drawing.Point(12, 7)
        Me.picResult.Margin = New System.Windows.Forms.Padding(0)
        Me.picResult.Name = "picResult"
        Me.picResult.Size = New System.Drawing.Size(50, 48)
        Me.picResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picResult.TabIndex = 30
        Me.picResult.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.btnRunContinuously)
        Me.Panel1.Controls.Add(Me.btnRunOnce)
        Me.Panel1.Controls.Add(Me.btnHalt)
        Me.Panel1.Controls.Add(Me.frmSettings)
        Me.Panel1.Controls.Add(Me.frmReadings)
        Me.Panel1.Controls.Add(Me.chkSaveSettings)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.btnExit)
        Me.Panel1.Location = New System.Drawing.Point(1719, 765)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(177, 121)
        Me.Panel1.TabIndex = 18
        Me.Panel1.Visible = False
        '
        'btnRunContinuously
        '
        Me.btnRunContinuously.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnRunContinuously.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunContinuously.Location = New System.Drawing.Point(5, 42)
        Me.btnRunContinuously.Name = "btnRunContinuously"
        Me.btnRunContinuously.Size = New System.Drawing.Size(168, 40)
        Me.btnRunContinuously.TabIndex = 2
        Me.btnRunContinuously.Text = "Run Continuously"
        Me.btnRunContinuously.UseVisualStyleBackColor = False
        '
        'btnRunOnce
        '
        Me.btnRunOnce.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnRunOnce.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunOnce.Location = New System.Drawing.Point(5, 3)
        Me.btnRunOnce.Name = "btnRunOnce"
        Me.btnRunOnce.Size = New System.Drawing.Size(112, 40)
        Me.btnRunOnce.TabIndex = 1
        Me.btnRunOnce.Text = "Run Once"
        Me.btnRunOnce.UseVisualStyleBackColor = False
        '
        'btnHalt
        '
        Me.btnHalt.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnHalt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHalt.Location = New System.Drawing.Point(5, 81)
        Me.btnHalt.Name = "btnHalt"
        Me.btnHalt.Size = New System.Drawing.Size(104, 40)
        Me.btnHalt.TabIndex = 3
        Me.btnHalt.Text = "Halt"
        Me.btnHalt.UseVisualStyleBackColor = False
        '
        'frmSettings
        '
        Me.frmSettings.Controls.Add(Me.chkLabels)
        Me.frmSettings.Controls.Add(Me.chkBlackBlobs)
        Me.frmSettings.Controls.Add(Me.lblMaxArea)
        Me.frmSettings.Controls.Add(Me.hscrMaxArea)
        Me.frmSettings.Controls.Add(Me.Label5)
        Me.frmSettings.Controls.Add(Me.lblMinArea)
        Me.frmSettings.Controls.Add(Me.hscrMinArea)
        Me.frmSettings.Controls.Add(Me.Label3)
        Me.frmSettings.Controls.Add(Me.lblThreshold)
        Me.frmSettings.Controls.Add(Me.hscrThreshold)
        Me.frmSettings.Controls.Add(Me.Label2)
        Me.frmSettings.Controls.Add(Me.lblMaxReturned)
        Me.frmSettings.Controls.Add(Me.Label1)
        Me.frmSettings.Controls.Add(Me.hscrMaxReturned)
        Me.frmSettings.Location = New System.Drawing.Point(5, 144)
        Me.frmSettings.Name = "frmSettings"
        Me.frmSettings.Size = New System.Drawing.Size(648, 96)
        Me.frmSettings.TabIndex = 5
        Me.frmSettings.TabStop = False
        Me.frmSettings.Text = "Connectivity settings"
        '
        'chkLabels
        '
        Me.chkLabels.AutoSize = True
        Me.chkLabels.Location = New System.Drawing.Point(554, 56)
        Me.chkLabels.Name = "chkLabels"
        Me.chkLabels.Size = New System.Drawing.Size(89, 17)
        Me.chkLabels.TabIndex = 13
        Me.chkLabels.Text = "Label objects"
        Me.chkLabels.UseVisualStyleBackColor = True
        '
        'chkBlackBlobs
        '
        Me.chkBlackBlobs.AutoSize = True
        Me.chkBlackBlobs.Location = New System.Drawing.Point(552, 32)
        Me.chkBlackBlobs.Name = "chkBlackBlobs"
        Me.chkBlackBlobs.Size = New System.Drawing.Size(90, 17)
        Me.chkBlackBlobs.TabIndex = 12
        Me.chkBlackBlobs.Text = "Black objects"
        Me.chkBlackBlobs.UseVisualStyleBackColor = True
        '
        'lblMaxArea
        '
        Me.lblMaxArea.BackColor = System.Drawing.Color.White
        Me.lblMaxArea.Location = New System.Drawing.Point(489, 58)
        Me.lblMaxArea.Name = "lblMaxArea"
        Me.lblMaxArea.Size = New System.Drawing.Size(55, 14)
        Me.lblMaxArea.TabIndex = 11
        Me.lblMaxArea.Text = "MaxArea"
        '
        'hscrMaxArea
        '
        Me.hscrMaxArea.LargeChange = 100
        Me.hscrMaxArea.Location = New System.Drawing.Point(398, 56)
        Me.hscrMaxArea.Maximum = 100000
        Me.hscrMaxArea.Minimum = 1
        Me.hscrMaxArea.Name = "hscrMaxArea"
        Me.hscrMaxArea.Size = New System.Drawing.Size(88, 16)
        Me.hscrMaxArea.TabIndex = 10
        Me.hscrMaxArea.Value = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(289, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Maximum object area"
        '
        'lblMinArea
        '
        Me.lblMinArea.BackColor = System.Drawing.Color.White
        Me.lblMinArea.Location = New System.Drawing.Point(488, 32)
        Me.lblMinArea.Name = "lblMinArea"
        Me.lblMinArea.Size = New System.Drawing.Size(56, 16)
        Me.lblMinArea.TabIndex = 8
        Me.lblMinArea.Text = "MinArea"
        '
        'hscrMinArea
        '
        Me.hscrMinArea.LargeChange = 100
        Me.hscrMinArea.Location = New System.Drawing.Point(397, 30)
        Me.hscrMinArea.Maximum = 100000
        Me.hscrMinArea.Minimum = 1
        Me.hscrMinArea.Name = "hscrMinArea"
        Me.hscrMinArea.Size = New System.Drawing.Size(88, 16)
        Me.hscrMinArea.TabIndex = 7
        Me.hscrMinArea.Value = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(290, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Minimum object area"
        '
        'lblThreshold
        '
        Me.lblThreshold.BackColor = System.Drawing.Color.White
        Me.lblThreshold.Location = New System.Drawing.Point(204, 57)
        Me.lblThreshold.Name = "lblThreshold"
        Me.lblThreshold.Size = New System.Drawing.Size(60, 15)
        Me.lblThreshold.TabIndex = 5
        Me.lblThreshold.Text = "Threshold"
        '
        'hscrThreshold
        '
        Me.hscrThreshold.Location = New System.Drawing.Point(112, 56)
        Me.hscrThreshold.Maximum = 255
        Me.hscrThreshold.Name = "hscrThreshold"
        Me.hscrThreshold.Size = New System.Drawing.Size(88, 16)
        Me.hscrThreshold.TabIndex = 4
        Me.hscrThreshold.Value = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(54, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Threshold"
        '
        'lblMaxReturned
        '
        Me.lblMaxReturned.BackColor = System.Drawing.Color.White
        Me.lblMaxReturned.Location = New System.Drawing.Point(204, 33)
        Me.lblMaxReturned.Name = "lblMaxReturned"
        Me.lblMaxReturned.Size = New System.Drawing.Size(60, 15)
        Me.lblMaxReturned.TabIndex = 2
        Me.lblMaxReturned.Text = "MaxReturned"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 26)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Maximum number of " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "objects to measure"
        '
        'hscrMaxReturned
        '
        Me.hscrMaxReturned.Location = New System.Drawing.Point(112, 30)
        Me.hscrMaxReturned.Maximum = 1000
        Me.hscrMaxReturned.Minimum = 1
        Me.hscrMaxReturned.Name = "hscrMaxReturned"
        Me.hscrMaxReturned.Size = New System.Drawing.Size(88, 18)
        Me.hscrMaxReturned.TabIndex = 0
        Me.hscrMaxReturned.Value = 1
        '
        'frmReadings
        '
        Me.frmReadings.Controls.Add(Me.lstAreas)
        Me.frmReadings.Controls.Add(Me.Label6)
        Me.frmReadings.Controls.Add(Me.lblCount)
        Me.frmReadings.Controls.Add(Me.Label4)
        Me.frmReadings.Location = New System.Drawing.Point(5, 272)
        Me.frmReadings.Name = "frmReadings"
        Me.frmReadings.Size = New System.Drawing.Size(352, 96)
        Me.frmReadings.TabIndex = 6
        Me.frmReadings.TabStop = False
        Me.frmReadings.Text = "Connectivity readings"
        '
        'lstAreas
        '
        Me.lstAreas.FormattingEnabled = True
        Me.lstAreas.Location = New System.Drawing.Point(244, 24)
        Me.lstAreas.Name = "lstAreas"
        Me.lstAreas.Size = New System.Drawing.Size(98, 56)
        Me.lstAreas.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(174, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Object areas"
        '
        'lblCount
        '
        Me.lblCount.BackColor = System.Drawing.Color.White
        Me.lblCount.Location = New System.Drawing.Point(88, 24)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(64, 16)
        Me.lblCount.TabIndex = 1
        Me.lblCount.Text = "Count"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Object count"
        '
        'chkSaveSettings
        '
        Me.chkSaveSettings.AutoSize = True
        Me.chkSaveSettings.Location = New System.Drawing.Point(24, 249)
        Me.chkSaveSettings.Name = "chkSaveSettings"
        Me.chkSaveSettings.Size = New System.Drawing.Size(90, 17)
        Me.chkSaveSettings.TabIndex = 7
        Me.chkSaveSettings.Text = "Save settings"
        Me.chkSaveSettings.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblZoomFactor)
        Me.GroupBox1.Controls.Add(Me.hscrDisplayZoom)
        Me.GroupBox1.Location = New System.Drawing.Point(372, 278)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(125, 89)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Display zoom"
        '
        'lblZoomFactor
        '
        Me.lblZoomFactor.BackColor = System.Drawing.Color.White
        Me.lblZoomFactor.Location = New System.Drawing.Point(34, 54)
        Me.lblZoomFactor.Name = "lblZoomFactor"
        Me.lblZoomFactor.Size = New System.Drawing.Size(55, 14)
        Me.lblZoomFactor.TabIndex = 1
        Me.lblZoomFactor.Text = "0"
        '
        'hscrDisplayZoom
        '
        Me.hscrDisplayZoom.Location = New System.Drawing.Point(14, 26)
        Me.hscrDisplayZoom.Maximum = 40
        Me.hscrDisplayZoom.Name = "hscrDisplayZoom"
        Me.hscrDisplayZoom.Size = New System.Drawing.Size(88, 16)
        Me.hscrDisplayZoom.TabIndex = 0
        '
        'lblRoleLevel
        '
        Me.lblRoleLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRoleLevel.AutoSize = True
        Me.lblRoleLevel.BackColor = System.Drawing.Color.FromArgb(CType(CType(171, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.lblRoleLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoleLevel.ForeColor = System.Drawing.Color.White
        Me.lblRoleLevel.Location = New System.Drawing.Point(1725, 110)
        Me.lblRoleLevel.Name = "lblRoleLevel"
        Me.lblRoleLevel.Size = New System.Drawing.Size(121, 18)
        Me.lblRoleLevel.TabIndex = 33
        Me.lblRoleLevel.Text = "Level: Contractor"
        '
        'lblFormat
        '
        Me.lblFormat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFormat.BackColor = System.Drawing.Color.FromArgb(CType(CType(171, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.lblFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormat.ForeColor = System.Drawing.Color.White
        Me.lblFormat.Location = New System.Drawing.Point(1716, 599)
        Me.lblFormat.Name = "lblFormat"
        Me.lblFormat.Size = New System.Drawing.Size(200, 44)
        Me.lblFormat.TabIndex = 40
        Me.lblFormat.Text = "Format"
        Me.lblFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblHealthy
        '
        Me.lblHealthy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHealthy.BackColor = System.Drawing.Color.Green
        Me.lblHealthy.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHealthy.ForeColor = System.Drawing.Color.White
        Me.lblHealthy.Location = New System.Drawing.Point(1716, 948)
        Me.lblHealthy.Name = "lblHealthy"
        Me.lblHealthy.Size = New System.Drawing.Size(200, 44)
        Me.lblHealthy.TabIndex = 41
        Me.lblHealthy.Text = "Healthy"
        Me.lblHealthy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblHealthy.Visible = False
        '
        'lblSwitch
        '
        Me.lblSwitch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSwitch.BackColor = System.Drawing.Color.Green
        Me.lblSwitch.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSwitch.ForeColor = System.Drawing.Color.White
        Me.lblSwitch.Location = New System.Drawing.Point(1716, 893)
        Me.lblSwitch.Name = "lblSwitch"
        Me.lblSwitch.Size = New System.Drawing.Size(200, 44)
        Me.lblSwitch.TabIndex = 42
        Me.lblSwitch.Text = "Switch"
        Me.lblSwitch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblSwitch.Visible = False
        '
        'actionTimer
        '
        Me.actionTimer.Interval = 2500
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(546, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(597, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 5
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(648, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 13)
        Me.Label13.TabIndex = 6
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Black
        Me.Label23.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(0, 65)
        Me.Label23.Name = "Label23"
        Me.Label23.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.Label23.Size = New System.Drawing.Size(126, 35)
        Me.Label23.TabIndex = 35
        Me.Label23.Text = "Upper Limit"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Black
        Me.Panel8.Controls.Add(Me.Label15)
        Me.Panel8.Controls.Add(Me.lblPercentageYield2)
        Me.Panel8.Location = New System.Drawing.Point(3, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(175, 125)
        Me.Panel8.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Black
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(0, 65)
        Me.Label15.Name = "Label15"
        Me.Label15.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.Label15.Size = New System.Drawing.Size(91, 35)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "% Yield"
        '
        'lblPercentageYield2
        '
        Me.lblPercentageYield2.AutoSize = True
        Me.lblPercentageYield2.BackColor = System.Drawing.Color.Black
        Me.lblPercentageYield2.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblPercentageYield2.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentageYield2.ForeColor = System.Drawing.Color.White
        Me.lblPercentageYield2.Location = New System.Drawing.Point(0, 0)
        Me.lblPercentageYield2.Name = "lblPercentageYield2"
        Me.lblPercentageYield2.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblPercentageYield2.Size = New System.Drawing.Size(52, 65)
        Me.lblPercentageYield2.TabIndex = 34
        Me.lblPercentageYield2.Text = "0"
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.Black
        Me.Panel14.Controls.Add(Me.Label26)
        Me.Panel14.Controls.Add(Me.lblLowerLimit3)
        Me.Panel14.Location = New System.Drawing.Point(3, 3)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(175, 125)
        Me.Panel14.TabIndex = 3
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Black
        Me.Label26.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(0, 65)
        Me.Label26.Name = "Label26"
        Me.Label26.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.Label26.Size = New System.Drawing.Size(126, 35)
        Me.Label26.TabIndex = 35
        Me.Label26.Text = "Lower Limit"
        '
        'lblLowerLimit3
        '
        Me.lblLowerLimit3.AutoSize = True
        Me.lblLowerLimit3.BackColor = System.Drawing.Color.Black
        Me.lblLowerLimit3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblLowerLimit3.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLowerLimit3.ForeColor = System.Drawing.Color.White
        Me.lblLowerLimit3.Location = New System.Drawing.Point(0, 0)
        Me.lblLowerLimit3.Name = "lblLowerLimit3"
        Me.lblLowerLimit3.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblLowerLimit3.Size = New System.Drawing.Size(52, 65)
        Me.lblLowerLimit3.TabIndex = 34
        Me.lblLowerLimit3.Text = "0"
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.Black
        Me.Panel13.Controls.Add(Me.Label23)
        Me.Panel13.Controls.Add(Me.lblUpperLimit3)
        Me.Panel13.Location = New System.Drawing.Point(184, 3)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(175, 125)
        Me.Panel13.TabIndex = 2
        '
        'lblUpperLimit3
        '
        Me.lblUpperLimit3.AutoSize = True
        Me.lblUpperLimit3.BackColor = System.Drawing.Color.Black
        Me.lblUpperLimit3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblUpperLimit3.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpperLimit3.ForeColor = System.Drawing.Color.White
        Me.lblUpperLimit3.Location = New System.Drawing.Point(0, 0)
        Me.lblUpperLimit3.Name = "lblUpperLimit3"
        Me.lblUpperLimit3.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblUpperLimit3.Size = New System.Drawing.Size(52, 65)
        Me.lblUpperLimit3.TabIndex = 34
        Me.lblUpperLimit3.Text = "0"
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Black
        Me.Panel12.Controls.Add(Me.Label19)
        Me.Panel12.Controls.Add(Me.lblPercentageYield3)
        Me.Panel12.Location = New System.Drawing.Point(3, 3)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(175, 125)
        Me.Panel12.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Black
        Me.Label19.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(0, 65)
        Me.Label19.Name = "Label19"
        Me.Label19.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.Label19.Size = New System.Drawing.Size(91, 35)
        Me.Label19.TabIndex = 35
        Me.Label19.Text = "% Yield"
        '
        'lblPercentageYield3
        '
        Me.lblPercentageYield3.AutoSize = True
        Me.lblPercentageYield3.BackColor = System.Drawing.Color.Black
        Me.lblPercentageYield3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblPercentageYield3.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentageYield3.ForeColor = System.Drawing.Color.White
        Me.lblPercentageYield3.Location = New System.Drawing.Point(0, 0)
        Me.lblPercentageYield3.Name = "lblPercentageYield3"
        Me.lblPercentageYield3.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblPercentageYield3.Size = New System.Drawing.Size(52, 65)
        Me.lblPercentageYield3.TabIndex = 34
        Me.lblPercentageYield3.Text = "0"
        '
        'txtEncoderPosition
        '
        Me.txtEncoderPosition.Location = New System.Drawing.Point(81, 3)
        Me.txtEncoderPosition.Name = "txtEncoderPosition"
        Me.txtEncoderPosition.Size = New System.Drawing.Size(116, 20)
        Me.txtEncoderPosition.TabIndex = 51
        '
        'pnlEncoder
        '
        Me.pnlEncoder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEncoder.Controls.Add(Me.Label10)
        Me.pnlEncoder.Controls.Add(Me.txtFifoStatus)
        Me.pnlEncoder.Controls.Add(Me.Label9)
        Me.pnlEncoder.Controls.Add(Me.txtComparator)
        Me.pnlEncoder.Controls.Add(Me.Label8)
        Me.pnlEncoder.Controls.Add(Me.txtEncoderPosition)
        Me.pnlEncoder.Location = New System.Drawing.Point(1716, 646)
        Me.pnlEncoder.Name = "pnlEncoder"
        Me.pnlEncoder.Size = New System.Drawing.Size(200, 113)
        Me.pnlEncoder.TabIndex = 52
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 13)
        Me.Label10.TabIndex = 56
        Me.Label10.Text = "Fifo Status"
        '
        'txtFifoStatus
        '
        Me.txtFifoStatus.Location = New System.Drawing.Point(81, 55)
        Me.txtFifoStatus.Name = "txtFifoStatus"
        Me.txtFifoStatus.Size = New System.Drawing.Size(116, 20)
        Me.txtFifoStatus.TabIndex = 55
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "Comparator"
        '
        'txtComparator
        '
        Me.txtComparator.Location = New System.Drawing.Point(81, 29)
        Me.txtComparator.Name = "txtComparator"
        Me.txtComparator.Size = New System.Drawing.Size(116, 20)
        Me.txtComparator.TabIndex = 53
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 13)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "Encoder Pos."
        '
        'btnSystemConfiguration
        '
        Me.btnSystemConfiguration.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnSystemConfiguration.FlatAppearance.BorderSize = 0
        Me.btnSystemConfiguration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSystemConfiguration.Image = Global.VB2005_Sherlock7.My.Resources.Resources.System_config
        Me.btnSystemConfiguration.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnSystemConfiguration.Location = New System.Drawing.Point(134, 2)
        Me.btnSystemConfiguration.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSystemConfiguration.Name = "btnSystemConfiguration"
        Me.btnSystemConfiguration.Size = New System.Drawing.Size(208, 137)
        Me.btnSystemConfiguration.TabIndex = 39
        Me.btnSystemConfiguration.UseVisualStyleBackColor = True
        '
        'btnImages
        '
        Me.btnImages.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImages.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnImages.FlatAppearance.BorderSize = 0
        Me.btnImages.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImages.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Images
        Me.btnImages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImages.Location = New System.Drawing.Point(1711, 291)
        Me.btnImages.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImages.Name = "btnImages"
        Me.btnImages.Size = New System.Drawing.Size(212, 106)
        Me.btnImages.TabIndex = 27
        Me.btnImages.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnLogin.FlatAppearance.BorderSize = 0
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Login
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnLogin.Location = New System.Drawing.Point(608, 2)
        Me.btnLogin.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(208, 137)
        Me.btnLogin.TabIndex = 15
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'btnManual
        '
        Me.btnManual.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnManual.FlatAppearance.BorderSize = 0
        Me.btnManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManual.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Manual
        Me.btnManual.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnManual.Location = New System.Drawing.Point(474, 2)
        Me.btnManual.Margin = New System.Windows.Forms.Padding(0)
        Me.btnManual.Name = "btnManual"
        Me.btnManual.Size = New System.Drawing.Size(138, 137)
        Me.btnManual.TabIndex = 12
        Me.btnManual.UseVisualStyleBackColor = True
        '
        'btnStatistics
        '
        Me.btnStatistics.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnStatistics.FlatAppearance.BorderSize = 0
        Me.btnStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStatistics.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Stats
        Me.btnStatistics.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnStatistics.Location = New System.Drawing.Point(340, 2)
        Me.btnStatistics.Margin = New System.Windows.Forms.Padding(0)
        Me.btnStatistics.Name = "btnStatistics"
        Me.btnStatistics.Size = New System.Drawing.Size(137, 137)
        Me.btnStatistics.TabIndex = 11
        Me.btnStatistics.UseVisualStyleBackColor = True
        '
        'btnSetup
        '
        Me.btnSetup.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnSetup.FlatAppearance.BorderSize = 0
        Me.btnSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetup.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Setup
        Me.btnSetup.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnSetup.Location = New System.Drawing.Point(0, 2)
        Me.btnSetup.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSetup.Name = "btnSetup"
        Me.btnSetup.Size = New System.Drawing.Size(134, 137)
        Me.btnSetup.TabIndex = 10
        Me.btnSetup.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Location = New System.Drawing.Point(1716, 1145)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(171, 69)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'btnLogout
        '
        Me.btnLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.Transparent
        Me.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnLogout.FlatAppearance.BorderSize = 0
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Logout
        Me.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogout.Location = New System.Drawing.Point(1711, 2)
        Me.btnLogout.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(212, 137)
        Me.btnLogout.TabIndex = 14
        Me.btnLogout.UseVisualStyleBackColor = False
        '
        'btnStartBatch
        '
        Me.btnStartBatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStartBatch.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnStartBatch.FlatAppearance.BorderSize = 0
        Me.btnStartBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStartBatch.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Start_Batch
        Me.btnStartBatch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStartBatch.Location = New System.Drawing.Point(1711, 392)
        Me.btnStartBatch.Margin = New System.Windows.Forms.Padding(0)
        Me.btnStartBatch.Name = "btnStartBatch"
        Me.btnStartBatch.Size = New System.Drawing.Size(212, 106)
        Me.btnStartBatch.TabIndex = 47
        Me.btnStartBatch.UseVisualStyleBackColor = True
        '
        'btnEndBatch
        '
        Me.btnEndBatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEndBatch.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnEndBatch.FlatAppearance.BorderSize = 0
        Me.btnEndBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEndBatch.Image = Global.VB2005_Sherlock7.My.Resources.Resources.End_Batch
        Me.btnEndBatch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEndBatch.Location = New System.Drawing.Point(1711, 392)
        Me.btnEndBatch.Margin = New System.Windows.Forms.Padding(0)
        Me.btnEndBatch.Name = "btnEndBatch"
        Me.btnEndBatch.Size = New System.Drawing.Size(212, 106)
        Me.btnEndBatch.TabIndex = 48
        Me.btnEndBatch.UseVisualStyleBackColor = True
        Me.btnEndBatch.Visible = False
        '
        'btnFormat
        '
        Me.btnFormat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFormat.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnFormat.FlatAppearance.BorderSize = 0
        Me.btnFormat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFormat.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Format
        Me.btnFormat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFormat.Location = New System.Drawing.Point(1711, 493)
        Me.btnFormat.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFormat.Name = "btnFormat"
        Me.btnFormat.Size = New System.Drawing.Size(212, 106)
        Me.btnFormat.TabIndex = 26
        Me.btnFormat.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(893, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Label17"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(689, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(45, 13)
        Me.Label24.TabIndex = 10
        Me.Label24.Text = "Label24"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(638, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(45, 13)
        Me.Label25.TabIndex = 11
        Me.Label25.Text = "Label25"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(546, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(86, 13)
        Me.Label27.TabIndex = 12
        Me.Label27.Text = "LabelPage1Batc"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(3, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(45, 13)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Label21"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(3, 13)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(45, 13)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Label22"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 26)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 2
        Me.Label28.Text = "Label28"
        '
        'pnlMainLayout
        '
        Me.pnlMainLayout.ColumnCount = 1
        Me.pnlMainLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.pnlMainLayout.Location = New System.Drawing.Point(2, 284)
        Me.pnlMainLayout.Name = "pnlMainLayout"
        Me.pnlMainLayout.RowCount = 3
        Me.pnlMainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlMainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlMainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.pnlMainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.pnlMainLayout.Size = New System.Drawing.Size(1688, 747)
        Me.pnlMainLayout.TabIndex = 53
        '
        'pnlButtons
        '
        Me.pnlButtons.ColumnCount = 1
        Me.pnlButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.pnlButtons.Location = New System.Drawing.Point(2, 139)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.RowCount = 3
        Me.pnlButtons.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.pnlButtons.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me.pnlButtons.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.pnlButtons.Size = New System.Drawing.Size(1688, 86)
        Me.pnlButtons.TabIndex = 54
        '
        'pnlSystemStats
        '
        Me.pnlSystemStats.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSystemStats.Controls.Add(Me.pnlLowerLimit)
        Me.pnlSystemStats.Controls.Add(Me.pnlYield)
        Me.pnlSystemStats.Controls.Add(Me.pnlUpperLimit)
        Me.pnlSystemStats.Controls.Add(Me.pnlFrameRate)
        Me.pnlSystemStats.Controls.Add(Me.pnlOtherStats)
        Me.pnlSystemStats.Location = New System.Drawing.Point(2, 1061)
        Me.pnlSystemStats.Name = "pnlSystemStats"
        Me.pnlSystemStats.Size = New System.Drawing.Size(1697, 147)
        Me.pnlSystemStats.TabIndex = 55
        Me.pnlSystemStats.WrapContents = False
        '
        'pnlLowerLimit
        '
        Me.pnlLowerLimit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlLowerLimit.BackColor = System.Drawing.Color.Black
        Me.pnlLowerLimit.Controls.Add(Me.lblLowerLimitLabel)
        Me.pnlLowerLimit.Controls.Add(Me.lblLowerLimit)
        Me.pnlLowerLimit.Location = New System.Drawing.Point(3, 3)
        Me.pnlLowerLimit.Name = "pnlLowerLimit"
        Me.pnlLowerLimit.Size = New System.Drawing.Size(175, 125)
        Me.pnlLowerLimit.TabIndex = 3
        '
        'lblLowerLimitLabel
        '
        Me.lblLowerLimitLabel.AutoSize = True
        Me.lblLowerLimitLabel.BackColor = System.Drawing.Color.Black
        Me.lblLowerLimitLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblLowerLimitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLowerLimitLabel.ForeColor = System.Drawing.Color.White
        Me.lblLowerLimitLabel.Location = New System.Drawing.Point(0, 65)
        Me.lblLowerLimitLabel.Name = "lblLowerLimitLabel"
        Me.lblLowerLimitLabel.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.lblLowerLimitLabel.Size = New System.Drawing.Size(126, 35)
        Me.lblLowerLimitLabel.TabIndex = 35
        Me.lblLowerLimitLabel.Text = "Lower Limit"
        '
        'lblLowerLimit
        '
        Me.lblLowerLimit.AutoSize = True
        Me.lblLowerLimit.BackColor = System.Drawing.Color.Black
        Me.lblLowerLimit.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblLowerLimit.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLowerLimit.ForeColor = System.Drawing.Color.White
        Me.lblLowerLimit.Location = New System.Drawing.Point(0, 0)
        Me.lblLowerLimit.Name = "lblLowerLimit"
        Me.lblLowerLimit.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblLowerLimit.Size = New System.Drawing.Size(52, 65)
        Me.lblLowerLimit.TabIndex = 34
        Me.lblLowerLimit.Text = "0"
        '
        'pnlYield
        '
        Me.pnlYield.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlYield.BackColor = System.Drawing.Color.Black
        Me.pnlYield.Controls.Add(Me.lblYieldLabel)
        Me.pnlYield.Controls.Add(Me.lblYield)
        Me.pnlYield.Location = New System.Drawing.Point(184, 3)
        Me.pnlYield.Name = "pnlYield"
        Me.pnlYield.Size = New System.Drawing.Size(175, 125)
        Me.pnlYield.TabIndex = 36
        '
        'lblYieldLabel
        '
        Me.lblYieldLabel.AutoSize = True
        Me.lblYieldLabel.BackColor = System.Drawing.Color.Black
        Me.lblYieldLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblYieldLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYieldLabel.ForeColor = System.Drawing.Color.White
        Me.lblYieldLabel.Location = New System.Drawing.Point(0, 65)
        Me.lblYieldLabel.Name = "lblYieldLabel"
        Me.lblYieldLabel.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.lblYieldLabel.Size = New System.Drawing.Size(66, 35)
        Me.lblYieldLabel.TabIndex = 35
        Me.lblYieldLabel.Text = "Yield"
        '
        'lblYield
        '
        Me.lblYield.AutoSize = True
        Me.lblYield.BackColor = System.Drawing.Color.Black
        Me.lblYield.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblYield.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYield.ForeColor = System.Drawing.Color.White
        Me.lblYield.Location = New System.Drawing.Point(0, 0)
        Me.lblYield.Name = "lblYield"
        Me.lblYield.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblYield.Size = New System.Drawing.Size(96, 65)
        Me.lblYield.TabIndex = 34
        Me.lblYield.Text = "0%"
        '
        'pnlUpperLimit
        '
        Me.pnlUpperLimit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlUpperLimit.BackColor = System.Drawing.Color.Black
        Me.pnlUpperLimit.Controls.Add(Me.lblUpperLimitLabel)
        Me.pnlUpperLimit.Controls.Add(Me.lblUpperLimit)
        Me.pnlUpperLimit.Location = New System.Drawing.Point(365, 3)
        Me.pnlUpperLimit.Name = "pnlUpperLimit"
        Me.pnlUpperLimit.Size = New System.Drawing.Size(175, 125)
        Me.pnlUpperLimit.TabIndex = 2
        '
        'lblUpperLimitLabel
        '
        Me.lblUpperLimitLabel.AutoSize = True
        Me.lblUpperLimitLabel.BackColor = System.Drawing.Color.Black
        Me.lblUpperLimitLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblUpperLimitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpperLimitLabel.ForeColor = System.Drawing.Color.White
        Me.lblUpperLimitLabel.Location = New System.Drawing.Point(0, 65)
        Me.lblUpperLimitLabel.Name = "lblUpperLimitLabel"
        Me.lblUpperLimitLabel.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.lblUpperLimitLabel.Size = New System.Drawing.Size(126, 35)
        Me.lblUpperLimitLabel.TabIndex = 35
        Me.lblUpperLimitLabel.Text = "Upper Limit"
        '
        'lblUpperLimit
        '
        Me.lblUpperLimit.AutoSize = True
        Me.lblUpperLimit.BackColor = System.Drawing.Color.Black
        Me.lblUpperLimit.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblUpperLimit.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpperLimit.ForeColor = System.Drawing.Color.White
        Me.lblUpperLimit.Location = New System.Drawing.Point(0, 0)
        Me.lblUpperLimit.Name = "lblUpperLimit"
        Me.lblUpperLimit.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblUpperLimit.Size = New System.Drawing.Size(52, 65)
        Me.lblUpperLimit.TabIndex = 34
        Me.lblUpperLimit.Text = "0"
        '
        'pnlFrameRate
        '
        Me.pnlFrameRate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlFrameRate.BackColor = System.Drawing.Color.Black
        Me.pnlFrameRate.Controls.Add(Me.lblFrameRateLabel)
        Me.pnlFrameRate.Controls.Add(Me.lblFrameRate)
        Me.pnlFrameRate.Location = New System.Drawing.Point(546, 3)
        Me.pnlFrameRate.Name = "pnlFrameRate"
        Me.pnlFrameRate.Size = New System.Drawing.Size(175, 125)
        Me.pnlFrameRate.TabIndex = 0
        '
        'lblFrameRateLabel
        '
        Me.lblFrameRateLabel.AutoSize = True
        Me.lblFrameRateLabel.BackColor = System.Drawing.Color.Black
        Me.lblFrameRateLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblFrameRateLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFrameRateLabel.ForeColor = System.Drawing.Color.White
        Me.lblFrameRateLabel.Location = New System.Drawing.Point(0, 65)
        Me.lblFrameRateLabel.Name = "lblFrameRateLabel"
        Me.lblFrameRateLabel.Padding = New System.Windows.Forms.Padding(5, 0, 0, 10)
        Me.lblFrameRateLabel.Size = New System.Drawing.Size(135, 35)
        Me.lblFrameRateLabel.TabIndex = 35
        Me.lblFrameRateLabel.Text = "Frames/sec."
        '
        'lblFrameRate
        '
        Me.lblFrameRate.AutoSize = True
        Me.lblFrameRate.BackColor = System.Drawing.Color.Black
        Me.lblFrameRate.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblFrameRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFrameRate.ForeColor = System.Drawing.Color.White
        Me.lblFrameRate.Location = New System.Drawing.Point(0, 0)
        Me.lblFrameRate.Name = "lblFrameRate"
        Me.lblFrameRate.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.lblFrameRate.Size = New System.Drawing.Size(52, 65)
        Me.lblFrameRate.TabIndex = 34
        Me.lblFrameRate.Text = "0"
        '
        'pnlOtherStats
        '
        Me.pnlOtherStats.Controls.Add(Me.lblWorkOrder)
        Me.pnlOtherStats.Controls.Add(Me.lblPrintRef)
        Me.pnlOtherStats.Controls.Add(Me.lblReelId)
        Me.pnlOtherStats.Controls.Add(Me.lbllaserDiameter)
        Me.pnlOtherStats.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.pnlOtherStats.Location = New System.Drawing.Point(727, 3)
        Me.pnlOtherStats.Name = "pnlOtherStats"
        Me.pnlOtherStats.Size = New System.Drawing.Size(200, 125)
        Me.pnlOtherStats.TabIndex = 37
        '
        'lblWorkOrder
        '
        Me.lblWorkOrder.AutoSize = True
        Me.lblWorkOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkOrder.Location = New System.Drawing.Point(3, 0)
        Me.lblWorkOrder.Name = "lblWorkOrder"
        Me.lblWorkOrder.Size = New System.Drawing.Size(0, 16)
        Me.lblWorkOrder.TabIndex = 4
        '
        'lblPrintRef
        '
        Me.lblPrintRef.AutoSize = True
        Me.lblPrintRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrintRef.Location = New System.Drawing.Point(3, 16)
        Me.lblPrintRef.Name = "lblPrintRef"
        Me.lblPrintRef.Size = New System.Drawing.Size(0, 16)
        Me.lblPrintRef.TabIndex = 5
        '
        'lblReelId
        '
        Me.lblReelId.AutoSize = True
        Me.lblReelId.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lblReelId.Location = New System.Drawing.Point(3, 32)
        Me.lblReelId.Name = "lblReelId"
        Me.lblReelId.Size = New System.Drawing.Size(0, 16)
        Me.lblReelId.TabIndex = 6
        '
        'lbllaserDiameter
        '
        Me.lbllaserDiameter.AutoSize = True
        Me.lbllaserDiameter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbllaserDiameter.Location = New System.Drawing.Point(3, 48)
        Me.lbllaserDiameter.Name = "lbllaserDiameter"
        Me.lbllaserDiameter.Size = New System.Drawing.Size(0, 16)
        Me.lbllaserDiameter.TabIndex = 83
        '
        'btnDecreaseFineAdjust
        '
        Me.btnDecreaseFineAdjust.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnDecreaseFineAdjust.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDecreaseFineAdjust.ForeColor = System.Drawing.Color.White
        Me.btnDecreaseFineAdjust.Location = New System.Drawing.Point(2, 225)
        Me.btnDecreaseFineAdjust.Name = "btnDecreaseFineAdjust"
        Me.btnDecreaseFineAdjust.Size = New System.Drawing.Size(382, 60)
        Me.btnDecreaseFineAdjust.TabIndex = 56
        Me.btnDecreaseFineAdjust.Text = "Decrease Hole Position"
        Me.btnDecreaseFineAdjust.UseVisualStyleBackColor = False
        '
        'btnIncreaseFineAdjust
        '
        Me.btnIncreaseFineAdjust.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnIncreaseFineAdjust.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIncreaseFineAdjust.ForeColor = System.Drawing.Color.White
        Me.btnIncreaseFineAdjust.Location = New System.Drawing.Point(1308, 225)
        Me.btnIncreaseFineAdjust.Name = "btnIncreaseFineAdjust"
        Me.btnIncreaseFineAdjust.Size = New System.Drawing.Size(382, 60)
        Me.btnIncreaseFineAdjust.TabIndex = 57
        Me.btnIncreaseFineAdjust.Text = "Increase Hole Position"
        Me.btnIncreaseFineAdjust.UseVisualStyleBackColor = False
        '
        'lblPositionNormal
        '
        Me.lblPositionNormal.AutoSize = True
        Me.lblPositionNormal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPositionNormal.Location = New System.Drawing.Point(603, 242)
        Me.lblPositionNormal.Name = "lblPositionNormal"
        Me.lblPositionNormal.Size = New System.Drawing.Size(24, 25)
        Me.lblPositionNormal.TabIndex = 58
        Me.lblPositionNormal.Text = "0"
        '
        'lblPositionCorrection
        '
        Me.lblPositionCorrection.AutoSize = True
        Me.lblPositionCorrection.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPositionCorrection.Location = New System.Drawing.Point(1076, 242)
        Me.lblPositionCorrection.Name = "lblPositionCorrection"
        Me.lblPositionCorrection.Size = New System.Drawing.Size(24, 25)
        Me.lblPositionCorrection.TabIndex = 59
        Me.lblPositionCorrection.Text = "0"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1922, 1212)
        Me.Controls.Add(Me.lblPositionCorrection)
        Me.Controls.Add(Me.lblPositionNormal)
        Me.Controls.Add(Me.btnIncreaseFineAdjust)
        Me.Controls.Add(Me.btnDecreaseFineAdjust)
        Me.Controls.Add(Me.pnlSystemStats)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.pnlMainLayout)
        Me.Controls.Add(Me.pnlEncoder)
        Me.Controls.Add(Me.lblSwitch)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblHealthy)
        Me.Controls.Add(Me.lblFormat)
        Me.Controls.Add(Me.btnSystemConfiguration)
        Me.Controls.Add(Me.lblRoleLevel)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.btnImages)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.btnManual)
        Me.Controls.Add(Me.btnStatistics)
        Me.Controls.Add(Me.btnSetup)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnLogout)
        Me.Controls.Add(Me.btnStartBatch)
        Me.Controls.Add(Me.btnEndBatch)
        Me.Controls.Add(Me.btnFormat)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MainForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vision System"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.picResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.frmSettings.ResumeLayout(False)
        Me.frmSettings.PerformLayout()
        Me.frmReadings.ResumeLayout(False)
        Me.frmReadings.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.pnlEncoder.ResumeLayout(False)
        Me.pnlEncoder.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSystemStats.ResumeLayout(False)
        Me.pnlLowerLimit.ResumeLayout(False)
        Me.pnlLowerLimit.PerformLayout()
        Me.pnlYield.ResumeLayout(False)
        Me.pnlYield.PerformLayout()
        Me.pnlUpperLimit.ResumeLayout(False)
        Me.pnlUpperLimit.PerformLayout()
        Me.pnlFrameRate.ResumeLayout(False)
        Me.pnlFrameRate.PerformLayout()
        Me.pnlOtherStats.ResumeLayout(False)
        Me.pnlOtherStats.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSetup As System.Windows.Forms.Button
    Friend WithEvents btnStatistics As System.Windows.Forms.Button
    Friend WithEvents btnManual As System.Windows.Forms.Button
    Friend WithEvents btnLogout As System.Windows.Forms.Button
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnFormat As System.Windows.Forms.Button
    Friend WithEvents btnImages As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents picResult As System.Windows.Forms.PictureBox
    Friend WithEvents lblResultMs As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnRunContinuously As System.Windows.Forms.Button
    Friend WithEvents btnRunOnce As System.Windows.Forms.Button
    Friend WithEvents btnHalt As System.Windows.Forms.Button
    Friend WithEvents frmSettings As System.Windows.Forms.GroupBox
    Friend WithEvents chkLabels As System.Windows.Forms.CheckBox
    Friend WithEvents chkBlackBlobs As System.Windows.Forms.CheckBox
    Friend WithEvents lblMaxArea As System.Windows.Forms.Label
    Friend WithEvents hscrMaxArea As System.Windows.Forms.HScrollBar
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblMinArea As System.Windows.Forms.Label
    Friend WithEvents hscrMinArea As System.Windows.Forms.HScrollBar
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblThreshold As System.Windows.Forms.Label
    Friend WithEvents hscrThreshold As System.Windows.Forms.HScrollBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblMaxReturned As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents hscrMaxReturned As System.Windows.Forms.HScrollBar
    Friend WithEvents frmReadings As System.Windows.Forms.GroupBox
    Friend WithEvents lstAreas As System.Windows.Forms.ListBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkSaveSettings As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblZoomFactor As System.Windows.Forms.Label
    Friend WithEvents hscrDisplayZoom As System.Windows.Forms.HScrollBar
    Friend WithEvents lblRoleLevel As System.Windows.Forms.Label
    Friend WithEvents btnSystemConfiguration As System.Windows.Forms.Button
    Friend WithEvents lblFormat As System.Windows.Forms.Label
    Friend WithEvents lblHealthy As System.Windows.Forms.Label
    Friend WithEvents lblSwitch As System.Windows.Forms.Label
    Friend WithEvents actionTimer As System.Windows.Forms.Timer
    Friend WithEvents btnEndBatch As System.Windows.Forms.Button
    Friend WithEvents btnStartBatch As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblPercentageYield2 As System.Windows.Forms.Label
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblPercentageYield3 As System.Windows.Forms.Label
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblUpperLimit3 As System.Windows.Forms.Label
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblLowerLimit3 As System.Windows.Forms.Label
    Friend WithEvents txtEncoderPosition As System.Windows.Forms.TextBox
    Friend WithEvents pnlEncoder As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtFifoStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtComparator As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents pnlMainLayout As TableLayoutPanel
    Friend WithEvents pnlButtons As TableLayoutPanel
    Friend WithEvents pnlSystemStats As FlowLayoutPanel
    Friend WithEvents pnlYield As Panel
    Friend WithEvents lblYieldLabel As Label
    Friend WithEvents lblYield As Label
    Friend WithEvents pnlLowerLimit As Panel
    Friend WithEvents lblLowerLimitLabel As Label
    Friend WithEvents lblLowerLimit As Label
    Friend WithEvents pnlUpperLimit As Panel
    Friend WithEvents lblUpperLimitLabel As Label
    Friend WithEvents lblUpperLimit As Label
    Friend WithEvents pnlFrameRate As Panel
    Friend WithEvents lblFrameRateLabel As Label
    Friend WithEvents lblFrameRate As Label
    Friend WithEvents pnlOtherStats As FlowLayoutPanel
    Friend WithEvents lblWorkOrder As Label
    Friend WithEvents lblPrintRef As Label
    Friend WithEvents lblReelId As Label
    Friend WithEvents lbllaserDiameter As Label
    Friend WithEvents btnDecreaseFineAdjust As System.Windows.Forms.Button
    Friend WithEvents btnIncreaseFineAdjust As System.Windows.Forms.Button
    Friend WithEvents lblPositionNormal As System.Windows.Forms.Label
    Friend WithEvents lblPositionCorrection As System.Windows.Forms.Label
End Class
