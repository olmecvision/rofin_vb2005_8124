﻿Public Class EndBatchForm

    Dim m_MainScreen As MainForm

    Private Sub EndBatchForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

        UpdateScreen()

    End Sub

#Region "Private Functions"

    Private Sub UpdateScreen()

        If (m_MainScreen Is Nothing) Then
            Return
        End If

    End Sub

    Private Sub GenerateBatchReport()

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        Dim batchId = "Unknown"
        Dim statsCollection As Dictionary(Of String, StatisticInfo) = m_MainScreen.Statistics()
        ' Dim stat As StatisticInfo = StatisticsReader.GetStatistic(statsCollection, "1_EXT_100")

        'If (stat IsNot Nothing) Then
        Try
            Dim firstHead As HeadInfo = m_MainScreen.GetFirstHead()
            Dim varPrefix = firstHead.LaserId.ToString + "_" + firstHead.HeadId.ToString.ToString + "_" + MainForm.STA_WORKS_ORDER
            batchId = StatisticsReader.GetStatisticValue(statsCollection.Item(varPrefix))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString)
        End Try

        'End If

        Dim fileName As String = String.Format("batch_report_{0}.pdf", batchId)
        Dim filePath As String = String.Format("C:\\olmec\\batch_report\\{0}", fileName)

        ReportWriter.WriteBatchReport(m_MainScreen, filePath)

    End Sub

    Private Function GenerateRandomDigitCode(length As String) As String

        Dim random As Random = New Random()
        Dim str As String = String.Empty

        For i As Integer = 0 To length - 1
            str = String.Concat(str, random.Next(10).ToString())
        Next
        Return str

    End Function

#End Region

#Region "Button Events"

    'todo: this
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        ' generate the batch report
        GenerateBatchReport()

        '
        ' write the variables
        '
        m_MainScreen.VarSetBool(MainForm.BAT_START_BATCH, False)

        ' save the settings back to the file
        m_MainScreen.SaveInspection()

        ' close the form
        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

#End Region

#Region "Properties"

    Public Property MainScreen() As MainForm
        Get
            Return m_MainScreen
        End Get
        Set(ByVal value As MainForm)
            m_MainScreen = value
        End Set
    End Property

#End Region

End Class