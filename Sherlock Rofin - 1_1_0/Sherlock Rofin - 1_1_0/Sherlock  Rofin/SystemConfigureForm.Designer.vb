﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SystemConfigureForm
    Inherits VB2005_Sherlock7.FormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GradientPanel3 = New VB2005_Sherlock7.GradientPanel()
        Me.NumericUpDown18 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown17 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown16 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown15 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown14 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown13 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown12 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown11 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown10 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.lblValue18 = New System.Windows.Forms.Label()
        Me.hScroll18 = New System.Windows.Forms.HScrollBar()
        Me.lbl18 = New System.Windows.Forms.Label()
        Me.lblValue17 = New System.Windows.Forms.Label()
        Me.hScroll17 = New System.Windows.Forms.HScrollBar()
        Me.lbl17 = New System.Windows.Forms.Label()
        Me.lblValue16 = New System.Windows.Forms.Label()
        Me.hScroll16 = New System.Windows.Forms.HScrollBar()
        Me.lbl16 = New System.Windows.Forms.Label()
        Me.lblValue15 = New System.Windows.Forms.Label()
        Me.hScroll15 = New System.Windows.Forms.HScrollBar()
        Me.lbl15 = New System.Windows.Forms.Label()
        Me.lblValue14 = New System.Windows.Forms.Label()
        Me.hScroll14 = New System.Windows.Forms.HScrollBar()
        Me.lbl14 = New System.Windows.Forms.Label()
        Me.lblValue13 = New System.Windows.Forms.Label()
        Me.hScroll13 = New System.Windows.Forms.HScrollBar()
        Me.lbl13 = New System.Windows.Forms.Label()
        Me.lblValue12 = New System.Windows.Forms.Label()
        Me.hScroll12 = New System.Windows.Forms.HScrollBar()
        Me.lbl12 = New System.Windows.Forms.Label()
        Me.lblValue11 = New System.Windows.Forms.Label()
        Me.hScroll11 = New System.Windows.Forms.HScrollBar()
        Me.lbl11 = New System.Windows.Forms.Label()
        Me.lblValue10 = New System.Windows.Forms.Label()
        Me.hScroll10 = New System.Windows.Forms.HScrollBar()
        Me.lbl10 = New System.Windows.Forms.Label()
        Me.lblValue9 = New System.Windows.Forms.Label()
        Me.hScroll9 = New System.Windows.Forms.HScrollBar()
        Me.lbl9 = New System.Windows.Forms.Label()
        Me.lblValue8 = New System.Windows.Forms.Label()
        Me.hScroll8 = New System.Windows.Forms.HScrollBar()
        Me.lbl8 = New System.Windows.Forms.Label()
        Me.lblValue7 = New System.Windows.Forms.Label()
        Me.hScroll7 = New System.Windows.Forms.HScrollBar()
        Me.lbl7 = New System.Windows.Forms.Label()
        Me.lblValue6 = New System.Windows.Forms.Label()
        Me.hScroll6 = New System.Windows.Forms.HScrollBar()
        Me.lbl6 = New System.Windows.Forms.Label()
        Me.lblValue5 = New System.Windows.Forms.Label()
        Me.hScroll5 = New System.Windows.Forms.HScrollBar()
        Me.lbl5 = New System.Windows.Forms.Label()
        Me.lblValue4 = New System.Windows.Forms.Label()
        Me.hScroll4 = New System.Windows.Forms.HScrollBar()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.lblValue3 = New System.Windows.Forms.Label()
        Me.hScroll3 = New System.Windows.Forms.HScrollBar()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.lblValue2 = New System.Windows.Forms.Label()
        Me.hScroll2 = New System.Windows.Forms.HScrollBar()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.lblValue1 = New System.Windows.Forms.Label()
        Me.hScroll1 = New System.Windows.Forms.HScrollBar()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlKeyboard.SuspendLayout()
        Me.GradientPanel3.SuspendLayout()
        CType(Me.NumericUpDown18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'KeyboardControl1
        '
        Me.KeyboardControl1.Enabled = False
        Me.KeyboardControl1.Visible = False
        '
        'pnlKeyboard
        '
        Me.pnlKeyboard.Dock = System.Windows.Forms.DockStyle.None
        Me.pnlKeyboard.Enabled = False
        Me.pnlKeyboard.Location = New System.Drawing.Point(1125, 1219)
        Me.pnlKeyboard.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.Transparent
        Me.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Image = Global.VB2005_Sherlock7.My.Resources.Resources.OK
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnSave.Location = New System.Drawing.Point(724, 327)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(150, 122)
        Me.btnSave.TabIndex = 12
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(874, 327)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(150, 122)
        Me.btnExit.TabIndex = 11
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'GradientPanel3
        '
        Me.GradientPanel3.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown18)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown17)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown16)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown15)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown14)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown13)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown12)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown11)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown10)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown9)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown8)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown7)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown6)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown5)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown4)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown3)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown2)
        Me.GradientPanel3.Controls.Add(Me.NumericUpDown1)
        Me.GradientPanel3.Controls.Add(Me.lblValue18)
        Me.GradientPanel3.Controls.Add(Me.hScroll18)
        Me.GradientPanel3.Controls.Add(Me.lbl18)
        Me.GradientPanel3.Controls.Add(Me.lblValue17)
        Me.GradientPanel3.Controls.Add(Me.hScroll17)
        Me.GradientPanel3.Controls.Add(Me.lbl17)
        Me.GradientPanel3.Controls.Add(Me.lblValue16)
        Me.GradientPanel3.Controls.Add(Me.hScroll16)
        Me.GradientPanel3.Controls.Add(Me.lbl16)
        Me.GradientPanel3.Controls.Add(Me.lblValue15)
        Me.GradientPanel3.Controls.Add(Me.hScroll15)
        Me.GradientPanel3.Controls.Add(Me.lbl15)
        Me.GradientPanel3.Controls.Add(Me.lblValue14)
        Me.GradientPanel3.Controls.Add(Me.hScroll14)
        Me.GradientPanel3.Controls.Add(Me.lbl14)
        Me.GradientPanel3.Controls.Add(Me.lblValue13)
        Me.GradientPanel3.Controls.Add(Me.hScroll13)
        Me.GradientPanel3.Controls.Add(Me.lbl13)
        Me.GradientPanel3.Controls.Add(Me.lblValue12)
        Me.GradientPanel3.Controls.Add(Me.hScroll12)
        Me.GradientPanel3.Controls.Add(Me.lbl12)
        Me.GradientPanel3.Controls.Add(Me.lblValue11)
        Me.GradientPanel3.Controls.Add(Me.hScroll11)
        Me.GradientPanel3.Controls.Add(Me.lbl11)
        Me.GradientPanel3.Controls.Add(Me.lblValue10)
        Me.GradientPanel3.Controls.Add(Me.hScroll10)
        Me.GradientPanel3.Controls.Add(Me.lbl10)
        Me.GradientPanel3.Controls.Add(Me.lblValue9)
        Me.GradientPanel3.Controls.Add(Me.hScroll9)
        Me.GradientPanel3.Controls.Add(Me.lbl9)
        Me.GradientPanel3.Controls.Add(Me.lblValue8)
        Me.GradientPanel3.Controls.Add(Me.hScroll8)
        Me.GradientPanel3.Controls.Add(Me.lbl8)
        Me.GradientPanel3.Controls.Add(Me.lblValue7)
        Me.GradientPanel3.Controls.Add(Me.hScroll7)
        Me.GradientPanel3.Controls.Add(Me.lbl7)
        Me.GradientPanel3.Controls.Add(Me.lblValue6)
        Me.GradientPanel3.Controls.Add(Me.hScroll6)
        Me.GradientPanel3.Controls.Add(Me.lbl6)
        Me.GradientPanel3.Controls.Add(Me.lblValue5)
        Me.GradientPanel3.Controls.Add(Me.hScroll5)
        Me.GradientPanel3.Controls.Add(Me.lbl5)
        Me.GradientPanel3.Controls.Add(Me.lblValue4)
        Me.GradientPanel3.Controls.Add(Me.hScroll4)
        Me.GradientPanel3.Controls.Add(Me.lbl4)
        Me.GradientPanel3.Controls.Add(Me.lblValue3)
        Me.GradientPanel3.Controls.Add(Me.hScroll3)
        Me.GradientPanel3.Controls.Add(Me.lbl3)
        Me.GradientPanel3.Controls.Add(Me.lblValue2)
        Me.GradientPanel3.Controls.Add(Me.hScroll2)
        Me.GradientPanel3.Controls.Add(Me.lbl2)
        Me.GradientPanel3.Controls.Add(Me.lblValue1)
        Me.GradientPanel3.Controls.Add(Me.hScroll1)
        Me.GradientPanel3.Controls.Add(Me.lbl1)
        Me.GradientPanel3.EndColour = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.GradientPanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel3.Location = New System.Drawing.Point(0, 8)
        Me.GradientPanel3.Name = "GradientPanel3"
        Me.GradientPanel3.Size = New System.Drawing.Size(712, 711)
        Me.GradientPanel3.StartColour = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(36, Byte), Integer))
        Me.GradientPanel3.TabIndex = 13
        '
        'NumericUpDown18
        '
        Me.NumericUpDown18.DecimalPlaces = 2
        Me.NumericUpDown18.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown18.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown18.Location = New System.Drawing.Point(412, 656)
        Me.NumericUpDown18.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown18.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown18.Name = "NumericUpDown18"
        Me.NumericUpDown18.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown18.TabIndex = 78
        '
        'NumericUpDown17
        '
        Me.NumericUpDown17.DecimalPlaces = 2
        Me.NumericUpDown17.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown17.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown17.Location = New System.Drawing.Point(412, 618)
        Me.NumericUpDown17.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown17.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown17.Name = "NumericUpDown17"
        Me.NumericUpDown17.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown17.TabIndex = 77
        '
        'NumericUpDown16
        '
        Me.NumericUpDown16.DecimalPlaces = 2
        Me.NumericUpDown16.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown16.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown16.Location = New System.Drawing.Point(412, 581)
        Me.NumericUpDown16.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown16.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown16.Name = "NumericUpDown16"
        Me.NumericUpDown16.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown16.TabIndex = 76
        '
        'NumericUpDown15
        '
        Me.NumericUpDown15.DecimalPlaces = 2
        Me.NumericUpDown15.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown15.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown15.Location = New System.Drawing.Point(412, 544)
        Me.NumericUpDown15.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown15.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown15.Name = "NumericUpDown15"
        Me.NumericUpDown15.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown15.TabIndex = 75
        '
        'NumericUpDown14
        '
        Me.NumericUpDown14.DecimalPlaces = 2
        Me.NumericUpDown14.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown14.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown14.Location = New System.Drawing.Point(412, 506)
        Me.NumericUpDown14.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown14.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown14.Name = "NumericUpDown14"
        Me.NumericUpDown14.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown14.TabIndex = 74
        '
        'NumericUpDown13
        '
        Me.NumericUpDown13.DecimalPlaces = 2
        Me.NumericUpDown13.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown13.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown13.Location = New System.Drawing.Point(412, 469)
        Me.NumericUpDown13.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown13.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown13.Name = "NumericUpDown13"
        Me.NumericUpDown13.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown13.TabIndex = 73
        '
        'NumericUpDown12
        '
        Me.NumericUpDown12.DecimalPlaces = 2
        Me.NumericUpDown12.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown12.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown12.Location = New System.Drawing.Point(412, 431)
        Me.NumericUpDown12.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown12.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown12.Name = "NumericUpDown12"
        Me.NumericUpDown12.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown12.TabIndex = 72
        '
        'NumericUpDown11
        '
        Me.NumericUpDown11.DecimalPlaces = 2
        Me.NumericUpDown11.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown11.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown11.Location = New System.Drawing.Point(412, 394)
        Me.NumericUpDown11.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown11.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown11.Name = "NumericUpDown11"
        Me.NumericUpDown11.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown11.TabIndex = 71
        '
        'NumericUpDown10
        '
        Me.NumericUpDown10.DecimalPlaces = 2
        Me.NumericUpDown10.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown10.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown10.Location = New System.Drawing.Point(412, 357)
        Me.NumericUpDown10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown10.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown10.Name = "NumericUpDown10"
        Me.NumericUpDown10.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown10.TabIndex = 70
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.DecimalPlaces = 2
        Me.NumericUpDown9.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown9.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown9.Location = New System.Drawing.Point(412, 319)
        Me.NumericUpDown9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown9.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown9.TabIndex = 69
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.DecimalPlaces = 2
        Me.NumericUpDown8.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown8.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown8.Location = New System.Drawing.Point(412, 282)
        Me.NumericUpDown8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown8.TabIndex = 68
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.DecimalPlaces = 2
        Me.NumericUpDown7.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown7.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown7.Location = New System.Drawing.Point(412, 245)
        Me.NumericUpDown7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown7.TabIndex = 67
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.DecimalPlaces = 2
        Me.NumericUpDown6.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown6.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown6.Location = New System.Drawing.Point(412, 207)
        Me.NumericUpDown6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown6.TabIndex = 66
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.DecimalPlaces = 2
        Me.NumericUpDown5.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown5.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown5.Location = New System.Drawing.Point(412, 170)
        Me.NumericUpDown5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown5.TabIndex = 65
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.DecimalPlaces = 2
        Me.NumericUpDown4.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown4.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown4.Location = New System.Drawing.Point(412, 132)
        Me.NumericUpDown4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown4.TabIndex = 64
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.DecimalPlaces = 2
        Me.NumericUpDown3.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown3.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown3.Location = New System.Drawing.Point(412, 95)
        Me.NumericUpDown3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown3.TabIndex = 63
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.DecimalPlaces = 2
        Me.NumericUpDown2.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown2.Location = New System.Drawing.Point(412, 58)
        Me.NumericUpDown2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown2.TabIndex = 62
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DecimalPlaces = 2
        Me.NumericUpDown1.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown1.Location = New System.Drawing.Point(412, 20)
        Me.NumericUpDown1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NumericUpDown1.Size = New System.Drawing.Size(166, 34)
        Me.NumericUpDown1.TabIndex = 61
        Me.NumericUpDown1.TabStop = False
        '
        'lblValue18
        '
        Me.lblValue18.BackColor = System.Drawing.Color.Transparent
        Me.lblValue18.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue18.ForeColor = System.Drawing.Color.White
        Me.lblValue18.Location = New System.Drawing.Point(580, 658)
        Me.lblValue18.Name = "lblValue18"
        Me.lblValue18.Size = New System.Drawing.Size(111, 36)
        Me.lblValue18.TabIndex = 60
        Me.lblValue18.Text = "Value 18"
        Me.lblValue18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll18
        '
        Me.hScroll18.Enabled = False
        Me.hScroll18.LargeChange = 1
        Me.hScroll18.Location = New System.Drawing.Point(412, 662)
        Me.hScroll18.Name = "hScroll18"
        Me.hScroll18.Size = New System.Drawing.Size(165, 36)
        Me.hScroll18.TabIndex = 59
        Me.hScroll18.Visible = False
        '
        'lbl18
        '
        Me.lbl18.BackColor = System.Drawing.Color.Transparent
        Me.lbl18.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl18.ForeColor = System.Drawing.Color.White
        Me.lbl18.Location = New System.Drawing.Point(19, 658)
        Me.lbl18.Name = "lbl18"
        Me.lbl18.Size = New System.Drawing.Size(370, 36)
        Me.lbl18.TabIndex = 58
        Me.lbl18.Text = "Label 18"
        Me.lbl18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue17
        '
        Me.lblValue17.BackColor = System.Drawing.Color.Transparent
        Me.lblValue17.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue17.ForeColor = System.Drawing.Color.White
        Me.lblValue17.Location = New System.Drawing.Point(580, 621)
        Me.lblValue17.Name = "lblValue17"
        Me.lblValue17.Size = New System.Drawing.Size(111, 36)
        Me.lblValue17.TabIndex = 57
        Me.lblValue17.Text = "Value 17"
        Me.lblValue17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll17
        '
        Me.hScroll17.Enabled = False
        Me.hScroll17.LargeChange = 1
        Me.hScroll17.Location = New System.Drawing.Point(412, 625)
        Me.hScroll17.Name = "hScroll17"
        Me.hScroll17.Size = New System.Drawing.Size(165, 36)
        Me.hScroll17.TabIndex = 56
        Me.hScroll17.Visible = False
        '
        'lbl17
        '
        Me.lbl17.BackColor = System.Drawing.Color.Transparent
        Me.lbl17.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl17.ForeColor = System.Drawing.Color.White
        Me.lbl17.Location = New System.Drawing.Point(19, 621)
        Me.lbl17.Name = "lbl17"
        Me.lbl17.Size = New System.Drawing.Size(370, 36)
        Me.lbl17.TabIndex = 55
        Me.lbl17.Text = "Label 17"
        Me.lbl17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue16
        '
        Me.lblValue16.BackColor = System.Drawing.Color.Transparent
        Me.lblValue16.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue16.ForeColor = System.Drawing.Color.White
        Me.lblValue16.Location = New System.Drawing.Point(580, 583)
        Me.lblValue16.Name = "lblValue16"
        Me.lblValue16.Size = New System.Drawing.Size(111, 36)
        Me.lblValue16.TabIndex = 54
        Me.lblValue16.Text = "Value 16"
        Me.lblValue16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll16
        '
        Me.hScroll16.Enabled = False
        Me.hScroll16.LargeChange = 1
        Me.hScroll16.Location = New System.Drawing.Point(412, 587)
        Me.hScroll16.Name = "hScroll16"
        Me.hScroll16.Size = New System.Drawing.Size(165, 36)
        Me.hScroll16.TabIndex = 53
        Me.hScroll16.Visible = False
        '
        'lbl16
        '
        Me.lbl16.BackColor = System.Drawing.Color.Transparent
        Me.lbl16.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl16.ForeColor = System.Drawing.Color.White
        Me.lbl16.Location = New System.Drawing.Point(19, 583)
        Me.lbl16.Name = "lbl16"
        Me.lbl16.Size = New System.Drawing.Size(370, 36)
        Me.lbl16.TabIndex = 52
        Me.lbl16.Text = "Label 16"
        Me.lbl16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue15
        '
        Me.lblValue15.BackColor = System.Drawing.Color.Transparent
        Me.lblValue15.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue15.ForeColor = System.Drawing.Color.White
        Me.lblValue15.Location = New System.Drawing.Point(580, 544)
        Me.lblValue15.Name = "lblValue15"
        Me.lblValue15.Size = New System.Drawing.Size(111, 36)
        Me.lblValue15.TabIndex = 51
        Me.lblValue15.Text = "Value 15"
        Me.lblValue15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll15
        '
        Me.hScroll15.Enabled = False
        Me.hScroll15.LargeChange = 1
        Me.hScroll15.Location = New System.Drawing.Point(412, 548)
        Me.hScroll15.Name = "hScroll15"
        Me.hScroll15.Size = New System.Drawing.Size(165, 36)
        Me.hScroll15.TabIndex = 50
        Me.hScroll15.Visible = False
        '
        'lbl15
        '
        Me.lbl15.BackColor = System.Drawing.Color.Transparent
        Me.lbl15.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl15.ForeColor = System.Drawing.Color.White
        Me.lbl15.Location = New System.Drawing.Point(19, 544)
        Me.lbl15.Name = "lbl15"
        Me.lbl15.Size = New System.Drawing.Size(370, 36)
        Me.lbl15.TabIndex = 49
        Me.lbl15.Text = "Label 15"
        Me.lbl15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue14
        '
        Me.lblValue14.BackColor = System.Drawing.Color.Transparent
        Me.lblValue14.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue14.ForeColor = System.Drawing.Color.White
        Me.lblValue14.Location = New System.Drawing.Point(580, 505)
        Me.lblValue14.Name = "lblValue14"
        Me.lblValue14.Size = New System.Drawing.Size(111, 36)
        Me.lblValue14.TabIndex = 48
        Me.lblValue14.Text = "Value 14"
        Me.lblValue14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll14
        '
        Me.hScroll14.Enabled = False
        Me.hScroll14.LargeChange = 1
        Me.hScroll14.Location = New System.Drawing.Point(412, 509)
        Me.hScroll14.Name = "hScroll14"
        Me.hScroll14.Size = New System.Drawing.Size(165, 36)
        Me.hScroll14.TabIndex = 47
        Me.hScroll14.Visible = False
        '
        'lbl14
        '
        Me.lbl14.BackColor = System.Drawing.Color.Transparent
        Me.lbl14.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl14.ForeColor = System.Drawing.Color.White
        Me.lbl14.Location = New System.Drawing.Point(19, 505)
        Me.lbl14.Name = "lbl14"
        Me.lbl14.Size = New System.Drawing.Size(370, 36)
        Me.lbl14.TabIndex = 46
        Me.lbl14.Text = "Label 14"
        Me.lbl14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue13
        '
        Me.lblValue13.BackColor = System.Drawing.Color.Transparent
        Me.lblValue13.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue13.ForeColor = System.Drawing.Color.White
        Me.lblValue13.Location = New System.Drawing.Point(580, 466)
        Me.lblValue13.Name = "lblValue13"
        Me.lblValue13.Size = New System.Drawing.Size(111, 36)
        Me.lblValue13.TabIndex = 45
        Me.lblValue13.Text = "Value 13"
        Me.lblValue13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll13
        '
        Me.hScroll13.Enabled = False
        Me.hScroll13.LargeChange = 1
        Me.hScroll13.Location = New System.Drawing.Point(412, 470)
        Me.hScroll13.Name = "hScroll13"
        Me.hScroll13.Size = New System.Drawing.Size(165, 36)
        Me.hScroll13.TabIndex = 44
        Me.hScroll13.Visible = False
        '
        'lbl13
        '
        Me.lbl13.BackColor = System.Drawing.Color.Transparent
        Me.lbl13.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl13.ForeColor = System.Drawing.Color.White
        Me.lbl13.Location = New System.Drawing.Point(19, 466)
        Me.lbl13.Name = "lbl13"
        Me.lbl13.Size = New System.Drawing.Size(370, 36)
        Me.lbl13.TabIndex = 43
        Me.lbl13.Text = "Label 13"
        Me.lbl13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue12
        '
        Me.lblValue12.BackColor = System.Drawing.Color.Transparent
        Me.lblValue12.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue12.ForeColor = System.Drawing.Color.White
        Me.lblValue12.Location = New System.Drawing.Point(580, 426)
        Me.lblValue12.Name = "lblValue12"
        Me.lblValue12.Size = New System.Drawing.Size(111, 36)
        Me.lblValue12.TabIndex = 42
        Me.lblValue12.Text = "Value 12"
        Me.lblValue12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll12
        '
        Me.hScroll12.Enabled = False
        Me.hScroll12.LargeChange = 1
        Me.hScroll12.Location = New System.Drawing.Point(412, 431)
        Me.hScroll12.Name = "hScroll12"
        Me.hScroll12.Size = New System.Drawing.Size(165, 36)
        Me.hScroll12.TabIndex = 41
        Me.hScroll12.Visible = False
        '
        'lbl12
        '
        Me.lbl12.BackColor = System.Drawing.Color.Transparent
        Me.lbl12.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl12.ForeColor = System.Drawing.Color.White
        Me.lbl12.Location = New System.Drawing.Point(19, 426)
        Me.lbl12.Name = "lbl12"
        Me.lbl12.Size = New System.Drawing.Size(370, 36)
        Me.lbl12.TabIndex = 40
        Me.lbl12.Text = "Label 12"
        Me.lbl12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue11
        '
        Me.lblValue11.BackColor = System.Drawing.Color.Transparent
        Me.lblValue11.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue11.ForeColor = System.Drawing.Color.White
        Me.lblValue11.Location = New System.Drawing.Point(580, 388)
        Me.lblValue11.Name = "lblValue11"
        Me.lblValue11.Size = New System.Drawing.Size(111, 36)
        Me.lblValue11.TabIndex = 39
        Me.lblValue11.Text = "Value 11"
        Me.lblValue11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll11
        '
        Me.hScroll11.Enabled = False
        Me.hScroll11.LargeChange = 1
        Me.hScroll11.Location = New System.Drawing.Point(412, 392)
        Me.hScroll11.Name = "hScroll11"
        Me.hScroll11.Size = New System.Drawing.Size(165, 36)
        Me.hScroll11.TabIndex = 38
        Me.hScroll11.Visible = False
        '
        'lbl11
        '
        Me.lbl11.BackColor = System.Drawing.Color.Transparent
        Me.lbl11.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl11.ForeColor = System.Drawing.Color.White
        Me.lbl11.Location = New System.Drawing.Point(19, 388)
        Me.lbl11.Name = "lbl11"
        Me.lbl11.Size = New System.Drawing.Size(370, 36)
        Me.lbl11.TabIndex = 37
        Me.lbl11.Text = "Label 11"
        Me.lbl11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue10
        '
        Me.lblValue10.BackColor = System.Drawing.Color.Transparent
        Me.lblValue10.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue10.ForeColor = System.Drawing.Color.White
        Me.lblValue10.Location = New System.Drawing.Point(580, 350)
        Me.lblValue10.Name = "lblValue10"
        Me.lblValue10.Size = New System.Drawing.Size(111, 36)
        Me.lblValue10.TabIndex = 36
        Me.lblValue10.Text = "Value 10"
        Me.lblValue10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll10
        '
        Me.hScroll10.Enabled = False
        Me.hScroll10.LargeChange = 1
        Me.hScroll10.Location = New System.Drawing.Point(412, 354)
        Me.hScroll10.Name = "hScroll10"
        Me.hScroll10.Size = New System.Drawing.Size(165, 36)
        Me.hScroll10.TabIndex = 35
        Me.hScroll10.Visible = False
        '
        'lbl10
        '
        Me.lbl10.BackColor = System.Drawing.Color.Transparent
        Me.lbl10.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl10.ForeColor = System.Drawing.Color.White
        Me.lbl10.Location = New System.Drawing.Point(19, 350)
        Me.lbl10.Name = "lbl10"
        Me.lbl10.Size = New System.Drawing.Size(370, 36)
        Me.lbl10.TabIndex = 34
        Me.lbl10.Text = "Label 10"
        Me.lbl10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue9
        '
        Me.lblValue9.BackColor = System.Drawing.Color.Transparent
        Me.lblValue9.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue9.ForeColor = System.Drawing.Color.White
        Me.lblValue9.Location = New System.Drawing.Point(580, 312)
        Me.lblValue9.Name = "lblValue9"
        Me.lblValue9.Size = New System.Drawing.Size(111, 36)
        Me.lblValue9.TabIndex = 33
        Me.lblValue9.Text = "Value 9"
        Me.lblValue9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll9
        '
        Me.hScroll9.Enabled = False
        Me.hScroll9.LargeChange = 1
        Me.hScroll9.Location = New System.Drawing.Point(412, 316)
        Me.hScroll9.Name = "hScroll9"
        Me.hScroll9.Size = New System.Drawing.Size(165, 36)
        Me.hScroll9.TabIndex = 32
        Me.hScroll9.Visible = False
        '
        'lbl9
        '
        Me.lbl9.BackColor = System.Drawing.Color.Transparent
        Me.lbl9.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl9.ForeColor = System.Drawing.Color.White
        Me.lbl9.Location = New System.Drawing.Point(19, 312)
        Me.lbl9.Name = "lbl9"
        Me.lbl9.Size = New System.Drawing.Size(370, 36)
        Me.lbl9.TabIndex = 31
        Me.lbl9.Text = "Label 9"
        Me.lbl9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue8
        '
        Me.lblValue8.BackColor = System.Drawing.Color.Transparent
        Me.lblValue8.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue8.ForeColor = System.Drawing.Color.White
        Me.lblValue8.Location = New System.Drawing.Point(580, 272)
        Me.lblValue8.Name = "lblValue8"
        Me.lblValue8.Size = New System.Drawing.Size(111, 36)
        Me.lblValue8.TabIndex = 30
        Me.lblValue8.Text = "Value 8"
        Me.lblValue8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll8
        '
        Me.hScroll8.Enabled = False
        Me.hScroll8.LargeChange = 1
        Me.hScroll8.Location = New System.Drawing.Point(412, 276)
        Me.hScroll8.Name = "hScroll8"
        Me.hScroll8.Size = New System.Drawing.Size(165, 36)
        Me.hScroll8.TabIndex = 29
        Me.hScroll8.Visible = False
        '
        'lbl8
        '
        Me.lbl8.BackColor = System.Drawing.Color.Transparent
        Me.lbl8.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl8.ForeColor = System.Drawing.Color.White
        Me.lbl8.Location = New System.Drawing.Point(19, 272)
        Me.lbl8.Name = "lbl8"
        Me.lbl8.Size = New System.Drawing.Size(370, 36)
        Me.lbl8.TabIndex = 28
        Me.lbl8.Text = "Label 8"
        Me.lbl8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue7
        '
        Me.lblValue7.BackColor = System.Drawing.Color.Transparent
        Me.lblValue7.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue7.ForeColor = System.Drawing.Color.White
        Me.lblValue7.Location = New System.Drawing.Point(580, 235)
        Me.lblValue7.Name = "lblValue7"
        Me.lblValue7.Size = New System.Drawing.Size(111, 36)
        Me.lblValue7.TabIndex = 27
        Me.lblValue7.Text = "Value 7"
        Me.lblValue7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll7
        '
        Me.hScroll7.Enabled = False
        Me.hScroll7.LargeChange = 1
        Me.hScroll7.Location = New System.Drawing.Point(412, 239)
        Me.hScroll7.Name = "hScroll7"
        Me.hScroll7.Size = New System.Drawing.Size(165, 36)
        Me.hScroll7.TabIndex = 26
        Me.hScroll7.Visible = False
        '
        'lbl7
        '
        Me.lbl7.BackColor = System.Drawing.Color.Transparent
        Me.lbl7.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl7.ForeColor = System.Drawing.Color.White
        Me.lbl7.Location = New System.Drawing.Point(19, 235)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(370, 36)
        Me.lbl7.TabIndex = 25
        Me.lbl7.Text = "Label 7"
        Me.lbl7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue6
        '
        Me.lblValue6.BackColor = System.Drawing.Color.Transparent
        Me.lblValue6.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue6.ForeColor = System.Drawing.Color.White
        Me.lblValue6.Location = New System.Drawing.Point(580, 198)
        Me.lblValue6.Name = "lblValue6"
        Me.lblValue6.Size = New System.Drawing.Size(111, 36)
        Me.lblValue6.TabIndex = 24
        Me.lblValue6.Text = "Value 6"
        Me.lblValue6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll6
        '
        Me.hScroll6.Enabled = False
        Me.hScroll6.LargeChange = 1
        Me.hScroll6.Location = New System.Drawing.Point(412, 202)
        Me.hScroll6.Name = "hScroll6"
        Me.hScroll6.Size = New System.Drawing.Size(165, 36)
        Me.hScroll6.TabIndex = 23
        Me.hScroll6.Visible = False
        '
        'lbl6
        '
        Me.lbl6.BackColor = System.Drawing.Color.Transparent
        Me.lbl6.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl6.ForeColor = System.Drawing.Color.White
        Me.lbl6.Location = New System.Drawing.Point(19, 198)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(370, 36)
        Me.lbl6.TabIndex = 22
        Me.lbl6.Text = "Label 6"
        Me.lbl6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue5
        '
        Me.lblValue5.BackColor = System.Drawing.Color.Transparent
        Me.lblValue5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue5.ForeColor = System.Drawing.Color.White
        Me.lblValue5.Location = New System.Drawing.Point(580, 161)
        Me.lblValue5.Name = "lblValue5"
        Me.lblValue5.Size = New System.Drawing.Size(111, 36)
        Me.lblValue5.TabIndex = 21
        Me.lblValue5.Text = "Value 5"
        Me.lblValue5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll5
        '
        Me.hScroll5.Enabled = False
        Me.hScroll5.LargeChange = 1
        Me.hScroll5.Location = New System.Drawing.Point(412, 165)
        Me.hScroll5.Name = "hScroll5"
        Me.hScroll5.Size = New System.Drawing.Size(165, 36)
        Me.hScroll5.TabIndex = 20
        Me.hScroll5.Visible = False
        '
        'lbl5
        '
        Me.lbl5.BackColor = System.Drawing.Color.Transparent
        Me.lbl5.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl5.ForeColor = System.Drawing.Color.White
        Me.lbl5.Location = New System.Drawing.Point(19, 161)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(370, 36)
        Me.lbl5.TabIndex = 19
        Me.lbl5.Text = "Label 5"
        Me.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue4
        '
        Me.lblValue4.BackColor = System.Drawing.Color.Transparent
        Me.lblValue4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue4.ForeColor = System.Drawing.Color.White
        Me.lblValue4.Location = New System.Drawing.Point(580, 125)
        Me.lblValue4.Name = "lblValue4"
        Me.lblValue4.Size = New System.Drawing.Size(111, 36)
        Me.lblValue4.TabIndex = 18
        Me.lblValue4.Text = "Value 4"
        Me.lblValue4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll4
        '
        Me.hScroll4.Enabled = False
        Me.hScroll4.LargeChange = 1
        Me.hScroll4.Location = New System.Drawing.Point(412, 129)
        Me.hScroll4.Name = "hScroll4"
        Me.hScroll4.Size = New System.Drawing.Size(165, 36)
        Me.hScroll4.TabIndex = 17
        Me.hScroll4.Visible = False
        '
        'lbl4
        '
        Me.lbl4.BackColor = System.Drawing.Color.Transparent
        Me.lbl4.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl4.ForeColor = System.Drawing.Color.White
        Me.lbl4.Location = New System.Drawing.Point(19, 125)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(370, 36)
        Me.lbl4.TabIndex = 16
        Me.lbl4.Text = "Label 4"
        Me.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue3
        '
        Me.lblValue3.BackColor = System.Drawing.Color.Transparent
        Me.lblValue3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue3.ForeColor = System.Drawing.Color.White
        Me.lblValue3.Location = New System.Drawing.Point(580, 89)
        Me.lblValue3.Name = "lblValue3"
        Me.lblValue3.Size = New System.Drawing.Size(111, 36)
        Me.lblValue3.TabIndex = 15
        Me.lblValue3.Text = "Value 3"
        Me.lblValue3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll3
        '
        Me.hScroll3.Enabled = False
        Me.hScroll3.LargeChange = 1
        Me.hScroll3.Location = New System.Drawing.Point(412, 93)
        Me.hScroll3.Name = "hScroll3"
        Me.hScroll3.Size = New System.Drawing.Size(165, 36)
        Me.hScroll3.TabIndex = 14
        Me.hScroll3.Visible = False
        '
        'lbl3
        '
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl3.ForeColor = System.Drawing.Color.White
        Me.lbl3.Location = New System.Drawing.Point(19, 89)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(370, 36)
        Me.lbl3.TabIndex = 13
        Me.lbl3.Text = "Label 3"
        Me.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue2
        '
        Me.lblValue2.BackColor = System.Drawing.Color.Transparent
        Me.lblValue2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue2.ForeColor = System.Drawing.Color.White
        Me.lblValue2.Location = New System.Drawing.Point(580, 53)
        Me.lblValue2.Name = "lblValue2"
        Me.lblValue2.Size = New System.Drawing.Size(111, 36)
        Me.lblValue2.TabIndex = 12
        Me.lblValue2.Text = "Value 2"
        Me.lblValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll2
        '
        Me.hScroll2.Enabled = False
        Me.hScroll2.LargeChange = 1
        Me.hScroll2.Location = New System.Drawing.Point(412, 57)
        Me.hScroll2.Name = "hScroll2"
        Me.hScroll2.Size = New System.Drawing.Size(165, 36)
        Me.hScroll2.TabIndex = 11
        Me.hScroll2.Visible = False
        '
        'lbl2
        '
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl2.ForeColor = System.Drawing.Color.White
        Me.lbl2.Location = New System.Drawing.Point(19, 53)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(370, 36)
        Me.lbl2.TabIndex = 10
        Me.lbl2.Text = "Label 2"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue1
        '
        Me.lblValue1.BackColor = System.Drawing.Color.Transparent
        Me.lblValue1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lblValue1.ForeColor = System.Drawing.Color.White
        Me.lblValue1.Location = New System.Drawing.Point(580, 17)
        Me.lblValue1.Name = "lblValue1"
        Me.lblValue1.Size = New System.Drawing.Size(111, 36)
        Me.lblValue1.TabIndex = 9
        Me.lblValue1.Text = "Value 1"
        Me.lblValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hScroll1
        '
        Me.hScroll1.Enabled = False
        Me.hScroll1.LargeChange = 1
        Me.hScroll1.Location = New System.Drawing.Point(412, 21)
        Me.hScroll1.Name = "hScroll1"
        Me.hScroll1.Size = New System.Drawing.Size(165, 36)
        Me.hScroll1.TabIndex = 8
        Me.hScroll1.Visible = False
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Font = New System.Drawing.Font("Century Gothic", 14.25!)
        Me.lbl1.ForeColor = System.Drawing.Color.White
        Me.lbl1.Location = New System.Drawing.Point(19, 17)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(370, 36)
        Me.lbl1.TabIndex = 0
        Me.lbl1.Text = "Label 1"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(726, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(298, 209)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Inspection Parameters"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SystemConfigureForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.GradientPanel3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "SystemConfigureForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configure System"
        Me.Controls.SetChildIndex(Me.pnlKeyboard, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnSave, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.GradientPanel3, 0)
        Me.pnlKeyboard.ResumeLayout(False)
        Me.pnlKeyboard.PerformLayout()
        Me.GradientPanel3.ResumeLayout(False)
        CType(Me.NumericUpDown18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents GradientPanel3 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents hScroll1 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblValue1 As System.Windows.Forms.Label
    Friend WithEvents lblValue2 As System.Windows.Forms.Label
    Friend WithEvents hScroll2 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lblValue10 As System.Windows.Forms.Label
    Friend WithEvents hScroll10 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl10 As System.Windows.Forms.Label
    Friend WithEvents lblValue9 As System.Windows.Forms.Label
    Friend WithEvents hScroll9 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl9 As System.Windows.Forms.Label
    Friend WithEvents lblValue8 As System.Windows.Forms.Label
    Friend WithEvents hScroll8 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl8 As System.Windows.Forms.Label
    Friend WithEvents lblValue7 As System.Windows.Forms.Label
    Friend WithEvents hScroll7 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblValue6 As System.Windows.Forms.Label
    Friend WithEvents hScroll6 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl6 As System.Windows.Forms.Label
    Friend WithEvents lblValue5 As System.Windows.Forms.Label
    Friend WithEvents hScroll5 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents lblValue4 As System.Windows.Forms.Label
    Friend WithEvents hScroll4 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents lblValue3 As System.Windows.Forms.Label
    Friend WithEvents hScroll3 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblValue13 As System.Windows.Forms.Label
    Friend WithEvents hScroll13 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl13 As System.Windows.Forms.Label
    Friend WithEvents lblValue12 As System.Windows.Forms.Label
    Friend WithEvents hScroll12 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl12 As System.Windows.Forms.Label
    Friend WithEvents lblValue11 As System.Windows.Forms.Label
    Friend WithEvents hScroll11 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl11 As System.Windows.Forms.Label
    Friend WithEvents lblValue16 As System.Windows.Forms.Label
    Friend WithEvents hScroll16 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl16 As System.Windows.Forms.Label
    Friend WithEvents lblValue15 As System.Windows.Forms.Label
    Friend WithEvents hScroll15 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl15 As System.Windows.Forms.Label
    Friend WithEvents lblValue14 As System.Windows.Forms.Label
    Friend WithEvents hScroll14 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl14 As System.Windows.Forms.Label
    Friend WithEvents lblValue18 As System.Windows.Forms.Label
    Friend WithEvents hScroll18 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl18 As System.Windows.Forms.Label
    Friend WithEvents lblValue17 As System.Windows.Forms.Label
    Friend WithEvents hScroll17 As System.Windows.Forms.HScrollBar
    Friend WithEvents lbl17 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown16 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown15 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown14 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown13 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown12 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown11 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown10 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown18 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown17 As System.Windows.Forms.NumericUpDown

End Class
