﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImageViewerForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnImage1 = New System.Windows.Forms.Button()
        Me.btnImage2 = New System.Windows.Forms.Button()
        Me.btnImage3 = New System.Windows.Forms.Button()
        Me.lblNoImagesFound = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblImageName = New System.Windows.Forms.Label()
        Me.lblImageSize = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblCreatedDate = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnDeleteCurrent = New System.Windows.Forms.Button()
        Me.imgMain = New System.Windows.Forms.PictureBox()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnDeleteAll = New System.Windows.Forms.Button()
        Me.GradientPanel3 = New VB2005_Sherlock7.GradientPanel()
        CType(Me.imgMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnImage1
        '
        Me.btnImage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnImage1.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnImage1.FlatAppearance.BorderSize = 0
        Me.btnImage1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImage1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImage1.Location = New System.Drawing.Point(320, 6)
        Me.btnImage1.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImage1.Name = "btnImage1"
        Me.btnImage1.Size = New System.Drawing.Size(161, 100)
        Me.btnImage1.TabIndex = 35
        Me.btnImage1.UseVisualStyleBackColor = True
        '
        'btnImage2
        '
        Me.btnImage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnImage2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnImage2.FlatAppearance.BorderSize = 3
        Me.btnImage2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImage2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImage2.Location = New System.Drawing.Point(485, 6)
        Me.btnImage2.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImage2.Name = "btnImage2"
        Me.btnImage2.Size = New System.Drawing.Size(161, 100)
        Me.btnImage2.TabIndex = 36
        Me.btnImage2.UseVisualStyleBackColor = True
        '
        'btnImage3
        '
        Me.btnImage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnImage3.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnImage3.FlatAppearance.BorderSize = 0
        Me.btnImage3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImage3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImage3.Location = New System.Drawing.Point(651, 6)
        Me.btnImage3.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImage3.Name = "btnImage3"
        Me.btnImage3.Size = New System.Drawing.Size(161, 100)
        Me.btnImage3.TabIndex = 37
        Me.btnImage3.UseVisualStyleBackColor = True
        '
        'lblNoImagesFound
        '
        Me.lblNoImagesFound.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblNoImagesFound.AutoSize = True
        Me.lblNoImagesFound.BackColor = System.Drawing.Color.Transparent
        Me.lblNoImagesFound.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoImagesFound.ForeColor = System.Drawing.Color.Gray
        Me.lblNoImagesFound.Location = New System.Drawing.Point(366, 346)
        Me.lblNoImagesFound.Margin = New System.Windows.Forms.Padding(0)
        Me.lblNoImagesFound.Name = "lblNoImagesFound"
        Me.lblNoImagesFound.Size = New System.Drawing.Size(293, 39)
        Me.lblNoImagesFound.TabIndex = 39
        Me.lblNoImagesFound.Text = "No Images Found"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(800, 111)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 22)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Image Name"
        '
        'lblImageName
        '
        Me.lblImageName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblImageName.AutoSize = True
        Me.lblImageName.BackColor = System.Drawing.Color.Transparent
        Me.lblImageName.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImageName.ForeColor = System.Drawing.Color.Gray
        Me.lblImageName.Location = New System.Drawing.Point(809, 142)
        Me.lblImageName.Margin = New System.Windows.Forms.Padding(0)
        Me.lblImageName.Name = "lblImageName"
        Me.lblImageName.Size = New System.Drawing.Size(0, 22)
        Me.lblImageName.TabIndex = 41
        '
        'lblImageSize
        '
        Me.lblImageSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblImageSize.AutoSize = True
        Me.lblImageSize.BackColor = System.Drawing.Color.Transparent
        Me.lblImageSize.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImageSize.ForeColor = System.Drawing.Color.Gray
        Me.lblImageSize.Location = New System.Drawing.Point(809, 199)
        Me.lblImageSize.Margin = New System.Windows.Forms.Padding(0)
        Me.lblImageSize.Name = "lblImageSize"
        Me.lblImageSize.Size = New System.Drawing.Size(0, 22)
        Me.lblImageSize.TabIndex = 43
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(800, 167)
        Me.Label3.Margin = New System.Windows.Forms.Padding(0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 22)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Size"
        '
        'lblCreatedDate
        '
        Me.lblCreatedDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCreatedDate.AutoSize = True
        Me.lblCreatedDate.BackColor = System.Drawing.Color.Transparent
        Me.lblCreatedDate.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreatedDate.ForeColor = System.Drawing.Color.Gray
        Me.lblCreatedDate.Location = New System.Drawing.Point(809, 260)
        Me.lblCreatedDate.Margin = New System.Windows.Forms.Padding(0)
        Me.lblCreatedDate.Name = "lblCreatedDate"
        Me.lblCreatedDate.Size = New System.Drawing.Size(0, 22)
        Me.lblCreatedDate.TabIndex = 45
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(800, 223)
        Me.Label4.Margin = New System.Windows.Forms.Padding(0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 22)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Saved"
        '
        'btnDeleteCurrent
        '
        Me.btnDeleteCurrent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteCurrent.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnDeleteCurrent.FlatAppearance.BorderSize = 0
        Me.btnDeleteCurrent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteCurrent.Image = Global.VB2005_Sherlock7.My.Resources.Resources.delete_current
        Me.btnDeleteCurrent.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnDeleteCurrent.Location = New System.Drawing.Point(769, 386)
        Me.btnDeleteCurrent.Margin = New System.Windows.Forms.Padding(0)
        Me.btnDeleteCurrent.Name = "btnDeleteCurrent"
        Me.btnDeleteCurrent.Size = New System.Drawing.Size(212, 135)
        Me.btnDeleteCurrent.TabIndex = 48
        Me.btnDeleteCurrent.UseVisualStyleBackColor = True
        '
        'imgMain
        '
        Me.imgMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imgMain.Location = New System.Drawing.Point(5, 108)
        Me.imgMain.Margin = New System.Windows.Forms.Padding(0)
        Me.imgMain.Name = "imgMain"
        Me.imgMain.Size = New System.Drawing.Size(762, 618)
        Me.imgMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgMain.TabIndex = 38
        Me.imgMain.TabStop = False
        '
        'btnNext
        '
        Me.btnNext.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnNext.FlatAppearance.BorderSize = 0
        Me.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNext.Image = Global.VB2005_Sherlock7.My.Resources.Resources.next_image
        Me.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNext.Location = New System.Drawing.Point(813, 2)
        Me.btnNext.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(212, 106)
        Me.btnNext.TabIndex = 33
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnPrevious.FlatAppearance.BorderSize = 0
        Me.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrevious.Image = Global.VB2005_Sherlock7.My.Resources.Resources.prev_image
        Me.btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrevious.Location = New System.Drawing.Point(109, 2)
        Me.btnPrevious.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(206, 106)
        Me.btnPrevious.TabIndex = 32
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(769, 495)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(206, 162)
        Me.btnExit.TabIndex = 30
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = Global.VB2005_Sherlock7.My.Resources.Resources.report_logo
        Me.PictureBox1.Location = New System.Drawing.Point(769, 657)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(171, 69)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 28
        Me.PictureBox1.TabStop = False
        '
        'btnDeleteAll
        '
        Me.btnDeleteAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteAll.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnDeleteAll.FlatAppearance.BorderSize = 0
        Me.btnDeleteAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteAll.Image = Global.VB2005_Sherlock7.My.Resources.Resources.delete_all
        Me.btnDeleteAll.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnDeleteAll.Location = New System.Drawing.Point(769, 260)
        Me.btnDeleteAll.Margin = New System.Windows.Forms.Padding(0)
        Me.btnDeleteAll.Name = "btnDeleteAll"
        Me.btnDeleteAll.Size = New System.Drawing.Size(212, 126)
        Me.btnDeleteAll.TabIndex = 49
        Me.btnDeleteAll.UseVisualStyleBackColor = True
        '
        'GradientPanel3
        '
        Me.GradientPanel3.BackColor = System.Drawing.Color.Transparent
        Me.GradientPanel3.EndColour = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.GradientPanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal
        Me.GradientPanel3.Location = New System.Drawing.Point(5, 7)
        Me.GradientPanel3.Name = "GradientPanel3"
        Me.GradientPanel3.Size = New System.Drawing.Size(103, 98)
        Me.GradientPanel3.StartColour = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(147, Byte), Integer), CType(CType(30, Byte), Integer))
        Me.GradientPanel3.TabIndex = 34
        '
        'ImageViewerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnDeleteAll)
        Me.Controls.Add(Me.btnDeleteCurrent)
        Me.Controls.Add(Me.lblCreatedDate)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblImageSize)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblImageName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblNoImagesFound)
        Me.Controls.Add(Me.imgMain)
        Me.Controls.Add(Me.btnImage3)
        Me.Controls.Add(Me.btnImage2)
        Me.Controls.Add(Me.btnImage1)
        Me.Controls.Add(Me.GradientPanel3)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ImageViewerForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImageViewerForm"
        CType(Me.imgMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GradientPanel3 As VB2005_Sherlock7.GradientPanel
    Friend WithEvents btnImage1 As System.Windows.Forms.Button
    Friend WithEvents btnImage2 As System.Windows.Forms.Button
    Friend WithEvents btnImage3 As System.Windows.Forms.Button
    Friend WithEvents imgMain As System.Windows.Forms.PictureBox
    Friend WithEvents lblNoImagesFound As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblImageName As System.Windows.Forms.Label
    Friend WithEvents lblImageSize As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblCreatedDate As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnDeleteAll As System.Windows.Forms.Button
    Friend WithEvents btnDeleteCurrent As System.Windows.Forms.Button
End Class
