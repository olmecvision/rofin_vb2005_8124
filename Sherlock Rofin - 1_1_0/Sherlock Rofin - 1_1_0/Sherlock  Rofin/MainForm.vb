﻿Imports System.ComponentModel
Imports VB2005_Sherlock7.EncoderService
Imports System.Runtime.Remoting.Messaging

Public Class MainForm

#Region "Constants"

    Public Const SYSTEM_REVISION As String = "1.0.2"

    Public Const ETH_Error As String = "ETH_Error"
    Public Const ETH_ErrorType As String = "ETH_ErrorType"
    Public Const ETH_ErrorMessage As String = "ETH_ErrorMessage"
    Public Const ETH_Result_Hole_Present As String = "ETH_Result_Hole_Present"
    Public Const ETH_Result_Hole_Size As String = "ETH_Result_Hole_Size"
    Public Const ETH_Status As String = "ETH_Status"
    Public Const ETH_Ready As String = "ETH_Ready"
    Public Const EXT_PASS As String = "EXT_01"
    Public Const EXT_TIME As String = "EXT_02"
    Public Const EXT_VISION_SYSTEM_HEALTHY As String = "EXT_03"
    Public Const STA_WORKS_ORDER As String = "EXT_100"
    Public Const STA_PRINT_REFERENCE As String = "EXT_101"
    Public Const STA_REEL_ID As String = "EXT_102"
    Public Const STA_HOLE_DIAMETER As String = "EXT_103"
    Public Const EXT_FREE_RUN As String = "EXT_FREE_RUN"
    Public Const EXT_ENCODER1_POSITION_DELAY As String = "EXT_ENCODER1_POSITION_DELAY"
    Public Const EXT_ENCODER2_POSITION_DELAY As String = "EXT_ENCODER2_POSITION_DELAY"
    Public Const STA_RESET_STATISTICS As String = "STA_00"
    Public Const STA_CAM_DIAMETER As String = "STA_08"
    Public Const STA_CAM_CIRCULARITY As String = "STA_38"
    Public Const STA_AVERAGE_DIAMETER As String = "STA_35"
    Public Const STA_PERCENTAGE_YIELD As String = "STA_36"
    Public Const STA_UPPER_LIMIT As String = "STA_16"
    Public Const STA_LOWER_LIMIT As String = "STA_15"
    Public Const BAT_START_BATCH As String = "BAT_00"
    Public Const STA_FRAMES_PER_SECOND As String = "FPS"
    Public verifyState As Boolean = False
    Public EncoderFineAdjustOne As Double = 0
    Public EncoderFineAdjustTwo As Double = 0
    Public EncoderFineAdjustThree As Double = 0
    Public EncoderFineAdjustFour As Double = 0
    Public currentLaser As Integer = 1

    Private Const WM_SETREDRAW As Integer = 11

#End Region

#Region "Member Variables"

    ' lasers collection
    Private m_Lasers As List(Of LaserInfo)
    Private m_CurrentLaser As LaserInfo
    Private WithEvents m_DisplayWindows As List(Of AxIpeDspCtrlLib.AxIpeDspCtrl) = New List(Of AxIpeDspCtrlLib.AxIpeDspCtrl)()

    'starflex form
    Private WithEvents m_StarFlexForm As StarflexForm
    Private WithEvents m_UpdateTimer As Timer
    Private WithEvents m_EncoderService As EncoderService

    Private m_TimerLock As Object = New Object()
    Private m_CurrentUser As UserInfo
    Private m_Variables As Dictionary(Of String, VariableInfo)
    Private m_Statistics As Dictionary(Of String, StatisticInfo)
    Private m_Formats As Dictionary(Of String, FormatInfo)
    Private m_StatisticsViewerForm As StatisticsViewerForm
    Private m_BatchStart As DateTime
    Private m_ShiftStart As DateTime
    Private m_VerifyState As Boolean = False

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
 
            Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
            'Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size
            'Size = New Size(1024, 768)
            Size = New Size(1280, 1024)

            Dim logoFile = My.Settings("LogoPath") + "\\screen-logo.png"
            PictureBox1.Load(logoFile)

            ' setup the user rights
            ConfigureUserRights()

            ' setup the hardware
            ConfigureHardware()

            ' setup the main form
            ConfigureMainFormLayout()

            ' setup the variables
            ConfigureVariables()

            ' setup the statistics
            ConfigureStatistics()

            ' setup the formats
            ConfigureFormats()

            ' setup the switch button
            ConfigureSwitchButton()

            ' select current format
            SetupLastKnownFormat()


            'starflex form before server starts
            If My.Settings.EnableStarFlex Then
                m_StarFlexForm = New StarflexForm()
            End If

            'setup StarFlex TCP Server
            If My.Settings.EnableStarFlex Then
                Tellan.StarFlexTcp.StarFlexTcpManager.Instance.Open(My.Settings.StarFlexTcpPort)
                AddHandler Tellan.StarFlexTcp.StarFlexTcpManager.Instance.SelectUnitHeadReceived, AddressOf SelectUnitHeadReceivedOverTcp
                AddHandler Tellan.StarFlexTcp.StarFlexTcpManager.Instance.DataSentReceived, AddressOf StarFlexDataReceived
            End If

            ' setup the update timer
            ConfigureUIUpdateTimer()

            ' setup the encoder system
            ConfigureEncoder()

            ' start the watchdog
            actionTimer.Start()


            m_BatchStart = DateTime.Now
            m_ShiftStart = m_BatchStart



        Catch ex As Exception
            MessageBox.Show(String.Format("{0} - {1}", ex.Message, ex.StackTrace.ToString()))
        End Try

    End Sub

#Region "Framework"

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()


    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#End Region

#Region "StarFlex TCP Events"

    Private Sub SiumiationChanged() Handles m_StarFlexForm.SimulationChanged
        'Added an event in case someone wanted to do something when it enters simulation mode
    End Sub

    Private Sub SendStatusAfterStartOrTrain(ByVal sherlock As IpeEngCtrlLib.Engine, ByVal laserUnitid As UInteger, ByVal head As UInteger)
        System.Threading.Thread.Sleep(200)
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraReady(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Ready, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Status, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Result_Hole_Present, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleDiameterStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Result_Hole_Size, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
    End Sub

    Private Sub SendStatusHealthyTick(ByVal sherlock As IpeEngCtrlLib.Engine, ByVal laserUnitid As UInteger, ByVal head As UInteger)
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraReady(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Ready, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Status, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
    End Sub

    Private Sub SendStatusResultTick(ByVal sherlock As IpeEngCtrlLib.Engine, ByVal laserUnitid As UInteger, ByVal head As UInteger)
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Result_Hole_Present, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
        Tellan.StarFlexTcp.StarFlexTcpManager.Instance.HoleDiameterStatus(laserUnitid, head, CType(SherlockReader.ReadProperty(sherlock, ETH_Result_Hole_Size, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
    End Sub

    Private Sub SendStatusAfterStartOrTrainComplete(ByVal ar As IAsyncResult)
        Dim result As AsyncResult = CType(ar, AsyncResult)
        Dim caller As SendStatusAfterStartOrTrainDelegate = CType(result.AsyncDelegate, SendStatusAfterStartOrTrainDelegate)
        caller.EndInvoke(ar)
    End Sub

    Private Sub StarFlexDataReceived(ByVal sender As Object, ByVal e As Tellan.StarFlexTcp.DataSentReceivedEventArgs)
        If (InvokeRequired) Then
            Invoke(New EventHandler(Of Tellan.StarFlexTcp.DataSentReceivedEventArgs)(AddressOf StarFlexDataReceived), New Object() {sender, e})
        Else
            m_StarFlexForm.AddData(e.TxData, e.Data)
        End If
    End Sub

    Private Sub SelectUnitHeadReceivedOverTcp(ByVal sender As Object, ByVal e As Tellan.StarFlexTcp.SelectUnitHeadReceivedEventArgs)
        If (InvokeRequired) Then
            Invoke(New EventHandler(Of Tellan.StarFlexTcp.SelectUnitHeadReceivedEventArgs)(AddressOf SelectUnitHeadReceivedOverTcp), New Object() {sender, e})
        Else
            Select Case e.Command
                Case Tellan.StarFlexTcp.StarFlexCommand.SelectUnit

                    VarSetBool(e.LaserUnitId, "EXT_104", e.State)

                Case Tellan.StarFlexTcp.StarFlexCommand.SelectHead

                    VarSetBool(e.LaserUnitId, e.Head, "EXT_104", e.State)

                Case Tellan.StarFlexTcp.StarFlexCommand.SetTeachIn

                    VarSetBool(e.LaserUnitId, e.Head, "ETH_Teach", e.State)

                    ' get the engine for a laser/head combination
                    Dim head As HeadInfo = GetHead(e.LaserUnitId, e.Head)
                    If (head Is Nothing Or head.Engine Is Nothing) Then
                        Return
                    End If

                    Dim sendStatus As SendStatusAfterStartOrTrainDelegate = New SendStatusAfterStartOrTrainDelegate(AddressOf SendStatusAfterStartOrTrain)
                    sendStatus.BeginInvoke(head.Engine, e.LaserUnitId, e.Head, AddressOf SendStatusAfterStartOrTrainComplete, Nothing)

                Case Tellan.StarFlexTcp.StarFlexCommand.StartCamera

                    VarSetBool(e.LaserUnitId, e.Head, "ETH_Run", e.State)

                    ' get the engine for a laser/head combination
                    Dim head As HeadInfo = GetHead(e.LaserUnitId, e.Head)
                    If (head Is Nothing Or head.Engine Is Nothing) Then
                        Return
                    End If

                    Dim sendStatus As SendStatusAfterStartOrTrainDelegate = New SendStatusAfterStartOrTrainDelegate(AddressOf SendStatusAfterStartOrTrain)
                    sendStatus.BeginInvoke(head.Engine, e.LaserUnitId, e.Head, AddressOf SendStatusAfterStartOrTrainComplete, Nothing)

            End Select
        End If
    End Sub

#End Region

#Region "StarFlex Delegates"

    Private Delegate Sub SetEncoderPositionCallback(position As Integer)

    Private Delegate Sub SetComparatorCallback(cmpData As Integer)

    Private Delegate Sub SetFifoStatusCallback(fifoStatus As Short)

    Private Delegate Sub SendStatusAfterStartOrTrainDelegate(ByVal sherlock As IpeEngCtrlLib.Engine, ByVal laserUnitid As UInteger, ByVal head As UInteger)

    Private Delegate Sub UpdateEncoderPositionDelegate(ByVal position)

#End Region

#Region "StarFlex Events"

    Private Sub MainForm_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If m_StarFlexForm Is Nothing Then
            Return
        End If
        If e.Alt Then
            If e.KeyCode = Keys.S Then
                m_StarFlexForm.Show()
                m_StarFlexForm.BringToFront()
                m_StarFlexForm.Focus()
            End If
        End If
    End Sub

#End Region

#Region "Button Events"

    Private Sub btnLaser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim lbl As Label = TryCast(sender, Label)
        If (lbl Is Nothing) Then
            Return
        End If

        Dim laser As LaserInfo = TryCast(lbl.Tag, LaserInfo)
        If (laser Is Nothing) Then
            Return
        End If

        Cursor = Cursors.WaitCursor

        SetupLaserCameraScreen(laser)

        Cursor = Cursors.Arrow

        'configure fine adjust

        'get laser number, load appropriate numbers to screen
        currentLaser = Convert.ToInt32(laser.LaserId)
        updateFineAdjust()


    End Sub

    Private Sub updateFineAdjust()

        Select Case currentLaser
            Case 0
                'disable and hide buttons
                btnIncreaseFineAdjust.Enabled = False
                btnDecreaseFineAdjust.Enabled = False

                btnIncreaseFineAdjust.Visible = False
                btnDecreaseFineAdjust.Visible = False

                lblPositionCorrection.Visible = False
                lblPositionNormal.Visible = False

            Case 1
                'enable buttons and set as 1
                btnIncreaseFineAdjust.Enabled = True
                btnDecreaseFineAdjust.Enabled = True

                btnIncreaseFineAdjust.Visible = True
                btnDecreaseFineAdjust.Visible = True

                lblPositionCorrection.Visible = True
                lblPositionNormal.Visible = True

                If EncoderFineAdjustOne < 0.0R Then
                    lblPositionCorrection.Text = Math.Round(EncoderFineAdjustOne, 2).ToString() + " mm"
                Else
                    lblPositionCorrection.Text = "+ " + Math.Round(EncoderFineAdjustOne, 2).ToString() + " mm"
                End If

            Case 2
                'enable buttons and set as 2
                btnIncreaseFineAdjust.Enabled = True
                btnDecreaseFineAdjust.Enabled = True

                btnIncreaseFineAdjust.Visible = True
                btnDecreaseFineAdjust.Visible = True

                lblPositionCorrection.Visible = True
                lblPositionNormal.Visible = True


                If EncoderFineAdjustTwo < 0.0R Then
                    lblPositionCorrection.Text = Math.Round(EncoderFineAdjustTwo, 2).ToString() + " mm"
                Else
                    lblPositionCorrection.Text = "+ " + Math.Round(EncoderFineAdjustTwo, 2).ToString() + " mm"
                End If

            Case 3
                'enable buttons and set as 3
            Case 4
                'enable buttons and set as 4
            Case Else
                'disable and hide buttons
                btnIncreaseFineAdjust.Enabled = False
                btnDecreaseFineAdjust.Enabled = False

                btnIncreaseFineAdjust.Visible = False
                btnDecreaseFineAdjust.Visible = False

                lblPositionCorrection.Visible = False
                lblPositionNormal.Visible = False


        End Select


    End Sub

    Private Sub btnZoom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim lbl As Label = TryCast(sender, Label)
        If (lbl Is Nothing) Then
            Return
        End If

        Dim heads As List(Of HeadInfo) = TryCast(lbl.Tag, List(Of HeadInfo))
        If (heads Is Nothing) Then
            Return
        End If

        Cursor = Cursors.WaitCursor

        btnIncreaseFineAdjust.Enabled = False
        btnDecreaseFineAdjust.Enabled = False

        btnIncreaseFineAdjust.Visible = False
        btnDecreaseFineAdjust.Visible = False

        lblPositionCorrection.Visible = False
        lblPositionNormal.Visible = False

        SetupZoomCameraScreen(heads)

        Cursor = Cursors.Arrow

    End Sub

    Private Sub btnSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetup.Click

        Dim frmUserConfigure As UserConfigureForm

        ' load the configure form
        frmUserConfigure = New UserConfigureForm()
        frmUserConfigure.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmUserConfigure.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        Dim result = frmUserConfigure.ShowDialog()

        ' check the result
        If (result = DialogResult.OK Or result = DialogResult.Yes) Then

            ' apply the user rights to the screen
            ConfigureUserRights()
        End If

    End Sub

    Private Sub btnSystemConfiguration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSystemConfiguration.Click

        Dim frmSystemConfigure As SystemConfigureForm

        ' load the configure form
        frmSystemConfigure = New SystemConfigureForm()
        frmSystemConfigure.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmSystemConfigure.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        'frmSystemConfigure.MainScreen = Me
        Dim result = frmSystemConfigure.ShowDialog()

        ' check the result
        If (result = DialogResult.OK Or result = DialogResult.Yes) Then

        End If

    End Sub

    Private Sub btnStatistics_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStatistics.Click

        ' load the configure form
        m_StatisticsViewerForm = New StatisticsViewerForm()
        m_StatisticsViewerForm.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        m_StatisticsViewerForm.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        m_StatisticsViewerForm.MainScreen = Me
        Dim result = m_StatisticsViewerForm.ShowDialog()

        ' check the result
        If (result = DialogResult.OK Or result = DialogResult.Yes) Then

        End If

    End Sub

    Private Sub btnManual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnManual.Click

        Dim fileName As String = My.Settings("ManualFile")

        'Dim proc As Process = New Process()
        'proc.StartInfo.FileName = fileName
        'proc.StartInfo.UseShellExecute = True
        'proc.Start()

        ' load the configure form
        Dim frmPDFViewer = New PDFViewerForm()
        frmPDFViewer.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmPDFViewer.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        frmPDFViewer.Url = String.Format("file:///{0}", fileName)

        Cursor = Cursors.Arrow

        frmPDFViewer.ShowDialog()

    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim frmLogin As LoginForm

        ' load the login form
        frmLogin = New LoginForm()
        frmLogin.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmLogin.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        Dim result = frmLogin.ShowDialog()

        ' check the result
        If (result = DialogResult.OK Or result = DialogResult.Yes) Then

            ' save the logged in user
            m_CurrentUser = frmLogin.User

            ' apply the user rights to the screen
            ConfigureUserRights()
        End If

    End Sub

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click

        ' save the logged in user
        m_CurrentUser = Nothing

        ' apply the user rights to the screen
        ConfigureUserRights()

    End Sub

    Private Sub btnTrigger_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_ONCE)

    End Sub

    Private Sub btnImages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImages.Click

        Dim frmImageViewer As ImageViewerForm

        ' load the configure form
        frmImageViewer = New ImageViewerForm()
        frmImageViewer.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmImageViewer.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        Dim result = frmImageViewer.ShowDialog()

    End Sub

    Private Sub btnFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFormat.Click
        If (False) Then ' remove change format routine
            Dim frmFormatPicker As FormatPickerForm

            ' load the configure form
            frmFormatPicker = New FormatPickerForm()
            frmFormatPicker.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
            frmFormatPicker.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

            frmFormatPicker.MainScreen = Me
            Dim result = frmFormatPicker.ShowDialog()
            If (frmFormatPicker.SelectedFormat IsNot Nothing) Then

                ' change the format
                ChangeFormat(frmFormatPicker.SelectedFormat)

            End If
        End If

        Dim verifyWrite As Boolean = False
        Dim displayThresholdState As Boolean = False
        Dim displayThresholdwrite As Boolean = False
        Dim freerunState As Boolean = False

        VarGetBool("EXT_Verify", verifyState)
        VarGetBool("EXT_29", displayThresholdState)
        VarGetBool("EXT_FREE_RUN", freerunState)


        If verifyState Then
            If displayThresholdState Then
                verifyWrite = False
                displayThresholdwrite = False
                freerunState = False
            Else
                verifyWrite = True
                displayThresholdwrite = True
                freerunState = True
            End If
        Else
            verifyWrite = True
            displayThresholdwrite = False
            freerunState = True
        End If

        VarSetBool("EXT_Verify", verifyWrite)
        VarSetBool("EXT_29", displayThresholdwrite)
        VarSetBool("EXT_FREE_RUN", freerunState)

    End Sub

    Private Sub btnStartBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartBatch.Click

        Dim frmStartBatch As StartBatchForm

        ' load the configure form
        frmStartBatch = New StartBatchForm()
        frmStartBatch.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmStartBatch.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        frmStartBatch.MainScreen = Me
        Dim result As DialogResult = frmStartBatch.ShowDialog()
        If (result = DialogResult.OK) Then

            ' we are now in a batch
            btnStartBatch.Visible = False
            btnEndBatch.Visible = True
            If (My.Settings.Encoder1Enable Or My.Settings.Encoder2Enable) Then
                m_EncoderService.ClearEncoder()
            End If

            System.Threading.Thread.Sleep(300)

            ' get the first head
            Dim firstHead As HeadInfo = GetFirstHead()

            '
            ' write the variables
            '
            firstHead.Engine.VarSetString("EXT_100", frmStartBatch.txtWorksOrder.Text)
            firstHead.Engine.VarSetString("EXT_101", frmStartBatch.txtPrintReference.Text)
            firstHead.Engine.VarSetString("EXT_102", frmStartBatch.txtReelId.Text)

            Dim holeDiameter As Double = 0
            Dim holeDiameter2 As Double = 0
            Double.TryParse(frmStartBatch.txtHoleDiameter.Text, holeDiameter)
            Double.TryParse(frmStartBatch.txtHoleDiameter2.Text, holeDiameter2)

            VarSetDoubleLaser("EXT_103", holeDiameter, 1)
            VarSetDoubleLaser("EXT_103", holeDiameter2, 2)

            VarSetBool(MainForm.STA_RESET_STATISTICS, True)
            System.Threading.Thread.Sleep(300)
            VarSetBool(MainForm.BAT_START_BATCH, True)

            ' save the settings back to the file
            SaveInspection()


            System.Threading.Thread.Sleep(300)

            VarSetDouble("STA_32", 0.0R)
            VarSetDouble("STA_33", 0.0R)

            UpdateScreen()
            ' set the start time
            BatchStart = DateTime.Now
            ShiftStart = BatchStart
        End If
    End Sub

    Private Sub btnEndBatch_Click(sender As System.Object, e As System.EventArgs) Handles btnEndBatch.Click

        Dim frmEndBatch As EndBatchForm

        ' load the configure form
        frmEndBatch = New EndBatchForm()
        frmEndBatch.Location = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Location
        frmEndBatch.Size = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size

        frmEndBatch.MainScreen = Me
        Dim result As DialogResult = frmEndBatch.ShowDialog()
        If (result = DialogResult.OK) Then

            ' we are now in a batch
            btnEndBatch.Visible = False
            btnStartBatch.Visible = True

        End If

    End Sub

    Private Sub lblSwitch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSwitch.Click
        Try
            AppActivate(My.Settings.SwitchToApplicationName)
        Catch ex As Exception
        End Try
    End Sub


#End Region

#Region "Sherlock Events"

    Private Sub btnRunOnce_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunOnce.Click

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_ONCE)

    End Sub

    Private Sub btnRunContinuously_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunContinuously.Click

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_CONT)

        btnRunOnce.Enabled = False
        btnExit.Enabled = False

    End Sub

    Private Sub btnHalt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHalt.Click

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_HALT_AFTER_ITERATION)

        btnRunOnce.Enabled = True
        btnExit.Enabled = True

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click

        'If chkSaveSettings.Checked Then
        '    'nErr = hSherlock.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple1.ivs")
        '    nErr = m_ipeEng1.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple1.ivs")
        '    nErr = m_ipeEng2.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple2.ivs")

        '    nErr = m_ipeEng3.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple3.ivs")
        '    nErr = m_ipeEng4.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple4.ivs")
        '    nErr = m_ipeEng5.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple5.ivs")
        '    nErr = m_ipeEng6.InvSave(System.AppDomain.CurrentDomain.BaseDirectory & "\Simple6.ivs")
        'End If

        Me.Close()

    End Sub

    Private Sub hscrThreshold_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrThreshold.Scroll

        lblThreshold.Text = hscrThreshold.Value

        VarSetDouble("varThreshold", hscrThreshold.Value)

    End Sub

    Private Sub hscrThreshold_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hscrThreshold.ValueChanged

        lblThreshold.Text = hscrThreshold.Value

    End Sub

    Private Sub hscrMaxReturned_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMaxReturned.Scroll

        lblMaxReturned.Text = hscrMaxReturned.Value

        VarSetDouble("varMaxReturned", hscrMaxReturned.Value)

    End Sub

    Private Sub hscrMaxReturned_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hscrMaxReturned.ValueChanged

        lblMaxReturned.Text = hscrMaxReturned.Value

    End Sub

    Private Sub hscrMinArea_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMinArea.Scroll

        lblMinArea.Text = hscrMinArea.Value

        VarSetDouble("varMinArea", hscrMinArea.Value)

    End Sub

    Private Sub hscrMinArea_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hscrMinArea.ValueChanged

        lblMinArea.Text = hscrMinArea.Value

    End Sub

    Private Sub hscrMaxArea_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMaxArea.Scroll

        lblMaxArea.Text = hscrMaxArea.Value

        VarSetDouble("varMaxArea", hscrMaxArea.Value)

    End Sub

    Private Sub hscrMaxArea_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hscrMaxArea.ValueChanged

        lblMaxArea.Text = hscrMaxArea.Value

    End Sub

    Private Sub chkBlackBlobs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBlackBlobs.CheckedChanged

        VarSetBool("varBlackBlobs", chkBlackBlobs.Checked)

    End Sub

    Private Sub chkLabels_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLabels.CheckedChanged

        VarSetBool("varLabels", chkLabels.Checked)

    End Sub

    Private Sub hscrDisplayZoom_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrDisplayZoom.Scroll

        Dim dblZoom As Double

        dblZoom = hscrDisplayZoom.Value / 10.0#
        lblZoomFactor.Text = dblZoom

        Dim allControls As List(Of AxIpeDspCtrlLib.AxIpeDspCtrl) = GetAllDisplayControls()
        If (allControls Is Nothing Or allControls Is Nothing Or allControls.Count = 0) Then
            Return
        End If

        For Each ctrl As AxIpeDspCtrlLib.AxIpeDspCtrl In allControls

            ctrl.SetZoom(dblZoom)

        Next

    End Sub

#End Region

#Region "Events"

    Private Sub UIUpdateTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles m_UpdateTimer.Tick

        UpdateAllItems()

    End Sub

#End Region

#Region "Private Functions"

    Private Sub CleanUpDisplayWindows()

        If (m_DisplayWindows Is Nothing Or m_DisplayWindows.Count = 0) Then
            Return
        End If

        For Each dsp As AxIpeDspCtrlLib.AxIpeDspCtrl In m_DisplayWindows

            If (dsp.GetWindow() > 0) Then
                dsp.DisconnectImgWindow()
            End If

            dsp.DisconnectEngine()
            dsp.Dispose()

        Next

        m_DisplayWindows.Clear()

    End Sub

    Private Sub SetupLaserCameraScreen(laser As LaserInfo)

        If (laser Is Nothing) Then
            Return
        End If

        If (laser.Heads Is Nothing Or laser.Heads.Count = 0) Then
            Return
        End If

        ' store the current laser number
        m_CurrentLaser = laser

        BeginControlUpdate(pnlMainLayout)

        ' clear the controls
        pnlMainLayout.Controls.Clear()

        ' clear the column styles
        pnlMainLayout.ColumnStyles.Clear()

        ' clear up here ?
        CleanUpDisplayWindows()

        ' get the heads
        Dim index As Integer = 0

        For Each item As HeadInfo In laser.Heads

            ' create a display component
            Dim dsp As AxIpeDspCtrlLib.AxIpeDspCtrl = New AxIpeDspCtrlLib.AxIpeDspCtrl()

            DirectCast(dsp, ISupportInitialize).BeginInit()

            dsp.Enabled = True

            DirectCast(dsp, ISupportInitialize).EndInit()

            dsp.Dock = DockStyle.Fill

            ' add the button
            pnlMainLayout.Controls.Add(dsp, index, 0)

            ' add the column style
            pnlMainLayout.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 100))

            ' span 2 rows
            pnlMainLayout.SetRowSpan(dsp, 2)

            dsp.CreateControl()
            dsp.ShowToolbar(True)
            dsp.SetZoom(0)

            dsp.ConnectEngine(item.Engine.GetEngineObj())

            dsp.Tag = item

            m_DisplayWindows.Add(dsp)

            ' increment the column index
            index = index + 1

        Next

        ' space the columns
        pnlMainLayout.ColumnCount = laser.Heads.Count

        ' recalculate the spacing
        For Each columnStyle As ColumnStyle In pnlMainLayout.ColumnStyles
            columnStyle.SizeType = SizeType.Percent
            columnStyle.Width = 25%
        Next

        Dim columnIndex = 0

        ' add the stats panels
        For Each item As HeadInfo In laser.Heads

            Dim pnl As TableLayoutPanel = CreateHeadStatsPanel((columnIndex = 0), item)

            pnlMainLayout.Controls.Add(pnl, columnIndex, 2)

            columnIndex = columnIndex + 1

        Next

        EndControlUpdate(pnlMainLayout)

        If (m_DisplayWindows IsNot Nothing And m_DisplayWindows.Count > 0) Then

            For Each ctrl As AxIpeDspCtrlLib.AxIpeDspCtrl In m_DisplayWindows

                ctrl.ConnectImgWindow("Cam1")

            Next

        End If

    End Sub

    Private Function CreateHeadStatsPanel(firstColumn As Boolean, head As HeadInfo) As TableLayoutPanel

        ' for the first panel add the labels
        Dim pnl As TableLayoutPanel = New TableLayoutPanel()
        pnl.Tag = "StatsPanel"

        pnl.SuspendLayout()

        If (firstColumn) Then

            pnl.ColumnCount = 2

            ' columns styles
            pnl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.0!))
            pnl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.0!))

        Else

            pnl.ColumnCount = 1

            ' columns styles
            pnl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))

        End If

        ' row styles
        pnl.RowCount = 3
        pnl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        pnl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        pnl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))

        ' other styles
        pnl.Dock = System.Windows.Forms.DockStyle.Fill

        ' create a diameter label
        If (firstColumn) Then

            Dim lblDiameterLabel As Label = New Label()
            lblDiameterLabel.Dock = System.Windows.Forms.DockStyle.Fill
            lblDiameterLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
            lblDiameterLabel.ForeColor = System.Drawing.Color.Black
            lblDiameterLabel.Text = "Diameters"
            lblDiameterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft

            ' add the labels to the panel
            pnl.Controls.Add(lblDiameterLabel, 0, 0)

        End If

        ' create a diameter value label
        Dim lblDiameter As Label = New Label()
        lblDiameter.Tag = String.Format("{0}_{1}_{2}", head.LaserId, head.HeadId, STA_CAM_DIAMETER)
        lblDiameter.Dock = System.Windows.Forms.DockStyle.Fill
        lblDiameter.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        lblDiameter.ForeColor = System.Drawing.Color.Black
        lblDiameter.Text = "0"
        lblDiameter.TextAlign = System.Drawing.ContentAlignment.MiddleRight

        If (firstColumn) Then
            pnl.Controls.Add(lblDiameter, 1, 0)
        Else
            pnl.Controls.Add(lblDiameter, 0, 0)
        End If

        ' create a circularity label
        If (firstColumn) Then

            Dim lblCircularityLabel As Label = New Label()
            lblCircularityLabel.Dock = System.Windows.Forms.DockStyle.Fill
            lblCircularityLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
            lblCircularityLabel.ForeColor = System.Drawing.Color.Black
            lblCircularityLabel.Text = "Circularity"
            lblCircularityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft

            ' add the labels to the panel
            pnl.Controls.Add(lblCircularityLabel, 0, 1)

        End If

        ' create a diameter value label
        Dim lblCircularity As Label = New Label()
        lblCircularity.Tag = String.Format("{0}_{1}_{2}", head.LaserId, head.HeadId, STA_CAM_CIRCULARITY)
        lblCircularity.Dock = System.Windows.Forms.DockStyle.Fill
        lblCircularity.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        lblCircularity.ForeColor = System.Drawing.Color.Black
        lblCircularity.Text = "0"
        lblCircularity.TextAlign = System.Drawing.ContentAlignment.MiddleRight

        If (firstColumn) Then
            pnl.Controls.Add(lblCircularity, 1, 1)
        Else
            pnl.Controls.Add(lblCircularity, 0, 1)
        End If

        ' create an avg diameter label
        If (firstColumn) Then

            Dim lblAvgDiameterLabel As Label = New Label()
            lblAvgDiameterLabel.Dock = System.Windows.Forms.DockStyle.Fill
            lblAvgDiameterLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
            lblAvgDiameterLabel.ForeColor = System.Drawing.Color.Black
            lblAvgDiameterLabel.Text = "Avg. Diameter"
            lblAvgDiameterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft

            ' add the labels to the panel
            pnl.Controls.Add(lblAvgDiameterLabel, 0, 2)

        End If

        ' create a diameter value label
        Dim lblAvgDiameter As Label = New Label()
        lblAvgDiameter.Tag = String.Format("{0}_{1}_{2}", head.LaserId, head.HeadId, STA_AVERAGE_DIAMETER)
        lblAvgDiameter.Dock = System.Windows.Forms.DockStyle.Fill
        lblAvgDiameter.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        lblAvgDiameter.ForeColor = System.Drawing.Color.Black
        lblAvgDiameter.Text = "0"
        lblAvgDiameter.TextAlign = System.Drawing.ContentAlignment.MiddleRight

        If (firstColumn) Then
            pnl.Controls.Add(lblAvgDiameter, 1, 2)
        Else
            pnl.Controls.Add(lblAvgDiameter, 0, 2)
        End If

        pnl.ResumeLayout()

        Return pnl

    End Function

    Private Sub SetupZoomCameraScreen(heads As List(Of HeadInfo))

        If (heads Is Nothing Or heads.Count = 0) Then
            Return
        End If

        BeginControlUpdate(pnlMainLayout)

        ' clear the controls
        pnlMainLayout.Controls.Clear()
        pnlMainLayout.ColumnStyles.Clear()

        ' clear up here ?
        CleanUpDisplayWindows()

        ' get the heads
        Dim index As Integer = 0
        Dim currentRowIndex As Integer = 0
        Dim currentColumnIndex As Integer = 0

        For Each item As HeadInfo In heads

            ' create a display component
            Dim dsp As AxIpeDspCtrlLib.AxIpeDspCtrl = New AxIpeDspCtrlLib.AxIpeDspCtrl()

            dsp.Dock = DockStyle.Fill

            ' make sure we get the correct row
            Dim rowIndex As Integer
            If (heads.Count > 6) Then
                rowIndex = If((index > 3), 1, 0)

            ElseIf (heads.Count > 4) Then
                rowIndex = If((index > 2), 1, 0)

            ElseIf (heads.Count > 3) Then
                rowIndex = If((index > 1), 1, 0)

            Else
                rowIndex = 0
            End If

            ' if we have changed the row index then we need to rest the column index
            If (currentRowIndex <> rowIndex) Then
                currentRowIndex = rowIndex
                currentColumnIndex = 0
            End If

            ' add the button
            pnlMainLayout.Controls.Add(dsp, currentColumnIndex, currentRowIndex)

            If (currentRowIndex = 0) Then
                ' add the column style
                pnlMainLayout.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 100))
            End If

            ' span 2 rows
            If (heads.Count < 4) Then
                pnlMainLayout.SetRowSpan(dsp, 2)
            End If

            dsp.CreateControl()
            dsp.ShowToolbar(True)
            dsp.SetZoom(0)
            dsp.ConnectEngine(item.Engine.GetEngineObj())

            m_DisplayWindows.Add(dsp)

            ' increment the column index
            index = index + 1
            currentColumnIndex = currentColumnIndex + 1

        Next

        ' space the columns
        pnlMainLayout.ColumnCount = heads.Count

        ' recalculate the spacing
        For Each columnStyle As ColumnStyle In pnlMainLayout.ColumnStyles
            columnStyle.SizeType = SizeType.Percent
            columnStyle.Width = 25%
        Next

        EndControlUpdate(pnlMainLayout)

        If (m_DisplayWindows IsNot Nothing And m_DisplayWindows.Count > 0) Then

            For Each ctrl As AxIpeDspCtrlLib.AxIpeDspCtrl In m_DisplayWindows

                ctrl.ConnectImgWindow("Zoom_Cam1")

            Next

        End If

    End Sub

    Private Sub SetupLastKnownFormat()

        Dim currentFormat As FormatInfo = Nothing

        ' get the last known format
        Dim fileName = My.Settings("CurrentFormatFile")
        Dim currentFormatStr As String = CurrentFormatService.ReadCurrentFormat(fileName)

        If (Not String.IsNullOrEmpty(currentFormatStr)) Then
            currentFormat = FormatsReader.GetFormat(m_Formats, currentFormatStr)
        End If

        If (currentFormat Is Nothing) Then
            currentFormat = FormatsReader.GetFirst(m_Formats)
        End If

        If (currentFormat Is Nothing) Then
            MsgBox("Error loading format.", MsgBoxStyle.Critical, "Error")
            Me.Close()
        End If

        ' Change Format
        ChangeFormat(currentFormat)

    End Sub

    'todo: this
    Private Sub UpdateScreen()

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return
        End If

        ' update the passed status
        If (m_Variables.ContainsKey(EXT_PASS)) Then

            Dim passed As Boolean = True

            ' get the value from each head
            For Each head As HeadInfo In allHeads

                Dim headPassed As Boolean
                head.Engine.VarGetBool(EXT_PASS, headPassed)

                ' check the result
                If (Not headPassed) Then
                    passed = False
                    Exit For
                End If

            Next

            If (passed) Then
                picResult.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Star_green
                lblResult.Text = "PASS"

                VarSetBool("EXT_Result", True)
            Else
                picResult.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Star_red
                lblResult.Text = "FAIL"

                VarSetBool("EXT_Result", False)
            End If

            'For Each head As HeadInfo In allHeads

            '    Dim b As Boolean
            '    head.Engine.VarGetBool("EXT_Result", b)
            '    Console.WriteLine(b)

            'Next
        End If

        CalculateYield()

        ' update the time information
        If (m_Variables.ContainsKey(EXT_TIME)) Then

            Dim time As Double = m_Variables.Item(EXT_TIME).Value
            lblResultMs.Text = String.Format("{0}mS", CType(time, Integer))
        End If

        ' update the healthy information
        If (m_Variables.ContainsKey(EXT_VISION_SYSTEM_HEALTHY)) Then

            Dim healthy As Boolean = m_Variables.Item(EXT_VISION_SYSTEM_HEALTHY).Value
            If (healthy) Then
                lblHealthy.Text = "Healthy"
                lblHealthy.BackColor = Color.Green
            Else
                lblHealthy.Text = "Not Healthy"
                lblHealthy.BackColor = Color.Red
            End If

        End If

        ' update the main information
        ' get the value from each head
        For Each head As HeadInfo In allHeads

            UpdateStatsLabel(STA_CAM_DIAMETER, head.LaserId, head.HeadId)
            UpdateStatsLabel(STA_CAM_CIRCULARITY, head.LaserId, head.HeadId)
            UpdateStatsLabel(STA_AVERAGE_DIAMETER, head.LaserId, head.HeadId)

        Next

        Dim firstHead As HeadInfo = GetFirstHead()
        If (firstHead IsNot Nothing) Then

            'works order
            Dim workOrderStatsKey As String = String.Format("{0}_{1}_{2}", firstHead.LaserId, firstHead.HeadId, STA_WORKS_ORDER)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(workOrderStatsKey)) Then
                lblWorkOrder.Text = "W/ Order: " + StatisticsReader.GetStatisticValue(m_Statistics.Item(workOrderStatsKey))
            End If

            'print reference
            Dim printReferenceStatsKey As String = String.Format("{0}_{1}_{2}", firstHead.LaserId, firstHead.HeadId, STA_PRINT_REFERENCE)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(printReferenceStatsKey)) Then
                lblWorkOrder.Text = "Print Ref: " + StatisticsReader.GetStatisticValue(m_Statistics.Item(printReferenceStatsKey))
            End If

            'reel id
            Dim reelIdStatsKey As String = String.Format("{0}_{1}_{2}", firstHead.LaserId, firstHead.HeadId, STA_REEL_ID)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(reelIdStatsKey)) Then
                lblReelId.Text = "Reel ID: " + StatisticsReader.GetStatisticValue(m_Statistics.Item(reelIdStatsKey))
            End If



        End If

        Dim firstHeadOfCurrentLaser As HeadInfo = m_CurrentLaser.Heads(0)
        If (firstHeadOfCurrentLaser IsNot Nothing) Then

            'lower limit
            Dim lowerLimitStatsKey As String = String.Format("{0}_{1}_{2}", firstHeadOfCurrentLaser.LaserId, firstHeadOfCurrentLaser.HeadId, STA_LOWER_LIMIT)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(lowerLimitStatsKey)) Then
                lblLowerLimit.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(lowerLimitStatsKey))
            End If

            'upper limit
            Dim upperLimitStatsKey As String = String.Format("{0}_{1}_{2}", firstHeadOfCurrentLaser.LaserId, firstHeadOfCurrentLaser.HeadId, STA_UPPER_LIMIT)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(upperLimitStatsKey)) Then
                lblUpperLimit.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(upperLimitStatsKey))
            End If

            'frame rate
            Dim frameRateStatsKey As String = String.Format("{0}_{1}_{2}", firstHeadOfCurrentLaser.LaserId, firstHeadOfCurrentLaser.HeadId, STA_FRAMES_PER_SECOND)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(frameRateStatsKey)) Then
                lblFrameRate.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(frameRateStatsKey))
            End If

            'laser diameter
            Dim laserDiameterStatsKey As String = String.Format("{0}_{1}_{2}", firstHeadOfCurrentLaser.LaserId, firstHeadOfCurrentLaser.HeadId, STA_FRAMES_PER_SECOND)
            If (m_Statistics IsNot Nothing And m_Statistics.ContainsKey(laserDiameterStatsKey)) Then
                lbllaserDiameter.Text = String.Format("Laser {0}: ", firstHeadOfCurrentLaser.LaserId) + StatisticsReader.GetStatisticValue(m_Statistics.Item(laserDiameterStatsKey))
            End If

        End If

        If (Not IsNothing(m_EncoderService)) Then
            If (m_Variables.ContainsKey(EXT_FREE_RUN)) Then

                Dim freeRun As Boolean = m_Variables.Item(EXT_FREE_RUN).Value
                If (freeRun) Then
                    m_EncoderService.StartAutoTrigger()
                Else
                    m_EncoderService.StopAutoTrigger()
                End If
            End If

            If (m_Variables.ContainsKey(EXT_ENCODER1_POSITION_DELAY)) Then
                Dim encoder1PositionDelayMillimetres As Double = Convert.ToDouble(m_Variables.Item(EXT_ENCODER1_POSITION_DELAY).Value) + EncoderFineAdjustOne
                Dim encoder1Res As Double = My.Settings.Encoder1Resolution
                If encoder1PositionDelayMillimetres > 0.0R And encoder1Res > 0.0R Then
                    Dim encoder1PositionDelay As Integer = Convert.ToInt32(encoder1PositionDelayMillimetres / (encoder1Res / 1000.0R))
                    m_EncoderService.Encoder1PositionDelay = encoder1PositionDelay
                End If
                If currentLaser = 1 Then
                    lblPositionNormal.Text = m_Variables.Item(EXT_ENCODER1_POSITION_DELAY).Value.ToString() + " mm"
                End If
            End If

            If (m_Variables.ContainsKey(EXT_ENCODER2_POSITION_DELAY)) Then
                Dim encoder2PositionDelayMillimetres As Double = Convert.ToDouble(m_Variables.Item(EXT_ENCODER2_POSITION_DELAY).Value) + EncoderFineAdjustTwo
                Dim encoder2Res As Double = My.Settings.Encoder2Resolution
                If encoder2PositionDelayMillimetres > 0.0R And encoder2Res > 0.0R Then
                    Dim encoder2PositionDelay As Integer = Convert.ToInt32(encoder2PositionDelayMillimetres / (encoder2Res / 1000.0R))
                    m_EncoderService.Encoder2PositionDelay = encoder2PositionDelay
                End If
            End If
            If currentLaser = 2 Then
                lblPositionNormal.Text = m_Variables.Item(EXT_ENCODER2_POSITION_DELAY).Value.ToString() + " mm"
            End If
        End If


    End Sub

    'todo: this
    Private Sub CalculateYield()

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return
        End If

        Dim totalImage As Integer = 0
        Dim totalPassedImage As Integer = 0


        ' get the value from each head
        For Each head As HeadInfo In allHeads

            totalImage = totalImage + SherlockReader.ReadProperty(head.Engine, "STA_32", IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT)
            totalPassedImage = totalPassedImage + SherlockReader.ReadProperty(head.Engine, "STA_33", IpeEngCtrlLib.I_VAR_TYPE.I_VAR_INT)
        Next

        Dim yield As Double = (totalPassedImage / totalImage) * 100
        If totalImage = 0 Or totalImage = 0 Then
            yield = 0.0R
        End If
        Dim yieldString = String.Format("{0:0.0}%", yield)

        lblYield.Text = yieldString

    End Sub

    Private Sub LoadInvestigation(ByVal fileName As String)

        ' abort any outstanding actions
        Try
            AbortSherlock()
        Catch ex As Exception

        End Try

        Dim nErr As IpeEngCtrlLib.I_ENG_ERROR

        ' load the investigations
        nErr = InvLoadAll(fileName)
        If Not nErr = IpeEngCtrlLib.I_ENG_ERROR.I_OK Then
            Return
        End If

        'Wait 1 second
        System.Threading.Thread.Sleep(1000)

        Try

            ' select the first laser
            Dim lbl As Label = New Label()
            lbl.Tag = m_Lasers(0)

            btnLaser_Click(lbl, EventArgs.Empty)

        Catch ex As Exception
            MsgBox("No camera available; you must use an image sequence.", MsgBoxStyle.Information)
            Return
        End Try

    End Sub

    Private Sub ChangeFormat(ByVal format As FormatInfo)

        If (format Is Nothing) Then
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim investigationName As String = String.Format("{0}\\{1}", My.Settings("InvestigationsFolder"), format.FormatFile)

        ' load the investigation
        LoadInvestigation(investigationName)

        ' update the screen
        lblFormat.Text = String.Format("{0}{1}{2}", format.FormatCode1, vbCrLf, format.FormatDescription)

        UpdateScreen()

        ' Save the format
        CurrentFormatService.SaveCurrentFormat(format)

        ' run in continuous mode
        RunContinuousSherlock()

        Cursor = Cursors.Default

    End Sub

    Private Sub HaltSherlock()

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_HALT_AFTER_ITERATION)

    End Sub

    Private Sub AbortSherlock()

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_HALT)
        InvModeWaitForHaltAll()

    End Sub

    Private Sub RunContinuousSherlock()

        Try
            AbortSherlock()
        Catch
        End Try

        InvModeSetAll(IpeEngCtrlLib.I_MODE.I_EXE_MODE_CONT)

    End Sub

    Private Sub actionTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actionTimer.Tick

        SyncLock m_TimerLock

            actionTimer.Stop()

            Dim firstHead As HeadInfo = GetFirstHead()
            '    If Not (firstHead Is Nothing) Then
            If False Then
                Dim heads As List(Of HeadInfo) = GetAllHeads()

                If True Then 'configure polled camera healthy status
                    VariablesReader.UpdateValues(m_Variables, firstHead.Engine)
                End If

                StatisticsReader.UpdateValues(m_Statistics, Me)

                If My.Settings.EnableStarFlex Then
                    UpdateStarFlexVariables(holeSizeStatus:=False, holeStatus:=False, camHealthyStatus:=True, camReady:=True)
                End If



                Dim formatChangeRequired As Boolean = SherlockReader.ReadProperty(firstHead.Engine, "CHANGE_IVS", IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL)
                If (formatChangeRequired) Then

                    ' read the new ivs file
                    Dim newFormatFile As String = SherlockReader.ReadProperty(firstHead.Engine, "REC_00", IpeEngCtrlLib.I_VAR_TYPE.I_VAR_STRING)
                    If (Not String.IsNullOrEmpty(newFormatFile)) Then

                        ' get the format
                        Dim newFormat As FormatInfo = FormatsReader.GetFormat(m_Formats, newFormatFile.ToString().ToUpper())
                        If (Not newFormat Is Nothing) Then

                            ' change to that format
                            ChangeFormat(newFormat)

                            ' clear the flag
                            firstHead.Engine.VarSetBool("CHANGE_IVS", False)
                        End If
                    End If
                End If

            End If

            actionTimer.Start()

        End SyncLock

    End Sub

    Private Sub InvModeSetAll(mode As IpeEngCtrlLib.I_MODE)

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return
        End If

        For Each head As HeadInfo In allHeads

            head.Engine.InvModeSet(mode)

        Next

    End Sub

    Private Sub InvModeWaitForHaltAll()

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return
        End If

        For Each head As HeadInfo In allHeads

            head.Engine.InvModeWaitForHalt()

        Next

    End Sub

    Private Function InvLoadAll(fileName As String) As IpeEngCtrlLib.I_ENG_ERROR

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allHeads

            Dim nErr As IpeEngCtrlLib.I_ENG_ERROR

            nErr = head.Engine.InvLoad(fileName)
            If (nErr <> IpeEngCtrlLib.I_ENG_ERROR.I_OK) Then

                MsgBox("Error loading investigation.", MsgBoxStyle.Critical, "Error")
                Return nErr

            End If
            Threading.Thread.Sleep(2000)
        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Private Function GetAllDisplayControls() As List(Of AxIpeDspCtrlLib.AxIpeDspCtrl)

        If (pnlMainLayout Is Nothing Or pnlMainLayout.Controls Is Nothing Or pnlMainLayout.Controls.Count = 0) Then
            Return Nothing
        End If

        Dim allControls As List(Of AxIpeDspCtrlLib.AxIpeDspCtrl) = New List(Of AxIpeDspCtrlLib.AxIpeDspCtrl)

        For Each ctrl As Control In pnlMainLayout.Controls

            Dim c As AxIpeDspCtrlLib.AxIpeDspCtrl = TryCast(ctrl, AxIpeDspCtrlLib.AxIpeDspCtrl)
            If Not (c Is Nothing) Then

                allControls.Add(c)

            End If

            Return allControls

        Next

    End Function

    Private Sub UpdateStatsLabel(labelName As String, laserNumber As Integer, headNumber As Integer)

        Dim statsKey As String = String.Format("{0}_{1}_{2}", laserNumber, headNumber, labelName)

        If (m_Statistics Is Nothing Or Not m_Statistics.ContainsKey(statsKey)) Then
            Return
        End If

        If (pnlMainLayout Is Nothing Or pnlMainLayout.Controls Is Nothing Or pnlMainLayout.Controls.Count = 0) Then
            Return
        End If

        ' each stat panel
        For Each ctrlMainLayout As Control In pnlMainLayout.Controls

            Dim ctrlTableLayoutPanel As TableLayoutPanel = TryCast(ctrlMainLayout, TableLayoutPanel)
            If (ctrlTableLayoutPanel Is Nothing) Then
                Continue For
            End If

            If (ctrlTableLayoutPanel.Tag <> "StatsPanel") Then
                Continue For
            End If

            If (ctrlTableLayoutPanel.Controls Is Nothing Or ctrlTableLayoutPanel.Controls.Count = 0) Then
                Continue For
            End If

            ' each label
            For Each ctrl As Control In ctrlTableLayoutPanel.Controls

                Dim lbl As Label = TryCast(ctrl, Label)
                If (lbl Is Nothing) Then
                    Continue For
                End If

                If (lbl.Tag Is Nothing) Then
                    Continue For
                End If

                If (lbl.Tag.ToString() <> statsKey) Then
                    Continue For
                End If

                If (statsKey.Contains(STA_CAM_DIAMETER)) Or (statsKey.Contains(STA_AVERAGE_DIAMETER)) Or (statsKey.Contains(STA_HOLE_DIAMETER)) Then
                    lbl.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(statsKey)) + " µm"
                ElseIf (statsKey.Contains(STA_CAM_CIRCULARITY)) Then
                    lbl.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(statsKey)) + " / 1"
                Else
                    lbl.Text = StatisticsReader.GetStatisticValue(m_Statistics.Item(statsKey))
                End If
                'ryan

            Next

        Next

    End Sub

    Public Shared Sub BeginControlUpdate(control As Control)
        Dim msgSuspendUpdate As Message = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero)

        Dim window As NativeWindow = NativeWindow.FromHandle(control.Handle)
        window.DefWndProc(msgSuspendUpdate)
    End Sub

    Public Shared Sub EndControlUpdate(control As Control)
        ' Create a C "true" boolean as an IntPtr
        Dim wparam As New IntPtr(1)
        Dim msgResumeUpdate As Message = Message.Create(control.Handle, WM_SETREDRAW, wparam, IntPtr.Zero)

        Dim window As NativeWindow = NativeWindow.FromHandle(control.Handle)
        window.DefWndProc(msgResumeUpdate)
        control.Invalidate()
        control.Refresh()
    End Sub

#End Region

#Region "Configuration"

    Public Sub ConfigureUserRights()

        Me.ControlBox = False
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.Text = "Vision System"

        ' set the role to default access
        Dim userNumber = 0

        If (m_CurrentUser IsNot Nothing) Then
            userNumber = m_CurrentUser.UserNumber

            ' set the role text
            lblRoleLevel.Text = m_CurrentUser.UserName
        Else
            lblRoleLevel.Text = String.Empty
        End If

        ' disable all options
        btnSetup.Enabled = False
        btnLogout.Enabled = False
        btnSystemConfiguration.Enabled = False
        lblRoleLevel.Visible = False
        Panel1.Visible = False
        pnlEncoder.Visible = False

        If (userNumber >= 1) Then
            btnLogout.Enabled = True
            lblRoleLevel.Visible = True
        End If

        If (userNumber >= 2) Then
            btnSystemConfiguration.Enabled = True
        End If

        If (userNumber >= 3) Then
        End If

        If (userNumber >= 4) Then
        End If

        If (userNumber >= 5) Then
            btnSetup.Enabled = True
        End If

        If (userNumber >= 6) Then
            Panel1.Visible = True
            pnlEncoder.Visible = True
            Me.ControlBox = True
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
            Me.Text = "Vision System - Revision " + SYSTEM_REVISION
        End If

        If btnSetup.Enabled Then
            btnSetup.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Setup
        Else
            btnSetup.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Setup2
        End If

        If btnStatistics.Enabled Then
            btnStatistics.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Statistics
        Else
            btnStatistics.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Statistics2
        End If

        If btnLogout.Enabled Then
            btnLogout.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Logout
        Else
            btnLogout.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Logout2
        End If

    End Sub

    Public Sub ConfigureHardware()

        Dim fileName = My.Settings("HardwareConfigurationFile")
        m_Lasers = HardwareConfigurationReader.ReadConfiguration(fileName)

    End Sub

    Public Sub ConfigureMainFormLayout()

        ConfigureLaserButtons()
        ConfigureZoomButtons()

    End Sub

    Public Sub ConfigureLaserButtons()

        If (m_Lasers Is Nothing Or m_Lasers.Count = 0) Then
            Return
        End If

        ' set the button columns
        Dim columnCount = m_Lasers.Count

        ' add the buttons
        Dim index As Integer = 0

        For Each laser As LaserInfo In m_Lasers

            ' create a camera button
            Dim btn As System.Windows.Forms.Label = New System.Windows.Forms.Label()
            btn.Tag = laser
            btn.BackColor = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
            btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            btn.ForeColor = System.Drawing.Color.White
            btn.Image = Global.VB2005_Sherlock7.My.Resources.Resources.Camera
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
            btn.Size = New System.Drawing.Size(138, 58)
            btn.Text = Space(17) & laser.LaserName
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            btn.Dock = DockStyle.Fill

            ' wire an event handler
            AddHandler btn.Click, AddressOf btnLaser_Click

            ' add the button
            pnlButtons.Controls.Add(btn, index, 0)

            ' add the column style
            pnlButtons.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 100))

            ' increment the column index
            index = index + 1

        Next

        ' space the columns
        pnlButtons.ColumnCount = m_Lasers.Count

        ' recalculate the spacing
        For Each columnStyle As ColumnStyle In pnlButtons.ColumnStyles
            columnStyle.SizeType = SizeType.Percent
            columnStyle.Width = 25%
        Next

    End Sub

    Public Sub ConfigureZoomButtons()

        If (m_Lasers Is Nothing Or m_Lasers.Count = 0) Then
            Return
        End If

        ' set the button columns
        Dim columnCount = m_Lasers.Count

        ' add the buttons
        Dim index As Integer = 0

        For Each laser As LaserInfo In m_Lasers

            ' check for an even number
            If ((index Mod 2) = 0) Then

                ' create a zoom button
                Dim btn As System.Windows.Forms.Label = New System.Windows.Forms.Label()
                'btn.Tag = laser
                btn.BackColor = System.Drawing.Color.FromArgb(CType(CType(166, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(82, Byte), Integer))
                btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                btn.ForeColor = System.Drawing.Color.White
                btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                btn.Text = "Zoom"
                btn.Dock = DockStyle.Fill

                ' this will store the collection of heads for the zoom button
                Dim headsCollection As List(Of HeadInfo) = New List(Of HeadInfo)()

                ' attach all of the heads for the lasers
                For Each item As HeadInfo In laser.Heads
                    headsCollection.Add(item)
                Next

                ' wire an event handler
                AddHandler btn.Click, AddressOf btnZoom_Click

                ' add the button
                pnlButtons.Controls.Add(btn, index, 2)

                ' span 2 columns
                If ((index + 1) < columnCount) Then

                    ' only span the columns if the is a laser after this one
                    pnlButtons.SetColumnSpan(btn, 2)

                    ' if we are spanning columns then we should also add the additional heads 
                    ' to the heads collection on the tag for the event later
                    For Each item As HeadInfo In m_Lasers(index + 1).Heads
                        headsCollection.Add(item)
                    Next


                End If

                ' update the tag
                btn.Tag = headsCollection
            End If

            ' increment the column index
            index = index + 1

        Next

    End Sub

    Public Sub ConfigureVariables()

        Dim fileName = My.Settings("VariablesDefinitionFile")
        m_Variables = VariablesReader.ReadVariables(fileName)

    End Sub

    Public Sub ConfigureStatistics()

        Dim fileName = My.Settings("StatisticsDefinitionFile")
        m_Statistics = StatisticsReader.ReadStatistics(fileName, GetAllHeads())


    End Sub

    Public Sub ConfigureFormats()

        Dim fileName = My.Settings("FormatsDefinitionFile")
        m_Formats = FormatsReader.ReadFormats(fileName)

    End Sub

    Public Sub ConfigureUIUpdateTimer()

        Dim updateInterval As Int32 = My.Settings("UpdateInterval")

        m_UpdateTimer = New Timer()
        m_UpdateTimer.Interval = updateInterval
        m_UpdateTimer.Start()

    End Sub

    Public Sub ConfigureEncoder()

        m_EncoderService = New EncoderService()
        m_EncoderService.Start()

    End Sub

    Private Sub EncoderService_EncoderPositionChanged(ByVal eventSender As System.Object, ByVal eventArgs As EncoderPositionEventArgs) Handles m_EncoderService.EncoderPositionChanged

        'Console.WriteLine(eventArgs.EncoderPosition)

        SetEncoderPosition(eventArgs.EncoderPosition)
        SetComparator(eventArgs.CmpData)
        SetFifoStatus(eventArgs.FifoStatus)

    End Sub

    Public Sub ConfigureSwitchButton()

        Dim appName = My.Settings.SwitchToApplicationName
        Dim buttonText = My.Settings.SwitchToApplicationButtonText

        lblSwitch.Text = buttonText

    End Sub

#End Region

#Region "Public Methods"

    Public Function GetFirstHead() As HeadInfo

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return Nothing
        End If

        Return allHeads(0)

    End Function

    Public Sub UpdateAllItems()

        Dim firstHead As HeadInfo = GetFirstHead()
        If Not (firstHead Is Nothing) Then
            VariablesReader.UpdateValues(m_Variables, firstHead.Engine)
        End If

        StatisticsReader.UpdateValues(m_Statistics, Me)

        If My.Settings.EnableStarFlex Then
            UpdateStarFlexVariables()
        End If

        UpdateScreen()

        ' update the statistics screen if it has been opened
        If (m_StatisticsViewerForm IsNot Nothing) Then
            m_StatisticsViewerForm.UpdateScreen()
        End If

    End Sub

    Public Sub UpdateStarFlexVariables(Optional ByVal camReady As Boolean = True, Optional ByVal camHealthyStatus As Boolean = True, Optional ByVal holeStatus As Boolean = True, Optional ByVal holeSizeStatus As Boolean = True)

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()
        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return
        End If

        If (Not m_StarFlexForm Is Nothing) Then
            If (m_StarFlexForm.Simulation) Then
                Return
            End If
        End If

        For Each head As HeadInfo In allHeads
            If True Then
                ' Ready
                '   CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                '  String.Format("LaserUnit{0}Head{0}Ready", head.LaserId, head.HeadId),
                ' CallType.Set,
                'CType(SherlockReader.ReadProperty(head.Engine, ETH_Ready, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
                SendStatusHealthyTick(head.Engine, Convert.ToUInt32(head.LaserId), Convert.ToUInt32(head.HeadId))
            End If

            If True Then
                ' Status
                '  CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                ' String.Format("LaserUnit{0}Head{0}Status", head.LaserId, head.HeadId),
                'CallType.Set,
                'CType(SherlockReader.ReadProperty(head.Engine, ETH_Status, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))
                SendStatusHealthyTick(head.Engine, Convert.ToUInt32(head.LaserId), Convert.ToUInt32(head.HeadId))
            End If

            If holeStatus Then
                'HoleStatus
                '  CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                '            String.Format("LaserUnit{0}Head{0}HoleStatus", head.LaserId, head.HeadId),
                '           CallType.Set,
                '          CType(SherlockReader.ReadProperty(head.Engine, ETH_Result_Hole_Present, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))

                ' Chekck HoleStatus

                SendStatusResultTick(head.Engine, Convert.ToUInt32(head.LaserId), Convert.ToUInt32(head.HeadId))
                Try

                    Dim result As Boolean = CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                               String.Format("LaserUnit{0}Head{0}HoleStatus", head.LaserId, head.HeadId),
                               CallType.Get)

                    If Not (result) Then
                        head.Engine.VarSetBool(ETH_Result_Hole_Present, True) 'reset to true if fail result captured
                    End If

                Catch ex As Exception

                End Try
            End If

            If holeSizeStatus Then

                ' HoleDiameter
                'CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                '          String.Format("LaserUnit{0}Head{0}HoleDiameter", head.LaserId, head.HeadId),
                '         CallType.Set,
                '        CType(SherlockReader.ReadProperty(head.Engine, ETH_Result_Hole_Size, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean))


                ' Check HoleDiameter
                SendStatusHealthyTick(head.Engine, Convert.ToUInt32(head.LaserId), Convert.ToUInt32(head.HeadId))
                Try

                    Dim result As Boolean = CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                               String.Format("LaserUnit{0}Head{0}HoleDiameter", head.LaserId, head.HeadId),
                               CallType.Get)

                    If Not (result) Then
                        head.Engine.VarSetBool(ETH_Result_Hole_Size, True) 'reset to true if fail result captured
                    End If

                Catch ex As Exception

                End Try
            End If

        Next

        ' handle the errors
        For Each head As HeadInfo In allHeads

            Dim errorActive As Boolean = CType(SherlockReader.ReadProperty(head.Engine, ETH_Error, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_BOOL), Boolean)

            Dim result As Boolean = CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                           String.Format("LaserUnit{0}Head{0}Error", head.LaserId, head.HeadId),
                           CallType.Get)

            If (errorActive And Not result) Then
                Dim errorType As String = CType(SherlockReader.ReadProperty(head.Engine, ETH_ErrorType, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_STRING), String)
                Dim errorMessage As String = CType(SherlockReader.ReadProperty(head.Engine, ETH_ErrorMessage, IpeEngCtrlLib.I_VAR_TYPE.I_VAR_STRING), String)
                Tellan.StarFlexTcp.StarFlexTcpManager.Instance.CameraError(errorType, errorMessage)
            End If

            ' set the var
            CallByName(Tellan.StarFlexTcp.StarFlexTcpManager.Instance.StarFlexVariables,
                       String.Format("LaserUnit{0}Head{0}Error", head.LaserId, head.HeadId),
                       CallType.Set,
                      errorActive)
        Next

    End Sub

    Public Function GetAllHeads() As List(Of HeadInfo)

        If (m_Lasers Is Nothing Or m_Lasers.Count = 0) Then
            Return Nothing
        End If

        Dim allHeads As List(Of HeadInfo) = New List(Of HeadInfo)

        For Each laser As LaserInfo In m_Lasers

            For Each head As HeadInfo In laser.Heads

                allHeads.Add(head)

            Next

        Next

        Return allHeads

    End Function

    Public Function GetLaser(laserNumber As Integer) As LaserInfo

        If (m_Lasers Is Nothing Or m_Lasers.Count = 0) Then
            Return Nothing
        End If

        Dim found As LaserInfo = Nothing

        For Each laser As LaserInfo In m_Lasers

            If (laser.LaserId = laserNumber) Then

                found = laser
                Exit For

            End If
        Next

        Return found

    End Function

    Public Function GetHead(laser As LaserInfo, headNumber As Integer) As HeadInfo

        If (laser Is Nothing Or laser.Heads Is Nothing Or laser.Heads.Count = 0) Then
            Return Nothing
        End If

        Dim found As HeadInfo = Nothing

        For Each head As HeadInfo In laser.Heads

            If (head.HeadId = headNumber) Then

                found = head
                Exit For

            End If
        Next

        Return found

    End Function

    Public Function GetHead(laserNumber As Integer, headNumber As Integer) As HeadInfo

        Dim laser As LaserInfo = GetLaser(laserNumber)
        If (laser Is Nothing) Then
            Return Nothing
        End If

        Dim found As HeadInfo = Nothing

        For Each head As HeadInfo In laser.Heads

            If (head.HeadId = headNumber) Then

                found = head
                Exit For

            End If
        Next

        Return found

    End Function

    Public Function GetHeadsPerLaser(laserNumber As Integer) As List(Of HeadInfo)

        Dim laser As LaserInfo = GetLaser(laserNumber)
        If (laser Is Nothing) Then
            Return Nothing
        End If


        If (Not laser.Heads Is Nothing) Then
            Return laser.Heads
        Else
            'no heads found add functionality Olmec
            Return laser.Heads
        End If

    End Function

    Public Function VarGetBool(name As String, ByRef value As Boolean) As IpeEngCtrlLib.I_ENG_ERROR

        Dim head As HeadInfo = GetFirstHead()
        If (head Is Nothing) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        Return head.Engine.VarGetBool(name, value)

    End Function

    Public Function VarSetBool(name As String, value As Boolean) As IpeEngCtrlLib.I_ENG_ERROR

        ' set all heads
        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allHeads

            head.Engine.VarSetBool(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetBool(laserNumber As Integer, name As String, value As Boolean) As IpeEngCtrlLib.I_ENG_ERROR

        Dim laser As LaserInfo = GetLaser(laserNumber)
        If (laser Is Nothing) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        If (laser.Heads Is Nothing Or laser.Heads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In laser.Heads

            head.Engine.VarSetBool(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetBool(laserNumber As Integer, headNumber As Integer, name As String, value As Boolean) As IpeEngCtrlLib.I_ENG_ERROR

        Dim head As HeadInfo = GetHead(laserNumber, headNumber)
        If (head Is Nothing) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        head.Engine.VarSetBool(name, value)

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetString(name As String, value As String) As IpeEngCtrlLib.I_ENG_ERROR

        ' set all heads
        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allHeads

            head.Engine.VarSetString(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetString(laserNumber As Integer, name As String, value As String) As IpeEngCtrlLib.I_ENG_ERROR

        Dim laser As LaserInfo = GetLaser(laserNumber)
        If (laser Is Nothing) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        If (laser.Heads Is Nothing Or laser.Heads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In laser.Heads

            head.Engine.VarSetString(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetString(laserNumber As Integer, headNumber As Integer, name As String, value As String) As IpeEngCtrlLib.I_ENG_ERROR

        Dim head As HeadInfo = GetHead(laserNumber, headNumber)
        If (head Is Nothing) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        head.Engine.VarSetString(name, value)

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetDouble(name As String, value As Double) As IpeEngCtrlLib.I_ENG_ERROR

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allHeads

            head.Engine.VarSetDouble(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function VarSetDoubleLaser(name As String, value As Double, laserID As Integer) As IpeEngCtrlLib.I_ENG_ERROR

        Dim allLaserHeads As List(Of HeadInfo) = GetHeadsPerLaser(laserID)

        If (allLaserHeads Is Nothing Or allLaserHeads?.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allLaserHeads

            head.Engine.VarSetDouble(name, value)

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

    Public Function SaveInspection() As IpeEngCtrlLib.I_ENG_ERROR

        Dim allHeads As List(Of HeadInfo) = GetAllHeads()

        If (allHeads Is Nothing Or allHeads.Count = 0) Then
            Return IpeEngCtrlLib.I_ENG_ERROR.I_DOES_NOT_EXISTS
        End If

        For Each head As HeadInfo In allHeads

            Dim investigationName As String = String.Empty
            Dim err As IpeEngCtrlLib.I_ENG_ERROR

            err = head.Engine.InvGetName(investigationName)
            If (err <> IpeEngCtrlLib.I_ENG_ERROR.I_OK) Then
                Return err
            End If

            err = head.Engine.InvSave(investigationName)
            If (err <> IpeEngCtrlLib.I_ENG_ERROR.I_OK) Then
                Return err
            End If

        Next

        Return IpeEngCtrlLib.I_ENG_ERROR.I_OK

    End Function

#End Region

#Region "Properties"

    Public Property Variables() As Dictionary(Of String, VariableInfo)
        Get
            Return m_Variables
        End Get
        Private Set(ByVal value As Dictionary(Of String, VariableInfo))
            m_Variables = value
        End Set
    End Property

    Public Property Statistics() As Dictionary(Of String, StatisticInfo)
        Get
            Return m_Statistics
        End Get
        Private Set(ByVal value As Dictionary(Of String, StatisticInfo))
            m_Statistics = value
        End Set
    End Property

    Public Property Formats() As Dictionary(Of String, FormatInfo)
        Get
            Return m_Formats
        End Get
        Private Set(ByVal value As Dictionary(Of String, FormatInfo))
            m_Formats = value
        End Set
    End Property

    Public Property BatchStart() As DateTime
        Get
            Return m_BatchStart
        End Get
        Set(ByVal value As DateTime)
            m_BatchStart = value
        End Set
    End Property

    Public Property ShiftStart() As DateTime
        Get
            Return m_ShiftStart
        End Get
        Set(ByVal value As DateTime)
            m_ShiftStart = value
        End Set
    End Property

    Private Sub SetEncoderPosition(ByVal position As Integer)

        ' InvokeRequired required compares the thread ID of the 
        ' calling thread to the thread ID of the creating thread. 
        ' If these threads are different, it returns true. 
        If Me.txtEncoderPosition.InvokeRequired Then
            Dim d As New SetEncoderPositionCallback(AddressOf SetEncoderPosition)
            Me.Invoke(d, New Object() {position})
        Else
            Me.txtEncoderPosition.Text = position.ToString()
        End If
    End Sub

    Private Sub SetComparator(ByVal cmpData As Integer)

        ' InvokeRequired required compares the thread ID of the 
        ' calling thread to the thread ID of the creating thread. 
        ' If these threads are different, it returns true. 
        If Me.txtComparator.InvokeRequired Then
            Dim d As New SetComparatorCallback(AddressOf SetComparator)
            Me.Invoke(d, New Object() {cmpData})
        Else
            Me.txtComparator.Text = cmpData.ToString()
        End If
    End Sub

    Private Sub SetFifoStatus(ByVal fifoStatus As Short)

        ' InvokeRequired required compares the thread ID of the 
        ' calling thread to the thread ID of the creating thread. 
        ' If these threads are different, it returns true. 
        If Me.txtFifoStatus.InvokeRequired Then
            Dim d As New SetFifoStatusCallback(AddressOf SetFifoStatus)
            Me.Invoke(d, New Object() {fifoStatus})
        Else
            Me.txtFifoStatus.Text = fifoStatus.ToString()
        End If
    End Sub

#End Region

    Private Sub ButtonAdjustDecrease_Click(sender As Object, e As EventArgs) Handles btnDecreaseFineAdjust.Click

        changeFineAdjustValue(False)
    End Sub

    Private Sub changeFineAdjustValue(increase As Boolean)
        Dim changeAmount As Double = 0.02R
        Dim currentFineAdjust As Double = 0
        Select Case currentLaser
            Case 0
                'disable and hide buttons

            Case 1
                currentFineAdjust = EncoderFineAdjustOne

                If increase Then
                    currentFineAdjust += changeAmount
                Else
                    currentFineAdjust -= changeAmount
                End If

                If currentFineAdjust > My.Settings.EncoderFineAdjustLimit Then
                    currentFineAdjust = My.Settings.EncoderFineAdjustLimit
                End If

                If currentFineAdjust < My.Settings.EncoderFineAdjustLimit * -1 Then
                    currentFineAdjust = My.Settings.EncoderFineAdjustLimit * -1
                End If

                EncoderFineAdjustOne = currentFineAdjust

            Case 2
                currentFineAdjust = EncoderFineAdjustTwo

                If increase Then
                    currentFineAdjust += changeAmount
                Else
                    currentFineAdjust -= changeAmount
                End If

                If currentFineAdjust > My.Settings.EncoderFineAdjustLimit Then
                    currentFineAdjust = My.Settings.EncoderFineAdjustLimit
                End If

                If currentFineAdjust < My.Settings.EncoderFineAdjustLimit * -1 Then
                    currentFineAdjust = My.Settings.EncoderFineAdjustLimit * -1
                End If

                EncoderFineAdjustTwo = currentFineAdjust

            Case 3
                'enable buttons and set as 3
            Case 4
                'enable buttons and set as 4
            Case Else
        End Select

        updateFineAdjust()

    End Sub

    Private Sub btnIncreaseFineAdjust_Click(sender As Object, e As EventArgs) Handles btnIncreaseFineAdjust.Click
        changeFineAdjustValue(True)
    End Sub

    Private Sub pnlMainLayout_Paint(sender As Object, e As PaintEventArgs) Handles pnlMainLayout.Paint

    End Sub
End Class