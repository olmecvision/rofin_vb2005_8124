﻿Imports System.Runtime.InteropServices

Public Class FormatPickerForm

    Dim m_MainScreen As MainForm
    Dim m_dictionary As Dictionary(Of String, FormatInfo)
    Dim m_SelectedFormat As FormatInfo

    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

    Private Sub FormatPickerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If (m_MainScreen Is Nothing) Then
            Return
        End If

        If (m_MainScreen.Formats Is Nothing) Then
            Return
        End If

        ' create the dictionary
        m_dictionary = New Dictionary(Of String, FormatInfo)()

        ' set up the auto complete database
        Dim autoComplete = New AutoCompleteStringCollection()

        Dim enumerator As IEnumerator = m_MainScreen.Formats.GetEnumerator()
        While (enumerator.MoveNext())

            ' get the item
            Dim item As FormatInfo = CType(enumerator.Current, KeyValuePair(Of String, FormatInfo)).Value

            ' format the item
            Dim strItem = item.FormatCode1
            If ((Not String.IsNullOrEmpty(strItem)) And (Not String.IsNullOrEmpty(item.FormatCode2))) Then
                strItem = strItem + " - "
            End If

            If (Not String.IsNullOrEmpty(item.FormatCode2)) Then
                strItem = strItem + item.FormatCode2
            End If

            ' add to the dictionary
            m_dictionary.Add(strItem, item)

            'set the item to the autocomplete list
            autoComplete.Add(strItem)

        End While

        lbFormat.DataSource = autoComplete

        ' hook it up
        txtFormat.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtFormat.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtFormat.AutoCompleteCustomSource = autoComplete

    End Sub

#Region "Button Events"

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        m_SelectedFormat = Nothing
        Me.Close()
    End Sub

    Private Sub btnChangeFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeFormat.Click
        Me.Close()
    End Sub

    Private Sub txtFormat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFormat.TextChanged

        m_SelectedFormat = Nothing

        If (Not String.IsNullOrEmpty(txtFormat.Text)) Then
            If (m_dictionary.ContainsKey(txtFormat.Text)) Then
                txtFormatDescription.Text = m_dictionary(txtFormat.Text).FormatDescription
                m_SelectedFormat = m_dictionary(txtFormat.Text)
            Else
                txtFormatDescription.Text = Nothing
            End If
        End If

        UpdateScreen()

    End Sub

#End Region

    Private Sub UpdateScreen()

        Dim enableChangeButton As Boolean = False

        If (Not String.IsNullOrEmpty(txtFormat.Text)) Then
            If (m_dictionary.ContainsKey(txtFormat.Text)) Then
                enableChangeButton = True
            End If
        End If

        btnChangeFormat.Enabled = enableChangeButton

    End Sub

#Region "Properties"

    Public Property MainScreen() As MainForm
        Get
            Return m_MainScreen
        End Get
        Set(ByVal value As MainForm)
            m_MainScreen = value
        End Set
    End Property

    Public Property SelectedFormat() As FormatInfo
        Get
            Return m_SelectedFormat
        End Get
        Set(ByVal value As FormatInfo)
            m_SelectedFormat = value
        End Set
    End Property

#End Region

    Private Sub lbFormat_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbFormat.SelectedValueChanged

        If (My.Settings.ScanFolderForInvestigations) Then
            txtFormat.Text = lbFormat.SelectedValue

            lbFormat.Visible = False
        End If

    End Sub

    Private Sub txtFormat_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFormat.Enter

        If (My.Settings.ScanFolderForInvestigations) Then
            lbFormat.Visible = True
        End If

    End Sub

   
End Class