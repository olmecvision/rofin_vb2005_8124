﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KeyboardControl
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnl1Left = New System.Windows.Forms.Button
        Me.pnl1 = New System.Windows.Forms.Button
        Me.pnl2 = New System.Windows.Forms.Button
        Me.pnl3 = New System.Windows.Forms.Button
        Me.pnl4 = New System.Windows.Forms.Button
        Me.pnl5 = New System.Windows.Forms.Button
        Me.pnl6 = New System.Windows.Forms.Button
        Me.pnl7 = New System.Windows.Forms.Button
        Me.pnl8 = New System.Windows.Forms.Button
        Me.pnl9 = New System.Windows.Forms.Button
        Me.pnl0 = New System.Windows.Forms.Button
        Me.pnlMinus = New System.Windows.Forms.Button
        Me.pnlPlus = New System.Windows.Forms.Button
        Me.pnlBackspace = New System.Windows.Forms.Button
        Me.pnlSpace = New System.Windows.Forms.Button
        Me.pnlBracketRight = New System.Windows.Forms.Button
        Me.pnlBracketLeft = New System.Windows.Forms.Button
        Me.pnlP = New System.Windows.Forms.Button
        Me.pnlO = New System.Windows.Forms.Button
        Me.pnlI = New System.Windows.Forms.Button
        Me.pnlU = New System.Windows.Forms.Button
        Me.pnlY = New System.Windows.Forms.Button
        Me.pnlT = New System.Windows.Forms.Button
        Me.pnlR = New System.Windows.Forms.Button
        Me.pnlE = New System.Windows.Forms.Button
        Me.pnlW = New System.Windows.Forms.Button
        Me.pnlQ = New System.Windows.Forms.Button
        Me.ButtonEx27 = New System.Windows.Forms.Button
        Me.ButtonEx28 = New System.Windows.Forms.Button
        Me.pnlCaps = New System.Windows.Forms.Button
        Me.pnlAt = New System.Windows.Forms.Button
        Me.pnlColon = New System.Windows.Forms.Button
        Me.pnlL = New System.Windows.Forms.Button
        Me.pnlK = New System.Windows.Forms.Button
        Me.pnlJ = New System.Windows.Forms.Button
        Me.pnlH = New System.Windows.Forms.Button
        Me.pnlG = New System.Windows.Forms.Button
        Me.pnlF = New System.Windows.Forms.Button
        Me.pnlD = New System.Windows.Forms.Button
        Me.pnlS = New System.Windows.Forms.Button
        Me.pnlA = New System.Windows.Forms.Button
        Me.pnlEnter = New System.Windows.Forms.Button
        Me.pnlShift1 = New System.Windows.Forms.Button
        Me.pnlShift2 = New System.Windows.Forms.Button
        Me.pnlSlash = New System.Windows.Forms.Button
        Me.pnlDot = New System.Windows.Forms.Button
        Me.pnlComma = New System.Windows.Forms.Button
        Me.pnlM = New System.Windows.Forms.Button
        Me.pnlN = New System.Windows.Forms.Button
        Me.pnlB = New System.Windows.Forms.Button
        Me.pnlV = New System.Windows.Forms.Button
        Me.pnlC = New System.Windows.Forms.Button
        Me.pnlX = New System.Windows.Forms.Button
        Me.pnlZ = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'pnl1Left
        '
        Me.pnl1Left.BackColor = System.Drawing.Color.White
        Me.pnl1Left.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl1Left.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl1Left.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1Left.Location = New System.Drawing.Point(3, 3)
        Me.pnl1Left.Name = "pnl1Left"
        Me.pnl1Left.Size = New System.Drawing.Size(48, 48)
        Me.pnl1Left.TabIndex = 0
        Me.pnl1Left.Tag = "`"
        Me.pnl1Left.Text = "¬" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "`"
        Me.pnl1Left.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl1Left.UseVisualStyleBackColor = False
        '
        'pnl1
        '
        Me.pnl1.BackColor = System.Drawing.Color.White
        Me.pnl1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(57, 3)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(48, 48)
        Me.pnl1.TabIndex = 1
        Me.pnl1.Tag = "1"
        Me.pnl1.Text = "!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1"
        Me.pnl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl1.UseVisualStyleBackColor = False
        '
        'pnl2
        '
        Me.pnl2.BackColor = System.Drawing.Color.White
        Me.pnl2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl2.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl2.Location = New System.Drawing.Point(111, 3)
        Me.pnl2.Name = "pnl2"
        Me.pnl2.Size = New System.Drawing.Size(48, 48)
        Me.pnl2.TabIndex = 2
        Me.pnl2.Tag = "2"
        Me.pnl2.Text = """" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2"
        Me.pnl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl2.UseVisualStyleBackColor = False
        '
        'pnl3
        '
        Me.pnl3.BackColor = System.Drawing.Color.White
        Me.pnl3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl3.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl3.Location = New System.Drawing.Point(165, 3)
        Me.pnl3.Name = "pnl3"
        Me.pnl3.Size = New System.Drawing.Size(48, 48)
        Me.pnl3.TabIndex = 3
        Me.pnl3.Tag = "3"
        Me.pnl3.Text = "£" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "3"
        Me.pnl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl3.UseVisualStyleBackColor = False
        '
        'pnl4
        '
        Me.pnl4.BackColor = System.Drawing.Color.White
        Me.pnl4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl4.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl4.Location = New System.Drawing.Point(219, 3)
        Me.pnl4.Name = "pnl4"
        Me.pnl4.Size = New System.Drawing.Size(48, 48)
        Me.pnl4.TabIndex = 4
        Me.pnl4.Tag = "4"
        Me.pnl4.Text = "$" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "4"
        Me.pnl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl4.UseVisualStyleBackColor = False
        '
        'pnl5
        '
        Me.pnl5.BackColor = System.Drawing.Color.White
        Me.pnl5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl5.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl5.Location = New System.Drawing.Point(273, 3)
        Me.pnl5.Name = "pnl5"
        Me.pnl5.Size = New System.Drawing.Size(48, 48)
        Me.pnl5.TabIndex = 5
        Me.pnl5.Tag = "5"
        Me.pnl5.Text = "%" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "5"
        Me.pnl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl5.UseVisualStyleBackColor = False
        '
        'pnl6
        '
        Me.pnl6.BackColor = System.Drawing.Color.White
        Me.pnl6.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl6.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl6.Location = New System.Drawing.Point(327, 3)
        Me.pnl6.Name = "pnl6"
        Me.pnl6.Size = New System.Drawing.Size(48, 48)
        Me.pnl6.TabIndex = 6
        Me.pnl6.Tag = "6"
        Me.pnl6.Text = "^" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "6"
        Me.pnl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl6.UseVisualStyleBackColor = False
        '
        'pnl7
        '
        Me.pnl7.BackColor = System.Drawing.Color.White
        Me.pnl7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl7.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl7.Location = New System.Drawing.Point(381, 3)
        Me.pnl7.Name = "pnl7"
        Me.pnl7.Size = New System.Drawing.Size(48, 48)
        Me.pnl7.TabIndex = 7
        Me.pnl7.Tag = "7"
        Me.pnl7.Text = "&&" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "7"
        Me.pnl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl7.UseVisualStyleBackColor = False
        '
        'pnl8
        '
        Me.pnl8.BackColor = System.Drawing.Color.White
        Me.pnl8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl8.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl8.Location = New System.Drawing.Point(435, 3)
        Me.pnl8.Name = "pnl8"
        Me.pnl8.Size = New System.Drawing.Size(48, 48)
        Me.pnl8.TabIndex = 8
        Me.pnl8.Tag = "8"
        Me.pnl8.Text = "*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "8"
        Me.pnl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl8.UseVisualStyleBackColor = False
        '
        'pnl9
        '
        Me.pnl9.BackColor = System.Drawing.Color.White
        Me.pnl9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl9.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl9.Location = New System.Drawing.Point(489, 3)
        Me.pnl9.Name = "pnl9"
        Me.pnl9.Size = New System.Drawing.Size(48, 48)
        Me.pnl9.TabIndex = 9
        Me.pnl9.Tag = "9"
        Me.pnl9.Text = "(" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "9"
        Me.pnl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl9.UseVisualStyleBackColor = False
        '
        'pnl0
        '
        Me.pnl0.BackColor = System.Drawing.Color.White
        Me.pnl0.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnl0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnl0.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnl0.Location = New System.Drawing.Point(543, 3)
        Me.pnl0.Name = "pnl0"
        Me.pnl0.Size = New System.Drawing.Size(48, 48)
        Me.pnl0.TabIndex = 10
        Me.pnl0.Tag = "0"
        Me.pnl0.Text = ")" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "0"
        Me.pnl0.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnl0.UseVisualStyleBackColor = False
        '
        'pnlMinus
        '
        Me.pnlMinus.BackColor = System.Drawing.Color.White
        Me.pnlMinus.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlMinus.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlMinus.Location = New System.Drawing.Point(598, 3)
        Me.pnlMinus.Name = "pnlMinus"
        Me.pnlMinus.Size = New System.Drawing.Size(48, 48)
        Me.pnlMinus.TabIndex = 11
        Me.pnlMinus.Tag = "-"
        Me.pnlMinus.Text = "_" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "-"
        Me.pnlMinus.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlMinus.UseVisualStyleBackColor = False
        '
        'pnlPlus
        '
        Me.pnlPlus.BackColor = System.Drawing.Color.White
        Me.pnlPlus.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlPlus.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlPlus.Location = New System.Drawing.Point(652, 3)
        Me.pnlPlus.Name = "pnlPlus"
        Me.pnlPlus.Size = New System.Drawing.Size(48, 48)
        Me.pnlPlus.TabIndex = 12
        Me.pnlPlus.Tag = "="
        Me.pnlPlus.Text = "+" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "="
        Me.pnlPlus.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlPlus.UseVisualStyleBackColor = False
        '
        'pnlBackspace
        '
        Me.pnlBackspace.BackColor = System.Drawing.Color.White
        Me.pnlBackspace.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlBackspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlBackspace.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlBackspace.Location = New System.Drawing.Point(706, 3)
        Me.pnlBackspace.Name = "pnlBackspace"
        Me.pnlBackspace.Size = New System.Drawing.Size(92, 48)
        Me.pnlBackspace.TabIndex = 13
        Me.pnlBackspace.Tag = "{BACKSPACE}"
        Me.pnlBackspace.Text = "Backspace"
        Me.pnlBackspace.UseVisualStyleBackColor = False
        '
        'pnlSpace
        '
        Me.pnlSpace.BackColor = System.Drawing.Color.White
        Me.pnlSpace.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlSpace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlSpace.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSpace.Location = New System.Drawing.Point(273, 219)
        Me.pnlSpace.Name = "pnlSpace"
        Me.pnlSpace.Size = New System.Drawing.Size(264, 48)
        Me.pnlSpace.TabIndex = 53
        Me.pnlSpace.Tag = " "
        Me.pnlSpace.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlSpace.UseVisualStyleBackColor = False
        '
        'pnlBracketRight
        '
        Me.pnlBracketRight.BackColor = System.Drawing.Color.White
        Me.pnlBracketRight.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlBracketRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlBracketRight.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlBracketRight.Location = New System.Drawing.Point(673, 57)
        Me.pnlBracketRight.Name = "pnlBracketRight"
        Me.pnlBracketRight.Size = New System.Drawing.Size(48, 48)
        Me.pnlBracketRight.TabIndex = 26
        Me.pnlBracketRight.Tag = "]"
        Me.pnlBracketRight.Text = "}" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "]"
        Me.pnlBracketRight.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlBracketRight.UseVisualStyleBackColor = False
        '
        'pnlBracketLeft
        '
        Me.pnlBracketLeft.BackColor = System.Drawing.Color.White
        Me.pnlBracketLeft.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlBracketLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlBracketLeft.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlBracketLeft.Location = New System.Drawing.Point(619, 57)
        Me.pnlBracketLeft.Name = "pnlBracketLeft"
        Me.pnlBracketLeft.Size = New System.Drawing.Size(48, 48)
        Me.pnlBracketLeft.TabIndex = 25
        Me.pnlBracketLeft.Tag = "["
        Me.pnlBracketLeft.Text = "{" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "["
        Me.pnlBracketLeft.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlBracketLeft.UseVisualStyleBackColor = False
        '
        'pnlP
        '
        Me.pnlP.BackColor = System.Drawing.Color.White
        Me.pnlP.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlP.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlP.Location = New System.Drawing.Point(565, 57)
        Me.pnlP.Name = "pnlP"
        Me.pnlP.Size = New System.Drawing.Size(48, 48)
        Me.pnlP.TabIndex = 24
        Me.pnlP.Tag = "p"
        Me.pnlP.Text = "P"
        Me.pnlP.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlP.UseVisualStyleBackColor = False
        '
        'pnlO
        '
        Me.pnlO.BackColor = System.Drawing.Color.White
        Me.pnlO.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlO.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlO.Location = New System.Drawing.Point(511, 57)
        Me.pnlO.Name = "pnlO"
        Me.pnlO.Size = New System.Drawing.Size(48, 48)
        Me.pnlO.TabIndex = 23
        Me.pnlO.Tag = "o"
        Me.pnlO.Text = "O"
        Me.pnlO.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlO.UseVisualStyleBackColor = False
        '
        'pnlI
        '
        Me.pnlI.BackColor = System.Drawing.Color.White
        Me.pnlI.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlI.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlI.Location = New System.Drawing.Point(457, 57)
        Me.pnlI.Name = "pnlI"
        Me.pnlI.Size = New System.Drawing.Size(48, 48)
        Me.pnlI.TabIndex = 22
        Me.pnlI.Tag = "i"
        Me.pnlI.Text = "I"
        Me.pnlI.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlI.UseVisualStyleBackColor = False
        '
        'pnlU
        '
        Me.pnlU.BackColor = System.Drawing.Color.White
        Me.pnlU.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlU.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlU.Location = New System.Drawing.Point(403, 57)
        Me.pnlU.Name = "pnlU"
        Me.pnlU.Size = New System.Drawing.Size(48, 48)
        Me.pnlU.TabIndex = 21
        Me.pnlU.Tag = "u"
        Me.pnlU.Text = "U"
        Me.pnlU.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlU.UseVisualStyleBackColor = False
        '
        'pnlY
        '
        Me.pnlY.BackColor = System.Drawing.Color.White
        Me.pnlY.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlY.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlY.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlY.Location = New System.Drawing.Point(349, 57)
        Me.pnlY.Name = "pnlY"
        Me.pnlY.Size = New System.Drawing.Size(48, 48)
        Me.pnlY.TabIndex = 20
        Me.pnlY.Tag = "y"
        Me.pnlY.Text = "Y"
        Me.pnlY.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlY.UseVisualStyleBackColor = False
        '
        'pnlT
        '
        Me.pnlT.BackColor = System.Drawing.Color.White
        Me.pnlT.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlT.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlT.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlT.Location = New System.Drawing.Point(295, 57)
        Me.pnlT.Name = "pnlT"
        Me.pnlT.Size = New System.Drawing.Size(48, 48)
        Me.pnlT.TabIndex = 19
        Me.pnlT.Tag = "t"
        Me.pnlT.Text = "T"
        Me.pnlT.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlT.UseVisualStyleBackColor = False
        '
        'pnlR
        '
        Me.pnlR.BackColor = System.Drawing.Color.White
        Me.pnlR.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlR.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlR.Location = New System.Drawing.Point(241, 57)
        Me.pnlR.Name = "pnlR"
        Me.pnlR.Size = New System.Drawing.Size(48, 48)
        Me.pnlR.TabIndex = 18
        Me.pnlR.Tag = "r"
        Me.pnlR.Text = "R"
        Me.pnlR.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlR.UseVisualStyleBackColor = False
        '
        'pnlE
        '
        Me.pnlE.BackColor = System.Drawing.Color.White
        Me.pnlE.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlE.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlE.Location = New System.Drawing.Point(187, 57)
        Me.pnlE.Name = "pnlE"
        Me.pnlE.Size = New System.Drawing.Size(48, 48)
        Me.pnlE.TabIndex = 17
        Me.pnlE.Tag = "e"
        Me.pnlE.Text = "E"
        Me.pnlE.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlE.UseVisualStyleBackColor = False
        '
        'pnlW
        '
        Me.pnlW.BackColor = System.Drawing.Color.White
        Me.pnlW.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlW.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlW.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlW.Location = New System.Drawing.Point(133, 57)
        Me.pnlW.Name = "pnlW"
        Me.pnlW.Size = New System.Drawing.Size(48, 48)
        Me.pnlW.TabIndex = 16
        Me.pnlW.Tag = "w"
        Me.pnlW.Text = "W"
        Me.pnlW.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlW.UseVisualStyleBackColor = False
        '
        'pnlQ
        '
        Me.pnlQ.BackColor = System.Drawing.Color.White
        Me.pnlQ.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlQ.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlQ.Location = New System.Drawing.Point(79, 57)
        Me.pnlQ.Name = "pnlQ"
        Me.pnlQ.Size = New System.Drawing.Size(48, 48)
        Me.pnlQ.TabIndex = 15
        Me.pnlQ.Tag = "q"
        Me.pnlQ.Text = "Q"
        Me.pnlQ.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlQ.UseVisualStyleBackColor = False
        '
        'ButtonEx27
        '
        Me.ButtonEx27.BackColor = System.Drawing.Color.White
        Me.ButtonEx27.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonEx27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonEx27.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEx27.Location = New System.Drawing.Point(3, 57)
        Me.ButtonEx27.Name = "ButtonEx27"
        Me.ButtonEx27.Size = New System.Drawing.Size(70, 48)
        Me.ButtonEx27.TabIndex = 14
        Me.ButtonEx27.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.ButtonEx27.UseVisualStyleBackColor = False
        '
        'ButtonEx28
        '
        Me.ButtonEx28.BackColor = System.Drawing.Color.White
        Me.ButtonEx28.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonEx28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonEx28.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEx28.Location = New System.Drawing.Point(728, 57)
        Me.ButtonEx28.Name = "ButtonEx28"
        Me.ButtonEx28.Size = New System.Drawing.Size(70, 48)
        Me.ButtonEx28.TabIndex = 27
        Me.ButtonEx28.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.ButtonEx28.UseVisualStyleBackColor = False
        '
        'pnlCaps
        '
        Me.pnlCaps.BackColor = System.Drawing.Color.White
        Me.pnlCaps.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlCaps.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlCaps.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlCaps.Location = New System.Drawing.Point(3, 111)
        Me.pnlCaps.Name = "pnlCaps"
        Me.pnlCaps.Size = New System.Drawing.Size(102, 48)
        Me.pnlCaps.TabIndex = 28
        Me.pnlCaps.Tag = ""
        Me.pnlCaps.Text = "Caps Lock"
        Me.pnlCaps.UseVisualStyleBackColor = False
        '
        'pnlAt
        '
        Me.pnlAt.BackColor = System.Drawing.Color.White
        Me.pnlAt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlAt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlAt.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlAt.Location = New System.Drawing.Point(652, 111)
        Me.pnlAt.Name = "pnlAt"
        Me.pnlAt.Size = New System.Drawing.Size(48, 48)
        Me.pnlAt.TabIndex = 39
        Me.pnlAt.Tag = "'"
        Me.pnlAt.Text = "@" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "'"
        Me.pnlAt.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlAt.UseVisualStyleBackColor = False
        '
        'pnlColon
        '
        Me.pnlColon.BackColor = System.Drawing.Color.White
        Me.pnlColon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlColon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlColon.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlColon.Location = New System.Drawing.Point(598, 111)
        Me.pnlColon.Name = "pnlColon"
        Me.pnlColon.Size = New System.Drawing.Size(48, 48)
        Me.pnlColon.TabIndex = 38
        Me.pnlColon.Tag = ";"
        Me.pnlColon.Text = ":" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & ";"
        Me.pnlColon.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlColon.UseVisualStyleBackColor = False
        '
        'pnlL
        '
        Me.pnlL.BackColor = System.Drawing.Color.White
        Me.pnlL.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlL.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlL.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlL.Location = New System.Drawing.Point(543, 111)
        Me.pnlL.Name = "pnlL"
        Me.pnlL.Size = New System.Drawing.Size(48, 48)
        Me.pnlL.TabIndex = 37
        Me.pnlL.Tag = "l"
        Me.pnlL.Text = "L"
        Me.pnlL.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlL.UseVisualStyleBackColor = False
        '
        'pnlK
        '
        Me.pnlK.BackColor = System.Drawing.Color.White
        Me.pnlK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlK.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlK.Location = New System.Drawing.Point(489, 111)
        Me.pnlK.Name = "pnlK"
        Me.pnlK.Size = New System.Drawing.Size(48, 48)
        Me.pnlK.TabIndex = 36
        Me.pnlK.Tag = "k"
        Me.pnlK.Text = "K"
        Me.pnlK.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlK.UseVisualStyleBackColor = False
        '
        'pnlJ
        '
        Me.pnlJ.BackColor = System.Drawing.Color.White
        Me.pnlJ.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlJ.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJ.Location = New System.Drawing.Point(435, 111)
        Me.pnlJ.Name = "pnlJ"
        Me.pnlJ.Size = New System.Drawing.Size(48, 48)
        Me.pnlJ.TabIndex = 35
        Me.pnlJ.Tag = "j"
        Me.pnlJ.Text = "J"
        Me.pnlJ.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlJ.UseVisualStyleBackColor = False
        '
        'pnlH
        '
        Me.pnlH.BackColor = System.Drawing.Color.White
        Me.pnlH.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlH.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlH.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlH.Location = New System.Drawing.Point(381, 111)
        Me.pnlH.Name = "pnlH"
        Me.pnlH.Size = New System.Drawing.Size(48, 48)
        Me.pnlH.TabIndex = 34
        Me.pnlH.Tag = "h"
        Me.pnlH.Text = "H"
        Me.pnlH.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlH.UseVisualStyleBackColor = False
        '
        'pnlG
        '
        Me.pnlG.BackColor = System.Drawing.Color.White
        Me.pnlG.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlG.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlG.Location = New System.Drawing.Point(327, 111)
        Me.pnlG.Name = "pnlG"
        Me.pnlG.Size = New System.Drawing.Size(48, 48)
        Me.pnlG.TabIndex = 33
        Me.pnlG.Tag = "g"
        Me.pnlG.Text = "G"
        Me.pnlG.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlG.UseVisualStyleBackColor = False
        '
        'pnlF
        '
        Me.pnlF.BackColor = System.Drawing.Color.White
        Me.pnlF.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlF.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlF.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlF.Location = New System.Drawing.Point(273, 111)
        Me.pnlF.Name = "pnlF"
        Me.pnlF.Size = New System.Drawing.Size(48, 48)
        Me.pnlF.TabIndex = 32
        Me.pnlF.Tag = "f"
        Me.pnlF.Text = "F"
        Me.pnlF.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlF.UseVisualStyleBackColor = False
        '
        'pnlD
        '
        Me.pnlD.BackColor = System.Drawing.Color.White
        Me.pnlD.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlD.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlD.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlD.Location = New System.Drawing.Point(219, 111)
        Me.pnlD.Name = "pnlD"
        Me.pnlD.Size = New System.Drawing.Size(48, 48)
        Me.pnlD.TabIndex = 31
        Me.pnlD.Tag = "d"
        Me.pnlD.Text = "D"
        Me.pnlD.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlD.UseVisualStyleBackColor = False
        '
        'pnlS
        '
        Me.pnlS.BackColor = System.Drawing.Color.White
        Me.pnlS.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlS.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlS.Location = New System.Drawing.Point(165, 111)
        Me.pnlS.Name = "pnlS"
        Me.pnlS.Size = New System.Drawing.Size(48, 48)
        Me.pnlS.TabIndex = 30
        Me.pnlS.Tag = "s"
        Me.pnlS.Text = "S"
        Me.pnlS.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlS.UseVisualStyleBackColor = False
        '
        'pnlA
        '
        Me.pnlA.BackColor = System.Drawing.Color.White
        Me.pnlA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlA.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlA.Location = New System.Drawing.Point(111, 111)
        Me.pnlA.Name = "pnlA"
        Me.pnlA.Size = New System.Drawing.Size(48, 48)
        Me.pnlA.TabIndex = 29
        Me.pnlA.Tag = "a"
        Me.pnlA.Text = "A"
        Me.pnlA.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlA.UseVisualStyleBackColor = False
        '
        'pnlEnter
        '
        Me.pnlEnter.BackColor = System.Drawing.Color.White
        Me.pnlEnter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlEnter.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlEnter.Location = New System.Drawing.Point(706, 111)
        Me.pnlEnter.Name = "pnlEnter"
        Me.pnlEnter.Size = New System.Drawing.Size(92, 48)
        Me.pnlEnter.TabIndex = 40
        Me.pnlEnter.Tag = "{ENTER}"
        Me.pnlEnter.Text = "Enter"
        Me.pnlEnter.UseVisualStyleBackColor = False
        '
        'pnlShift1
        '
        Me.pnlShift1.BackColor = System.Drawing.Color.White
        Me.pnlShift1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlShift1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlShift1.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlShift1.Location = New System.Drawing.Point(3, 165)
        Me.pnlShift1.Name = "pnlShift1"
        Me.pnlShift1.Size = New System.Drawing.Size(124, 48)
        Me.pnlShift1.TabIndex = 41
        Me.pnlShift1.Tag = ""
        Me.pnlShift1.Text = "Shift"
        Me.pnlShift1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.pnlShift1.UseVisualStyleBackColor = False
        '
        'pnlShift2
        '
        Me.pnlShift2.BackColor = System.Drawing.Color.White
        Me.pnlShift2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlShift2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlShift2.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlShift2.Location = New System.Drawing.Point(673, 165)
        Me.pnlShift2.Name = "pnlShift2"
        Me.pnlShift2.Size = New System.Drawing.Size(125, 48)
        Me.pnlShift2.TabIndex = 52
        Me.pnlShift2.Tag = ""
        Me.pnlShift2.Text = "Shift"
        Me.pnlShift2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.pnlShift2.UseVisualStyleBackColor = False
        '
        'pnlSlash
        '
        Me.pnlSlash.BackColor = System.Drawing.Color.White
        Me.pnlSlash.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlSlash.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlSlash.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlSlash.Location = New System.Drawing.Point(619, 165)
        Me.pnlSlash.Name = "pnlSlash"
        Me.pnlSlash.Size = New System.Drawing.Size(48, 48)
        Me.pnlSlash.TabIndex = 51
        Me.pnlSlash.Tag = "/"
        Me.pnlSlash.Text = "?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "/"
        Me.pnlSlash.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlSlash.UseVisualStyleBackColor = False
        '
        'pnlDot
        '
        Me.pnlDot.BackColor = System.Drawing.Color.White
        Me.pnlDot.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlDot.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlDot.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlDot.Location = New System.Drawing.Point(565, 165)
        Me.pnlDot.Name = "pnlDot"
        Me.pnlDot.Size = New System.Drawing.Size(48, 48)
        Me.pnlDot.TabIndex = 50
        Me.pnlDot.Tag = "."
        Me.pnlDot.Text = ">" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "."
        Me.pnlDot.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlDot.UseVisualStyleBackColor = False
        '
        'pnlComma
        '
        Me.pnlComma.BackColor = System.Drawing.Color.White
        Me.pnlComma.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlComma.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlComma.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.pnlComma.Location = New System.Drawing.Point(511, 165)
        Me.pnlComma.Name = "pnlComma"
        Me.pnlComma.Size = New System.Drawing.Size(48, 48)
        Me.pnlComma.TabIndex = 49
        Me.pnlComma.Tag = ","
        Me.pnlComma.Text = "<" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & ","
        Me.pnlComma.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlComma.UseVisualStyleBackColor = False
        '
        'pnlM
        '
        Me.pnlM.BackColor = System.Drawing.Color.White
        Me.pnlM.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlM.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlM.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlM.Location = New System.Drawing.Point(457, 165)
        Me.pnlM.Name = "pnlM"
        Me.pnlM.Size = New System.Drawing.Size(48, 48)
        Me.pnlM.TabIndex = 48
        Me.pnlM.Tag = "m"
        Me.pnlM.Text = "M"
        Me.pnlM.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlM.UseVisualStyleBackColor = False
        '
        'pnlN
        '
        Me.pnlN.BackColor = System.Drawing.Color.White
        Me.pnlN.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlN.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlN.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlN.Location = New System.Drawing.Point(403, 165)
        Me.pnlN.Name = "pnlN"
        Me.pnlN.Size = New System.Drawing.Size(48, 48)
        Me.pnlN.TabIndex = 47
        Me.pnlN.Tag = "n"
        Me.pnlN.Text = "N"
        Me.pnlN.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlN.UseVisualStyleBackColor = False
        '
        'pnlB
        '
        Me.pnlB.BackColor = System.Drawing.Color.White
        Me.pnlB.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlB.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlB.Location = New System.Drawing.Point(349, 165)
        Me.pnlB.Name = "pnlB"
        Me.pnlB.Size = New System.Drawing.Size(48, 48)
        Me.pnlB.TabIndex = 46
        Me.pnlB.Tag = "b"
        Me.pnlB.Text = "B"
        Me.pnlB.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlB.UseVisualStyleBackColor = False
        '
        'pnlV
        '
        Me.pnlV.BackColor = System.Drawing.Color.White
        Me.pnlV.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlV.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlV.Location = New System.Drawing.Point(295, 165)
        Me.pnlV.Name = "pnlV"
        Me.pnlV.Size = New System.Drawing.Size(48, 48)
        Me.pnlV.TabIndex = 45
        Me.pnlV.Tag = "v"
        Me.pnlV.Text = "V"
        Me.pnlV.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlV.UseVisualStyleBackColor = False
        '
        'pnlC
        '
        Me.pnlC.BackColor = System.Drawing.Color.White
        Me.pnlC.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlC.Location = New System.Drawing.Point(241, 165)
        Me.pnlC.Name = "pnlC"
        Me.pnlC.Size = New System.Drawing.Size(48, 48)
        Me.pnlC.TabIndex = 44
        Me.pnlC.Tag = "c"
        Me.pnlC.Text = "C"
        Me.pnlC.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlC.UseVisualStyleBackColor = False
        '
        'pnlX
        '
        Me.pnlX.BackColor = System.Drawing.Color.White
        Me.pnlX.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlX.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlX.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlX.Location = New System.Drawing.Point(187, 165)
        Me.pnlX.Name = "pnlX"
        Me.pnlX.Size = New System.Drawing.Size(48, 48)
        Me.pnlX.TabIndex = 43
        Me.pnlX.Tag = "x"
        Me.pnlX.Text = "X"
        Me.pnlX.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlX.UseVisualStyleBackColor = False
        '
        'pnlZ
        '
        Me.pnlZ.BackColor = System.Drawing.Color.White
        Me.pnlZ.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.pnlZ.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlZ.Location = New System.Drawing.Point(133, 165)
        Me.pnlZ.Name = "pnlZ"
        Me.pnlZ.Size = New System.Drawing.Size(48, 48)
        Me.pnlZ.TabIndex = 42
        Me.pnlZ.Tag = "z"
        Me.pnlZ.Text = "Z"
        Me.pnlZ.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.pnlZ.UseVisualStyleBackColor = False
        '
        'KeyboardControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.pnlSlash)
        Me.Controls.Add(Me.pnlShift2)
        Me.Controls.Add(Me.pnlDot)
        Me.Controls.Add(Me.pnlShift1)
        Me.Controls.Add(Me.pnlComma)
        Me.Controls.Add(Me.pnlEnter)
        Me.Controls.Add(Me.pnlM)
        Me.Controls.Add(Me.pnlCaps)
        Me.Controls.Add(Me.pnlN)
        Me.Controls.Add(Me.pnlAt)
        Me.Controls.Add(Me.pnlB)
        Me.Controls.Add(Me.ButtonEx28)
        Me.Controls.Add(Me.pnlV)
        Me.Controls.Add(Me.pnlColon)
        Me.Controls.Add(Me.pnlC)
        Me.Controls.Add(Me.pnlSpace)
        Me.Controls.Add(Me.pnlX)
        Me.Controls.Add(Me.pnlL)
        Me.Controls.Add(Me.pnlZ)
        Me.Controls.Add(Me.pnlBracketRight)
        Me.Controls.Add(Me.pnlK)
        Me.Controls.Add(Me.pnlBracketLeft)
        Me.Controls.Add(Me.pnlJ)
        Me.Controls.Add(Me.pnlP)
        Me.Controls.Add(Me.pnlH)
        Me.Controls.Add(Me.pnlO)
        Me.Controls.Add(Me.pnlG)
        Me.Controls.Add(Me.pnlI)
        Me.Controls.Add(Me.pnlF)
        Me.Controls.Add(Me.pnlU)
        Me.Controls.Add(Me.pnlD)
        Me.Controls.Add(Me.pnlY)
        Me.Controls.Add(Me.pnlS)
        Me.Controls.Add(Me.pnlT)
        Me.Controls.Add(Me.pnlA)
        Me.Controls.Add(Me.pnlR)
        Me.Controls.Add(Me.pnlE)
        Me.Controls.Add(Me.pnlW)
        Me.Controls.Add(Me.pnlQ)
        Me.Controls.Add(Me.ButtonEx27)
        Me.Controls.Add(Me.pnlBackspace)
        Me.Controls.Add(Me.pnlPlus)
        Me.Controls.Add(Me.pnlMinus)
        Me.Controls.Add(Me.pnl0)
        Me.Controls.Add(Me.pnl9)
        Me.Controls.Add(Me.pnl8)
        Me.Controls.Add(Me.pnl7)
        Me.Controls.Add(Me.pnl6)
        Me.Controls.Add(Me.pnl5)
        Me.Controls.Add(Me.pnl4)
        Me.Controls.Add(Me.pnl3)
        Me.Controls.Add(Me.pnl2)
        Me.Controls.Add(Me.pnl1)
        Me.Controls.Add(Me.pnl1Left)
        Me.Name = "KeyboardControl"
        Me.Size = New System.Drawing.Size(802, 271)
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents pnl1Left As Button
    Public WithEvents pnl1 As Button
    Public WithEvents pnl2 As Button
    Public WithEvents pnl3 As Button
    Public WithEvents pnl4 As Button
    Public WithEvents pnl5 As Button
    Public WithEvents pnl6 As Button
    Public WithEvents pnl7 As Button
    Public WithEvents pnl8 As Button
    Public WithEvents pnl9 As Button
    Public WithEvents pnl0 As Button
    Public WithEvents pnlMinus As Button
    Public WithEvents pnlPlus As Button
    Public WithEvents pnlBackspace As Button
    Public WithEvents pnlSpace As Button
    Public WithEvents pnlBracketRight As Button
    Public WithEvents pnlBracketLeft As Button
    Public WithEvents pnlP As Button
    Public WithEvents pnlO As Button
    Public WithEvents pnlI As Button
    Public WithEvents pnlU As Button
    Public WithEvents pnlY As Button
    Public WithEvents pnlT As Button
    Public WithEvents pnlR As Button
    Public WithEvents pnlE As Button
    Public WithEvents pnlW As Button
    Public WithEvents pnlQ As Button
    Public WithEvents ButtonEx27 As Button
    Public WithEvents ButtonEx28 As Button
    Public WithEvents pnlCaps As Button
    Public WithEvents pnlAt As Button
    Public WithEvents pnlColon As Button
    Public WithEvents pnlL As Button
    Public WithEvents pnlK As Button
    Public WithEvents pnlJ As Button
    Public WithEvents pnlH As Button
    Public WithEvents pnlG As Button
    Public WithEvents pnlF As Button
    Public WithEvents pnlD As Button
    Public WithEvents pnlS As Button
    Public WithEvents pnlA As Button
    Public WithEvents pnlEnter As Button
    Public WithEvents pnlShift1 As Button
    Public WithEvents pnlShift2 As Button
    Public WithEvents pnlSlash As Button
    Public WithEvents pnlDot As Button
    Public WithEvents pnlComma As Button
    Public WithEvents pnlM As Button
    Public WithEvents pnlN As Button
    Public WithEvents pnlB As Button
    Public WithEvents pnlV As Button
    Public WithEvents pnlC As Button
    Public WithEvents pnlX As Button
    Public WithEvents pnlZ As Button

End Class
