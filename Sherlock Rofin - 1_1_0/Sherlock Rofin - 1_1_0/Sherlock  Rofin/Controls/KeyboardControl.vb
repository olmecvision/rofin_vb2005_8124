﻿Public Class KeyboardControl

    Private shiftIndicator As Boolean = False
    Private capsIndicator As Boolean = False

#Region "Public Events"
    Dim inButtonClick As Boolean

    Public Delegate Sub KeyboardDelegate(ByVal sender As Object, ByVal e As KeyboardEventArgs)

    Public Class KeyboardEventArgs
        Inherits EventArgs
        Private ReadOnly _keyboardKeyPressed As String

        Public Sub New(ByVal keyboardKeyPressed As String)
            Me._keyboardKeyPressed = keyboardKeyPressed
        End Sub

        Public ReadOnly Property KeyboardKeyPressed() As String
            Get
                Return _keyboardKeyPressed
            End Get
        End Property
    End Class

    Public Event UserKeyPressed As KeyboardDelegate
    Protected Overridable Sub OnUserKeyPressed(ByVal e As KeyboardEventArgs)
        RaiseEvent UserKeyPressed(Me, e)
    End Sub

#End Region

    Private Sub pnlShiftableButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pnlPlus.Click, pnlMinus.Click, pnl9.Click, pnl8.Click, pnl7.Click, pnl6.Click, pnl5.Click, pnl4.Click, pnl3.Click, pnl2.Click, pnl1Left.Click, pnl1.Click, pnl0.Click, pnlBracketRight.Click, pnlBracketLeft.Click, pnlSlash.Click, pnlDot.Click, pnlComma.Click, pnlColon.Click, pnlAt.Click
        Dim keyPressed As String = String.Empty

        Dim pnl As Button = TryCast(sender, Button)
        If pnl IsNot Nothing Then
            keyPressed = HandleShiftableKey(pnl.Tag.ToString())
        End If

        Dim dea As New KeyboardEventArgs(keyPressed)

        OnUserKeyPressed(dea)
    End Sub

    Private Sub pnlButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pnlBackspace.Click, pnlSpace.Click
        Dim keyPressed As String = String.Empty

        Dim pnl As Button = TryCast(sender, Button)
        If pnl IsNot Nothing Then
            keyPressed = pnl.Tag.ToString()
        End If

        Dim dea As New KeyboardEventArgs(keyPressed)

        OnUserKeyPressed(dea)
    End Sub

    Private Sub pnlShiftableCapsLockableButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pnlZ.Click, pnlY.Click, pnlX.Click, pnlW.Click, pnlV.Click, pnlU.Click, pnlT.Click, pnlS.Click, pnlR.Click, pnlQ.Click, pnlP.Click, pnlO.Click, pnlN.Click, pnlM.Click, pnlL.Click, pnlK.Click, pnlJ.Click, pnlI.Click, pnlH.Click, pnlG.Click, pnlF.Click, pnlE.Click, pnlD.Click, pnlC.Click, pnlB.Click, pnlA.Click
        Dim keyPressed As String = String.Empty

        Dim pnl As Button = TryCast(sender, Button)
        If pnl IsNot Nothing Then
            keyPressed = HandleShiftableCapsLockKey(pnl.Tag.ToString())
        End If

        Dim dea As New KeyboardEventArgs(keyPressed)

        OnUserKeyPressed(dea)
    End Sub

    Private Sub pnlShiftButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pnlShift2.Click, pnlShift1.Click
        HandleShiftClick()
    End Sub

    Private Sub pnlCapsLockButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles pnlCaps.Click
        HandleCapsLockClick()
    End Sub

    Private Function HandleShiftableKey(ByVal theKey As String) As String
        If shiftIndicator Then
            Return "+" & theKey
        Else
            Return theKey
        End If
    End Function

    Private Function HandleShiftableCapsLockKey(ByVal theKey As String) As String
        If capsIndicator Then
            Return "+" & theKey
        ElseIf shiftIndicator Then
            Return "+" & theKey
        Else
            Return theKey
        End If
    End Function

    Private Sub HandleShiftClick()
        If shiftIndicator Then
            shiftIndicator = False
            pnlShift1.BackColor = Color.White
            pnlShift2.BackColor = Color.White
        Else
            shiftIndicator = True
            pnlShift1.BackColor = Color.CornflowerBlue
            pnlShift2.BackColor = Color.CornflowerBlue
        End If
    End Sub

    Private Sub HandleCapsLockClick()
        If capsIndicator Then
            capsIndicator = False
            pnlCaps.BackColor = Color.White
        Else
            capsIndicator = True
            pnlCaps.BackColor = Color.CornflowerBlue
        End If
    End Sub

    Public Sub SetMode(ByVal numbersOnly As Boolean)

        pnl1Left.Visible = Not numbersOnly
        pnlMinus.Visible = Not numbersOnly
        pnlPlus.Visible = Not numbersOnly
        pnlQ.Visible = Not numbersOnly
        pnlW.Visible = Not numbersOnly
        pnlE.Visible = Not numbersOnly
        pnlR.Visible = Not numbersOnly
        pnlT.Visible = Not numbersOnly
        pnlY.Visible = Not numbersOnly
        pnlU.Visible = Not numbersOnly
        pnlI.Visible = Not numbersOnly
        pnlO.Visible = Not numbersOnly
        pnlP.Visible = Not numbersOnly
        pnlBracketLeft.Visible = Not numbersOnly
        pnlBracketRight.Visible = Not numbersOnly
        pnlCaps.Visible = Not numbersOnly
        pnlA.Visible = Not numbersOnly
        pnlS.Visible = Not numbersOnly
        pnlD.Visible = Not numbersOnly
        pnlF.Visible = Not numbersOnly
        pnlG.Visible = Not numbersOnly
        pnlH.Visible = Not numbersOnly
        pnlJ.Visible = Not numbersOnly
        pnlK.Visible = Not numbersOnly
        pnlL.Visible = Not numbersOnly
        pnlColon.Visible = Not numbersOnly
        pnlAt.Visible = Not numbersOnly
        pnlEnter.Visible = Not numbersOnly
        pnlShift1.Visible = Not numbersOnly
        pnlShift2.Visible = Not numbersOnly
        pnlZ.Visible = Not numbersOnly
        pnlX.Visible = Not numbersOnly
        pnlC.Visible = Not numbersOnly
        pnlV.Visible = Not numbersOnly
        pnlB.Visible = Not numbersOnly
        pnlN.Visible = Not numbersOnly
        pnlM.Visible = Not numbersOnly
        pnlComma.Visible = Not numbersOnly
        pnlDot.Visible = Not numbersOnly
        pnlSlash.Visible = Not numbersOnly
        ButtonEx27.Visible = Not numbersOnly
        ButtonEx28.Visible = Not numbersOnly
        pnlSpace.Visible = Not numbersOnly

    End Sub

End Class
