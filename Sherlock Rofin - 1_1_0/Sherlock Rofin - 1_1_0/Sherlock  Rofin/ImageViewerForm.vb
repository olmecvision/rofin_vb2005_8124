﻿Imports System.IO

Public Class ImageViewerForm

    Private m_ImageFiles As FileSystemInfo()
    Private m_Images As Image()
    Private m_Thumbs As Image()
    Private m_CurrentPosition As Integer

    Private Sub ImageViewerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim logoFile = My.Settings("LogoPath") + "\\screen-logo.png"
        PictureBox1.Load(logoFile)

        ' assume we didn't find images
        SetupScreen(False)

        ' load the list of files
        Dim directory As DirectoryInfo = New DirectoryInfo(My.Settings("ImageDirectory"))
        If (directory Is Nothing) Then
            Return
        End If

        If (Not directory.Exists()) Then

            ' try creating the image folder
            Try
                directory.Create()
            Catch ex As Exception
                Return
            End Try

        End If

        m_ImageFiles = directory.GetFileSystemInfos(My.Settings("ImageWildcard"))
        If (m_ImageFiles Is Nothing Or m_ImageFiles.Length = 0) Then
            Return
        End If

        ' sort the array
        Array.Sort(Of FileSystemInfo)(m_ImageFiles, Function(a As FileSystemInfo, b As FileSystemInfo) b.LastWriteTime.CompareTo(a.LastWriteTime))

        m_CurrentPosition = 0

        ' dimension the image arrays
        Array.Resize(m_Images, m_ImageFiles.Length)
        Array.Resize(m_Thumbs, m_ImageFiles.Length)

        LoadImages()

        SetupScreen(True)

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub LoadImages()

        Cursor = Cursors.WaitCursor

        ' dispose all visible images
        ' we are now caching these images and disposing them
        ' as we exit the screen
        'If (imgMain.Image IsNot Nothing) Then
        '    imgMain.Image.Dispose()
        '    imgMain.Image = Nothing
        'End If

        'If (btnImage1.Image IsNot Nothing) Then
        '    btnImage1.Image.Dispose()
        '    btnImage1.Image = Nothing
        'End If

        'If (btnImage2.Image IsNot Nothing) Then
        '    btnImage2.Image.Dispose()
        '    btnImage2.Image = Nothing
        'End If

        'If (btnImage3.Image IsNot Nothing) Then
        '    btnImage3.Image.Dispose()
        '    btnImage3.Image = Nothing
        'End If

        ' centre image
        Dim file As FileInfo = m_ImageFiles.GetValue(m_CurrentPosition)
        If (m_Images(m_CurrentPosition) Is Nothing) Then
            m_Images(m_CurrentPosition) = LoadImageFromFile(file.FullName)
        End If
        imgMain.Image = m_Images(m_CurrentPosition)

        ' image details
        Dim size As Decimal = (file.Length / 1024 / 1024)
        lblImageName.Text = file.Name
        lblImageSize.Text = String.Format("{0} MB", Decimal.Round(size, 2))
        lblCreatedDate.Text = file.LastWriteTime.ToString("dd/MM/yyyy HH:mm")


        ' centre thumnail
        If (m_Thumbs(m_CurrentPosition) Is Nothing) Then
            m_Thumbs(m_CurrentPosition) = LoadThumbnailFromFile(m_Images(m_CurrentPosition))
        End If
        btnImage2.BackgroundImage = m_Thumbs(m_CurrentPosition)

        ' previous thumb
        If (m_CurrentPosition > 0) Then
            Dim previousPosition As Integer = m_CurrentPosition - 1
            file = m_ImageFiles.GetValue(previousPosition)
            If (m_Images(previousPosition) Is Nothing) Then
                m_Images(previousPosition) = LoadImageFromFile(file.FullName)
            End If
            If (m_Thumbs(previousPosition) Is Nothing) Then
                m_Thumbs(previousPosition) = LoadThumbnailFromFile(m_Images(previousPosition))
            End If
            btnImage1.BackgroundImage = m_Thumbs(previousPosition)
        Else
            btnImage1.BackgroundImage = Nothing
        End If

        ' next thumb
        If ((m_CurrentPosition < m_ImageFiles.Length - 1) And (m_CurrentPosition < 199)) Then
            Dim nextPosition As Integer = m_CurrentPosition + 1
            file = m_ImageFiles.GetValue(nextPosition)
            If (m_Images(nextPosition) Is Nothing) Then
                m_Images(nextPosition) = LoadImageFromFile(file.FullName)
            End If
            If (m_Thumbs(nextPosition) Is Nothing) Then
                m_Thumbs(nextPosition) = LoadThumbnailFromFile(m_Images(nextPosition))
            End If
            btnImage3.BackgroundImage = m_Thumbs(nextPosition)
        Else
            btnImage3.BackgroundImage = Nothing
        End If

        Cursor = Cursors.Default

    End Sub

    Private Function LoadThumbnailFromFile(ByVal fileName As String) As Image
        Dim imageThumb As Image = Nothing

        Try
            Dim image As Image = Nothing

            If (Not String.IsNullOrEmpty(fileName)) Then
                image = image.FromFile(fileName)
            End If
            If (Not image Is Nothing) Then
                imageThumb = image.GetThumbnailImage(160, 97, Nothing, New IntPtr())
            End If

            If (image IsNot Nothing) Then
                image.Dispose()
                image = Nothing
            End If

        Catch ex As Exception

        End Try

        Return imageThumb
    End Function

    Private Function LoadThumbnailFromFile(ByVal image As Image) As Image
        Dim imageThumb As Image = Nothing

        Try
            If (Not image Is Nothing) Then
                imageThumb = image.GetThumbnailImage(160, 97, Nothing, New IntPtr())
            End If
        Catch ex As Exception

        End Try

        Return imageThumb
    End Function

    Private Function LoadImageFromFile(ByVal fileName As String) As Image

        Dim image As Image = Nothing

        Try
            If (Not String.IsNullOrEmpty(fileName)) Then
                image = image.FromFile(fileName)
            End If
        Catch ex As Exception

        End Try

        Return image
    End Function

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click, btnImage1.Click
        If (m_CurrentPosition <= 0) Then
            Return
        End If

        m_CurrentPosition = m_CurrentPosition - 1
        LoadImages()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click, btnImage3.Click
        If (m_ImageFiles Is Nothing) Then
            Return
        End If

        If (m_CurrentPosition >= m_ImageFiles.Length - 1) Then
            Return
        End If

        m_CurrentPosition = m_CurrentPosition + 1
        LoadImages()
    End Sub

    Private Sub ImageViewerForm_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If (m_Thumbs IsNot Nothing) Then
            For Each img As Image In m_Thumbs
                If (img IsNot Nothing) Then
                    img.Dispose()
                End If
            Next
        End If

        If (m_Images IsNot Nothing) Then
            For Each img As Image In m_Images
                If (img IsNot Nothing) Then
                    img.Dispose()
                End If
            Next
        End If

    End Sub

    Private Sub SetupScreen(ByVal hasImages As Boolean)
        If (hasImages) Then
            lblNoImagesFound.Visible = False
            btnImage1.Visible = True
            btnImage2.Visible = True
            btnImage3.Visible = True
        Else
            lblNoImagesFound.Visible = True
            btnImage1.Visible = False
            btnImage2.Visible = False
            btnImage3.Visible = False
        End If
    End Sub


    Private Sub btnDeleteCurrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteCurrent.Click
        If (m_ImageFiles Is Nothing) Then
            Return
        End If

        If (m_ImageFiles.Length = 0) Then
            Return
        End If

        Dim currentIndex As Integer = m_CurrentPosition

        Dim file As FileInfo = m_ImageFiles.GetValue(currentIndex)
        If (file IsNot Nothing) Then

            Cursor = Cursors.WaitCursor

            ' dispose the main image
            If (imgMain.Image IsNot Nothing) Then
                imgMain.Image.Dispose()
                imgMain.Image = Nothing
            End If

            ' remove the file
            file.Delete()

            CleanUp()
            Array.Resize(m_Images, 0)
            Array.Resize(m_Thumbs, 0)
            Array.Resize(m_ImageFiles, 0)

            ImageViewerForm_Load(Me, EventArgs.Empty)

            Cursor = Cursors.Default

            If (m_ImageFiles Is Nothing) Then
                Return
            End If

            If (m_ImageFiles.Length = 0) Then
                Return
            End If

            m_CurrentPosition = currentIndex

            If (m_CurrentPosition > m_ImageFiles.Length - 1) Then
                m_CurrentPosition = (m_ImageFiles.Length - 1)
            End If

            LoadImages()

        End If
    End Sub

    Private Sub btnDeleteAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteAll.Click
        If (m_ImageFiles Is Nothing) Then
            Return
        End If

        If (m_ImageFiles.Length = 0) Then
            Return
        End If

        Dim result As DialogResult = MessageBox.Show(String.Format("Delete all images, are you sure?{0}", vbCrLf + vbCrLf), "Vision System", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        If Not (result = Windows.Forms.DialogResult.Yes) Then
            Return
        End If

        Cursor = Cursors.WaitCursor

        ' dispose the main image
        If (imgMain.Image IsNot Nothing) Then
            imgMain.Image.Dispose()
            imgMain.Image = Nothing
        End If

        CleanUp()

        For Each file As FileInfo In m_ImageFiles
            If (file IsNot Nothing) Then

                ' remove the file
                file.Delete()

            End If
        Next

        Array.Resize(m_Images, 0)
        Array.Resize(m_Thumbs, 0)
        Array.Resize(m_ImageFiles, 0)

        ImageViewerForm_Load(Me, EventArgs.Empty)

        Cursor = Cursors.Default

        If (m_ImageFiles Is Nothing) Then
            Return
        End If

        If (m_ImageFiles.Length = 0) Then
            Return
        End If

        LoadImages()

    End Sub
    Private Sub CleanUp()
        If (m_Thumbs IsNot Nothing) Then
            For Each img As Image In m_Thumbs
                If (img IsNot Nothing) Then
                    img.Dispose()
                End If
            Next
        End If

        If (m_Images IsNot Nothing) Then
            For Each img As Image In m_Images
                If (img IsNot Nothing) Then
                    img.Dispose()
                End If
            Next
        End If

    End Sub

    Private Function RemoveElementFromArray(ByVal objArray As System.Array, ByVal objElement As Object, ByVal objType As System.Type)
        Dim objArrayList As New ArrayList(objArray)
        objArrayList.Remove(objElement)
        Return objArrayList.ToArray(objType)
    End Function


End Class



