﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StarflexForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvMessages = New System.Windows.Forms.ListView()
        Me.ColTime = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMessage = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gpSendData = New System.Windows.Forms.GroupBox()
        Me.chkSimulationMode = New System.Windows.Forms.CheckBox()
        Me.lblLaserUnitId = New System.Windows.Forms.Label()
        Me.numLaserUnitId = New System.Windows.Forms.NumericUpDown()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.numHead = New System.Windows.Forms.NumericUpDown()
        Me.lblState = New System.Windows.Forms.Label()
        Me.cbState = New System.Windows.Forms.ComboBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.cbMessage = New System.Windows.Forms.ComboBox()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.gpSendData.SuspendLayout()
        CType(Me.numLaserUnitId, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lvMessages
        '
        Me.lvMessages.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvMessages.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColTime, Me.colMessage})
        Me.lvMessages.FullRowSelect = True
        Me.lvMessages.Location = New System.Drawing.Point(1, 2)
        Me.lvMessages.MultiSelect = False
        Me.lvMessages.Name = "lvMessages"
        Me.lvMessages.Size = New System.Drawing.Size(568, 245)
        Me.lvMessages.TabIndex = 0
        Me.lvMessages.UseCompatibleStateImageBehavior = False
        Me.lvMessages.View = System.Windows.Forms.View.Details
        '
        'ColTime
        '
        Me.ColTime.Text = "Time"
        Me.ColTime.Width = 150
        '
        'colMessage
        '
        Me.colMessage.Text = "Message"
        Me.colMessage.Width = 400
        '
        'gpSendData
        '
        Me.gpSendData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpSendData.Controls.Add(Me.btnSend)
        Me.gpSendData.Controls.Add(Me.cbMessage)
        Me.gpSendData.Controls.Add(Me.lblMessage)
        Me.gpSendData.Controls.Add(Me.cbState)
        Me.gpSendData.Controls.Add(Me.lblState)
        Me.gpSendData.Controls.Add(Me.numHead)
        Me.gpSendData.Controls.Add(Me.lblHead)
        Me.gpSendData.Controls.Add(Me.numLaserUnitId)
        Me.gpSendData.Controls.Add(Me.lblLaserUnitId)
        Me.gpSendData.Controls.Add(Me.chkSimulationMode)
        Me.gpSendData.Location = New System.Drawing.Point(1, 253)
        Me.gpSendData.Name = "gpSendData"
        Me.gpSendData.Size = New System.Drawing.Size(568, 138)
        Me.gpSendData.TabIndex = 1
        Me.gpSendData.TabStop = False
        Me.gpSendData.Text = "StarFlex Data Sending"
        '
        'chkSimulationMode
        '
        Me.chkSimulationMode.AutoSize = True
        Me.chkSimulationMode.Location = New System.Drawing.Point(12, 20)
        Me.chkSimulationMode.Name = "chkSimulationMode"
        Me.chkSimulationMode.Size = New System.Drawing.Size(104, 17)
        Me.chkSimulationMode.TabIndex = 1
        Me.chkSimulationMode.Text = "Simulation Mode"
        Me.chkSimulationMode.UseVisualStyleBackColor = True
        '
        'lblLaserUnitId
        '
        Me.lblLaserUnitId.AutoSize = True
        Me.lblLaserUnitId.Location = New System.Drawing.Point(123, 20)
        Me.lblLaserUnitId.Name = "lblLaserUnitId"
        Me.lblLaserUnitId.Size = New System.Drawing.Size(69, 13)
        Me.lblLaserUnitId.TabIndex = 2
        Me.lblLaserUnitId.Text = "Laser Unit ID"
        '
        'numLaserUnitId
        '
        Me.numLaserUnitId.Location = New System.Drawing.Point(198, 17)
        Me.numLaserUnitId.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numLaserUnitId.Name = "numLaserUnitId"
        Me.numLaserUnitId.Size = New System.Drawing.Size(46, 20)
        Me.numLaserUnitId.TabIndex = 3
        Me.numLaserUnitId.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblHead
        '
        Me.lblHead.AutoSize = True
        Me.lblHead.Location = New System.Drawing.Point(250, 20)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(33, 13)
        Me.lblHead.TabIndex = 4
        Me.lblHead.Text = "Head"
        '
        'numHead
        '
        Me.numHead.Location = New System.Drawing.Point(289, 17)
        Me.numHead.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numHead.Name = "numHead"
        Me.numHead.Size = New System.Drawing.Size(46, 20)
        Me.numHead.TabIndex = 5
        Me.numHead.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblState
        '
        Me.lblState.AutoSize = True
        Me.lblState.Location = New System.Drawing.Point(123, 46)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(32, 13)
        Me.lblState.TabIndex = 6
        Me.lblState.Text = "State"
        '
        'cbState
        '
        Me.cbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbState.FormattingEnabled = True
        Me.cbState.Items.AddRange(New Object() {"OK", "Error"})
        Me.cbState.Location = New System.Drawing.Point(198, 46)
        Me.cbState.Name = "cbState"
        Me.cbState.Size = New System.Drawing.Size(137, 21)
        Me.cbState.TabIndex = 7
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(123, 81)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(50, 13)
        Me.lblMessage.TabIndex = 8
        Me.lblMessage.Text = "Message"
        '
        'cbMessage
        '
        Me.cbMessage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMessage.FormattingEnabled = True
        Me.cbMessage.Items.AddRange(New Object() {"Camera Status", "Holes", "Holes Diameter", "Ready Status"})
        Me.cbMessage.Location = New System.Drawing.Point(198, 78)
        Me.cbMessage.Name = "cbMessage"
        Me.cbMessage.Size = New System.Drawing.Size(137, 21)
        Me.cbMessage.TabIndex = 9
        '
        'btnSend
        '
        Me.btnSend.Enabled = False
        Me.btnSend.Location = New System.Drawing.Point(260, 105)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 10
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'StarflexForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 392)
        Me.ControlBox = False
        Me.Controls.Add(Me.gpSendData)
        Me.Controls.Add(Me.lvMessages)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StarflexForm"
        Me.Text = "StarFlex Debug Form"
        Me.gpSendData.ResumeLayout(False)
        Me.gpSendData.PerformLayout()
        CType(Me.numLaserUnitId, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvMessages As System.Windows.Forms.ListView
    Friend WithEvents ColTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents colMessage As System.Windows.Forms.ColumnHeader
    Friend WithEvents gpSendData As System.Windows.Forms.GroupBox
    Friend WithEvents numHead As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents numLaserUnitId As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLaserUnitId As System.Windows.Forms.Label
    Friend WithEvents chkSimulationMode As System.Windows.Forms.CheckBox
    Friend WithEvents cbMessage As System.Windows.Forms.ComboBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents cbState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents btnSend As System.Windows.Forms.Button
End Class
