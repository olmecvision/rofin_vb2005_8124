﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormatPickerForm
    Inherits VB2005_Sherlock7.FormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtFormat = New System.Windows.Forms.TextBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnChangeFormat = New System.Windows.Forms.Button
        Me.btnLogout = New System.Windows.Forms.Button
        Me.txtFormatDescription = New System.Windows.Forms.TextBox
        Me.lbFormat = New System.Windows.Forms.ListBox
        Me.pnlKeyboard.SuspendLayout()
        Me.SuspendLayout()
        '
        'KeyboardControl1
        '
        Me.KeyboardControl1.Location = New System.Drawing.Point(185, 6)
        Me.KeyboardControl1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.KeyboardControl1.Size = New System.Drawing.Size(1336, 401)
        '
        'pnlKeyboard
        '
        Me.pnlKeyboard.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.pnlKeyboard.Location = New System.Drawing.Point(0, 320)
        Me.pnlKeyboard.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.pnlKeyboard.Size = New System.Drawing.Size(1024, 410)
        '
        'txtFormat
        '
        Me.txtFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFormat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFormat.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormat.Location = New System.Drawing.Point(177, 103)
        Me.txtFormat.Margin = New System.Windows.Forms.Padding(0)
        Me.txtFormat.Name = "txtFormat"
        Me.txtFormat.Size = New System.Drawing.Size(292, 42)
        Me.txtFormat.TabIndex = 21
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Image = Global.VB2005_Sherlock7.My.Resources.Resources._Exit
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnExit.Location = New System.Drawing.Point(812, 233)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(212, 132)
        Me.btnExit.TabIndex = 24
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnChangeFormat
        '
        Me.btnChangeFormat.Enabled = False
        Me.btnChangeFormat.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnChangeFormat.FlatAppearance.BorderSize = 0
        Me.btnChangeFormat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeFormat.Image = Global.VB2005_Sherlock7.My.Resources.Resources.change_Format_Main_Screen_Btn
        Me.btnChangeFormat.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnChangeFormat.Location = New System.Drawing.Point(812, 1)
        Me.btnChangeFormat.Margin = New System.Windows.Forms.Padding(0)
        Me.btnChangeFormat.Name = "btnChangeFormat"
        Me.btnChangeFormat.Size = New System.Drawing.Size(208, 232)
        Me.btnChangeFormat.TabIndex = 23
        Me.btnChangeFormat.UseVisualStyleBackColor = True
        '
        'btnLogout
        '
        Me.btnLogout.AutoSize = True
        Me.btnLogout.BackColor = System.Drawing.Color.Transparent
        Me.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnLogout.FlatAppearance.BorderSize = 0
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnLogout.Image = Global.VB2005_Sherlock7.My.Resources.Resources.change_Format_Main_Screen
        Me.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogout.Location = New System.Drawing.Point(2, 2)
        Me.btnLogout.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(811, 365)
        Me.btnLogout.TabIndex = 22
        Me.btnLogout.UseVisualStyleBackColor = False
        '
        'txtFormatDescription
        '
        Me.txtFormatDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFormatDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFormatDescription.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormatDescription.Location = New System.Drawing.Point(177, 170)
        Me.txtFormatDescription.Margin = New System.Windows.Forms.Padding(0)
        Me.txtFormatDescription.Name = "txtFormatDescription"
        Me.txtFormatDescription.ReadOnly = True
        Me.txtFormatDescription.Size = New System.Drawing.Size(536, 42)
        Me.txtFormatDescription.TabIndex = 25
        '
        'lbFormat
        '
        Me.lbFormat.FormattingEnabled = True
        Me.lbFormat.ItemHeight = 30
        Me.lbFormat.Location = New System.Drawing.Point(177, 137)
        Me.lbFormat.Name = "lbFormat"
        Me.lbFormat.Size = New System.Drawing.Size(292, 304)
        Me.lbFormat.TabIndex = 26
        Me.lbFormat.Visible = False
        '
        'FormatPickerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(15.0!, 30.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1024, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.lbFormat)
        Me.Controls.Add(Me.txtFormatDescription)
        Me.Controls.Add(Me.txtFormat)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnChangeFormat)
        Me.Controls.Add(Me.btnLogout)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "FormatPickerForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Format"
        Me.Controls.SetChildIndex(Me.btnLogout, 0)
        Me.Controls.SetChildIndex(Me.btnChangeFormat, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.txtFormat, 0)
        Me.Controls.SetChildIndex(Me.txtFormatDescription, 0)
        Me.Controls.SetChildIndex(Me.pnlKeyboard, 0)
        Me.Controls.SetChildIndex(Me.lbFormat, 0)
        Me.pnlKeyboard.ResumeLayout(False)
        Me.pnlKeyboard.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtFormat As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnChangeFormat As System.Windows.Forms.Button
    Friend WithEvents btnLogout As System.Windows.Forms.Button
    Friend WithEvents txtFormatDescription As System.Windows.Forms.TextBox
    Friend WithEvents lbFormat As System.Windows.Forms.ListBox
End Class
