﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    abstract class StarFlexMessageBase
    {
        internal static char[] splitString = new char[] { ' ' };

        public uint SequenceNumber { get; set; }

        public abstract string Encode();
        public abstract bool Decode(string data);

        internal static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
