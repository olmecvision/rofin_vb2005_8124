﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    class VariableChangedEventArgs : EventArgs
    {
        public uint LaserUnitId { get; private set; }
        public uint Head { get; private set; }
        public StarFlexEventType EventType { get; set; }
        public bool State { get; private set; }

        public VariableChangedEventArgs(uint laserUnitId, uint head, StarFlexEventType eventType, bool status)
        {
            LaserUnitId = laserUnitId;
            Head = head;
            EventType = eventType;
            State = status;
        }
    }
}
