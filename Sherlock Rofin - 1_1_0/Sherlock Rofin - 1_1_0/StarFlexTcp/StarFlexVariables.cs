﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public class StarFlexVariables
    {
        // ready
        private bool laserUnit1Head1Ready;
        private bool laserUnit1Head2Ready;
        private bool laserUnit1Head3Ready;
        private bool laserUnit1Head4Ready;
        private bool laserUnit2Head1Ready;
        private bool laserUnit2Head2Ready;
        private bool laserUnit2Head3Ready;
        private bool laserUnit2Head4Ready;
        private bool laserUnit3Head1Ready;
        private bool laserUnit3Head2Ready;
        private bool laserUnit3Head3Ready;
        private bool laserUnit3Head4Ready;
        private bool laserUnit4Head1Ready;
        private bool laserUnit4Head2Ready;
        private bool laserUnit4Head3Ready;
        private bool laserUnit4Head4Ready;

        // status
        private bool laserUnit1Head1Status;
        private bool laserUnit1Head2Status;
        private bool laserUnit1Head3Status;
        private bool laserUnit1Head4Status;
        private bool laserUnit2Head1Status;
        private bool laserUnit2Head2Status;
        private bool laserUnit2Head3Status;
        private bool laserUnit2Head4Status;
        private bool laserUnit3Head1Status;
        private bool laserUnit3Head2Status;
        private bool laserUnit3Head3Status;
        private bool laserUnit3Head4Status;
        private bool laserUnit4Head1Status;
        private bool laserUnit4Head2Status;
        private bool laserUnit4Head3Status;
        private bool laserUnit4Head4Status;

        // hole status
        private bool laserUnit1Head1HoleStatus;
        private bool laserUnit1Head2HoleStatus;
        private bool laserUnit1Head3HoleStatus;
        private bool laserUnit1Head4HoleStatus;
        private bool laserUnit2Head1HoleStatus;
        private bool laserUnit2Head2HoleStatus;
        private bool laserUnit2Head3HoleStatus;
        private bool laserUnit2Head4HoleStatus;
        private bool laserUnit3Head1HoleStatus;
        private bool laserUnit3Head2HoleStatus;
        private bool laserUnit3Head3HoleStatus;
        private bool laserUnit3Head4HoleStatus;
        private bool laserUnit4Head1HoleStatus;
        private bool laserUnit4Head2HoleStatus;
        private bool laserUnit4Head3HoleStatus;
        private bool laserUnit4Head4HoleStatus;

        // hole diameter
        private bool laserUnit1Head1HoleDiameter;
        private bool laserUnit1Head2HoleDiameter;
        private bool laserUnit1Head3HoleDiameter;
        private bool laserUnit1Head4HoleDiameter;
        private bool laserUnit2Head1HoleDiameter;
        private bool laserUnit2Head2HoleDiameter;
        private bool laserUnit2Head3HoleDiameter;
        private bool laserUnit2Head4HoleDiameter;
        private bool laserUnit3Head1HoleDiameter;
        private bool laserUnit3Head2HoleDiameter;
        private bool laserUnit3Head3HoleDiameter;
        private bool laserUnit3Head4HoleDiameter;
        private bool laserUnit4Head1HoleDiameter;
        private bool laserUnit4Head2HoleDiameter;
        private bool laserUnit4Head3HoleDiameter;
        private bool laserUnit4Head4HoleDiameter;

        // error
        private bool laserUnit1Head1Error;
        private bool laserUnit1Head2Error;
        private bool laserUnit1Head3Error;
        private bool laserUnit1Head4Error;
        private bool laserUnit2Head1Error;
        private bool laserUnit2Head2Error;
        private bool laserUnit2Head3Error;
        private bool laserUnit2Head4Error;
        private bool laserUnit3Head1Error;
        private bool laserUnit3Head2Error;
        private bool laserUnit3Head3Error;
        private bool laserUnit3Head4Error;
        private bool laserUnit4Head1Error;
        private bool laserUnit4Head2Error;
        private bool laserUnit4Head3Error;
        private bool laserUnit4Head4Error;

        internal event EventHandler<VariableChangedEventArgs> Changed;

        // ready
        public bool LaserUnit1Head1Ready
        {
            get { return laserUnit1Head1Ready; }
            set
            {
                if (value != laserUnit1Head1Ready)
                {
                    laserUnit1Head1Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 1, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit1Head2Ready
        {
            get { return laserUnit1Head2Ready; }
            set
            {
                if (value != laserUnit1Head2Ready)
                {
                    laserUnit1Head2Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 2, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit1Head3Ready
        {
            get { return laserUnit1Head3Ready; }
            set
            {
                if (value != laserUnit1Head3Ready)
                {
                    laserUnit1Head3Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 3, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit1Head4Ready
        {
            get { return laserUnit1Head4Ready; }
            set
            {
                if (value != laserUnit1Head4Ready)
                {
                    laserUnit1Head4Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 4, StarFlexEventType.ReadyStatus, value));
                }
            }
        }

        public bool LaserUnit2Head1Ready
        {
            get { return laserUnit2Head1Ready; }
            set
            {
                if (value != laserUnit2Head1Ready)
                {
                    laserUnit2Head1Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 1, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit2Head2Ready
        {
            get { return laserUnit2Head2Ready; }
            set
            {
                if (value != laserUnit2Head2Ready)
                {
                    laserUnit2Head2Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 2, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit2Head3Ready
        {
            get { return laserUnit2Head3Ready; }
            set
            {
                if (value != laserUnit2Head3Ready)
                {
                    laserUnit2Head3Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 3, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit2Head4Ready
        {
            get { return laserUnit2Head4Ready; }
            set
            {
                if (value != laserUnit2Head4Ready)
                {
                    laserUnit2Head4Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 4, StarFlexEventType.ReadyStatus, value));
                }
            }
        }

        public bool LaserUnit3Head1Ready
        {
            get { return LaserUnit3Head1Ready; }
            set
            {
                if (value != LaserUnit3Head1Ready)
                {
                    LaserUnit3Head1Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 1, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit3Head2Ready
        {
            get { return LaserUnit3Head2Ready; }
            set
            {
                if (value != LaserUnit3Head2Ready)
                {
                    LaserUnit3Head2Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 2, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit3Head3Ready
        {
            get { return LaserUnit3Head3Ready; }
            set
            {
                if (value != LaserUnit3Head3Ready)
                {
                    LaserUnit3Head3Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 3, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit3Head4Ready
        {
            get { return LaserUnit3Head4Ready; }
            set
            {
                if (value != LaserUnit3Head4Ready)
                {
                    LaserUnit3Head4Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 4, StarFlexEventType.ReadyStatus, value));
                }
            }
        }

        public bool LaserUnit4Head1Ready
        {
            get { return LaserUnit4Head1Ready; }
            set
            {
                if (value != LaserUnit4Head1Ready)
                {
                    LaserUnit4Head1Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 1, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit4Head2Ready
        {
            get { return LaserUnit4Head2Ready; }
            set
            {
                if (value != LaserUnit4Head2Ready)
                {
                    LaserUnit4Head2Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 2, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit4Head3Ready
        {
            get { return LaserUnit4Head3Ready; }
            set
            {
                if (value != LaserUnit4Head3Ready)
                {
                    LaserUnit4Head3Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 3, StarFlexEventType.ReadyStatus, value));
                }
            }
        }
        public bool LaserUnit4Head4Ready
        {
            get { return LaserUnit4Head4Ready; }
            set
            {
                if (value != LaserUnit4Head4Ready)
                {
                    LaserUnit4Head4Ready = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 4, StarFlexEventType.ReadyStatus, value));
                }
            }
        }

        // status
        public bool LaserUnit1Head1Status
        {
            get { return laserUnit1Head1Status; }
            set
            {
                if (value != laserUnit1Head1Status)
                {
                    laserUnit1Head1Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 1, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit1Head2Status
        {
            get { return laserUnit1Head2Status; }
            set
            {
                if (value != laserUnit1Head2Status)
                {
                    laserUnit1Head2Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 2, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit1Head3Status
        {
            get { return laserUnit1Head3Status; }
            set
            {
                if (value != laserUnit1Head3Status)
                {
                    laserUnit1Head3Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 3, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit1Head4Status
        {
            get { return laserUnit1Head4Status; }
            set
            {
                if (value != laserUnit1Head4Status)
                {
                    laserUnit1Head4Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 4, StarFlexEventType.CameraStatus, value));
                }
            }
        }

        public bool LaserUnit2Head1Status
        {
            get { return laserUnit2Head1Status; }
            set
            {
                if (value != laserUnit2Head1Status)
                {
                    laserUnit2Head1Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 1, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit2Head2Status
        {
            get { return laserUnit2Head2Status; }
            set
            {
                if (value != laserUnit2Head2Status)
                {
                    laserUnit2Head2Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 2, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit2Head3Status
        {
            get { return laserUnit2Head3Status; }
            set
            {
                if (value != laserUnit2Head3Status)
                {
                    laserUnit2Head3Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 3, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit2Head4Status
        {
            get { return laserUnit2Head4Status; }
            set
            {
                if (value != laserUnit2Head4Status)
                {
                    laserUnit2Head4Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 4, StarFlexEventType.CameraStatus, value));
                }
            }
        }

        public bool LaserUnit3Head1Status
        {
            get { return laserUnit3Head1Status; }
            set
            {
                if (value != laserUnit3Head1Status)
                {
                    laserUnit3Head1Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 1, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit3Head2Status
        {
            get { return laserUnit3Head2Status; }
            set
            {
                if (value != laserUnit3Head2Status)
                {
                    laserUnit3Head2Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 2, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit3Head3Status
        {
            get { return laserUnit3Head3Status; }
            set
            {
                if (value != laserUnit3Head3Status)
                {
                    laserUnit3Head3Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 3, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit3Head4Status
        {
            get { return laserUnit3Head4Status; }
            set
            {
                if (value != laserUnit3Head4Status)
                {
                    laserUnit3Head4Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 4, StarFlexEventType.CameraStatus, value));
                }
            }
        }

        public bool LaserUnit4Head1Status
        {
            get { return laserUnit4Head1Status; }
            set
            {
                if (value != laserUnit4Head1Status)
                {
                    laserUnit4Head1Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 1, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit4Head2Status
        {
            get { return laserUnit4Head2Status; }
            set
            {
                if (value != laserUnit4Head2Status)
                {
                    laserUnit4Head2Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 2, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit4Head3Status
        {
            get { return laserUnit4Head3Status; }
            set
            {
                if (value != laserUnit4Head3Status)
                {
                    laserUnit4Head3Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 3, StarFlexEventType.CameraStatus, value));
                }
            }
        }
        public bool LaserUnit4Head4Status
        {
            get { return laserUnit4Head4Status; }
            set
            {
                if (value != laserUnit4Head4Status)
                {
                    laserUnit4Head4Status = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 4, StarFlexEventType.CameraStatus, value));
                }
            }
        }

        // hole status
        public bool LaserUnit1Head1HoleStatus
        {
            get { return laserUnit1Head1HoleStatus; }
            set
            {
                if (value != laserUnit1Head1HoleStatus)
                {
                    laserUnit1Head1HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 1, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit1Head2HoleStatus
        {
            get { return laserUnit1Head2HoleStatus; }
            set
            {
                if (value != laserUnit1Head2HoleStatus)
                {
                    laserUnit1Head2HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 2, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit1Head3HoleStatus
        {
            get { return laserUnit1Head3HoleStatus; }
            set
            {
                if (value != laserUnit1Head3HoleStatus)
                {
                    laserUnit1Head3HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 3, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit1Head4HoleStatus
        {
            get { return laserUnit1Head4HoleStatus; }
            set
            {
                if (value != laserUnit1Head4HoleStatus)
                {
                    laserUnit1Head4HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 4, StarFlexEventType.HolesStatus, value));
                }
            }
        }

        public bool LaserUnit2Head1HoleStatus
        {
            get { return laserUnit2Head1HoleStatus; }
            set
            {
                if (value != laserUnit2Head1HoleStatus)
                {
                    laserUnit2Head1HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 1, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit2Head2HoleStatus
        {
            get { return laserUnit2Head2HoleStatus; }
            set
            {
                if (value != laserUnit2Head2HoleStatus)
                {
                    laserUnit2Head2HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 2, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit2Head3HoleStatus
        {
            get { return laserUnit2Head3HoleStatus; }
            set
            {
                if (value != laserUnit2Head3HoleStatus)
                {
                    laserUnit2Head3HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 3, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit2Head4HoleStatus
        {
            get { return laserUnit2Head4HoleStatus; }
            set
            {
                if (value != laserUnit2Head4HoleStatus)
                {
                    laserUnit2Head4HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 4, StarFlexEventType.HolesStatus, value));
                }
            }
        }

        public bool LaserUnit3Head1HoleStatus
        {
            get { return laserUnit3Head1HoleStatus; }
            set
            {
                if (value != laserUnit3Head1HoleStatus)
                {
                    laserUnit3Head1HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 1, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit3Head2HoleStatus
        {
            get { return laserUnit3Head2HoleStatus; }
            set
            {
                if (value != laserUnit3Head2HoleStatus)
                {
                    laserUnit3Head2HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 2, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit3Head3HoleStatus
        {
            get { return laserUnit3Head3HoleStatus; }
            set
            {
                if (value != laserUnit3Head3HoleStatus)
                {
                    laserUnit3Head3HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 3, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit3Head4HoleStatus
        {
            get { return laserUnit3Head4HoleStatus; }
            set
            {
                if (value != laserUnit3Head4HoleStatus)
                {
                    laserUnit3Head4HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 4, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        
        public bool LaserUnit4Head1HoleStatus
        {
            get { return laserUnit4Head1HoleStatus; }
            set
            {
                if (value != laserUnit4Head1HoleStatus)
                {
                    laserUnit4Head1HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 1, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit4Head2HoleStatus
        {
            get { return laserUnit4Head2HoleStatus; }
            set
            {
                if (value != laserUnit4Head2HoleStatus)
                {
                    laserUnit4Head2HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 2, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit4Head3HoleStatus
        {
            get { return laserUnit4Head3HoleStatus; }
            set
            {
                if (value != laserUnit4Head3HoleStatus)
                {
                    laserUnit4Head3HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 3, StarFlexEventType.HolesStatus, value));
                }
            }
        }
        public bool LaserUnit4Head4HoleStatus
        {
            get { return laserUnit4Head4HoleStatus; }
            set
            {
                if (value != laserUnit4Head4HoleStatus)
                {
                    laserUnit4Head4HoleStatus = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 4, StarFlexEventType.HolesStatus, value));
                }
            }
        }

        // hole diameter
        public bool LaserUnit1Head1HoleDiameter
        {
            get { return laserUnit1Head1HoleDiameter; }
            set
            {
                if (value != laserUnit1Head1HoleDiameter)
                {
                    laserUnit1Head1HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 1, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit1Head2HoleDiameter
        {
            get { return laserUnit1Head2HoleDiameter; }
            set
            {
                if (value != laserUnit1Head2HoleDiameter)
                {
                    laserUnit1Head2HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 2, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit1Head3HoleDiameter
        {
            get { return laserUnit1Head3HoleDiameter; }
            set
            {
                if (value != laserUnit1Head3HoleDiameter)
                {
                    laserUnit1Head3HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 3, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit1Head4HoleDiameter
        {
            get { return laserUnit1Head4HoleDiameter; }
            set
            {
                if (value != laserUnit1Head4HoleDiameter)
                {
                    laserUnit1Head4HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(1, 4, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }

        public bool LaserUnit2Head1HoleDiameter
        {
            get { return laserUnit2Head1HoleDiameter; }
            set
            {
                if (value != laserUnit2Head1HoleDiameter)
                {
                    laserUnit2Head1HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 1, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit2Head2HoleDiameter
        {
            get { return laserUnit2Head2HoleDiameter; }
            set
            {
                if (value != laserUnit2Head2HoleDiameter)
                {
                    laserUnit2Head2HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 2, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit2Head3HoleDiameter
        {
            get { return laserUnit2Head3HoleDiameter; }
            set
            {
                if (value != laserUnit2Head3HoleDiameter)
                {
                    laserUnit2Head3HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 3, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit2Head4HoleDiameter
        {
            get { return laserUnit2Head4HoleDiameter; }
            set
            {
                if (value != laserUnit2Head4HoleDiameter)
                {
                    laserUnit2Head4HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(2, 4, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }

        public bool LaserUnit3Head1HoleDiameter
        {
            get { return laserUnit3Head1HoleDiameter; }
            set
            {
                if (value != laserUnit3Head1HoleDiameter)
                {
                    laserUnit3Head1HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 1, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit3Head2HoleDiameter
        {
            get { return laserUnit3Head2HoleDiameter; }
            set
            {
                if (value != laserUnit3Head2HoleDiameter)
                {
                    laserUnit3Head2HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 2, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit3Head3HoleDiameter
        {
            get { return laserUnit3Head3HoleDiameter; }
            set
            {
                if (value != laserUnit3Head3HoleDiameter)
                {
                    laserUnit3Head3HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 3, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit3Head4HoleDiameter
        {
            get { return laserUnit3Head4HoleDiameter; }
            set
            {
                if (value != laserUnit3Head4HoleDiameter)
                {
                    laserUnit3Head4HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(3, 4, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }

        public bool LaserUnit4Head1HoleDiameter
        {
            get { return laserUnit4Head1HoleDiameter; }
            set
            {
                if (value != laserUnit4Head1HoleDiameter)
                {
                    laserUnit4Head1HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 1, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit4Head2HoleDiameter
        {
            get { return laserUnit4Head2HoleDiameter; }
            set
            {
                if (value != laserUnit4Head2HoleDiameter)
                {
                    laserUnit4Head2HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 2, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit4Head3HoleDiameter
        {
            get { return laserUnit4Head3HoleDiameter; }
            set
            {
                if (value != laserUnit4Head3HoleDiameter)
                {
                    laserUnit4Head3HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 3, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }
        public bool LaserUnit4Head4HoleDiameter
        {
            get { return laserUnit4Head4HoleDiameter; }
            set
            {
                if (value != laserUnit4Head4HoleDiameter)
                {
                    laserUnit4Head4HoleDiameter = value;
                    if (Changed != null)
                        Changed(this, new VariableChangedEventArgs(4, 4, StarFlexEventType.HolesDiameterStatus, value));
                }
            }
        }

        // error
        public bool LaserUnit1Head1Error
        {
            get { return laserUnit1Head1Error; }
            set
            {
                if (value != laserUnit1Head1Error)
                {
                    laserUnit1Head1Error = value;
                }
            }
        }
        public bool LaserUnit1Head2Error
        {
            get { return laserUnit1Head2Error; }
            set
            {
                if (value != laserUnit1Head2Error)
                {
                    laserUnit1Head2Error = value;
                }
            }
        }
        public bool LaserUnit1Head3Error
        {
            get { return laserUnit1Head3Error; }
            set
            {
                if (value != laserUnit1Head3Error)
                {
                    laserUnit1Head3Error = value;
                }
            }
        }
        public bool LaserUnit1Head4Error
        {
            get { return laserUnit1Head4Error; }
            set
            {
                if (value != laserUnit1Head4Error)
                {
                    laserUnit1Head4Error = value;
                }
            }
        }

        public bool LaserUnit2Head1Error
        {
            get { return laserUnit2Head1Error; }
            set
            {
                if (value != laserUnit2Head1Error)
                {
                    laserUnit2Head1Error = value;
                }
            }
        }
        public bool LaserUnit2Head2Error
        {
            get { return laserUnit2Head2Error; }
            set
            {
                if (value != laserUnit2Head2Error)
                {
                    laserUnit2Head2Error = value;
                }
            }
        }
        public bool LaserUnit2Head3Error
        {
            get { return laserUnit2Head3Error; }
            set
            {
                if (value != laserUnit2Head3Error)
                {
                    laserUnit2Head3Error = value;
                }
            }
        }
        public bool LaserUnit2Head4Error
        {
            get { return laserUnit2Head4Error; }
            set
            {
                if (value != laserUnit2Head4Error)
                {
                    laserUnit2Head4Error = value;
                }
            }
        }

        public bool LaserUnit3Head1Error
        {
            get { return laserUnit3Head1Error; }
            set
            {
                if (value != laserUnit3Head1Error)
                {
                    laserUnit3Head1Error = value;
                }
            }
        }
        public bool LaserUnit3Head2Error
        {
            get { return laserUnit3Head2Error; }
            set
            {
                if (value != laserUnit3Head2Error)
                {
                    laserUnit3Head2Error = value;
                }
            }
        }
        public bool LaserUnit3Head3Error
        {
            get { return laserUnit3Head3Error; }
            set
            {
                if (value != laserUnit3Head3Error)
                {
                    laserUnit3Head3Error = value;
                }
            }
        }
        public bool LaserUnit3Head4Error
        {
            get { return laserUnit3Head4Error; }
            set
            {
                if (value != laserUnit3Head4Error)
                {
                    laserUnit3Head4Error = value;
                }
            }
        }

        public bool LaserUnit4Head1Error
        {
            get { return laserUnit4Head1Error; }
            set
            {
                if (value != laserUnit4Head1Error)
                {
                    laserUnit4Head1Error = value;
                }
            }
        }
        public bool LaserUnit4Head2Error
        {
            get { return laserUnit4Head2Error; }
            set
            {
                if (value != laserUnit4Head2Error)
                {
                    laserUnit4Head2Error = value;
                }
            }
        }
        public bool LaserUnit4Head3Error
        {
            get { return laserUnit4Head3Error; }
            set
            {
                if (value != laserUnit4Head3Error)
                {
                    laserUnit4Head3Error = value;
                }
            }
        }
        public bool LaserUnit4Head4Error
        {
            get { return laserUnit4Head4Error; }
            set
            {
                if (value != laserUnit4Head4Error)
                {
                    laserUnit4Head4Error = value;
                }
            }
        }
    }
}
