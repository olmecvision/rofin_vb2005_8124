﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    enum StarFlexEventType
    {
        Unknown = 0,
        ReadyStatus,
        CameraStatus,
        HolesStatus,
        HolesDiameterStatus,
        ErrorMessage
    }
}
