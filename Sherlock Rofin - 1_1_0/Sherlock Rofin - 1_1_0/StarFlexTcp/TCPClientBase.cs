using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Diagnostics;

namespace Tellan.StarFlexTcp
{
	public class TcpClientBase
	{
		private byte[] mReceivedData =  new byte[1024];
		private string mId = new Guid().ToString();

		public string Id
		{
			get
			{
				return mId;
			}
		}

		public bool IsOpen
		{
			get
			{
				try
				{
					if (TcpSocket != null)
					{
						return TcpSocket.Connected;
					}
					else
					{
						return false;
					}
				}
				catch (ObjectDisposedException)
				{
					//It has been closed
					return false;
				}
			}
		}

		public IPAddress RemoteIPAddress 
		{
			get
			{
				if (TcpSocket != null)
				{
					IPAddress ipAddress = IPAddress.Parse((TcpSocket.RemoteEndPoint as IPEndPoint).Address.ToString());

					return ipAddress;
				}
				else
				{	
					return null;
				}
			}
		}

        public event EventHandler<TcpSocketEventArgs> Connected;
        public event EventHandler<TcpSocketEventArgs> Disconnected;
        public event EventHandler<TcpDataReceivedEventArgs> DataReceived;

		private Socket mySocket = null;

		protected Socket TcpSocket
		{
			get
			{
				return mySocket;
			}
			set
			{
				mySocket = value;
			}
		}

		public TcpClientBase()
		{
			
		}

		public void StartReading()
		{
			BeginRead();
		}

        public byte[] StartReadBlockingAsBytes()
        {
            try
            {
                byte[] Temp = new byte[1024];
                int Length = TcpSocket.Receive(Temp);
                if (Length > 0)
                {
                    byte[] result = new byte[Length];
                    Buffer.BlockCopy(Temp, 0, result, 0, Length);
                    return result;
                }
                else
                {
                    Close();
                    return new byte[0];
                }
            }
            catch (ObjectDisposedException)
            {
                return new byte[0];
            }
            catch (SocketException)
            {
                return new byte[0];
            }
        }

        public bool Send(byte[] data)
        {
            if (data != null)
            {
                if (TcpSocket != null)
                {
                    lock (TcpSocket)
                    {
                        try
                        {
                            return TcpSocket.Send(data, 0, data.Length, SocketFlags.None) == data.Length;
                        }
                        catch (ObjectDisposedException)
                        {
                            //It has been closed
                            return false;
                        }
                        catch (SocketException)
                        {
                            Close();
                            return false;
                        }
                    }
                }
                else
                    return false;
            }
            else
                return true;
        }

		public void Close()
		{
			try
			{
				if (TcpSocket != null)
				{
					lock(TcpSocket)
					{
						if (TcpSocket.Connected)
						{
							TcpSocket.Shutdown(SocketShutdown.Both);
							TcpSocket.Close();
						}
					}
					EventDisconnected();
				}
			}
			catch(ObjectDisposedException)
			{
				//Ignore - Already closed
				EventDisconnected();
			}
			catch(SocketException)
			{
				EventDisconnected();
			}
		}

        private IAsyncResult readResult;
		protected void BeginRead()
		{
			try
			{
                readResult = TcpSocket.BeginReceive(mReceivedData, 0, mReceivedData.Length, SocketFlags.None, 
					DoRead, TcpSocket );
			}
			catch(ObjectDisposedException)
			{
				//Ignore - Already closed
			}

		}

		private void DoRead(IAsyncResult readResult)
		{
			try
			{
				// Socket was the passed in object
				Socket sock = readResult.AsyncState as Socket;

				// Check if we got any data
				int Count = sock.EndReceive(readResult);
				if( Count > 0 )
				{
                    byte[] data = new byte[Count];
                    Buffer.BlockCopy(mReceivedData, 0, data, 0, Count);

                    EventDataReceived(data);
					BeginRead();
				}
				else
				{
					Close();
				}
			}
#if DEBUG
			catch(SocketException e)
#else
			catch(SocketException)
#endif
			{
				Close();
#if DEBUG
				Console.WriteLine("Socket Exception in DoRead: " + e.Message);
#endif
			}
			catch(ObjectDisposedException)
			{
				//Ignore - Already closed
				Close();
#if DEBUG
				Console.WriteLine("Object Disposed Exception in DoRead");
#endif
			}
		}

		protected void EventConnected()
		{
            EventHandler<TcpSocketEventArgs> Temp = Connected;
			if (Temp != null)
			{
				TcpSocketEventArgs args = new TcpSocketEventArgs(this);
				Temp(this, args);
			}
		}

		protected void EventDisconnected()
		{
            EventHandler<TcpSocketEventArgs> Temp = Disconnected;
			if (Temp != null)
			{
				TcpSocketEventArgs args = new TcpSocketEventArgs(this);
				Temp(this, args);
			}
		}

		protected void EventDataReceived(byte[] data)
		{
            EventHandler<TcpDataReceivedEventArgs> Temp = DataReceived;
			if (Temp != null)
			{
				TcpDataReceivedEventArgs TcpEventArgs = new TcpDataReceivedEventArgs(data);
				Temp(this, TcpEventArgs);
			}
		}
	}

	
	public class TcpDataReceivedEventArgs : EventArgs
	{
        public TcpDataReceivedEventArgs(byte[] data)
		{
            this.data = data;
		}

        private byte[] data;

        public byte[] GetBytes()
        {
            return (byte[])data.Clone();
        }
	}

	public class TcpSocketEventArgs : EventArgs
	{
		public TcpSocketEventArgs(TcpClientBase socket)
		{
			this.socket = socket;
		}

		private TcpClientBase socket;

		public TcpClientBase EventSocket
		{
			get
			{
				return socket;
			}
		}
	}
}

