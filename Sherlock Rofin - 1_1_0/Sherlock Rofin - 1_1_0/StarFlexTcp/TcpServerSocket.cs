using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tellan.StarFlexTcp
{
    public class TcpServerSocket
    {
        private TcpListener mServerSocket;
        private List<TcpClientBase> mClientList = new List<TcpClientBase>();
        private bool mBlockingRead;

        private bool mOpen = false;
        public bool IsOpen
        {
            get
            {
                return mOpen;
            }
        }

        public event EventHandler<TcpSocketEventArgs> Connected;
        public event EventHandler<TcpSocketEventArgs> Disconnected;
        public event EventHandler<TcpDataReceivedEventArgs> DataReceived;

        public TcpServerSocket()
        {

        }

        ~TcpServerSocket()
        {
            Close();
        }

        public int NumberConnectedClients
        {
            get
            {
                return mClientList.Count;
            }
        }

        public TcpConnectedClient this[int index]
        {
            get
            {
                return mClientList[index] as TcpConnectedClient;
            }
        }

        public void Open(int port)
        {
            Open(port, false);
        }

        public void Open(int port, bool blockingRead)
        {
            if (!mOpen)
            {
                mBlockingRead = blockingRead;
                mServerSocket = new TcpListener(IPAddress.Any, port);
                mServerSocket.Start();
                mOpen = true;
                mServerSocket.BeginAcceptSocket(new AsyncCallback(DoListen), mServerSocket);
            }
        }

        public void Close()
        {
            try
            {
                if (mServerSocket != null)
                {
                    mServerSocket.Stop();
                }
                mClearClientList();
                mOpen = false;
            }
            catch (SocketException)
            {
            }
        }

        public static void SetTcpKeepAlive(Socket socket, uint keepAliveInterval, uint retryInterval)
        {
            int size = sizeof(UInt32);
            UInt32 on = 1;
            byte[] inArray = new byte[size * 3];
            Array.Copy(BitConverter.GetBytes(on), 0, inArray, 0, size);
            Array.Copy(BitConverter.GetBytes(keepAliveInterval), 0, inArray, size, size);
            Array.Copy(BitConverter.GetBytes(retryInterval), 0, inArray, size * 2, size);
            socket.IOControl(IOControlCode.KeepAliveValues, inArray, null);
        }

        public void SendToAll(byte[] data)
        {
            if (IsOpen)
            {
                for (int i = mClientList.Count - 1; i >= 0; i--)
                {
                    mClientList[i].Send(data);
                }
            }
        }

        public static bool Send(TcpClientBase client, byte[] data)
        {
            if (client != null)
                return client.Send(data);
            else
                return false;
        }

        public bool Send(string clientId, byte[] data)
        {
            TcpClientBase client = null;

            lock (mClientList)
            {
                foreach (TcpClientBase c in mClientList)
                {
                    if (c.Id.Equals(clientId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        client = c;
                        break;
                    }
                }
            }

            if (client != null)
                return client.Send(data);

            return false;
        }

        private void mClearClientList()
        {
            for (int i = mClientList.Count - 1; i >= 0; i--)
            {
                mClientList[i].Close();
            }
        }

        private void DoListen(IAsyncResult ar)
        {
            try
            {
                if (mOpen)
                {
                    TcpListener listener = (TcpListener)ar.AsyncState;

                    Socket socket = null;
                    try
                    {
                        socket = listener.EndAcceptSocket(ar);
                        SetTcpKeepAlive(socket, 10000, 1000);
                    }
                    catch (SocketException)
                    {
                        socket = null;
                    }
                    if (socket != null)
                    {
                        TcpConnectedClient connectedClient =
                            new TcpConnectedClient(socket);
                        mClientList.Add(connectedClient);

                        TcpSocketEventArgs e = new TcpSocketEventArgs(connectedClient);

                        EventConnected(connectedClient, e);

                        //Tie up events and do event here
                        connectedClient.Disconnected += new EventHandler<TcpSocketEventArgs>(mOnClientDisconnected);
                        connectedClient.DataReceived += new EventHandler<TcpDataReceivedEventArgs>(mOnClientDataReceived);
                        if (!mBlockingRead)
                        {
                            try
                            {
                                connectedClient.StartReading();
                            }
                            catch (SocketException)
                            {
                                mOnClientDisconnected(connectedClient, e);
                            }
                        }
                    }
                }
            }
            catch (ObjectDisposedException)
            {
            }
            catch (SocketException)
            {
            }
            finally
            {
                try
                {
                    mServerSocket.BeginAcceptSocket(new AsyncCallback(DoListen), mServerSocket);
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        private void EventConnected(TcpClientBase client, TcpSocketEventArgs e)
        {
            EventHandler<TcpSocketEventArgs> Temp = Connected;
            if (Temp != null)
            {
                Temp(client, e);
            }
        }

        private void EventDisconnected(TcpClientBase client, TcpSocketEventArgs e)
        {
            EventHandler<TcpSocketEventArgs> Temp = Disconnected;
            if (Temp != null)
            {
                Temp(client, e);
            }
        }

        private void EventDataReceived(TcpClientBase client, TcpDataReceivedEventArgs e)
        {
            EventHandler<TcpDataReceivedEventArgs> Temp = DataReceived;
            if (Temp != null)
            {
                Temp(client, e);
            }
        }

        private void mOnClientDisconnected(object sender, TcpSocketEventArgs e)
        {
            //Remove from the list and event
            EventDisconnected(e.EventSocket, e);
            //Remove the events
            TcpClientBase client = (TcpClientBase)sender;
            if (mClientList.Contains(client))
                mClientList.Remove(client);
            client.DataReceived -= new EventHandler<TcpDataReceivedEventArgs>(mOnClientDataReceived);
            client.Disconnected -= new EventHandler<TcpSocketEventArgs>(mOnClientDisconnected);
        }

        private void mOnClientDataReceived(object sender, TcpDataReceivedEventArgs e)
        {
            EventDataReceived(sender as TcpClientBase, e);
        }
    }
}
