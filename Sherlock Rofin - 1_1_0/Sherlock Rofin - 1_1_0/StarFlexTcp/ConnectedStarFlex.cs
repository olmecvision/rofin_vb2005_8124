﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    class ConnectedStarFlex
    {
        private TcpClientBase connectedClient;
        private StringBuilder dataString = new StringBuilder();

        public event EventHandler Disconnected;
        public event EventHandler<SelectUnitHeadReceivedEventArgs> SelectUnitHeadReceived;
        public event EventHandler<DataSentReceivedEventArgs> DataReceived;

        public ConnectedStarFlex(TcpClientBase connectedClient)
        {
            this.connectedClient = connectedClient;
            connectedClient.DataReceived += new EventHandler<TcpDataReceivedEventArgs>(connectedClient_DataReceived);
            connectedClient.Disconnected += new EventHandler<TcpSocketEventArgs>(connectedClient_Disconnected);
        }

        void connectedClient_Disconnected(object sender, TcpSocketEventArgs e)
        {
            if (Disconnected != null)
                Disconnected(this, EventArgs.Empty);
        }

        void connectedClient_DataReceived(object sender, TcpDataReceivedEventArgs e)
        {
            foreach (byte b in e.GetBytes())
            {
                ProcessData((char)b);
            }
        }

        private void ProcessData(char data)
        {
            if (data == '\r')
            {
                string contents = dataString.ToString();
                ParseContents(contents.Trim());
                dataString = new StringBuilder();
            }
            else
                dataString.Append(data);
        }

        private void ParseContents(string contents)
        {
            StarFlexMessageBase message = StarFlexCodec.Decode(contents);
            if (message != null)
            {
                Console.WriteLine(contents);
                if (message is RequestObjectMessage)
                {
                    ReturnValueMessage rv = new ReturnValueMessage();
                    rv.SequenceNumber = message.SequenceNumber;
                    connectedClient.Send(ASCIIEncoding.ASCII.GetBytes(rv.Encode()));
                    ProcessRequestObjectMessage((RequestObjectMessage)message);
                }
                
            }
            else
            {
                ExceptionMessage ex = new ExceptionMessage();
                ex.ErrorCode = ErrorReasonCode.NoHandlerFound;
                connectedClient.Send(ASCIIEncoding.ASCII.GetBytes(ex.Encode()));
                Console.WriteLine("Unknown message received - {0}", contents);
            }
            if (DataReceived != null)
                DataReceived(this, new DataSentReceivedEventArgs(false, contents));
        }

        private void ProcessRequestObjectMessage(RequestObjectMessage requestObjectMessage)
        {
            if (SelectUnitHeadReceived != null)
                SelectUnitHeadReceived(this, new SelectUnitHeadReceivedEventArgs(requestObjectMessage.State, requestObjectMessage.Head, requestObjectMessage.LaserUnitId, requestObjectMessage.Command));
        }
    }
}
