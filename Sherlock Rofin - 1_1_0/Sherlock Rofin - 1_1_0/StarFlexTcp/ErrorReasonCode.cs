﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    enum ErrorReasonCode
    {
        InvalidTelegramFormat,
        NoHandlerFound,
        NotEnoughArguments
    }
}
