﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public enum StarFlexCommand
    {
        Unknown = 0,
        SelectUnit,
        SelectHead,
        SetTeachIn,
        StartCamera,
        OpenShutter,
        CloseShutter,
        StartProduction,
        StartQuitError,
        GetProductionStatus,
        LoadOrder,
        SetOrderText,
        ExternalStart,
        ExternalStop
    }
}
