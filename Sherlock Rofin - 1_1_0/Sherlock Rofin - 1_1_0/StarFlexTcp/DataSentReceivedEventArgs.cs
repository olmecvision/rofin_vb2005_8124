﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public class DataSentReceivedEventArgs : EventArgs
    {
        public bool TxData { get; private set; }
        public string Data { get; private set; }

        public DataSentReceivedEventArgs(bool txData, string data)
        {
            this.TxData = txData;
            this.Data = data;
        }
    }
}
