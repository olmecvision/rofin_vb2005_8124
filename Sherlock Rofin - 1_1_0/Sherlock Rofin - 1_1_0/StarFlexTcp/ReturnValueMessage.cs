﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    class ReturnValueMessage : StarFlexMessageBase
    {
        public const string Header = "rqo";
        internal const int MimimumPacketLength = 2;

        public override bool Decode(string data)
        {
            string[] split = data.Split(splitString);
            if (split.Length >= MimimumPacketLength)
            {
                if (split[0].Equals(Header, StringComparison.InvariantCultureIgnoreCase))
                {
                    uint temp;
                    if (uint.TryParse(split[1], out temp))
                    {
                        SequenceNumber = temp;
                        return true;
                    }
                }
            }
            return false;
        }

        public override string Encode()
        {
            return string.Format("rv {0}\r\n", SequenceNumber);
        }
    }
}
