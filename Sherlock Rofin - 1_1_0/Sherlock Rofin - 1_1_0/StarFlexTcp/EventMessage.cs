﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    class EventMessage : StarFlexMessageBase
    {
        public const string Header = "ev";
        public StarFlexEventType EventType { get; set; }
        public uint LaserUnitId { get; set; }
        public uint Head { get; set; }
        public bool State { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }

        public override bool Decode(string data)
        {
            return true;
        }

        public override string Encode()
        {
            if (EventType != StarFlexEventType.ErrorMessage)
                return string.Format("{0} \"STATUS_CHANGE\" \"{1}\" {2} {3} {4}\r\n", Header, EventType.ToString(), LaserUnitId, Head, State ? "OK" : "ERROR");
            else
                return string.Format("{0} \"ERROR_MESSAGE\" \"{1}\" \"{2}\"\r\n", Header, ErrorType, ErrorMessage);
        }
    }
}
