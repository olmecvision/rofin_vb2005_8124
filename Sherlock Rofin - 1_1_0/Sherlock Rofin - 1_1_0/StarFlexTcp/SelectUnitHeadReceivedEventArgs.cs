﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public class SelectUnitHeadReceivedEventArgs : EventArgs
    {
        public bool State { get; private set; }
        public uint Head { get; private set; }
        public uint LaserUnitId { get; private set; }
        public StarFlexCommand Command { get; private set; }

        public SelectUnitHeadReceivedEventArgs(bool state, uint head, uint laserUnitId, StarFlexCommand command)
        {
            State = state;
            Head = head;
            LaserUnitId = laserUnitId;
            Command = command;
        }
    }
}
