﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public class StarFlexTcpManager
    {
        private static StarFlexTcpManager instance;
        private List<ConnectedStarFlex> connectedClients = new List<ConnectedStarFlex>();
        private static object lockObj = new object();
        public int TcpIpPort { get; private set; }
        public StarFlexVariables StarFlexVariables { get; private set; }

        public event EventHandler<SelectUnitHeadReceivedEventArgs> SelectUnitHeadReceived;
        public event EventHandler<DataSentReceivedEventArgs> DataSentReceived;

        private TcpServerSocket serverSocket;

        public static StarFlexTcpManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObj)
                    {
                        if (instance == null)
                            instance = new StarFlexTcpManager();
                    }
                }

                return instance;
            }
        }

        private StarFlexTcpManager()
        {
            serverSocket = new TcpServerSocket();
            serverSocket.Connected += new EventHandler<TcpSocketEventArgs>(serverSocket_Connected);
            StarFlexVariables = new StarFlexVariables();
            StarFlexVariables.Changed += new EventHandler<VariableChangedEventArgs>(starFlexVariables_Changed);
        }

        void starFlexVariables_Changed(object sender, VariableChangedEventArgs e)
        {
            switch (e.EventType)
            {
                case StarFlexEventType.CameraStatus:
                    CameraStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.ErrorMessage:
                    break;
                case StarFlexEventType.HolesStatus:
                    HoleStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.HolesDiameterStatus:
                    HoleDiameterStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.ReadyStatus:
                    CameraReady(e.LaserUnitId, e.Head, e.State);
                    break;
            }
        }

        public void SendData(string data)
        {
            Console.WriteLine(data);
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(data));
            if (DataSentReceived != null)
                DataSentReceived(this, new DataSentReceivedEventArgs(true, data));
        }

        public void CameraReady(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.ReadyStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            SendData(message.Encode());
        }

        public void CameraStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.CameraStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            SendData(message.Encode());
        }

        public void HoleStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.HolesStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            SendData(message.Encode());
        }

        public void HoleDiameterStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.HolesDiameterStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            SendData(message.Encode());
        }

        public void CameraError(string errorType, string errorMessage)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.ErrorMessage, ErrorType = errorType, ErrorMessage = errorMessage };
            SendData(message.Encode());
        }

        void serverSocket_Connected(object sender, TcpSocketEventArgs e)
        {
            ConnectedStarFlex client = new ConnectedStarFlex(e.EventSocket);
            lock (lockObj)
                connectedClients.Add(client);

            client.Disconnected += new EventHandler(client_Disconnected);
            client.SelectUnitHeadReceived += new EventHandler<SelectUnitHeadReceivedEventArgs>(client_SelectUnitHeadReceived);
            client.DataReceived += new EventHandler<DataSentReceivedEventArgs>(client_DataReceived);
        }

        void client_DataReceived(object sender, DataSentReceivedEventArgs e)
        {
            if (DataSentReceived != null)
                DataSentReceived(this, e);
        }

        void client_SelectUnitHeadReceived(object sender, SelectUnitHeadReceivedEventArgs e)
        {
            if (SelectUnitHeadReceived != null)
                SelectUnitHeadReceived(this, e);
        }

        void client_Disconnected(object sender, EventArgs e)
        {
            ConnectedStarFlex client = (ConnectedStarFlex)sender;
            client.Disconnected -= new EventHandler(client_Disconnected);
            client.SelectUnitHeadReceived -= new EventHandler<SelectUnitHeadReceivedEventArgs>(client_SelectUnitHeadReceived);
            client.DataReceived -= new EventHandler<DataSentReceivedEventArgs>(client_DataReceived);

            lock (lockObj)
                connectedClients.Remove(client);
        }

        public void Open(int port)
        {
            TcpIpPort = port;
            if (!serverSocket.IsOpen)
                serverSocket.Open(port);
        }
    }
}
