﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    sealed class StarFlexCodec
    {
        private StarFlexCodec()
        {
        }

        public static StarFlexMessageBase Decode(string data)
        {
            StarFlexMessageBase result = null;
            if (!string.IsNullOrEmpty(data))
            {
                if (data.StartsWith(RequestObjectMessage.Header))
                {
                    result = new RequestObjectMessage();
                    if (!result.Decode(data))
                        result = null;
                }
                else if (data.StartsWith(ExceptionMessage.Header))
                {
                    result = new ExceptionMessage();
                    if (!result.Decode(data))
                        result = null;
                }
                else if (data.StartsWith(EventMessage.Header))
                {
                    result = new EventMessage();
                    if (!result.Decode(data))
                        result = null;
                }
                else if (data.StartsWith(ReturnValueMessage.Header))
                {
                    result = new ReturnValueMessage();
                    if (!result.Decode(data))
                        result = null;
                }
            }
            return result;
        }
    }
}
