﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    class ExceptionMessage : StarFlexMessageBase
    {
        public const string Header = "ev";
        internal const int MimimumPacketLength = 4;

        public ErrorReasonCode ErrorCode { get; set; }

        public override bool Decode(string data)
        {
            //Not sent from client
            return true;
        }

        public override string Encode()
        {
            switch (ErrorCode)
            {
                case ErrorReasonCode.NoHandlerFound:
                    return string.Format("ev {0} \"sti\" \"no handler found\"\r\n", SequenceNumber);
                case ErrorReasonCode.NotEnoughArguments:
                    return string.Format("ev {0} \"sti\" \"not enough arguments\"\r\n", SequenceNumber);
                case ErrorReasonCode.InvalidTelegramFormat:
                default:
                    return string.Format("ev {0} \"sti\" \"invalid telegram format\"\r\n", SequenceNumber);
            }
        }
    }
}
