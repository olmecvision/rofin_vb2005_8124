﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tellan.StarFlexTcp
{
    public class StarFlexTcpManager
    {
        private static StarFlexTcpManager instance;
        private List<ConnectedStarFlex> connectedClients = new List<ConnectedStarFlex>();
        private static object lockObj = new object();
        public int TcpIpPort { get; private set; }
        public StarFlexVariables StarFlexVariables { get; private set; }

        public event EventHandler<SelectUnitHeadReceivedEventArgs> SelectUnitHeadReceived;

        private TcpServerSocket serverSocket;

        public static StarFlexTcpManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObj)
                    {
                        if (instance == null)
                            instance = new StarFlexTcpManager();
                    }
                }

                return instance;
            }
        }

        private StarFlexTcpManager()
        {
            serverSocket = new TcpServerSocket();
            serverSocket.Connected += new EventHandler<TcpSocketEventArgs>(serverSocket_Connected);
            StarFlexVariables = new StarFlexVariables();
            StarFlexVariables.Changed += new EventHandler<VariableChangedEventArgs>(starFlexVariables_Changed);
        }

        void starFlexVariables_Changed(object sender, VariableChangedEventArgs e)
        {
            switch (e.EventType)
            {
                case StarFlexEventType.CameraStatus:
                    CameraStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.ErrorMessage:
                    break;
                case StarFlexEventType.Holes:
                    HoleStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.HolesDiameter:
                    HoleDiameterStatus(e.LaserUnitId, e.Head, e.State);
                    break;
                case StarFlexEventType.ReadyStatus:
                    CameraReady(e.LaserUnitId, e.Head, e.State);
                    break;
            }
        }

        public void CameraReady(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.ReadyStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            Console.WriteLine(message.Encode());
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(message.Encode()));
        }

        public void CameraStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.CameraStatus, Head = head, LaserUnitId = laserUnitId, State = state };
            Console.WriteLine(message.Encode());
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(message.Encode()));
        }

        public void HoleStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.Holes, Head = head, LaserUnitId = laserUnitId, State = state };
            Console.WriteLine(message.Encode());
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(message.Encode()));
        }

        public void HoleDiameterStatus(uint laserUnitId, uint head, bool state)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.HolesDiameter, Head = head, LaserUnitId = laserUnitId, State = state };
            Console.WriteLine(message.Encode());
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(message.Encode()));
        }

        public void CameraError(string errorType, string errorMessage)
        {
            EventMessage message = new EventMessage() { EventType = StarFlexEventType.ErrorMessage, ErrorType = errorType, ErrorMessage = errorMessage };
            Console.WriteLine(message.Encode());
            serverSocket.SendToAll(ASCIIEncoding.ASCII.GetBytes(message.Encode()));
        }

        void serverSocket_Connected(object sender, TcpSocketEventArgs e)
        {
            ConnectedStarFlex client = new ConnectedStarFlex(e.EventSocket);
            lock (lockObj)
                connectedClients.Add(client);

            client.Disconnected += new EventHandler(client_Disconnected);
            client.SelectUnitHeadReceived += new EventHandler<SelectUnitHeadReceivedEventArgs>(client_SelectUnitHeadReceived);
        }

        void client_SelectUnitHeadReceived(object sender, SelectUnitHeadReceivedEventArgs e)
        {
            if (SelectUnitHeadReceived != null)
                SelectUnitHeadReceived(this, e);
        }

        void client_Disconnected(object sender, EventArgs e)
        {
            ConnectedStarFlex client = (ConnectedStarFlex)sender;
            client.Disconnected -= new EventHandler(client_Disconnected);
            client.SelectUnitHeadReceived -= new EventHandler<SelectUnitHeadReceivedEventArgs>(client_SelectUnitHeadReceived);

            lock (lockObj)
                connectedClients.Remove(client);
        }

        public void Open(int port)
        {
            TcpIpPort = port;
            if (!serverSocket.IsOpen)
                serverSocket.Open(port);
        }
    }
}
